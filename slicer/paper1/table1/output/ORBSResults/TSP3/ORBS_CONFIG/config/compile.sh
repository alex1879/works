cd $1 #Move to system directory
rm -f TSP.exe # Remove existing output
csc -out:TSP.exe -deterministic TSP.cs Tree.cs RandomGenerator.cs &> compile.log #Build
if [ -f TSP.exe ]; then #check build
    echo `md5sum TSP.exe`
else
    echo FAIL
fi
