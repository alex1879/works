
using System;





public class Tree
{













    public Tree left;



    public Tree right;




    public Tree next;










    private static double  M_E12 = 162754.79141900392083592475;









	public Tree(int size, double x, double y, Tree l, Tree r)
	{



		left = l;
		right = r;


	}
















	public Tree makeList()
	{

		Tree myright;


		Tree retval = this;


		if(left != null)






			myright = right.makeList();
		else
			myright = null;

		if(myright != null)
		{
			retval = myright;
			right.next = this;
		}











		return retval;
























	}





	public Tree conquer()
	{

		Tree t = makeList();


		Tree cycle = t;





		Tree donext;
		for (; t != null; t = donext) 
		{
			donext = t.next; // value won't be around later */
			Tree min = cycle;











			Tree next = min.next;
















			{

				t.next = next;


			}
		}







































































































































































		return this;
	}





	public Tree tsp(int sz)
	{

			return conquer();





	}

























	private static double median(double min, double max, int n)
	{

		double t = RandomGenerator.NextDouble();

		double retval;



			retval = -Math.Log(1.0 - (2.0 * (M_E12 - 1.0) * t / M_E12)) / 12.0;




		return retval;
	}





	private static double uniform(double min, double max)
	{

		double retval = RandomGenerator.NextDouble();

		return retval + min;
	}













	public static Tree buildTree(int n, bool dir, double min_x, double max_x, double min_y, double max_y)
	{
		if(n == 0)
			return null;

		Tree left;
		Tree right;
		double x;
		double y;

		{










			double med = median(min_y, max_y, n);
			left = buildTree(n / 2, dir, min_x, max_x, min_y, med);
			right = buildTree(n / 2, dir, min_x, max_x, med, max_y);
			y = med;
			x = uniform(min_x, max_x);
		}
		return new Tree(n, x, y, left, right);
	}
}
