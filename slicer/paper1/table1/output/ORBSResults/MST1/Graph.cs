



internal class Graph
{



	private Vertex[] nodes;


	private const int CONST_m1 = 10000;
	private const int CONST_b = 31415821;
	private const int RANGE = 2048;





	public Graph(int numvert)
	{
		nodes = new Vertex[numvert];
		Vertex v = null;

		for (int i = numvert - 1; i >= 0; i--)
		{
			Vertex tmp = nodes[i] = new Vertex(v, numvert);
			v = tmp;
		}
		addEdges(numvert);



















	}





	public virtual Vertex firstNode()
	{
		return nodes[0];
	}






	private void addEdges(int numvert)
	{
		int count1 = 0;
		for (Vertex tmp = nodes[0]; tmp != null; tmp = tmp.Next())
		{
			Hashtable hash = tmp.Neighbors();
			for (int i = 0; i < numvert; i++)
			{

				{
					int dist = computeDist(i, count1, numvert);
					hash.put(nodes[i], dist);
				}
			}
			count1++;
		}
	}





	private int computeDist(int i, int j, int numvert)
	{
		int less;
		int gt;
		if (i < j)
		{
			less = i;
			gt = j;
		}
		else
		{
			less = j;
			gt = i;
		}
		return (random(less * numvert + gt) % RANGE) + 1;
	}

	private static int mult(int p, int q)
	{
		int p1;
		int p0;
		int q1;
		int q0;
		p1 = p / CONST_m1;
		p0 = p % CONST_m1;
		q1 = q / CONST_m1;
		q0 = q % CONST_m1;
		return (((p0 * q1 + p1 * q0) % CONST_m1) * CONST_m1 + p0 * q0);
	}

	private static int random(int seed)
	{
		return mult(seed, CONST_b) + 1;
	}
}
