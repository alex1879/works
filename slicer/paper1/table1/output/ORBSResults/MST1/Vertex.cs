




internal class Vertex
{



	internal int mindist;



	internal Vertex next;



	internal Hashtable neighbors;





	internal Vertex(Vertex n, int numvert)
	{

		next = n;
		neighbors = new Hashtable(numvert / 4);
	}

	public virtual int Mindist()
	{
		return mindist;
	}






	public virtual Vertex Next()
	{
		return next;
	}

	public virtual void SetNext(Vertex v)
	{
		next = v;
	}

	public virtual Hashtable Neighbors()
	{
		return neighbors;
	}
}
