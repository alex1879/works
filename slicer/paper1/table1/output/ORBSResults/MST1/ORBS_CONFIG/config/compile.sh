cd $1 #Move to system directory
rm -f MST.exe # Remove existing output
csc -out:MST.exe -deterministic MST.cs Hashtable.cs Graph.cs Vertex.cs &> compile.log #Build
if [ -f MST.exe ]; then #check build
    echo `md5sum MST.exe`
else
    echo FAIL
fi
