
using System;





public class Tree
{



    public int sz;



    public double x;
    public double y;




    public Tree left;



    public Tree right;




    public Tree next;




	public Tree prev;





    private static double  M_E12 = 162754.79141900392083592475;









	public Tree(int size, double x, double y, Tree l, Tree r)
	{
		sz = size;
		this.x = x;
		this.y = y;
		left = l;
		right = r;


	}






	public double distance(Tree b)
	{
		return (Math.Sqrt((x - b.x) * (x - b.x) + (y - b.y) * (y - b.y)));
	}






	public Tree makeList()
	{
        Tree myleft;
		Tree myright;


		Tree retval = this;


		if(left != null)
			myleft = left.makeList();
		else
			myleft = null;


		if(right != null)
			myright = right.makeList();
		else
			myright = null;

		if(myright != null)
		{

			right.next = this;




			retval = myleft;

				left.next = myright;


		}


		return retval;
	}





	public void reverse()
	{

		prev.next = null;

		Tree back = this;



		for (Tree t = this.next; t != null; back = t, t = next) 
		{
		  next = t.next;
		  t.next = back;
		  back.prev = t;
		}



	}





	public Tree conquer()
	{

		Tree t = makeList();


		Tree cycle = t;
		t = t.next;
		cycle.next = cycle;



		Tree donext;
		for (; t != null; t = donext) 
		{
			donext = t.next; // value won't be around later */
			Tree min = cycle;

			for(Tree tmp = cycle.next; tmp != cycle; tmp = tmp.next)
			{


				{

					min = tmp;
				}
			}

			Tree next = min.next;
















			{

				t.next = next;
				min.next = t;
				t.prev = min;
			}
		}

		return cycle;
	}






	public Tree merge(Tree a, Tree b)
	{

		Tree min = a;
		double mindist = distance(a);
		Tree tmp = a;
		for(a = a.next; a != tmp; a = a.next)
		{
			double temp_test = distance(a);
			if(temp_test < mindist)
			{
				mindist = temp_test;
				min = a;
			}
		}

		Tree next = min.next;






		Tree p1;
		Tree n1;











		{

			p1 = min;
			n1 = next;


		}



		mindist = distance(b);
		tmp = b;
		for(b = b.next; b != tmp; b = b.next)
		{
			double temp_test = distance(b);
			if(temp_test < mindist)
			{
				mindist = temp_test;
				min = b;
			}
		}

		next = min.next;






		Tree p2;
		Tree n2;











		{

			p2 = min;
			n2 = next;


		}












		int choice = 1;



















		switch (choice)
		{
			case 1:





























				n1.reverse();
				n1.next = this;

				this.next = n2;

				p2.next = p1;

				break;
		}
		return this;
	}





	public Tree tsp(int sz)
	{
		if(this.sz <= sz)
			return conquer();

		Tree leftval = left.tsp(sz);
		Tree rightval = right.tsp(sz);

		return merge(leftval, rightval);
	}

























	private static double median(double min, double max, int n)
	{

		double t = RandomGenerator.NextDouble();

		double retval;
		if(t > 0.5)
			retval = Math.Log(1.0 - (2.0 * (M_E12 - 1.0) * (t - 0.5) / M_E12)) / 12.0;
		else
			retval = -Math.Log(1.0 - (2.0 * (M_E12 - 1.0) * t / M_E12)) / 12.0;


		retval = (retval + 1.0) * (max - min) / 2.0;
		retval = retval + min;
		return retval;
	}





	private static double uniform(double min, double max)
	{

		double retval = RandomGenerator.NextDouble();

		return retval + min;
	}













	public static Tree buildTree(int n, bool dir, double min_x, double max_x, double min_y, double max_y)
	{
		if(n == 0)
			return null;

		Tree left;
		Tree right;
		double x;
		double y;
		if(dir)
		{
			dir = !dir;
			double med = median(min_x, max_x, n);
			left = buildTree(n / 2, dir, min_x, med, min_y, max_y);
			right = buildTree(n / 2, dir, med, max_x, min_y, max_y);
			x = med;
			y = uniform(min_y, max_y);
		}
		else
		{
			dir = !dir;
			double med = median(min_y, max_y, n);
			left = buildTree(n / 2, dir, min_x, max_x, min_y, med);
			right = buildTree(n / 2, dir, min_x, max_x, med, max_y);
			y = med;
			x = uniform(min_x, max_x);
		}
		return new Tree(n, x, y, left, right);
	}
}
