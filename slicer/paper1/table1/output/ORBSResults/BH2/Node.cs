﻿using System;




public abstract class Node
{



	public double mass;



	public MathVector pos;


	public static int IMAX = 1073741824;


	public static double EPS = 0.05;




	public Node()
	{

		pos = new MathVector();
	}

	public abstract Cell loadTree(Body p, MathVector xpic, int l, BTree root);
	public abstract double hackcofm();
	public abstract HG walkSubTree(double dsq, HG hg);

	public static int oldSubindex(MathVector ic, int l)
	{
		int i = 0;
		for(int k = 0; k < MathVector.NDIM; k++)
		{
			if(((int)ic.value(k) & l) != 0)
				i += Cell.NSUB >> (k + 1);
		}
		return i;
	}













	public HG gravSub(HG hg)
	{
		MathVector dr = new MathVector();
		dr.subtraction2(pos, hg.pos0);

		double drsq = dr.dotProduct() + (EPS * EPS);
		double drabs = Math.Sqrt(drsq);

		double phii = mass / drabs;

		double mor3 = phii / drsq;
		dr.multScalar1(mor3);
		hg.acc0.addition(dr);
		return hg;
	}
}
