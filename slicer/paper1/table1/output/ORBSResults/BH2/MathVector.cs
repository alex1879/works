﻿using System;






public class MathVector
{



	public static readonly int NDIM = 3;



	public double[] data;




	public MathVector()
	{
		data = new double[NDIM];


	}





	public MathVector cloneMathVector()
	{
        try
        {
            MathVector v = new MathVector();

		    for(int i = 0; i < NDIM; i++)
			    v.data[i] = data[i];
            return v;
        }
        catch(Exception e)
        {
            throw;
        }
	}






	public double value(int i)
	{
		return data[i];
	}






	public void setValue(int i, double v)
	{
		data[i] = v;
	}


















	public void addition(MathVector u)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] + u.data[i];
	}






	public void subtraction1(MathVector u)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] - u.data[i];
	}






	public void subtraction2(MathVector u, MathVector v)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = u.data[i] - v.data[i];
	}





	public void multScalar1(double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] * s;
	}






	public void multScalar2(MathVector u, double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = u.data[i] * s;
	}





	public void divScalar(double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] / s;
	}





	public double dotProduct()
	{
		double s = 0.0;
		for(int i = 0; i < NDIM; i++)
			s += data[i] * data[i];
		return s;
	}






































































}
