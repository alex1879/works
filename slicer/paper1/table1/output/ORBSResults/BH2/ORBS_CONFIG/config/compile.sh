cd $1 #Move to system directory
rm -f BH.exe # Remove existing output
csc -out:BH.exe -deterministic BH.cs Body.cs BTree.cs Cell.cs HG.cs MathVector.cs Node.cs &> compile.log #Build
if [ -f BH.exe ]; then #check build
    echo `md5sum BH.exe`
else
    echo FAIL
fi
