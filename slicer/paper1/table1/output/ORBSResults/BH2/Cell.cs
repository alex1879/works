




public class Cell : Node
{

	public static int NSUB = 8; // 1 << NDIM





	public Node[] subp;


	public Cell() : base()
	{
		this.subp = new Node[NSUB];

	}










	public override Cell loadTree(Body p, MathVector xpic, int l, BTree tree)
	{

		int si = Node.oldSubindex(xpic, l);
		Node rt = subp[si];
		if(rt != null)
			subp[si] = rt.loadTree(p, xpic, l >> 1, tree);
		else
			subp[si] = p;
		return this;
	}





	public override double hackcofm()
	{
		double mq = 0.0;
		MathVector tmpPos = new MathVector();
		MathVector tmpv = new MathVector();
		for(int i = 0; i < NSUB; i++)
		{
			Node r = this.subp[i];
			if(r != null)
			{
				double mr = r.hackcofm();
				mq = mr + mq;
				tmpv.multScalar2(r.pos, mr);
				tmpPos.addition(tmpv);
			}
		}
		mass = mq;
		pos = tmpPos;
		pos.divScalar(mass);

		return mq;
	}




	public override HG walkSubTree(double dsq, HG hg)
	{
		if(subdivp(dsq, hg))
		{
			for(int k = 0; k < Cell.NSUB; k++)
			{
				Node r = this.subp[k];
				if(r != null)
					hg = r.walkSubTree(dsq / 4.0, hg);
			}
		}
		else
			hg = gravSub(hg);
		return hg;
	}





	public bool subdivp(double dsq, HG hg)
	{
		MathVector dr = new MathVector();
		dr.subtraction2(pos, hg.pos0);
		double drsq = dr.dotProduct();


		return (drsq < dsq);
	}









}
