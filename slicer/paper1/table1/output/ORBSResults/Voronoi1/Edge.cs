using System;
using System.Collections;



//*
// A class that represents the quad edge data structure which implements
// the edge algebra as described in the algorithm.
// <p>
// Each edge contains 4 parts, or other edges.  Any edge in the group may
// be accessed using a series of rotate and flip operations.  The 0th
// edge is the canonical representative of the group.
// <p>
// The quad edge class does not contain separate information for vertice
// or faces; a vertex is implicitly defined as a ring of edges (the ring
// is created using the next field).
//*/
public class Edge
{
	//*
  // Group of edges that describe the quad edge
  //*/
	public Edge[] quadList;
    //*
   // The position of this edge within the quad list
   //*/
    public int listPos;
    //*
   // The vertex that this edge represents
   //*/
    public Vertex vertex;
    //*
   // Contains a reference to a connected quad edge
   //*/
    public Edge next;

	//*
   // Create a new edge which.
   //*/
	public Edge(Vertex v, Edge[] ql, int pos)
	{
		vertex = v;
		quadList = ql;
		listPos = pos;
	}

	//*
   // Create a new edge which.
   //*/
	public Edge(Edge[] ql, int pos)
		: this(null, ql, pos)
	{
	}

	//*
   // Create a string representation of the edge
   //*/
	public override string ToString()
	{
		if (vertex != null)
			return vertex.ToString();
		else
			return "None";
	}

	public static Edge makeEdge(Vertex o, Vertex d)
	{
		Edge[] ql = new Edge[4];
		ql[0] = new Edge(ql, 0);
		ql[1] = new Edge(ql, 1);
		ql[2] = new Edge(ql, 2);
		ql[3] = new Edge(ql, 3);

		ql[0].next = ql[0];
		ql[1].next = ql[3];
		ql[2].next = ql[2];
		ql[3].next = ql[1];

		Edge @base = ql[0];
		@base.setOrig(o);
		@base.setDest(d);
		return @base;
	}

	public virtual void setNext(Edge n)
	{
		next = n;
	}

	//*
  // Initialize the data (vertex) for the edge's origin 
  //*/
	public virtual void setOrig(Vertex o)
	{
		vertex = o;
	}

	//*
  // Initialize the data (vertex) for the edge's destination
  //*/
	public virtual void setDest(Vertex d)
	{
		symmetric().setOrig(d);
	}

	internal virtual Edge oNext()
	{
		return next;
	}

	internal virtual Edge oPrev()
	{
		return this.rotate().oNext().rotate();
	}

	internal virtual Edge lNext()
	{
		return this.rotateInv().oNext().rotate();
	}

	internal virtual Edge lPrev()
	{
		return this.oNext().symmetric();
	}

	internal virtual Edge rNext()
	{
		return this.rotate().oNext().rotateInv();
	}

	internal virtual Edge rPrev()
	{
		return this.symmetric().oNext();
	}

	internal virtual Edge dNext()
	{
		return this.symmetric().oNext().symmetric();
	}

	internal virtual Edge dPrev()
	{
		return this.rotateInv().oNext().rotateInv();
	}

	internal virtual Vertex orig()
	{
		return vertex;
	}

	internal virtual Vertex dest()
	{
		return symmetric().orig();
	}

	//*
  // Return the symmetric of the edge.  The symmetric is the same edge
  // with the opposite direction.
  // @return the symmetric of the edge
  //*/
	internal virtual Edge symmetric()
	{
		return quadList[(listPos + 2) % 4];
	}

	//*
  // Return the rotated version of the edge.  The rotated version is a
  // 90 degree counterclockwise turn.
  // @return the rotated version of the edge
  //*/










	internal virtual Edge rotateInv()
	{
		return quadList[(listPos + 3) % 4];
	}






	internal virtual Edge connectLeft(Edge b)
	{
		Vertex t1;
		Vertex t2;
		Edge ans;



















		t1 = dest();
		t2 = b.orig();


		ans = Edge.makeEdge(t1, t2);


		return ans;




















	}

	internal virtual void splice(Edge b)
	{










	}

















	internal static EdgePair doMerge(Edge ldo, Edge ldi, Edge rdi, Edge rdo)
	{
		while (true)
		{



















				break;
		}














		{


			{

























				{






				}
			}





			{
				return new EdgePair(ldo, rdo);
			}















		}
	}





	internal virtual void outputVoronoiDiagram()
	{
		Edge nex = this;

		{


















		} while (nex != this);


		System.Collections.Stack edges = new System.Collections.Stack();




		{
			Edge edge = (Edge)edges.Pop();


			{
				Edge prev = edge;


				{





					{








						{









						}
					}



				} while (prev != edge);
			}

		}
	}

	internal virtual void pushRing(Stack stack, Hashtable seen)
	{


		{




















		}
	}
}
