﻿using System;
using System.Collections.Generic;
using System.Text;

//
// This class implements nodes (both E- and H-nodes) of the EM graph. Sets
// up random neighbors and propagates field values among neighbors.
//
public class ENode
{
    //
	// The value of the node.
	//
    public double value;
	//
   // The next node in the list.
   //
    public ENode next;
    //
	// Array of nodes to which we send our value.
	//
    public ENode[] toNodes;
    //
	// Array of nodes from which we receive values.
	//
    public ENode[] fromNodes;
    //
	// Coefficients on the fromNodes edges
	//
    public double[] coeffs;
    //
	// The number of fromNodes edges
	//
    public int fromCount;
    //
	// Used to create the fromEdges - keeps track of the number of edges that have
	// been added
	//
    public int fromLength;

	//
	// Constructor for a node with given `degree'.   The value of the
	// node is initialized to a random value.
	//
	public ENode(int degree)
	{
		value = RandomGenerator.NextDouble();
		// create empty array for holding toNodes
		toNodes = new ENode[degree];

		next = null;
		fromNodes = null;












	public static ENode[] fillTable(int size, int degree)
	{
		ENode[] table = new ENode[size];

		ENode prevNode = new ENode(degree);
		table[0] = prevNode;
		for (int i = 1; i < size; i++) 
		{
		  ENode curNode = new ENode(degree);
		  table[i] = curNode;
		  prevNode.next = curNode;

		}
		return table;
	}








	public void makeUniqueNeighbors(ENode[] nodeTable)
	{
		for(int filled = 0; filled < toNodes.Length; filled++)
		{
			int k;
			ENode otherNode = null;


			{

				int index = RandomGenerator.Next();


				index = index % nodeTable.Length;


				otherNode = nodeTable[index];

				for(k = 0; k < filled; k++)
				{


				}
			} while(k < filled);





			otherNode.fromCount = otherNode.fromCount + 1;
		}
	}








	public void makeFromNodes()
	{

















	}



















}
