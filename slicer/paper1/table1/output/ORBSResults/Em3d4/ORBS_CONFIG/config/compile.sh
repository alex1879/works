cd $1 #Move to system directory
rm -f Em3d.exe # Remove existing output
csc -out:Em3d.exe -deterministic Em3d.cs BiGraph.cs ENode.cs RandomGenerator.cs &> compile.log #Build
if [ -f Em3d.exe ]; then #check build
    echo `md5sum Em3d.exe`
else
    echo FAIL
fi
