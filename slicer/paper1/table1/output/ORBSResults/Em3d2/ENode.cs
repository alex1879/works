﻿using System;
using System.Collections.Generic;
using System.Text;

//
// This class implements nodes (both E- and H-nodes) of the EM graph. Sets
// up random neighbors and propagates field values among neighbors.
//
public class ENode
{
    //
	// The value of the node.
	//
    public double value;
	//
   // The next node in the list.
   //
    public ENode next;
    //
	// Array of nodes to which we send our value.
	//
    public ENode[] toNodes;
    //
	// Array of nodes from which we receive values.
	//
    public ENode[] fromNodes;
    //
	// Coefficients on the fromNodes edges
	//
    public double[] coeffs;
    //
	// The number of fromNodes edges
	//
    public int fromCount;
    //
	// Used to create the fromEdges - keeps track of the number of edges that have
	// been added
	//
    public int fromLength;

	//
	// Constructor for a node with given `degree'.   The value of the
	// node is initialized to a random value.
	//
	public ENode(int degree)
	{
		value = RandomGenerator.NextDouble();
		// create empty array for holding toNodes
		toNodes = new ENode[degree];

		next = null;
		fromNodes = null;
		coeffs = null;
		fromCount = 0;
		fromLength = 0;
	}

	//
	// Create the linked list of E or H nodes.  We create a table which is used
	// later to create links among the nodes.
	// @param size   the no. of nodes to create
	// @param degree the out degree of each node
	// @return a table containing all the nodes.
	//
	public static ENode[] fillTable(int size, int degree)
	{
		ENode[] table = new ENode[size];

		ENode prevNode = new ENode(degree);
		table[0] = prevNode;
		for (int i = 1; i < size; i++) 
		{
		  ENode curNode = new ENode(degree);
		  table[i] = curNode;
		  prevNode.next = curNode;
		  prevNode = curNode;
		}
		return table;
	}

	//
	// Create unique `degree' neighbors from the nodes given in nodeTable.
	// We do this by selecting a random node from the give nodeTable to
	// be neighbor. If this neighbor has been previously selected, then
	// a different random neighbor is chosen.
	// @param nodeTable the list of nodes to choose from.
	//
	public void makeUniqueNeighbors(ENode[] nodeTable)
	{
		for(int filled = 0; filled < toNodes.Length; filled++)
		{
			int k;
			ENode otherNode = null;

			do
			{
				// generate a random number in the correct range
				int index = RandomGenerator.Next();
				if(index < 0)
					index = -index;
				index = index % nodeTable.Length;

				// find a node with the random index in the given table
				otherNode = nodeTable[index];

				for(k = 0; k < filled; k++)
				{
					if(otherNode == toNodes[filled])
						break;
				}
			} while(k < filled);

			// other node is definitely unique among "filled" toNodes
			toNodes[filled] = otherNode;

			// update fromCount for the other node











	public void makeFromNodes()
	{

		coeffs = new double[fromCount];















	}



















}
