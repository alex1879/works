cd $1 #Move to system directory
rm -f Power.exe # Remove existing output
csc -out:Power.exe -deterministic Power.cs Branch.cs Demand.cs Lateral.cs Leaf.cs Root.cs &> compile.log #Build
if [ -f Power.exe ]; then #check build
    echo `md5sum Power.exe`
else
    echo FAIL
fi
