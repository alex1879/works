cd $1 #Move to system directory
rm -f TreeAdd.exe # Remove existing output
csc -out:TreeAdd.exe -deterministic TreeAdd.cs TreeNode.cs &> compile.log #Build
if [ -f TreeAdd.exe ]; then #check build
    echo `md5sum TreeAdd.exe`
else
    echo FAIL
fi
