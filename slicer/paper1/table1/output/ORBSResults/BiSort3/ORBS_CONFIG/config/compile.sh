cd $1 #Move to system directory
rm -f BiSort.exe # Remove existing output
csc -out:BiSort.exe -deterministic BiSort.cs Value.cs &> compile.log #Build
if [ -f BiSort.exe ]; then #check build
    echo `md5sum BiSort.exe`
else
    echo FAIL
fi
