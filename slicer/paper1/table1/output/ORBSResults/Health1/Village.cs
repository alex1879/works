﻿
using System;

//
// A class represnting a village in the Columbian health care system
// simulation.
//
public class Village
{
	private Village[] forward;
	private Village   back;
	private List returned;
	private Hospital hospital;
	private int label;
	public int seed;

	public static int IA   = 16807;
	public static double IM   = 2147483647;
	public static double AM   = ((float)1.0/IM);











	public Village(int level, int l, Village p, int s)
	{

		label = l;
		forward = new Village[4];
		seed = label * (IQ + s);
		hospital = new Hospital(level);
		returned = new List();
	}








	void addVillage(int i, Village c)
	{
        forward[i] = c;
	}
























	public static Village createVillage(int level, int label, Village back, int seed)
	{
		if(level == 0)
			return null;

		{
            Village village = new Village(level, label, back, seed);
            for (int i = 3; i >= 0; i--)
			{
				Village child = createVillage(level - 1, (label * 4) + i + 1, village, seed);
				village.addVillage(i, child);
			}
			return village;
		}
	}





	public List simulate()
	{

		var val = new List[4];

		for (int i = 3; i >= 0; i--) 
		{
			Village v = forward[i];
			if (v != null)
				val[i] = v.simulate();






			{







			}
		}

		hospital.checkPatientsInside(returned);
		List up = hospital.checkPatientsAssess(this);



		var p = generatePatient();
		if(p != null)
			hospital.putInHospital(p);

		return up;
	}





	public Results getResults()
	{
		var fval = new Results[4];
		for (int i = 3; i >=0 ; i--) 
		{
			Village v = forward[i];
			if (v != null)
				fval[i] = v.getResults();
		}

		Results r = new Results();
		for (int i = 3; i >= 0; i--) 
		{
			if (fval[i] != null) 
			{
				r.totalHospitals += fval[i].totalHospitals;


			}
		}

        var cur = returned.head;
        while (cur != null)
		{
			Patient p = (Patient)cur._object;
            r.totalHospitals += (float)p.hospitalsVisited;


            cur = cur.next;
        }

		return r;
	}





	private Patient generatePatient()
	{
		double rand = myRand(seed);

		Patient p = null;
		if(rand > 0.666)
			p = new Patient(this);
		return p;
	}









	public static double myRand(int idum)
	{
		idum = idum ^ MASK;
		int k = idum / IQ;
		idum = IA * (idum - k * IQ) - IR * k;
		idum = idum ^ MASK;


		double answer = AM * ((double)idum);
		return answer;
	}
}
