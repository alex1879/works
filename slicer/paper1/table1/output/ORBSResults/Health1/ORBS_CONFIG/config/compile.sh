cd $1 #Move to system directory
rm -f Health.exe # Remove existing output
csc -out:Health.exe -deterministic Health.cs Hospital.cs List.cs ListNode.cs Patient.cs Results.cs Village.cs &> compile.log #Build
if [ -f Health.exe ]; then #check build
    echo `md5sum Health.exe`
else
    echo FAIL
fi
