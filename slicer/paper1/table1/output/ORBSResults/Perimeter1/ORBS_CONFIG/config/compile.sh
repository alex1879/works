cd $1 #Move to system directory
rm -f Perimeter.exe # Remove existing output
csc -out:Perimeter.exe -deterministic Perimeter.cs BlackNode.cs GreyNode.cs NorthEast.cs NorthWest.cs Quadrant.cs QuadTreeNode.cs SouthEast.cs SouthWest.cs WhiteNode.cs &> compile.log #Build
if [ -f Perimeter.exe ]; then #check build
    echo `md5sum Perimeter.exe`
else
    echo FAIL
fi
