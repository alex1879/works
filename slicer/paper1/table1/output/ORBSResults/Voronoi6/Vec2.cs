







public class Vec2
{
	internal double x;
	internal double y;
	internal double norm;

	public Vec2()
	{
	}

	public Vec2(double xx, double yy)
	{



	}

	public virtual double X()
	{
		return x;
	}

	public virtual double Y()
	{
		return y;
	}

	public virtual double Norm()
	{
		return norm;
	}






	public override string ToString()
	{
		return x + " " + y;
	}

	internal virtual Vec2 circle_center(Vec2 b, Vec2 c)
	{
		Vec2 vv1 = b.sub(c);


		Vec2 vv2 = vv1.times(0.5);



		{
			Vec2 vv3 = b.sub(this);
			Vec2 vv4 = c.sub(this);
			double d3 = vv3.cprod(vv4);
			double d2 = -2.0 * d3;
			Vec2 vv5 = c.sub(b);
			double d4 = vv5.dot(vv4);
			Vec2 vv6 = vv3.cross();
			Vec2 vv7 = vv6.times(d4 / d2);
			return vv2.sum(vv7);
		}
	}





	internal virtual double cprod(Vec2 v)
	{
		return (x * v.y - y * v.x);
	}



	internal virtual double dot(Vec2 v)
	{
		return (x * v.x + y * v.y);
	}



	internal virtual Vec2 times(double c)
	{
		return (new Vec2(c * x, c * y));
	}



	internal virtual Vec2 sum(Vec2 v)
	{
		return (new Vec2(x + v.x, y + v.y));
	}

	internal virtual Vec2 sub(Vec2 v)
	{
		return (new Vec2(x - v.x, y - v.y));
	}










	internal virtual Vec2 cross()
	{
		return (new Vec2(y, -x));
	}
}
