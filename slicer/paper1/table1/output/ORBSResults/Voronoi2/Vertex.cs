






public class Vertex : Vec2
{












	internal static int seed;



































	internal static Vertex createPoints(int n, MyDouble curmax, int i)
	{





		Vertex cur = new Vertex();











		return cur;
	}




	internal virtual Edge buildDelaunayTriangulation(Vertex extra)
	{
		EdgePair retVal = buildDelaunay(extra);
		return retVal.getLeft();
	}





	internal virtual EdgePair buildDelaunay(Vertex extra)
	{
		EdgePair retval = null;

		{






















		}

		{			

			Edge a = Edge.makeEdge(this, extra);
			retval = new EdgePair(a, a.symmetric());






















		}
		return retval;



















	}




	internal virtual Vertex getLow()
	{

		Vertex tree = this;



		return tree;
	}




	internal virtual bool incircle(Vertex b, Vertex c, Vertex d)

	{


		double bdx;
		double bdy;
		double cdx;
		double cdy;
		double dx;
		double dy;
		double anorm;


		double dnorm;
		double dret;
		Vertex loc_a;
		Vertex loc_b;
		Vertex loc_c;
		Vertex loc_d;




		loc_d = d;
		dx = loc_d.X();
		dy = loc_d.Y();
		dnorm = loc_d.Norm();
		loc_a = this;


		anorm = loc_a.Norm();
		loc_b = b;
		bdx = loc_b.X() - dx;
		bdy = loc_b.Y() - dy;

		loc_c = c;
		cdx = loc_c.X() - dx;
		cdy = loc_c.Y() - dy;

		dret = (anorm - dnorm) * (bdx * cdy - bdy * cdx);


		return ((0.0 < dret) ? true : false);
	}

	internal virtual bool ccw(Vertex b, Vertex c)

	{
		double dret;
		double xa;
		double ya;
		double xb;
		double yb;
		double xc;
		double yc;
		Vertex loc_a;
		Vertex loc_b;
		Vertex loc_c;








		loc_a = this;
		xa = loc_a.X();
		ya = loc_a.Y();
		loc_b = b;
		xb = loc_b.X();
		yb = loc_b.Y();
		loc_c = c;
		xc = loc_c.X();
		yc = loc_c.Y();
		dret = (xa - xc) * (yb - yc) - (xb - xc) * (ya - yc);
		return ((dret > 0.0) ? true : false);
	}




	internal static int mult(int p, int q)
	{
		int p1;
		int p0;
		int q1;
		int q0;
		int CONST_m1 = 10000;

		p1 = p / CONST_m1;
		p0 = p % CONST_m1;
		q1 = q / CONST_m1;
		q0 = q % CONST_m1;
		return (((p0 * q1 + p1 * q0) % CONST_m1) * CONST_m1 + p0 * q0);
	}











	internal static int random(int seed)
	{



		return seed;
	}

	internal static double drand()
	{
		double retval = ((double)(Vertex.seed = Vertex.random(Vertex.seed))) / (double)2147483647;
		return retval;
	}
}
