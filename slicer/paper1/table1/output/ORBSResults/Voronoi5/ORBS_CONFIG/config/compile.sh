cd $1 #Move to system directory
rm -f Voronoi.exe # Remove existing output
csc -out:Voronoi.exe -deterministic Voronoi.cs Vertex.cs Vec2.cs MyDouble.cs EdgePair.cs Edge.cs &> compile.log #Build
if [ -f Voronoi.exe ]; then #check build
    echo `md5sum Voronoi.exe`
else
    echo FAIL
fi
