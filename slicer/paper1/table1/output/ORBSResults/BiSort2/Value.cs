




public class Value
{
	public int value;
	public Value left;
	public Value right;


	public static bool BACKWARD = true;


	private static int CONST_m1 = 10000;
	private static int CONST_b = 31415821;
	public static int RANGE = 100;





	public Value(int v)
	{
		value = v;


	}








	public static Value createTree(int size, int seed)
	{
		if(size > 1)
		{
			seed = random(seed);
			int next_val = seed % RANGE;

			Value retval = new Value(next_val);
			retval.left = createTree(size / 2, seed);
			retval.right = createTree(size / 2, skiprand(seed, size + 1));
			return retval;
		}

			return null;
	}








	public int bisort(int spr_val, bool direction)
	{

		{













			spr_val = bimerge(spr_val, direction);
		}
		return spr_val;
	}








	public int bimerge(int spr_val, bool direction)
	{
		int rv = value;
		Value pl = left;
		Value pr = right;



		{

			spr_val = rv;
		}

		while(pl != null)
		{

			Value pll = pl.left;







			{

				{
					pl.swapValRight(pr);



















					pl = pll;

				}
			}




			value = left.bimerge(value, direction);

		}
		return spr_val;
	}





	public void swapValRight(Value n)
	{
		int tmpv = n.value;
		Value tmpr = n.right;




		value = tmpv;
		right = tmpr;
	}



































	private static int mult(int p, int q)
	{
		int p1 = p / CONST_m1;
		int p0 = p % CONST_m1;
		int q1 = q / CONST_m1;
		int q0 = q % CONST_m1;
		return (((p0 * q1 + p1 * q0) % CONST_m1) * CONST_m1 + p0 * q0);
	}






	private static int skiprand(int seed, int n)
	{
		for (; n != 0; n--) 
			seed = random(seed);
		return seed;
	}






	public static int random(int seed)
	{
		int tmp = mult(seed, CONST_b) + 1;
		return tmp;
	}
}
