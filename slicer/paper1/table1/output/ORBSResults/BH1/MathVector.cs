﻿using System;
//
// A class representing a three dimensional vector that implements
// several math operations.  To improve speed we implement the
// vector as an array of doubles rather than use the exising
// code in the java.util.Vector class.
//
public class MathVector
{
	//
	// The number of dimensions in the vector
	//
	public static readonly int NDIM = 3;
	//
	// An array containing the values in the vector.
	//
	public double[] data;

	//
	// Construct an empty 3 dimensional vector for use in Barnes-Hut algorithm.
	//
	public MathVector()
	{
		data = new double[NDIM];
		for(int i = 0; i < NDIM; i++)
			data[i] = 0.0;
	}

	//
	// Create a copy of the vector.
	// @return a clone of the math vector
	//
	public MathVector cloneMathVector()
	{
        try
        {
            MathVector v = new MathVector();
		    v.data = new double[NDIM];
		    for(int i = 0; i < NDIM; i++)
			    v.data[i] = data[i];
            return v;
        }
        catch(Exception e)
        {
            throw;
        }
	}

	//
	// Return the value at the i'th index of the vector.
	// @param i the vector index
	// @return the value at the i'th index of the vector.
	//
	public double value(int i)
	{
		return data[i];
	}

	//
	// Set the value of the i'th index of the vector.
	// @param i the vector index
	// @param v the value to store
	//
	public void setValue(int i, double v)
	{
		data[i] = v;
	}

	//
	// Set one of the dimensions of the vector to 1.0
	// param j the dimension to set.
	//
	public void unit(int j)
	{
		for(int i = 0; i < NDIM; i++)
			if(i == j)
				data[i] = 1.0;
			else
				data[i] = 0.0;
	}

	//
	// Add two vectors and the result is placed in this vector.
	// @param u the other operand of the addition
	//
	public void addition(MathVector u)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] + u.data[i];
	}

	//
	// Subtract two vectors and the result is placed in this vector.
	// This vector contain the first operand.
	// @param u the other operand of the subtraction.
	//
	public void subtraction1(MathVector u)










	public void subtraction2(MathVector u, MathVector v)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = u.data[i] - v.data[i];
	}





	public void multScalar1(double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] * s;
	}






	public void multScalar2(MathVector u, double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = u.data[i] * s;
	}





	public void divScalar(double s)
	{
		for(int i = 0; i < NDIM; i++)
			data[i] = data[i] / s;
	}





	public double dotProduct()
	{
		double s = 0.0;
		for(int i = 0; i < NDIM; i++)
			s += data[i] * data[i];
		return s;
	}































































    public override string ToString()
    {
        var s = new System.Text.StringBuilder();
        for (int i = 0; i < NDIM; i++)
            s.Append(data[i] + " ");
        return s.ToString();
    }
}
