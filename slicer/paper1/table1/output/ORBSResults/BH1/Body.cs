﻿using System;




public class Body : Node
{
	public MathVector vel;
	public MathVector acc;
	public MathVector newAcc;


	private Body next;
	private Body procNext;




	public Body() : base()
	{
		this.vel = new MathVector();





	}





	  public void setNext(Body n)
	  {
		next = n;
	  }





    public Body getNext()
	  {
		return next;
	  }





      public void setProcNext(Body n)
	  {
		procNext = n;
	  }





	  public Body getProcNext()
	  {
		return procNext;
	  }






	public void expandBox(BTree tree, int nsteps)
	{
		MathVector rmid = new MathVector();

		bool inbox = icTest(tree);
		while(!inbox)
		{
			double rsize = tree.rsize;










			tree.rsize = 2.0 * rsize;

			{
				MathVector ic = tree.intcoord(rmid);


				int k = Node.oldSubindex(ic, Node.IMAX >> 1);
				Cell newt = new Cell();
				newt.subp[k] = tree.root;
				tree.root = newt;
				inbox = icTest(tree);
			}
		}
	}





	public bool icTest(BTree tree)
	{
		double pos0 = pos.value(0);
		double pos1 = pos.value(1);
		double pos2 = pos.value(2);


		bool result = true;

		double xsc = (pos0 - tree.rmin.value(0)) / tree.rsize;



		xsc = (pos1 - tree.rmin.value(1)) / tree.rsize;
		if(!(0.0 < xsc && xsc < 1.0))
			result = false;

		xsc = (pos2 - tree.rmin.value(2)) / tree.rsize;
		if(!(0.0 < xsc && xsc < 1.0))
			result = false;

		return result;
	}










	public override Cell loadTree(Body p, MathVector xpic, int l, BTree tree)
	{

		Cell retval = new Cell();
		int si = subindex(tree, l);

		retval.subp[si] = this;


		si = Node.oldSubindex(xpic, l);
		Node rt = retval.subp[si];
		if(rt != null)
			retval.subp[si] = rt.loadTree(p, xpic, l >> 1, tree);
		else
			retval.subp[si] = p;
		return retval;
	}





	public override double hackcofm()
	{
		return mass;
	}






	public int subindex(BTree tree, int l)
	{
		MathVector xp = new MathVector();

		double xsc = (pos.value(0) - tree.rmin.value(0)) / tree.rsize;
		xp.setValue(0, Math.Floor(IMAX * xsc));

		xsc = (pos.value(1) - tree.rmin.value(1)) / tree.rsize;
		xp.setValue(1, Math.Floor(IMAX * xsc));

		xsc = (pos.value(2) - tree.rmin.value(2)) / tree.rsize;
		xp.setValue(2, Math.Floor(IMAX * xsc));

		int i = 0;
		for(int k = 0; k < MathVector.NDIM; k++)
		{
			if(((int)xp.value(k) & l) != 0) //This used val & 1 != 0 so if there is a problem look here
				i += Cell.NSUB >> (k + 1);
		}
		return i;
	}






	public void hackGravity(double rsize, Node root)
	{


		HG hg = new HG(this, pos);
		hg = root.walkSubTree(rsize * rsize, hg);

		newAcc = hg.acc0;
	}




	public override HG walkSubTree(double dsq, HG hg)
	{

			hg = gravSub(hg);
		return hg;
	}









}
