Línea argumental:

Limitación actual:
* Los slicers tradicionales funcionan analizando la traza completa del programa cliente, como consecuencia el tiempo de análisis suele ser proporcional a la cantidad de traza generada, y por ende, a la ejecución del programa.
* La cantidad de traza generada se debe a varios motivos, uno a que se recolecta toda la información de las escrituras en memoria a bajo nivel, y otro a que se slicean las librerías, que en muchos casos son de cómputo intensivo. Si bien esta técnica es precisa, analizar dichas librerías es muy costoso y no provee un output directo al usuario el cual solo está interesado en slicear sus propias líneas de código.

Idea:
* La hipótesis inicial es que reduciendo drásticamente la cantidad de traza generada podemos mejorar varios órdenes de magnitud el tiempo de análisis sin perder considerablemente la precisión del slicers.
* Esto se puede lograr sliceando únicamente el flujo del programa cliente, dejando en claro que no se instrumentan las librerías. 

* Claramente al no tener las direcciones concretas de la memoria escritas/leídas necesitaremos un modelo de memoria que permita no solo mantener las relaciones entre las variables y los objetos instanciados en el programa cliente, sino también almacenar la información relacionada a cuando/donde se han escrito por última vez. 
* La interfaz provista debe permitir, dada una pequeña instrucción, representar su efecto en el modelo y poder consultar posteriormente dicha información. 

* Por lo tanto la interfaz es la siguiente:
- Alloc: 
- ...

* EJEMPLO INICIAL
* EXPLICACIÓN DEL EJEMPLO INICIAL

* Limitación: en muchos casos queremos analizar invocaciones a métodos en librerías. Al no tenerlas instrumentadas, necesitamos una forma de actualizar nuestro modelo de forma precisa. Para estos casos, la opción elegida fue *anotar* que escrituras/lecturas ocurren en dicha librería en relación con el caller, que nuevos objetos se crean y como se conectan. 
* EXTENDER EL EJEMPLO INICIAL CON UNA ANOTACIÓN
* MOSTRAR ALGO HERMOSO: Como se puede mantener LastDef con anotaciones hermosas.

--------- Acá termina la primer parte cerrada de forma épica y aplausos de pie.

Implementación:

Estructura:
* Como mencionamos previamente, la estructura será la misma que otros slicers, una parte instrumentará el programa cliente y otra analizará la traza
* La parte de instrumentación se realizará en alto nivel aprovechando la API provista por Microsoft. La condición para instrumentar es la siguiente: solo lo necesario para saber el flujo. Nada más. Restaría, oportunamente, hacer un estudio de trade-off agregar más información cuánto aumenta la traza y la precisión. 
* Otro beneficio de tener la traza de esta forma es que no ralentiza el programa cliente. Eso queda para discusión. 

* Una vez instrumentado el programa cliente se ejecuta, produce la traza que analizaremos. El input del slicer será: El programa cliente y el flujo.
Luego que queda, analizar cada instrucción ejecutada. Para simplificar el análisis de diversas estructuras sintácticas construimos una capa de más bajo nivel que provee de servicios básicos a la capa de análisis sintáctico. Esta capa permite tratar todas las expresiones sintácticas complejas que vamos analizando. 
La llamamos Broker, su interfaz es esta:

* Alloc, assign, etc. etc. 

* El Broker a su vez distribuye su trabajo en las 2 estructuras principales del slicer que veremos a continuación. 

1) Dependendy graph
* Para representar las relaciones entre cada statement utilizaremos una estructura muy conocida y empleada en el mundo del slicing conocida como dependency graph
* Al tener muchas menos instrucciones que representar no utilizaremos algoritmos complejos de compresión de esta estructura, no es el fin de este slicer, aunque claramente podría mejorarse. En esta ocasión utilizaremos el approach de forward computation de Agrawal y Horgan. 
* Si bien este approach es más costoso computacionalmente mantiene un grafo cuyo tamaño no depende de la cantidad de instrucciones ejecutadas. 
Por otro lado al mantener en todo momento las relaciones entre los statements nos proporciona una serie de ventajas que discutiremos más adelante (*).

2) Points-to graph
* El PtG es un modelo de memoria muy utilizado en otros ámbitos, especialmente en el mundo estático [Ref].
* Lo cierto es que el PTG adaptado para slicing es ideal ya que modela la memoria y permite obtener los last def.

--> Acá tendríamos que agregar la estructura de nuestro PTG y como implementa todas las operaciones descritas anteriormente.  

Para tratar un assign de forma completa por ejemplo...
EJEMPLO DE COMO FUNCIONARÍA UN ASSIGN

* Evidentemente en presencia de no instrumentadas, sin contar con otra información, sería necesario ejecutar en nuestras estructuras operaciones "destructivas" que empeorarían la precisión drásticamente. Para evitarlo desarrollamos un sistema de anotaciones que veremos a continuación. 

--------------- Acá termina implementación parte 1, veamos anotaciones.

Anotaciones:
* La intención es permitir modelar la memoria de tal forma que podamos expresar que escribimos, leemos, que objetos se crean y como los conectamos
* Entonces empecemos con la sintaxis:

SINTAXIS DEL LENGUAJE

SEMÁNTICA DEL LENGUAJE

Invariantes que tiene que mantener. 

EJEMPLO DE ANOTACIÓN PARA EL NEW y/o ADD de una lista que adentro posee un array (o alguno medio pelotude).

------------- Research questions:

Queremos saber que tan preciso y performante es el slicer en comparación con otros slicers existentes para nuestra debilidad y nuestra fortaleza.
Nuestra debilidad serían los programas con poco/nulo cómputo en frameworks y nuestra fortaleza serían programas cuyo cómputo se realice principalmente en librerías. 
Por lo tanto la primer RQ sería:
* Qué tan preciso y performante es DynAbs vs otros slicers en ausencia de frameworks?
Por otro lado, queremos saber en presencia de frameworks standard en el mercado si nuestro slicer puede competir. Por lo tanto la segunda RQ sería:
* Qué tan preciso y performante es DynAbs vs otros slicers en presencia de frameworks?
Veremos a continuación la evaluación que intenta responder estas preguntas. 

------------- Evaluación:

RQ1:
El Slicer está desarrollado para C#, y un slicer de alto nivel representativo del estado del arte es Javaslicer (Java). 
* Necesitamos una forma de compararlos para evaluar la precisión. Lo ideal sería tener un benchmark equivalente para ambos lenguajes, eso no es tan fácil. 
Afortunadamente encontramos OLDEN, un benchmark que se encuentra en C y tiene traducciones hechas por terceros para Java y C#. En los casos donde no hubo traducción por terceros lo tradujimos nosotros con el sharpen. Para que sea más fáci la comparación ajustamos manualmente las líneas para que coincidieran exactamente en ambos lenguajes. Así es que como la sintaxis de Java y C# se parecen mucho, cada línea realiza la misma acción semántica que la de su par en el otro lenguaje. 
* Para seleccionar los inputs y los criterios tomamos las siguientes decisiones. 
Inputs: ejecutamos cada programa midiendo cobertura, y seleccionamos el input minimal que tuviera mayor cobertura de líneas. 
Criterios: tomamos un criterio distinto para cada "resultado" que arrojara el programa. Por ejemplo, si el programa devuelve in cómputo como en treeadd esa sería la variable a slicear. En caso de devolver un vector se tomó para slicear tanto al vector como a alguna de sus posiciones. Por eso cada programa tiene distinta cantidad de criterios según su cantidad de variables "resultado". 

* Por último cabe destacar que ORBS es un slicer multilenguaje que se desarrolló los últimos años. Si bien no es contra quienes nos queremos comparar, ya que utiliza otro approach distinto, lo agregamos en la evaluación para medir precisión y performance en estos casos. ORBS tiene algunas cosas positivas como que devuelve resultados compilables y ejecutables, y para programas chicos funciona muy bien. 

Corrida:
* Corrimos todos los casos y medimos precisión y performance. 
ACÁ HABLAMOS DE ALGUNOS CASOS, CUALES BIEN; CUALES MAL; ETC. EN CUALES GANAMOS, EN CUALES PERDEMOS, ORBS SE DESTACA EN ... ETC. 

RQ2: 
La idea ahora es contruir un benchmark adecuado para evaluar estos slicers en presencia de frameworks. 
Al tener como principal referente de un slicer tradicional de alto nivel a Javaslicer, necesitamos programas cliente que sean sintácticamente iguales en ambos lenguajes y consuman frameworks que estén disponibles en ambos lenguajes. Para construir este set de programas cruzamos un listado de frameworks disponibles para C# y Java de la misma fuente (Wikipedia). Los frameworks obtenidos fueron: Hibernate y NHibernate, y JLog y NLog respectivamente. 

Luego seleccionamos los primeros 3 programas en GitHub que no fueran multithreading, que tuvieran ambos frameworks para Java que no utilizaran otros frameworks no disponibles para C#, y que además puedan ser sliceados por Javaslicer. Para esto último no pudieron ser seleccionados programas con interfaz gráfica o con estructuras sintácticas modernas de Java que no sean soportadas en Javaslicer. 

Una vez seleccionados los 3 programas los tradujimos con una herramienta de traducción automática como sharpen y manualmente acomodamos las líneas en cada programa en C# para que coincidieran exactamente con su par en Java. Una vez hecho esto, nos restaba seleccionar los inputs y criterios para slicear. 
2 de los 3 casos no tenían inputs, eran simples ejemplos de ABM en hibernate, mientras que el tercero tenía una interfaz de consola de usuario. En este último caso reemplazamos cada input del usuario por un hardcodeo manual del dato solicitado, y modificamos el código para que ejecute al menos una vez cada función con el fin de tener mayor cobertura. 
Las variables a slicear en cada caso fueron las que tuvieran algo que ver con el resultado, esto se hizo manualmente utilizando objetos creados en el programa cliente y/o obtenidos por queries en la BD. 

Como en el caso anterior, también corrimos ORBS ya que es multilenguaje y se banca todo esto, pero no pudimos hacerlo en su versión paralela porque tenía acceso a una base de datos que para ser sliceada necesitaba ser accedida de forma única. Evitando modificar el programa, decidimos correr la versión secuencial.

Corrida:
* Corrimos todos los casos y medimos precisión y performance. 
* Vemos que Javaslicer pierde dependencias asociadas a la base de datos que tienen que ver con su técnica. 
* También observamos que por limitaciones técnicas asociadas posiblemente al uso de funciones del metalenguaje (reflection) se pierden dependencias entre líneas del código cliente que deberían estar. 
Por lo tanto, para poder compararnos contra un slice completamente sound decidimos incluir manualmente tanto las instrucciones que consideramos que deberían estar como las dependencias de datos/control perdidas a través de la base de datos. Este slice fue realizado manualmente, revisando línea por línea cual debería ser incluido en el slice final.

Por otra parte para slicear todas las líneas que queríamos que formen parte del resultado, decidimos modelar la base de datos en nuestro PTG. Esta posibilidad la podemos hacer por nuestras anotaciones. Es un efecto colateral positivo. Luego, estos fueron los resultados:

Precisión: a veces somos muy buenos, pero también a veces malos...
También MOSTRAMOS COMO SE DEGRADA JAVASLICER. ORBS TAMPOCO ES LA PANACEA. 
Si bien nosotros perdemos precisión nuestra performance es aplastante incluso para pequeños inputs. 

--------- Conclusiones:
En casos donde no hay un uso de frameworks no perdemos tanta precisión pero definitivamente no somos más performantes.
Sin embargo, en presencia de frameworks de gran cantidad de cómputo nuestro slicer es definitivamente mucho más rápido sin perder demasiada precisión.

--------- Discusión:
Está escrita en el boceto en inglés. Habría que revisarla. 

--------- Future work:
Mantener una representación acotada del heap para que el PTG no crezca según la cantidad de objetos creados en el código cliente,
Optimizar el análisis de estructuras iterativas aprovechando un posible heap acotado y un DG acotado por el uso del approach 4. 
Creemos que es posible, y vamos en esa dirección. 
Otra posibilidad es escribir estas anotaciones de forma automática. Un buen sistema de anotaciones para modelar efectos en la memoria puede ser utilizado para distintas herramientas de análisis tanto estático como dinámico. 
