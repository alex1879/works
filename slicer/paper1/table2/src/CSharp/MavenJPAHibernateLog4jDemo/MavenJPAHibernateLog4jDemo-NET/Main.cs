using Com.Danielme.Demo.Model;
using log4net;
using NPersistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Danielme.Demo
{
    
	
	
	
	class MainClass
    {
        private static ILog logger = LogManager.GetLogger(typeof(MainClass));

        static void Main(string[] args)
        {
            logger.Info("demo begins...");

            EntityManagerFactory entityManagerFactory = Persistence.CreateEntityManagerFactory("MavenJPAHibernateLog4jDemo");
            EntityManager entityManager = entityManagerFactory.CreateEntityManager();
            EntityTransaction transaction = entityManager.GetTransaction();

            transaction.Begin();

            EntityA entityA = new EntityA();
            entityA.setName("parent");

            EntityB entityB1 = new EntityB();
            entityB1.setName("child 1");
            entityB1.setEntityA(entityA);

            EntityB entityB2 = new EntityB();
            entityB2.setName("child 2");
            entityB2.setEntityA(entityA);

            entityA.setEntities(new List<EntityB>());
            entityA.getEntities().Add(entityB1);
            entityA.getEntities().Add(entityB2);

            try
            {
                //children will be persisted on cascade
                entityManager.Persist(entityA);
                transaction.Commit();

                logger.Info("===================");
                logger.Info(entityA.getId() + " " + entityA.getName() + " : ");
                foreach (EntityB entityB in entityA.getEntities())
                {
                    logger.Info("  " + entityB.getId() + " " + entityB.getName());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message, ex);
                transaction.Rollback();
            }
            finally
            {
                entityManager.Close();
            }

            EntityManager slicingVariable1 = entityManager;
            EntityA slicingVariable2 = entityA;
            long slicingVariable3 = entityA.id;
            String slicingVariable4 = entityA.name;
            IList<EntityB> slicingVariable5 = entityA.entities;
            EntityB slicingVariable6 = entityA.entities[0];
            EntityB slicingVariable7 = entityB1;
            long slicingVariable8 = entityB1.id;
            String slicingVariable9 = entityB1.name;
            System.Console.WriteLine("Done!");
        }
    }
}
