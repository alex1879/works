﻿using NPersistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Danielme.Demo.Model
{
    
	[Entity(Name = "entityb")]
    public class EntityB
    {
        [Id]
        [GeneratedValue(Strategy = GenerationType.AUTO)]
        public virtual long id { get; set; }

        [Column(Name = "name", Nullable = false, Length= 50)]
        public virtual string name { get; set; }

        [ManyToOne(Optional= false)]
        [JoinColumn(Name = "entityA_id")]
        public virtual EntityA entityA { get; set; }


        public virtual long getId()
        {
            return id;
        }

        public virtual void setId(long id)
        {
            this.id = id;
        }

        public virtual string getName()
        {
            return name;
        }

        public virtual void setName(string name)
        {
            this.name = name;
        }

        public virtual EntityA getEntityA()
        {
            return this.entityA;
        }

        public virtual void setEntityA(EntityA entityA)
        {
            this.entityA = entityA;
        }
    }
}
