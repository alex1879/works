﻿using NPersistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Danielme.Demo.Model
{
    
	
	
	
	[Entity(Name = "entitya")]
    public class EntityA
    {
        [Id]
        [GeneratedValue(Strategy = GenerationType.AUTO)]
        public virtual long id { get; set; }

        [Column(Name ="name", Nullable = false, Length = 50)]
        public virtual string name { get; set; }

        [OneToMany(Cascade = new CascadeType[] { CascadeType.ALL }, MappedBy = "entityA")]
        public virtual IList<EntityB> entities { get; set; }


        public virtual IList<EntityB> getEntities()
        {
            return entities;
        }

        public virtual void setEntities(IList<EntityB> entities)
        {
            this.entities = entities;
        }

        public virtual string getName()
        {
            return name;
        }

        public virtual void setName(string name)
        {
            this.name = name;
        }

        public virtual long getId()
        {
            return id;
        }

        public virtual void setId(long id)
        {
            this.id = id;
        }
    }
}
