using System.Collections.Generic;
using Com.Syed.Dao;
using Com.Syed.Entity;
using log4net;


namespace Com.Syed.Service
{
	
	
	
	
	
	
	public class CustomerServiceImpl : CustomerService
	{
		internal PaymentMethodDao paymentMethodDao = new PaymentMethodDaoImpl();
		internal CustomerDao customerDao = new CustomerDaoImpl();

		static ILog logger = LogManager.GetLogger(typeof(CustomerServiceImpl));
		
		
		public virtual void UpdateAddress(Address address)
		{
		}

		
		public virtual void UpdateCustomer(Customer customer)
		{
		}

		
		public virtual void DeleteCustomer(long custId)
		{
			customerDao.DeleteCustomer(custId);
		}

		
		public virtual int DeletePaymentMethods(long custId)
		{
			return paymentMethodDao.DeletePaymentMethods(custId);
		}

		
		public virtual Customer CreateCustomer(Customer customer)
		{
			return customerDao.SaveCustomer(customer);
		}

		
		public virtual Customer FindCustomerFullData(long custId)
		{
			return customerDao.FindCustomerFullData(custId);
		}

		
		public virtual PaymentMethod UpdatePaymentMethod(PaymentMethod paymentMethod)
		{
			return paymentMethodDao.UpdatePaymentMethod(paymentMethod);
		}

		
		public virtual void CreatePaymentMethods(IList<PaymentMethod> paymentMethods)
		{
			paymentMethodDao.InsertPaymentMethods(paymentMethods);
		}

		
		public virtual IList<PaymentMethod> FindPaymentMethods(long customerId)
		{
			return paymentMethodDao.FindPaymentMehods(customerId);
		}

		
		public virtual Customer FindCustomer(long customerId)
		{
			return customerDao.FindCustomer(customerId);
		}

		
		public virtual IList<Customer> ViewAllCustomers()
		{
			return customerDao.GetAllCustomers();
		}
	}
}
