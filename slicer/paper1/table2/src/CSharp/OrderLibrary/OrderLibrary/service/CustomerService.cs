using System.Collections.Generic;
using Com.Syed.Entity;


namespace Com.Syed.Service
{
	
	public interface CustomerService
	{
		void UpdateAddress(Address address);

		void UpdateCustomer(Customer customer);

		void DeleteCustomer(long custId);

		int DeletePaymentMethods(long custId);

		Customer CreateCustomer(Customer customer);

		PaymentMethod UpdatePaymentMethod(PaymentMethod paymentMethod);

		Customer FindCustomerFullData(long custId);

		void CreatePaymentMethods(IList<PaymentMethod> paymentMethods);

		IList<PaymentMethod> FindPaymentMethods(long customerId);

		Customer FindCustomer(long customerId);

		IList<Customer> ViewAllCustomers();
	}
}
