using System;
using System.IO;
using System.Reflection;
using Com.Syed.Entity;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;

namespace Com.Syed.Util
{
	public class HibernateUtil
	{
		private static readonly ISessionFactory sessionFactory = BuildSessionFactory();


        private static ISessionFactory BuildSessionFactory()
		{
			try
			{
                var configuration = new Configuration();
                HbmSerializer.Default.Validate = true; HbmSerializer.Default.Serialize(System.Environment.CurrentDirectory, Assembly.GetExecutingAssembly());
                configuration.AddDirectory(new DirectoryInfo(System.Environment.CurrentDirectory));
                new SchemaExport(configuration).Execute(true, true, false);
                ISessionFactory sessionFactory = configuration.BuildSessionFactory();
                return sessionFactory;
			}
			catch (Exception ex)
			{
				System.Console.Error.WriteLine("Initial SessionFactory creation failed." + ex);
				throw ex;
			}
		}

		public static ISessionFactory GetSessionFactory()
		{
			return sessionFactory;
		}
	}
}
