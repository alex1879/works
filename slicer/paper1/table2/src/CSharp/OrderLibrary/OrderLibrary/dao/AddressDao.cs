using Com.Syed.Entity;

namespace Com.Syed.Dao
{
	public interface AddressDao
	{
		Address UpdateAddress(Address address);

		int DeleteAddress(long custId);
	}
}
