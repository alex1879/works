using System;
using System.Collections.Generic;
using Com.Syed.Entity;
using Com.Syed.Util;
using NHibernate;

namespace Com.Syed.Dao
{
	
	
	
	
	public class PaymentMethodDaoImpl : PaymentMethodDao
	{
		
		internal ISessionFactory sessionFactory;

		public PaymentMethodDaoImpl()
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
		}

		
		
		public virtual IList<PaymentMethod> FindPaymentMehods(long custId)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			IList<PaymentMethod> customerPaymentMethods = new List<PaymentMethod>();
			try
			{
				IQuery query = session.CreateQuery("FROM PaymentMethod where customer_id=:customer_id");
				query.SetParameter("customer_id", custId);
				customerPaymentMethods = query.List<PaymentMethod>();
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
			return customerPaymentMethods;
		}

		
		public virtual int DeletePaymentMethods(long custId)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
            int updatedRows = 0;
			session.BeginTransaction();
            try
            {
                IQuery query = session.CreateQuery("delete from PaymentMethod where customer_id = :customer_id");
                query.SetParameter("customer_id", custId);
                updatedRows = query.ExecuteUpdate();
            }
            catch (Exception e)
            {
                System.Console.Out.WriteLine(e.Message);
            }
            finally
            {
                session.Close();
            }
			return updatedRows;
		}

		
		public virtual PaymentMethod UpdatePaymentMethod(PaymentMethod paymentMethod)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			session.BeginTransaction();
			try
			{
				session.SaveOrUpdate(paymentMethod);
				session.Transaction.Commit();
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
			return null;
		}

		
		public virtual void InsertPaymentMethods(IList<PaymentMethod> paymentMethods)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			session.BeginTransaction();
			try
			{
				foreach (PaymentMethod paymentMethod in paymentMethods)
				{
					session.Save(paymentMethod);
				}
				session.Transaction.Commit();
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
		}
	}
}
