using System.Collections.Generic;
using Com.Syed.Entity;

namespace Com.Syed.Dao
{
	
	public interface PaymentMethodDao
	{
		IList<PaymentMethod> FindPaymentMehods(long custId);

		int DeletePaymentMethods(long custId);

		PaymentMethod UpdatePaymentMethod(PaymentMethod paymentMethod);

		void InsertPaymentMethods(IList<PaymentMethod> paymentMethods);
	}
}
