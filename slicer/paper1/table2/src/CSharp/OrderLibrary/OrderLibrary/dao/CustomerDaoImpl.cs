using System;
using System.Collections.Generic;
using Com.Syed.Entity;
using Com.Syed.Util;
using NHibernate;

namespace Com.Syed.Dao
{
	
	
	
	public class CustomerDaoImpl : CustomerDao
	{
		
		internal ISessionFactory sessionFactory;

		public CustomerDaoImpl()
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
		}

		
		public virtual Customer SaveCustomer(Customer customer)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			session.BeginTransaction();
			try
			{
				session.Save(customer);
				session.Transaction.Commit();
			}
			catch (Exception e)
			{
				System.Console.Error.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
			return customer;
		}

		
		public virtual Customer FindCustomerFullData(long custId)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			Customer customerAllInfo = null;
			try
			{
				session.BeginTransaction();
				customerAllInfo = (Customer)session.Get(typeof(Customer), custId);
				NHibernateUtil.Initialize(customerAllInfo.getPaymentType());
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
			return customerAllInfo;
		}

		
		public virtual void DeleteCustomer(long custId)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			try
			{
				session.BeginTransaction();
				Customer customer = (Customer)session.Load(typeof(Customer), custId);
				session.Delete(customer);
				System.Console.Out.WriteLine("***Selected customer deleted successfully***");
				session.Transaction.Commit();
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
		}

		
		public virtual Customer UpdateCustomer(Customer customer)
		{
			return null;
		}

		
		public virtual Customer FindCustomer(long customerId)
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			Customer foundCustomer = null;
			try
			{
				foundCustomer = (Customer)session.Get(typeof(Customer), customerId);
                NHibernateUtil.Initialize(foundCustomer.getPaymentType());
			}
			catch (Exception e)
			{
				System.Console.Out.WriteLine(e.Message);
			}
			finally
			{
				session.Close();
			}
			return foundCustomer;
		}

		
		
		public virtual IList<Customer> GetAllCustomers()
		{
			sessionFactory = HibernateUtil.GetSessionFactory();
			ISession session = sessionFactory.OpenSession();
			IList<Customer> allCustomers = session.CreateCriteria(typeof(Customer)).List<Customer>();
			return allCustomers;
		}
	}
}
