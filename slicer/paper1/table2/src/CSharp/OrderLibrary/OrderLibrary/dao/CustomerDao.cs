using System.Collections.Generic;
using Com.Syed.Entity;

namespace Com.Syed.Dao
{
	
	public interface CustomerDao
	{
		Customer SaveCustomer(Customer customer);

		// For Customer and customer with address (Note: Address is eager load)
		Customer FindCustomer(long customerId);

		Customer FindCustomerFullData(long custId);

		// will delete along the customer information also.
		void DeleteCustomer(long custId);

		Customer UpdateCustomer(Customer customer);

		IList<Customer> GetAllCustomers();
	}
}
