using Com.Syed.Util;
using Com.Syed.Entity;
using Com.Syed.Service;
using Com.Syed.Dao;
using System;
using System.Collections.Generic;
using System.Globalization;
using log4net;

namespace Com.Syed.Main
{
    
	
	
	public class OrderLibApp 
    {
	    //static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	    //static Scanner userInput = new Scanner(System.in);
	    private static CustomerService customerService = new CustomerServiceImpl();

	    // static ILog logger = LogManager.GetLogger(typeof(OrderLibApp));

	    public static void Main(string[] args)
	    {
		    displayOptions();
	    }

	    private static void displayOptions() 
	    {
		    // BasicConfigurator.configure();
		    // logger.debug("Orderdisplay options method called");
		
		    // LAFHIS CODE:
		    int array_length = 9;
		    int current_option = 0;
		    int[] options = new int[array_length];
		    options[0] = 1; // Create customer
		    options[1] = 2; // Add payment method
		    options[2] = 3; // Fetch payment methods for the customer 
		    options[3] = 4; // Fetch customer with address and payment methods
		    options[4] = 5; // Fetch customer without payment methods
		    options[5] = 7; // Update customer payment method
		    options[6] = 9; // Show all customers
		    options[7] = 8; // Delete payment method of a customer
		    options[8] = 6; // Delete the customer with all associated information
		    // END LAFHIS CODE
		
		    int option = 0;
		    string input = "";
		    do 
		    {
			    Console.WriteLine("1. Add Customer along with Address" + "\n2. Add PaymentMethods for a customer" + "\n3. Fetch all the PaymentMethods for given customer" + "\n4. Fetch Customer along with Address and Payment methods" + "\n5. Fetch Customer along with Address but no Payment methods" + "\n6. Delete Customer and all associated information" + "\n7. Update customer payment methods with new information" + "\n8. Delete a Payment method of a customer" + "\n9. Show all customers" + "\n10. Apply logging for all the above api/methods " + "\n0. EXIT" + "\nSelect an option from the above options: ");

			    //option = userInput.nextInt(); LAFHIS
			    option = options[current_option++]; // LAFHIS			

                // LAFHIS: Adding code for repetitions
                if (option == 1)
                {
                    switchOptions(option, -1);
                    switchOptions(option, -1);
                }
                if (option == 2)
                {
                    switchOptions(option, 1);
                    switchOptions(option, 2);
                }
                if (option == 3 || option == 4 || option == 5)
                {
                    switchOptions(option, 1);
                    switchOptions(option, 2);
                }
                if (option == 7)
                {
                    switchOptions(option, 1);
                }
                if (option == 6)
                    switchOptions(option, 1);
                if (option == 8)
                    switchOptions(option, 2);
                if (option == 9 || option == 0)
                    switchOptions(option, -1);

                Console.WriteLine("Do you want to continue (Y/N): ");
			    // input = userInput.next(); LAFHIS
			    input = "y"; // LAFHIS 
			    if (input.Equals("n")) 
			    {
				    switchOptions(0, -1);
			    }
		    } while (input.Equals("y") && current_option < array_length);
		    // LAFHIS: WE ADDED "&& current_option < array_length" FOR EXIT PROGRAM IN THIS LINE AND SLICE ALL THE STATIC VARIABLES HERE
		    long slicingVariable1 = tempSlicingVariable1;
		    String slicingVariable2 = tempSlicingVariable2;
		    long slicingVariable3 = tempSlicingVariable3;
		    String slicingVariable4 = tempSlicingVariable4;
		    long slicingVariable5 = tempSlicingVariable5;
		    String slicingVariable6 = tempSlicingVariable6;
		    String slicingVariable7 = tempSlicingVariable7;
		    String slicingVariable8 = tempSlicingVariable8;
		    long slicingVariable9 = tempSlicingVariable9;
		    long slicingVariable10 = tempSlicingVariable10;
		    String slicingVariable11 = tempSlicingVariable11;
		    long slicingVariable12 = tempSlicingVariable12;
		    long slicingVariable13 = tempSlicingVariable13;
		    String slicingVariable14 = tempSlicingVariable14;
		    Console.WriteLine("Done!");
		    System.Environment.Exit(0);
	    }

	    // For slice only one customer (LAFHIS)
	    static bool case1Sliced = false;
	    static bool case2Sliced = false;
	    static bool case3Sliced = false;
	    static bool case4Sliced = false;
	    static bool case5Sliced = false;
	    static bool case6Sliced = false;
	    static bool case7Sliced = false;
	    static bool case8Sliced = false;
	
	    static long tempSlicingVariable1 = 0;
	    static String tempSlicingVariable2 = "";
	    static long tempSlicingVariable3 = 0;
	    static String tempSlicingVariable4 = "";
	    static long tempSlicingVariable5 = 0;
	    static String tempSlicingVariable6 = "";
	    static String tempSlicingVariable7 = "";
	    static String tempSlicingVariable8 = "";
	    static long tempSlicingVariable9 = 0;
	    static long tempSlicingVariable10 = 0;
	    static String tempSlicingVariable11 = "";
	    static long tempSlicingVariable12 = 0;
	    static long tempSlicingVariable13 = 0;
	    static String tempSlicingVariable14 = "";
	
	    private static void switchOptions(int option, long predefinedId) 
	    {
		    switch (option) 
		    {
			    case 1:
				    Customer customer = null;
				    try 
				    {
					    customer = PopulateCustInfo();
				    } 
				    catch (Exception e) 
				    {
					    Console.Error.WriteLine(e.Message);
				    }
				    Customer savedCustomer = customerService.CreateCustomer(customer);
				    Console.WriteLine("***Entered customer has been added***");
				    Console.WriteLine(savedCustomer);
				
				    // SLICING OUTPUT (LAFHIS CODE)
				    if (!case1Sliced)
				    {
					    tempSlicingVariable1 = savedCustomer.customerId;
					    tempSlicingVariable2 = savedCustomer.name;
					    case1Sliced = true;
					    Console.WriteLine("OK");
				    }
				    break;
			    case 2:
				    Console.WriteLine("Enter the customer id you want to add payment method to: ");
				    //Customer checkedCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				    Customer checkedCustomer = customerService.FindCustomer(predefinedId); // LAFHIS
				    if (checkedCustomer == null) 
				    {
					    Console.WriteLine("Entered customer doesn't exist");
				    } 
				    else 
				    {
					    List<PaymentMethod> paymentMethods = null;
					    try 
					    {
						    paymentMethods = PopulatePaymentMethods(checkedCustomer);
					    } 
					    catch (Exception e) 
					    {
						    Console.Error.WriteLine(e.Message);
					    }
					    customerService.CreatePaymentMethods(paymentMethods);
					    Console.WriteLine("***Entered payment methods have been added***");
					
					    // SLICING OUTPUT (LAFHIS CODE)
					    if (!case2Sliced)
					    {
						    tempSlicingVariable3 = paymentMethods[0].paymentId;
						    tempSlicingVariable4 = paymentMethods[0].cardNumber;
						    case2Sliced = true;
						    Console.WriteLine("OK");
					    }
				    }
				    break;
			    case 3:
				    Console.WriteLine("Enter the customer id whose payment methods to be displayed: ");
				    //List<PaymentMethod> customerPaymentMethods = customerService.findPaymentMethods(userInput.nextLong()); LAFHIS
				    IList<PaymentMethod> customerPaymentMethods = customerService.FindPaymentMethods(predefinedId); // LAFHIS
				    if (customerPaymentMethods.Count > 0) 
				    {
					    foreach (PaymentMethod paymentMethod in customerPaymentMethods) 
					    {
						    Console.WriteLine(paymentMethod);
						
						    // SLICING OUTPUT (LAFHIS CODE)
						    if (!case3Sliced)
						    {
							    tempSlicingVariable5 = paymentMethod.paymentId;
							    tempSlicingVariable6 = paymentMethod.cardNumber;
							    case3Sliced = true;
							    Console.WriteLine("OK");
						    }
					    }
				    } 
				    else
					    Console.WriteLine("No payments methods found with entered ID");
				    break;
			    case 4:
				    Console.WriteLine("Enter the customer id whose customer to be displayed: ");
				    //Customer customerWithFullData = customerService.findCustomerFullData(userInput.nextLong()); LAFHIS
				    Customer customerWithFullData = customerService.FindCustomerFullData(predefinedId); // LAFHIS
				    if (customerWithFullData != null)
				    {
					    String tmp = customerWithFullData.PrintCustomerWithAllData(); // LAFHIS: intermediate line for slicing with this criterion
					    Console.WriteLine(tmp);
					
					    // SLICING OUTPUT (LAFHIS CODE)
					    if (!case4Sliced)
					    {
						    tempSlicingVariable7 = tmp;
						    case4Sliced = true;
						    Console.WriteLine("OK");
					    }
				    }
					
				    else
					    Console.WriteLine("No customer found with entered ID");
				    break;
			    case 5:
				    Console.WriteLine("Enter the customer id whose customer information to be loaded: ");
				    //Customer foundCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				    Customer foundCustomer = customerService.FindCustomer(predefinedId); // LAFHIS
				    if (foundCustomer != null)
				    {
					    String tmp = foundCustomer.ToString(); // LAFHIS: intermediate line for slicing with this criterion
					    Console.WriteLine(tmp);
					
					    // SLICING OUTPUT (LAFHIS CODE)
					    if (!case5Sliced)
					    {
						    tempSlicingVariable8 = tmp;
						    case5Sliced = true;
						    Console.WriteLine("OK");
					    }
				    }
				    else
					    Console.WriteLine("No customer found with entered ID");
				    break;
			    case 6:
				    Console.WriteLine("Enter the customer's id to delete: ");
				    //customerService.deleteCustomer(userInput.nextLong()); // LAFHIS
				    customerService.DeleteCustomer(predefinedId); // LAFHIS
				
				    // SLICING OUTPUT (LAFHIS CODE)
				    if (!case6Sliced)
				    {
					    tempSlicingVariable9 = predefinedId; // I don't know another variable to slice
					    case6Sliced = true;
					    Console.WriteLine("OK");
				    }
				    break;
			    case 7:
				    Console.WriteLine("Enter the customer id: ");
				    //Customer checkCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				    Customer checkCustomer = customerService.FindCustomer(predefinedId); // LAFHIS
				    if (checkCustomer != null) 
				    {
					    Console.WriteLine(checkCustomer.getPaymentType());
					    PaymentMethod paymentMethod = new PaymentMethod();
					    Console.WriteLine("Select the payment method id to update from above: ");
					    //paymentMethod.setPaymentId(userInput.nextLong()); LAFHIS
					    paymentMethod.setPaymentId(predefinedId); // LAFHIS
					    Console.WriteLine("***Enter payment method details***");
					    Console.WriteLine("Enter card name: ");
					    //paymentMethod.setCardName(userInput.next()); LAFHIS
					    paymentMethod.setCardName("MASTERCARD"); // LAFHIS
					    Console.WriteLine("Enter card number: ");
					    //paymentMethod.setCardNumber(userInput.next()); LAFHIS
					    paymentMethod.setCardNumber("6548123588224697"); // LAFHIS
					    Console.WriteLine("Enter card type ");
					    //paymentMethod.setCardType(userInput.next()); LAFHIS 
					    paymentMethod.setCardType("CREDIT2"); // LAFHIS
					    Console.WriteLine("Enter date from (YYYY/MM/DD): ");
					    try 
					    {
                            //paymentMethod.setDateFrom(sdf.parse(userInput.next())); LAFHIS
                            paymentMethod.setDateFrom(DateTime.ParseExact("2025/05/01", "yyyy/MM/dd", CultureInfo.InvariantCulture)); // LAFHIS
                        } 
					    catch (Exception e) 
					    {
                            throw;
					    }
					    paymentMethod.setCustomer(checkCustomer);
					    customerService.UpdatePaymentMethod(paymentMethod);
					
					    // SLICING OUTPUT (LAFHIS CODE)
					    if (!case7Sliced)
					    {
						    tempSlicingVariable10 = paymentMethod.paymentId;
						    tempSlicingVariable11 = paymentMethod.cardNumber;
						    case7Sliced = true;
						    Console.WriteLine("OK");
					    }
				    } 
				    else
					    Console.WriteLine("Customer doesn't exist");
				    break;
			    case 8:
				    Console.WriteLine("Enter customer id to delete payment methods: ");
				    //Long custId = userInput.nextLong(); LAFHIS
				    var custId = predefinedId; // LAFHIS
				    Customer checkdCustomer = customerService.FindCustomer(custId);
				    if (checkdCustomer != null) 
				    {
					    Console.WriteLine(customerService.DeletePaymentMethods(custId));
					
					    // SLICING OUTPUT (LAFHIS CODE)
					    if (!case8Sliced)
					    {
						    tempSlicingVariable12 = checkdCustomer.customerId; // I don't know another variable to slice
						    case8Sliced = true;
						    Console.WriteLine("OK");
					    }
				    } 
				    else
					    Console.WriteLine("Customer doesn't exist");
				    break;
			    case 9:
				    IList<Customer> allCustomers = customerService.ViewAllCustomers();
				    Console.WriteLine(allCustomers);
				
				    // SLICING OUTPUT (LAFHIS CODE)
				    tempSlicingVariable13 = allCustomers[0].customerId;
				    tempSlicingVariable14 = allCustomers[0].name;
				    Console.WriteLine("OK");
				    break;
			    case 0:
				    Console.WriteLine("Thank You, GoodBye!!!");
				    System.Environment.Exit(0);
				    break;
			    default:
				    Console.WriteLine("Invalid Choice, Try again");
				    break;
		    }
	    }

	    // private static Customer checkIfCustomerExists(Long customerId) {
	    // Customer checkCustomer = null;
	    // Session session = null;
	    // try {
	    // sessionFactory = HibernateUtil.getSessionFactory();
	    // Session session = sessionFactory.openSession();
	    // checkCustomer = session.get(Customer.class, customerId);
	    // Hibernate.initialize(checkCustomer.getPaymentType());
	    // } catch (Exception e) {
	    // Console.WriteLine(e.Message);
	    // } finally {
	    // session.close();
	    // }
	    // return checkCustomer;
	    // }

	    private static List<PaymentMethod> PopulatePaymentMethods(Customer checkedCustomer)
	    {
		    Console.WriteLine("***Enter payment method details***");
		    Console.WriteLine("Enter number of payments methods you want to add: ");
		    //int noOfPaymentMethods = userInput.nextInt(); LAFHIS
		    int noOfPaymentMethods = 1; // LAFHIS
		    PaymentMethod pm = null;
		    List<PaymentMethod> pmList = new List<PaymentMethod>();

		    while (noOfPaymentMethods != 0) 
		    {
			    pm = new PaymentMethod();
			    Console.WriteLine("Enter card name: ");
			    //pm.setCardName(userInput.next()); LAFHIS
			    pm.setCardName("VISA"); // LAFHIS
			    Console.WriteLine("Enter card number: ");
			    //pm.setCardNumber(userInput.next()); LAFHIS
			    pm.setCardNumber("4509790112345678"); // LAFHIS
			    Console.WriteLine("Enter card type ");
			    //pm.setCardType(userInput.next()); LAFHIS
			    pm.setCardType("CREDIT"); // LAFHIS
			    Console.WriteLine("Enter date from (YYYY/MM/DD): ");
                //pm.setDateFrom(sdf.parse(userInput.next())); LAFHIS
                pm.setDateFrom(DateTime.ParseExact("2022/10/22", "yyyy/MM/dd", CultureInfo.InvariantCulture)); // LAFHIS
                pm.setCustomer(checkedCustomer);
			    pmList.Add(pm);
			    Console.WriteLine("---------------------------------");
			    noOfPaymentMethods--;
		    }
		    return pmList;
	    }

	    private static Customer PopulateCustInfo()
	    {
		    Console.WriteLine("***Enter customer information***");
		    Customer customer = new Customer();
		    Console.WriteLine("Enter customer name: ");
		    //customer.setName(userInput.next()); LAFHIS
		    customer.setName("Gervasio"); // LAFHIS
		    Console.WriteLine("Enter date of birth (YYYY/MM/DD): ");
		    //customer.setDateOfBirth(sdf.parse(userInput.next())); LAFHIS
            customer.setDateOfBirth(DateTime.ParseExact("1991/01/06", "yyyy/MM/dd", CultureInfo.InvariantCulture)); // LAFHIS
            Console.WriteLine("Enter annual salary: ");
		    //customer.setAnnualSalary(userInput.nextDouble()); LAFHIS
		    customer.setAnnualSalary(100000); // LAFHIS
		    Console.WriteLine("Enter Address: ");
		    Address address = new Address();
		    Console.WriteLine("street: ");
		    //address.setStreet(userInput.next()); LAFHIS
		    address.setStreet("Int. Guiraldes 2160"); // LAFHIS
		    Console.WriteLine("city: ");
		    //address.setCity(userInput.next()); LAFHIS
		    address.setCity("Buenos Aires"); // LAFHIS
		    Console.WriteLine("state: ");
		    //address.setState(userInput.next()); LAFHIS
		    address.setState("Argentina"); // LAFHIS
		    Console.WriteLine("zip: ");
		    //address.setZip(userInput.next()); LAFHIS
		    address.setZip("A1416C"); // LAFHIS
		    customer.setAddress(address);
		    address.setCustomer(customer);
		    return customer;
	    }
    }
}