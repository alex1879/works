using System;
using NPersistence;

namespace Com.Syed.Entity
{
	
	
	
	
	
	
	
	[System.Serializable]
    [NHibernate.Mapping.Attributes.Class(Table = "paymentmethod")]
    public class PaymentMethod
	{
		
		private const long serialVersionUID = -8536345472128992332L;

		
        [NHibernate.Mapping.Attributes.Id(0, Name = "paymentId", Column = "payment_id", TypeType = typeof(long))]
        [NHibernate.Mapping.Attributes.Generator(1, Class = "increment")]
        public virtual long paymentId { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "cardNumber")]
        public virtual string cardNumber { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "cardName")]
        public virtual string cardName { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "dateFrom")]
        public virtual DateTime dateFrom { get; set; }

		
        public virtual string dateType { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "cardType")]
        public virtual string cardType { get; set; }

		
        [NHibernate.Mapping.Attributes.ManyToOne(Name = "customer", Column = "customer_id", ClassType = typeof(Customer)/*, NotNull = true*/)]
        public virtual Customer customer { get; set; }

        public virtual long getPaymentId()
		{
			return paymentId;
		}

		public virtual void setPaymentId(long paymentId)
		{
			this.paymentId = paymentId;
		}

		public virtual string getCardNumber()
		{
			return cardNumber;
		}

		public virtual void setCardNumber(string cardNumber)
		{
			this.cardNumber = cardNumber;
		}

		public virtual string getCardName()
		{
			return cardName;
		}

		public virtual void setCardName(string cardName)
		{
			this.cardName = cardName;
		}

		public virtual DateTime getDateFrom()
		{
			return dateFrom;
		}

		public virtual void setDateFrom(DateTime dateFrom)
		{
			this.dateFrom = dateFrom;
		}

		public virtual string getDateType()
		{
			return dateType;
		}

		public virtual void setDateType(string dateType)
		{
			this.dateType = dateType;
		}

		public virtual string getCardType()
		{
			return cardType;
		}

		public virtual void setCardType(string cardType)
		{
			this.cardType = cardType;
		}

		public virtual Customer getCustomer()
		{
			return customer;
		}

		public virtual void setCustomer(Customer customer)
		{
			this.customer = customer;
		}

		public static long getSerialversionuid()
		{
			return serialVersionUID;
		}

		
		public override string ToString()
		{
			return "PaymentMethod [paymentId=" + paymentId + ", cardNumber=" + cardNumber + ", cardName=" + cardName + ", dateFrom=" + dateFrom + ", cardType=" + cardType + "]";
		}
	}
}
