using NPersistence;

namespace Com.Syed.Entity
{
    
	
	
	
	
	
	
	
	
	[System.Serializable]
    [NHibernate.Mapping.Attributes.Class(Table = "address")]
    public class Address
    {
        private const long serialVersionUID = 8972378284796682635L;

        [NHibernate.Mapping.Attributes.Id(0, Name = "id", Column = "id", TypeType = typeof(long))]
        [NHibernate.Mapping.Attributes.Generator(1, Class = "foreign")]
        [NHibernate.Mapping.Attributes.Param(2, Name = "property", Content = "customer")]
        public virtual long id { get; set; }

		
        [NHibernate.Mapping.Attributes.OneToOne(Name="customer", ClassType = typeof(Customer), Cascade = "ALL")]
        public virtual Customer customer { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "street")]
        public virtual string street { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "city")]
        public virtual string city { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "state")]
        public virtual string state { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "zip")]
        public virtual string zip { get; set; }

        public virtual long getId()
		{
			return id;
		}

		public virtual void setId(long id)
		{
			this.id = id;
		}

		public virtual Customer getCustomer()
		{
			return customer;
		}

		public virtual void setCustomer(Customer customer)
		{
			this.customer = customer;
		}

		public virtual string getStreet()
		{
			return street;
		}

		public virtual void setStreet(string street)
		{
			this.street = street;
		}

		public virtual string getCity()
		{
			return city;
		}

		public virtual void setCity(string city)
		{
			this.city = city;
		}

		public virtual string getState()
		{
			return state;
		}

		public virtual void setState(string state)
		{
			this.state = state;
		}

		public virtual string getZip()
		{
			return zip;
		}

		public virtual void setZip(string zip)
		{
			this.zip = zip;
		}

		public static long GetSerialversionuid()
		{
			return serialVersionUID;
		}

		
		public override string ToString()
		{
			return "Address [id=" + id + ", street=" + street + ", city=" + city + ", state=" + state + ", zip=" + zip + "]";
		}
	}
}
