using System;
using System.Collections.Generic;
using NPersistence;

namespace Com.Syed.Entity
{
    
	
	
	
	
	
	
	
	
	
	[System.Serializable]
    [NHibernate.Mapping.Attributes.Class(Table = "customer")]
    public class Customer
    {
        private const long serialVersionUID = 6475837673609646016L;

        
		[NHibernate.Mapping.Attributes.Id(0, Name = "customerId", Column = "customer_id", TypeType = typeof(long))]
        [NHibernate.Mapping.Attributes.Generator(1, Class = "increment")]
        public virtual long customerId { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "name")]
        public virtual string name { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "date_of_birth")]
        public virtual DateTime dateOfBirth { get; set; }

        [NHibernate.Mapping.Attributes.Property(Column = "annualSalary")]
        public virtual double annualSalary { get; set; }

        [NHibernate.Mapping.Attributes.OneToOne(Cascade = "ALL", ClassType = typeof(Address))]
        public virtual Address address { get; set; }

        [NHibernate.Mapping.Attributes.Bag(0, Table="paymentmethod", Name = "paymentType", Inverse = true, Lazy = NHibernate.Mapping.Attributes.CollectionLazy.True, Cascade = "all-delete-orphan")]
        [NHibernate.Mapping.Attributes.Key(1, Column = "customer_id", ForeignKey = "customer_id")]
        [NHibernate.Mapping.Attributes.OneToMany(1, ClassType = typeof(PaymentMethod))]
        public virtual IList<PaymentMethod> paymentType { get; set; }

		public virtual long getCustomerId()
		{
			return customerId;
		}

		public virtual void setCustomerId(long customerId)
		{
			this.customerId = customerId;
		}

		public virtual string getName()
		{
			return name;
		}

		public virtual void setName(string name)
		{
			this.name = name;
		}

		public virtual DateTime getDateOfBirth()
		{
			return dateOfBirth;
		}

		public virtual void setDateOfBirth(DateTime dateOfBirth)
		{
			this.dateOfBirth = dateOfBirth;
		}

		public virtual double getAnnualSalary()
		{
			return annualSalary;
		}

		public virtual void setAnnualSalary(double annualSalary)
		{
			this.annualSalary = annualSalary;
		}

		public virtual Address getAddress()
		{
			return address;
		}

		public virtual void setAddress(Address address)
		{
			this.address = address;
		}

		public virtual IList<PaymentMethod> getPaymentType()
		{
			return paymentType;
		}

		public virtual void setPaymentType(IList<PaymentMethod> paymentType)
		{
			this.paymentType = paymentType;
		}

		public static long getSerialversionuid()
		{
			return serialVersionUID;
		}

		
		public override string ToString()
		{
			return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + "]";
		}

		public virtual string PrintCustomerWithAllData()
		{
			return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + ", paymentType=" + paymentType + "]";
		}
	}
}
