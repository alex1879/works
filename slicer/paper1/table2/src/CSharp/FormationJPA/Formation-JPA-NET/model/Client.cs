using NPersistence;
using System;
using System.Collections.Generic;









namespace model
{
    [Entity]
    [Table(Name = "client")]
    public class Client
	{
		
        [Id]
        [GeneratedValue(Strategy = GenerationType.IDENTITY)]
        public virtual long id { get; set; }

        [Basic]
        public virtual string lastname { get; set; }

        [Basic]
        public virtual string firstname { get; set; }
        
        [ManyToOne]
        [JoinColumn(Name = "bookPref_id")]
        public virtual Book bookPref { get; set; }

        [Basic]
        [Enumerated(EnumType.STRING)]
        public virtual Gender gender { get; set; }

        [ManyToMany]
        [JoinTable(Name = "client_book")]
        public virtual IList<Book> books { get; set; } = new List<Book>();

		public Client()
		{
			
		}

		public Client(string lastname, string firstname, Gender gender)
			: base()
		{
			this.lastname = lastname;
			this.firstname = firstname;
			this.gender = gender;
		}

        public virtual long getId()
        {
            return id;
        }

        public virtual void setId(long id)
        {
            this.id = id;
        }

        public virtual string getLastname()
        {
            return lastname;
        }

        public virtual void setLastname(string lastname)
        {
            this.lastname = lastname;
        }

        public virtual string getFirstname()
        {
            return firstname;
        }

        public virtual void setFirstname(string firstname)
        {
            this.firstname = firstname;
        }

        public virtual Gender getGender()
        {
            return gender;
        }

        public virtual void setGender(Gender gender)
        {
            this.gender = gender;
        }

        public virtual Book getBookPref()
        {
            return bookPref;
        }

        public virtual void setBookPref(Book bookPref)
        {
            this.bookPref = bookPref;
        }

        public virtual IList<Book> getBooks()
        {
            return books;
        }

        public virtual void setBooks(IList<Book> books)
        {
            this.books = books;
        }

        
		public override string ToString()
		{
			return "Client [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", bookPref=" + bookPref + ", gender=" + gender + "]";
		}
	}
}
