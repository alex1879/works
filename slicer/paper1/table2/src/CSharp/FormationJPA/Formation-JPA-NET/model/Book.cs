using NPersistence;
using System;
using System.Collections.Generic;






namespace model
{
    [Entity]
    [Table(Name = "book")]
    public class Book
	{
        [Id]
        [GeneratedValue(Strategy = GenerationType.IDENTITY)]
        public virtual long id { get; set; }

        [Basic]
        public virtual string title { get; set; }

        [Basic]
        public virtual string author { get; set; }

        [OneToMany(MappedBy = "bookPref")]
        public virtual IList<Client> clientsHavePref { get; set; }

        [ManyToMany(MappedBy = "books")]
        public virtual IList<Client> clients { get; set; }

        public Book()
		{
			
		}

		public Book(string title, string author)
			: base()
		{
			this.title = title;
			this.author = author;
		}

        public virtual string getTitle()
        {
            return title;
        }

        public virtual void setTitle(string title)
        {
            this.title = title;
        }

        public virtual string getAuthor()
        {
            return author;
        }

        public virtual long getId()
        {
            return id;
        }

        public virtual void setId(long id)
        {
            this.id = id;
        }

        public virtual void setAuthor(string author)
        {
            this.author = author;
        }

        public virtual IList<Client> getClientsHavePref()
        {
            return clientsHavePref;
        }

        public virtual void setClientsHavePref(IList<Client> clientsHavePref)
        {
            this.clientsHavePref = clientsHavePref;
        }

        public virtual IList<Client> getClients()
        {
            return clients;
        }

        public virtual void setClients(IList<Client> clients)
        {
            this.clients = clients;
        }

        
		public override string ToString()
		{
			return "Livre [id=" + id + ", title=" + title + ", author=" + author + "]";
		}
	}
}
