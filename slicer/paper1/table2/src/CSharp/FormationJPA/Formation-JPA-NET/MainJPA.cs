using NPersistence;
using System;
using model;





public class MainJPA
{
	
	public static void start()
	{
		
		EntityManager em = DAO.DatabaseHelper.createEntityManager();
		DAO.DatabaseHelper.beginTx(em);
		
		Client c = new Client("Uzumaki", "Naruto", Gender.M);
		Client c2 = new Client("Haruno", "Sakura", Gender.F);
		Client c3 = new Client("Uchiha", "Madara", Gender.M);
		Book b = new Book("L'art de la guerre", "Sun Tzu");
		Book b2 = new Book("Le vieil homme et la mer", "Ernest Hemingway");
		Book b3 = new Book("Le Seigneur des Anneaux", "JRR Tolkien");
		
		em.Persist(b);
		em.Persist(b2);
		em.Persist(b3);
		em.Persist(c);
		em.Persist(c2);
		em.Persist(c3);
		
		DAO.ServiceJPA.buyBook(em, c, b);
		System.Console.WriteLine("");
		DAO.ServiceJPA.buyBook(em, c2, b2);
		System.Console.WriteLine("");
		//ServiceJPA.buyBook(em, c3, b3);
		c.setBookPref(b);
		c2.setBookPref(b2);
		c3.setBookPref(b3);
		
		DAO.DatabaseHelper.commitTxAndClose(em);
		
		em = DAO.DatabaseHelper.createEntityManager();
		DAO.DatabaseHelper.beginTx(em);
		
		System.Console.WriteLine("");
		string t1 = DAO.ServiceJPA.displayBooksByCustomer(em, c);
		System.Console.WriteLine("");
		DAO.ServiceJPA.displayBooksByCustomer(em, c2);
		System.Console.WriteLine("");
		DAO.ServiceJPA.displayBooksByCustomer(em, c3);
		System.Console.WriteLine("");
		string t2 = DAO.ServiceJPA.displayCustomersByBook(em, b);
		System.Console.WriteLine("");
		DAO.ServiceJPA.displayCustomersByBook(em, b2);
		System.Console.WriteLine("");
		DAO.ServiceJPA.displayCustomersByBook(em, b3);
		System.Console.WriteLine("");
		string t3 = DAO.ServiceJPA.displayPurchasedBook(em);
		System.Console.WriteLine("");
		
		DAO.DatabaseHelper.commitTxAndClose(em);
		//em.close();
		
		EntityManager slicingVariable1 = em;
		Client slicingVariable2 = c;
		long slicingVariable3 = c.id;
		Gender slicingVariable4 = c.gender;
		Book slicingVariable5 = b;
		long slicingVariable6 = b.id;
		string slicingVariable7 = t1;
		string slicingVariable8 = t2;
		string slicingVariable9 = t3;
		System.Console.WriteLine("Done!");
	}
}
