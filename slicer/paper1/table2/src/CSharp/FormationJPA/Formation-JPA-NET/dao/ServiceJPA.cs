using NPersistence;
using System;






namespace DAO
{
	public class ServiceJPA
	{
		
		public static string displayBooksByCustomer(EntityManager em, model.Client c)
		{
			string returnValue = "";
			foreach (model.Book b in em.Find<model.Client>(typeof(model.Client), c.getId()).getBooks())
			{
				System.Console.WriteLine("Livres achetés par " + c.getFirstname() + " " + c.getLastname() + " :");
				System.Console.WriteLine(b.getTitle() + " écrit par " + b.getAuthor());
				returnValue = b.getTitle();
			}
			return returnValue;
		}

		public static string displayCustomersByBook(EntityManager em, model.Book b)
		{
			string returnValue = "";
			foreach (model.Client c in em.Find<model.Book>(typeof(model.Book), b.getId()).getClients())
			{
				System.Console.WriteLine("Acheteurs pour le livre " + b.getTitle() + " :");
				System.Console.WriteLine(c.getFirstname() + " / " + c.getLastname());
				returnValue = c.getFirstname();
			}
			return returnValue;
		}

		public static string displayPurchasedBook(EntityManager em)
		{
			string returnValue = "";
			TypedQuery<model.Book> query = em.CreateQuery<model.Book>("SELECT b" + " FROM Book b" + " INNER JOIN b.clients", typeof(model.Book));
			System.Collections.Generic.IList<model.Book> lb = query.GetResultList();
			System.Console.WriteLine("Livres achetés : ");
			System.Console.WriteLine("");
			foreach (model.Book b in lb)
			{
				System.Console.WriteLine(b.getTitle() + " écrit par " + b.getAuthor());
				returnValue = b.getTitle();
			}
			return returnValue;
		}

		public static void buyBook(EntityManager em, model.Client c, model.Book b)
		{
			System.Console.WriteLine("Achat du livre " + b.getTitle() + " par le client " + c.getFirstname() + " / " + c.getLastname());
			System.Collections.Generic.IList<model.Book> lb = c.getBooks();
			lb.Add(b);
		}
	}
}
