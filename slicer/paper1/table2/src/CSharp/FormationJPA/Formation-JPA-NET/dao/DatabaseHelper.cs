using NPersistence;
using System;


namespace DAO
{
	public sealed class DatabaseHelper
	{
		private static EntityManagerFactory entityManagerFactory;

		public static EntityManager createEntityManager()
		{
			if (entityManagerFactory == null)
			{
				entityManagerFactory = Persistence.CreateEntityManagerFactory("FormationJPA");
			}
			return entityManagerFactory.CreateEntityManager();
		}

		public static void commitTxAndClose(EntityManager entityManager)
		{
			entityManager.GetTransaction().Commit();
			entityManager.Close();
		}

		public static void rollbackTxAndClose(EntityManager entityManager)
		{
			entityManager.GetTransaction().Rollback();
			entityManager.Close();
		}

		public static void beginTx(EntityManager entityManager)
		{
			entityManager.GetTransaction().Begin();
		}
	}
}
