package com.syed.dao;

import java.util.List;

import com.syed.entity.Customer;

public interface CustomerDao 
{
	Customer saveCustomer(Customer customer);

	// For Customer and customer with address (Note: Address is eager load)
	Customer findCustomer(Long customerId);

	Customer findCustomerFullData(Long custId);

	// will delete along the customer information also.
	void deleteCustomer(Long custId); 

	Customer updateCustomer(Customer customer);
	
	List<Customer> getAllCustomers();
}

