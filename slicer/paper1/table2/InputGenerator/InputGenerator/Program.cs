﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var sequences = new List<string>();
            
            var possible_inputs = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            var rnd = new Random();
            var number_of_sequences = 10;
            var length_of_sequences = 20;
            var min_number_of_initial_inserts = 5;
            var max_number_of_initial_inserts = 20;
            for (var i = 0; i < number_of_sequences; i++)
            {
                var new_sequence = new StringBuilder();

                // Initial number of inserts
                var initial_number = rnd.Next(min_number_of_initial_inserts, max_number_of_initial_inserts);
                var ids = GetNumbersBetween(1, initial_number);
                var next_id = initial_number + 1;
                new_sequence.Append($"{{1,{initial_number}}}");
                var used_options = new HashSet<int>() { 1 };

                for (var j = 0; j < length_of_sequences; j++)
                {
                    // Random choice
                    var next_opt = possible_inputs[rnd.Next(0, 9)];
                    if (next_opt == 1)
                        // Add one customer
                        new_sequence.Append(",{1,1}");
                    else if (next_opt != 9)
                    {
                        // Perform an operation to a customer
                        var rnd_customer = rnd.Next(0, ids.Count);
                        new_sequence.Append($",{{{next_opt},{ids[rnd_customer]}}}");
                        if (next_opt == 6)
                            ids.RemoveAt(rnd_customer);
                    }
                    else
                        new_sequence.Append(",{9,0}");
                    used_options.Add(next_opt);
                }

                // We only save sequences which execute all the options
                if (used_options.Count == 9)
                    sequences.Add(new_sequence.ToString());
            }

            // Print:
            foreach (var s in sequences)
                Console.WriteLine(s);

            return;
        }

        static List<int> GetNumbersBetween(int min, int max)
        {
            var r = new List<int>();
            for (var i = min; i < max; i++)
                r.Add(i);
            return r;
        }
    }
}
