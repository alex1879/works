package com.syed.service;

import java.util.List;



import com.syed.dao.CustomerDao;
import com.syed.dao.CustomerDaoImpl;
import com.syed.dao.PaymentMethodDao;
import com.syed.dao.PaymentMethodDaoImpl;

import com.syed.entity.Customer;
import com.syed.entity.PaymentMethod;

public class CustomerServiceImpl implements CustomerService 
{
	PaymentMethodDao paymentMethodDao = new PaymentMethodDaoImpl();
	CustomerDao customerDao = new CustomerDaoImpl();




















	public int deletePaymentMethods(Long custId) 
	{
		return paymentMethodDao.deletePaymentMethods(custId);
	}


	public Customer createCustomer(Customer customer) 
	{
		return customerDao.saveCustomer(customer);
	}


	public Customer findCustomerFullData(Long custId) 
	{
		return customerDao.findCustomerFullData(custId);
	}


	public PaymentMethod updatePaymentMethod(PaymentMethod paymentMethod) 
	{
		return paymentMethodDao.updatePaymentMethod(paymentMethod);






	}


	public List<PaymentMethod> findPaymentMethods(Long customerId) 
	{
		return paymentMethodDao.findPaymentMehods(customerId);
	}


	public Customer findCustomer(Long customerId) 
	{
		return customerDao.findCustomer(customerId);
	}


	public List<Customer> viewAllCustomers() 
	{
		return customerDao.getAllCustomers();
	}
}
