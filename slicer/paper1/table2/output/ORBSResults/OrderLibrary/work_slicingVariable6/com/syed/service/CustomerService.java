package com.syed.service;

import java.util.List;

import com.syed.entity.Customer;
import com.syed.entity.PaymentMethod;

public interface CustomerService 
{








	Customer createCustomer(Customer customer);





	void createPaymentMethods(List<PaymentMethod> paymentMethods);

	List<PaymentMethod> findPaymentMethods(Long customerId);

	Customer findCustomer(Long customerId);


}

