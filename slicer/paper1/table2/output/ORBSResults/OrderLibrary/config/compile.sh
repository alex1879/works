#!/bin/sh

# This file is part of the ORBS distribution.
# See the file LICENSE.TXT for more information.

# The compilation script.
#
# This script is called from ORBS to actually compile (build) the system.
#
# The script must return a generated signature if compilation was
# successful and must return "FAIL" if compilation was not
# successful. The signature should be, for example, an md5 hash over
# the generated binary.

# Configure ORBS for this project.
#source config/config.sh

cd work

# cleanup from previous run (just to make sure)
#rm -f reader checker.class
rm -f com/syed/dao/AddressDaoImpl.class com/syed/dao/CustomerDaoImpl.class com/syed/dao/PaymentMethodDaoImpl.class com/syed/dao/AddressDao.class com/syed/dao/CustomerDao.class com/syed/dao/PaymentMethodDao.class com/syed/entity/Address.class com/syed/entity/Customer.class com/syed/entity/PaymentMethod.class com/syed/main/OrderLibApp.class com/syed/service/CustomerServiceImpl.class com/syed/service/CustomerService.class com/syed/util/HibernateUtil.class

# The actual compilation.  This can be replaced by any mechanism to
# build the system, e.g. by invoking make.

javac -cp ".:../repos/*" com/syed/dao/AddressDaoImpl.java com/syed/dao/CustomerDaoImpl.java com/syed/dao/PaymentMethodDaoImpl.java com/syed/dao/AddressDao.java com/syed/dao/CustomerDao.java com/syed/dao/PaymentMethodDao.java com/syed/entity/Address.java com/syed/entity/Customer.java com/syed/entity/PaymentMethod.java com/syed/main/OrderLibApp.java com/syed/service/CustomerServiceImpl.java com/syed/service/CustomerService.java com/syed/util/HibernateUtil.java 2>> compile.log #Build
#gcc $CFLAGS -o reader reader.c 2> compile.log
#javac checker.java 2>> compile.log

if [ -f com/syed/dao/AddressDaoImpl.class -a -f com/syed/dao/CustomerDaoImpl.class -a -f com/syed/dao/PaymentMethodDaoImpl.class -a -f com/syed/dao/AddressDao.class -a -f com/syed/dao/CustomerDao.class -a -f com/syed/dao/PaymentMethodDao.class -a -f com/syed/entity/Address.class -a -f com/syed/entity/Customer.class -a -f com/syed/entity/PaymentMethod.class -a -f com/syed/main/OrderLibApp.class -a -f com/syed/service/CustomerServiceImpl.class -a -f com/syed/service/CustomerService.class -a -f com/syed/util/HibernateUtil.class ]; then #check build
    echo `md5sum com/syed/dao/AddressDaoImpl.class` `md5sum com/syed/dao/CustomerDaoImpl.class` `md5sum com/syed/dao/PaymentMethodDaoImpl.class` `md5sum com/syed/dao/AddressDao.class` `md5sum com/syed/dao/CustomerDao.class` `md5sum com/syed/dao/PaymentMethodDao.class` `md5sum com/syed/entity/Address.class` `md5sum com/syed/entity/Customer.class` `md5sum com/syed/entity/PaymentMethod.class` `md5sum com/syed/main/OrderLibApp.class` `md5sum com/syed/service/CustomerServiceImpl.class` `md5sum com/syed/service/CustomerService.class` `md5sum com/syed/util/HibernateUtil.class`
else
    echo FAIL
fi

 