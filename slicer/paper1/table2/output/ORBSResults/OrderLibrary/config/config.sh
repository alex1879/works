
# This file is part of the ORBS distribution.  
# See the file LICENSE.TXT for more information.

# ORBS configuration for the EXAMPLE project.

# ORBS itself
#export ORBS_SCRIPT="../../orbs/orbs.py"
export ORBS_SCRIPT="orbs.py"

# The ORBS working space
export ORBS="$PWD"

# The list of files to be sliced
export ORBS_FILES="work/com/syed/dao/AddressDaoImpl.java work/com/syed/dao/CustomerDaoImpl.java work/com/syed/dao/PaymentMethodDaoImpl.java work/com/syed/dao/AddressDao.java work/com/syed/dao/CustomerDao.java work/com/syed/dao/PaymentMethodDao.java work/com/syed/entity/Address.java work/com/syed/entity/Customer.java work/com/syed/entity/PaymentMethod.java work/com/syed/main/OrderLibApp.java work/com/syed/service/CustomerServiceImpl.java work/com/syed/service/CustomerService.java work/com/syed/util/HibernateUtil.java"
