package com.syed.entity;

import java.io.Serializable;












public class Address implements Serializable 
{
	private static final long serialVersionUID = 8972378284796682635L;




	private Long id;



	private Customer customer;


	private String street;


	private String city;


	private String state;


	private String zip;

	public Long getId() 
	{
		return id;
	}






	public Customer getCustomer() 
	{
		return customer;





	}

	public String getStreet() 
	{
		return street;





	}

	public String getCity() 
	{
		return city;





	}

	public String getState() 
	{
		return state;





	}

	public String getZip() 
	{
		return zip;





	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}


	public String toString() 
	{
		return "Address [id=" + id + ", street=" + street + ", city=" + city + ", state=" + state + ", zip=" + zip + "]";
	}
}

