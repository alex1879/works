package com.syed.entity;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class PaymentMethod implements Serializable 
{

	private static final long serialVersionUID = -8536345472128992332L;

	@Id
	@GeneratedValue

	public Long paymentId;


	public String cardNumber;


	private String cardName;


	private Date dateFrom;


	private String dateType;


	private String cardType;

	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	public Long getPaymentId() 
	{
		return paymentId;





	}

	public String getCardNumber() 
	{
		return cardNumber;





	}

	public String getCardName() 
	{
		return cardName;





	}

	public Date getDateFrom() 
	{
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) 
	{

	}

	public String getDateType() 
	{
		return dateType;
	}






	public String getCardType() 
	{
		return cardType;





	}

	public Customer getCustomer() 
	{
		return customer;
	}

	public void setCustomer(Customer customer) 
	{
		this.customer = customer;
	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}


	public String toString() 
	{
		return "PaymentMethod [paymentId=" + paymentId + ", cardNumber=" + cardNumber + ", cardName=" + cardName + ", dateFrom=" + dateFrom + ", cardType=" + cardType +  "]";
	}

}
