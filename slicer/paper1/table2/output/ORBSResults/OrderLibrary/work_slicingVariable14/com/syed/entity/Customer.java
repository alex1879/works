package com.syed.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;


@Entity
public class Customer implements Serializable 
{
	private static final long serialVersionUID = 6475837673609646016L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	public Long customerId;


	public String name;


	private Date dateOfBirth;


	private double annualSalary;


	private Address address;


	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)

	private List<PaymentMethod> paymentType;

	public Long getCustomerId() 
	{
		return customerId;
	}






	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public Date getDateOfBirth() 
	{
		return dateOfBirth;





	}

	public double getAnnualSalary() 
	{
		return annualSalary;





	}

	public Address getAddress() 
	{
		return address;





	}

	public List<PaymentMethod> getPaymentType() 
	{
		return paymentType;
	}






	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}


	public String toString() 
	{
		return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + "]";
	}

	public String printCustomerWithAllData() 
	{
		return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + ", paymentType=" + paymentType + "]";
	}

}
