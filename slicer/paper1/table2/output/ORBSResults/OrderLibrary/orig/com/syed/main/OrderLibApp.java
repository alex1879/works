package com.syed.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
//import java.util.Scanner;

import com.syed.entity.Address;
import com.syed.entity.Customer;
import com.syed.entity.PaymentMethod;
import com.syed.service.CustomerService;
import com.syed.service.CustomerServiceImpl;

public class OrderLibApp 
{
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	//static Scanner userInput = new Scanner(System.in);
	private static CustomerService customerService = new CustomerServiceImpl();

	//static final Logger logger = Logger.getLogger(OrderLibApp.class);

	public static void main(String[] args) throws ParseException 
	{
		displayOptions();
	}

	private static void displayOptions() 
	{
		// BasicConfigurator.configure();
		// logger.debug("Orderdisplay options method called");
		
		// LAFHIS CODE:
		int array_length = 9;
		int current_option = 0;
		int options[] = new int[array_length];
		options[0] = 1; // Create customer
		options[1] = 2; // Add payment method
		options[2] = 3; // Fetch payment methods for the customer 
		options[3] = 4; // Fetch customer with address and payment methods
		options[4] = 5; // Fetch customer without payment methods
		options[5] = 7; // Update customer payment method
		options[6] = 9; // Show all customers
		options[7] = 8; // Delete payment method of a customer
		options[8] = 6; // Delete the customer with all associated information
		// END LAFHIS CODE
		
		int option = 0;
		String input = "";
		do 
		{
			System.out.println("1. Add Customer along with Address" + "\n2. Add PaymentMethods for a customer" + "\n3. Fetch all the PaymentMethods for given customer" + "\n4. Fetch Customer along with Address and Payment methods" + "\n5. Fetch Customer along with Address but no Payment methods" + "\n6. Delete Customer and all associated information" + "\n7. Update customer payment methods with new information" + "\n8. Delete a Payment method of a customer" + "\n9. Show all customers" + "\n10. Apply logging for all the above api/methods " + "\n0. EXIT" + "\nSelect an option from the above options: ");

			//option = userInput.nextInt(); LAFHIS
			option = options[current_option++]; // LAFHIS			
		
			// LAFHIS: Adding code for repetitions
			if (option == 1)
			{
				switchOptions(option, -1);
				switchOptions(option, -1);
			}
			if (option == 2)
			{
				switchOptions(option, 1);
				switchOptions(option, 2);
			}
			if (option == 3 || option == 4 || option == 5)
			{
				switchOptions(option, 1);
				switchOptions(option, 2);				
			}
			if (option == 7)
			{
				switchOptions(option, 1);
			}
			if (option == 6)
				switchOptions(option, 1);
			if (option == 8)
				switchOptions(option, 2);
			if (option == 9 || option == 0)
				switchOptions(option, -1);
			
			System.out.println("Do you want to continue (Y/N): ");
			// input = userInput.next(); LAFHIS
			input = "y"; // LAFHIS 
			if (input.equalsIgnoreCase("n")) 
			{
				switchOptions(0, -1);
			}
		} while (input.equalsIgnoreCase("y") && current_option < array_length);
		// LAFHIS: WE ADDED "&& current_option < array_length" FOR EXIT PROGRAM IN THIS LINE AND SLICE ALL THE STATIC VARIABLES HERE
		long slicingVariable1 = tempSlicingVariable1;
		String slicingVariable2 = tempSlicingVariable2;
		long slicingVariable3 = tempSlicingVariable3;
		String slicingVariable4 = tempSlicingVariable4;
		long slicingVariable5 = tempSlicingVariable5;
		String slicingVariable6 = tempSlicingVariable6;
		String slicingVariable7 = tempSlicingVariable7;
		String slicingVariable8 = tempSlicingVariable8;
		long slicingVariable9 = tempSlicingVariable9;
		long slicingVariable10 = tempSlicingVariable10;
		String slicingVariable11 = tempSlicingVariable11;
		long slicingVariable12 = tempSlicingVariable12;
		long slicingVariable13 = tempSlicingVariable13;
		String slicingVariable14 = tempSlicingVariable14;
		System.out.println("Slicing value is: " + slicingVariable14);
		System.exit(0);
	}

	// For slice only one customer (LAFHIS)
	static boolean case1Sliced = false;
	static boolean case2Sliced = false;
	static boolean case3Sliced = false;
	static boolean case4Sliced = false;
	static boolean case5Sliced = false;
	static boolean case6Sliced = false;
	static boolean case7Sliced = false;
	static boolean case8Sliced = false;
	
	static long tempSlicingVariable1 = 0;
	static String tempSlicingVariable2 = "";
	static long tempSlicingVariable3 = 0;
	static String tempSlicingVariable4 = "";
	static long tempSlicingVariable5 = 0;
	static String tempSlicingVariable6 = "";
	static String tempSlicingVariable7 = "";
	static String tempSlicingVariable8 = "";
	static long tempSlicingVariable9 = 0;
	static long tempSlicingVariable10 = 0;
	static String tempSlicingVariable11 = "";
	static long tempSlicingVariable12 = 0;
	static long tempSlicingVariable13 = 0;
	static String tempSlicingVariable14 = "";
	
	private static void switchOptions(int option, long predefinedId) 
	{
		switch (option) 
		{
			case 1:
				Customer customer = null;
				try 
				{
					customer = populateCustInfo();
				} 
				catch (ParseException e) 
				{
					System.err.println(e.getMessage());
				}
				Customer savedCustomer = customerService.createCustomer(customer);
				System.out.println("***Entered customer has been added***");
				System.out.println(savedCustomer);
				
				// SLICING OUTPUT (LAFHIS CODE)
				if (!case1Sliced)
				{
					tempSlicingVariable1 = savedCustomer.customerId;
					tempSlicingVariable2 = savedCustomer.name;
					case1Sliced = true;
					System.out.println("OK");
				}
				break;
			case 2:
				System.out.println("Enter the customer id you want to add payment method to: ");
				//Customer checkedCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				Customer checkedCustomer = customerService.findCustomer(predefinedId); // LAFHIS
				if (checkedCustomer == null) 
				{
					System.out.println("Entered customer doesn't exist");
				} 
				else 
				{
					List<PaymentMethod> paymentMethods = null;
					try 
					{
						paymentMethods = populatePaymentMethods(checkedCustomer);
					} 
					catch (ParseException e) 
					{
						System.err.println(e.getMessage());
					}
					customerService.createPaymentMethods(paymentMethods);
					System.out.println("***Entered payment methods have been added***");
					
					// SLICING OUTPUT (LAFHIS CODE)
					if (!case2Sliced)
					{
						tempSlicingVariable3 = paymentMethods.get(0).paymentId;
						tempSlicingVariable4 = paymentMethods.get(0).cardNumber;
						case2Sliced = true;
						System.out.println("OK");
					}
				}
				break;
			case 3:
				System.out.println("Enter the customer id whose payment methods to be displayed: ");
				//List<PaymentMethod> customerPaymentMethods = customerService.findPaymentMethods(userInput.nextLong()); LAFHIS
				List<PaymentMethod> customerPaymentMethods = customerService.findPaymentMethods(predefinedId); // LAFHIS
				if (!customerPaymentMethods.isEmpty()) 
				{
					for (PaymentMethod paymentMethod : customerPaymentMethods) 
					{
						System.out.println(paymentMethod);
						
						// SLICING OUTPUT (LAFHIS CODE)
						if (!case3Sliced)
						{
							tempSlicingVariable5 = paymentMethod.paymentId;
							tempSlicingVariable6 = paymentMethod.cardNumber;
							case3Sliced = true;
							System.out.println("OK");
						}
					}
				} 
				else
					System.err.println("No payments methods found with entered ID");
				break;
			case 4:
				System.out.println("Enter the customer id whose customer to be displayed: ");
				//Customer customerWithFullData = customerService.findCustomerFullData(userInput.nextLong()); LAFHIS
				Customer customerWithFullData = customerService.findCustomerFullData(predefinedId); // LAFHIS
				if (customerWithFullData != null)
				{
					String tmp = customerWithFullData.printCustomerWithAllData(); // LAFHIS: intermediate line for slicing with this criterion
					System.out.println(tmp);
					
					// SLICING OUTPUT (LAFHIS CODE)
					if (!case4Sliced)
					{
						tempSlicingVariable7 = tmp;
						case4Sliced = true;
						System.out.println("OK");
					}
				}
					
				else
					System.err.println("No customer found with entered ID");
				break;
			case 5:
				System.out.println("Enter the customer id whose customer information to be loaded: ");
				//Customer foundCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				Customer foundCustomer = customerService.findCustomer(predefinedId); // LAFHIS
				if (foundCustomer != null)
				{
					String tmp = foundCustomer.toString(); // LAFHIS: intermediate line for slicing with this criterion
					System.out.println(tmp);
					
					// SLICING OUTPUT (LAFHIS CODE)
					if (!case5Sliced)
					{
						tempSlicingVariable8 = tmp;
						case5Sliced = true;
						System.out.println("OK");
					}
				}
				else
					System.err.println("No customer found with entered ID");
				break;
			case 6:
				System.out.println("Enter the customer's id to delete: ");
				//customerService.deleteCustomer(userInput.nextLong()); // LAFHIS
				customerService.deleteCustomer(predefinedId); // LAFHIS
				
				// SLICING OUTPUT (LAFHIS CODE)
				if (!case6Sliced)
				{
					tempSlicingVariable9 = predefinedId; // I don't know another variable to slice
					case6Sliced = true;
					System.out.println("OK");
				}
				break;
			case 7:
				System.out.println("Enter the customer id: ");
				//Customer checkCustomer = customerService.findCustomer(userInput.nextLong()); LAFHIS
				Customer checkCustomer = customerService.findCustomer(predefinedId); // LAFHIS
				if (checkCustomer != null) 
				{
					System.out.println(checkCustomer.getPaymentType());
					PaymentMethod paymentMethod = new PaymentMethod();
					System.out.println("Select the payment method id to update from above: ");
					//paymentMethod.setPaymentId(userInput.nextLong()); LAFHIS
					paymentMethod.setPaymentId(predefinedId); // LAFHIS
					System.out.println("***Enter payment method details***");
					System.out.println("Enter card name: ");
					//paymentMethod.setCardName(userInput.next()); LAFHIS
					paymentMethod.setCardName("MASTERCARD"); // LAFHIS
					System.out.println("Enter card number: ");
					//paymentMethod.setCardNumber(userInput.next()); LAFHIS
					paymentMethod.setCardNumber("6548123588224697"); // LAFHIS
					System.out.println("Enter card type ");
					//paymentMethod.setCardType(userInput.next()); LAFHIS 
					paymentMethod.setCardType("CREDIT2"); // LAFHIS
					System.out.println("Enter date from (YYYY/MM/DD): ");
					try 
					{
						//paymentMethod.setDateFrom(sdf.parse(userInput.next())); LAFHIS
						paymentMethod.setDateFrom(sdf.parse("2025/05/01")); // LAFHIS
					} 
					catch (ParseException e) 
					{
						e.getMessage();
					}
					paymentMethod.setCustomer(checkCustomer);
					customerService.updatePaymentMethod(paymentMethod);
					
					// SLICING OUTPUT (LAFHIS CODE)
					if (!case7Sliced)
					{
						tempSlicingVariable10 = paymentMethod.paymentId;
						tempSlicingVariable11 = paymentMethod.cardNumber;
						case7Sliced = true;
						System.out.println("OK");
					}
				} 
				else
					System.err.println("Customer doesn't exist");
				break;
			case 8:
				System.out.println("Enter customer id to delete payment methods: ");
				//Long custId = userInput.nextLong(); LAFHIS
				Long custId = predefinedId; // LAFHIS
				Customer checkdCustomer = customerService.findCustomer(custId);
				if (checkdCustomer != null) 
				{
					System.out.println(customerService.deletePaymentMethods(custId));
					
					// SLICING OUTPUT (LAFHIS CODE)
					if (!case8Sliced)
					{
						tempSlicingVariable12 = checkdCustomer.customerId; // I don't know another variable to slice
						case8Sliced = true;
						System.out.println("OK");
					}
				} 
				else
					System.err.println("Customer doesn't exist");
				break;
			case 9:
				List<Customer> allCustomers = customerService.viewAllCustomers();
				System.out.println(allCustomers);
				
				// SLICING OUTPUT (LAFHIS CODE)
				tempSlicingVariable13 = allCustomers.get(0).customerId;
				tempSlicingVariable14 = allCustomers.get(0).name;
				System.out.println("OK");
				break;
			case 0:
				System.out.println("Thank You, GoodBye!!!");
				System.exit(0);
				break;
			default:
				System.err.println("Invalid Choice, Try again");
				break;
		}
	}

	// private static Customer checkIfCustomerExists(Long customerId) {
	// Customer checkCustomer = null;
	// Session session = null;
	// try {
	// sessionFactory = HibernateUtil.getSessionFactory();
	// Session session = sessionFactory.openSession();
	// checkCustomer = session.get(Customer.class, customerId);
	// Hibernate.initialize(checkCustomer.getPaymentType());
	// } catch (Exception e) {
	// System.out.println(e.getMessage());
	// } finally {
	// session.close();
	// }
	// return checkCustomer;
	// }

	private static List<PaymentMethod> populatePaymentMethods(Customer checkedCustomer) throws ParseException 
	{
		System.out.println("***Enter payment method details***");
		System.out.println("Enter number of payments methods you want to add: ");
		//int noOfPaymentMethods = userInput.nextInt(); LAFHIS
		int noOfPaymentMethods = 1; // LAFHIS
		PaymentMethod pm = null;
		List<PaymentMethod> pmList = new ArrayList<>();

		while (noOfPaymentMethods != 0) 
		{
			pm = new PaymentMethod();
			System.out.println("Enter card name: ");
			//pm.setCardName(userInput.next()); LAFHIS
			pm.setCardName("VISA"); // LAFHIS
			System.out.println("Enter card number: ");
			//pm.setCardNumber(userInput.next()); LAFHIS
			pm.setCardNumber("4509790112345678"); // LAFHIS
			System.out.println("Enter card type ");
			//pm.setCardType(userInput.next()); LAFHIS
			pm.setCardType("CREDIT"); // LAFHIS
			System.out.println("Enter date from (YYYY/MM/DD): ");
			//pm.setDateFrom(sdf.parse(userInput.next())); LAFHIS
			pm.setDateFrom(sdf.parse("2022/10/22")); // LAFHIS
			pm.setCustomer(checkedCustomer);
			pmList.add(pm);
			System.out.println("---------------------------------");
			noOfPaymentMethods--;
		}
		return pmList;
	}

	private static Customer populateCustInfo() throws ParseException 
	{
		System.out.println("***Enter customer information***");
		Customer customer = new Customer();
		System.out.println("Enter customer name: ");
		//customer.setName(userInput.next()); LAFHIS
		customer.setName("Gervasio"); // LAFHIS
		System.out.println("Enter date of birth (YYYY/MM/DD): ");
		//customer.setDateOfBirth(sdf.parse(userInput.next())); LAFHIS
		customer.setDateOfBirth(sdf.parse("1991/01/06")); // LAFHIS
		System.out.println("Enter annual salary: ");
		//customer.setAnnualSalary(userInput.nextDouble()); LAFHIS
		customer.setAnnualSalary(100000); // LAFHIS
		System.out.println("Enter Address: ");
		Address address = new Address();
		System.out.println("street: ");
		//address.setStreet(userInput.next()); LAFHIS
		address.setStreet("Int. Guiraldes 2160"); // LAFHIS
		System.out.println("city: ");
		//address.setCity(userInput.next()); LAFHIS
		address.setCity("Buenos Aires"); // LAFHIS
		System.out.println("state: ");
		//address.setState(userInput.next()); LAFHIS
		address.setState("Argentina"); // LAFHIS
		System.out.println("zip: ");
		//address.setZip(userInput.next()); LAFHIS
		address.setZip("A1416C"); // LAFHIS
		customer.setAddress(address);
		address.setCustomer(customer);
		return customer;
	}
}
