#!/bin/bash

for i in $(seq 7 1 14)
do
   echo "Running slicing variable $i"
   sh config/set_slicing_variable.sh $i
   sh orbs.sh
   mv *.log work/
   mv "work" "work_slicingVariable"$i
done
