package com.syed.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;













public class Customer implements Serializable 
{
	private static final long serialVersionUID = 6475837673609646016L;




	public Long customerId;


	public String name;


	private Date dateOfBirth;


	private double annualSalary;


	private Address address;




	private List<PaymentMethod> paymentType;

	public Long getCustomerId() 
	{
		return customerId;
	}






	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public Date getDateOfBirth() 
	{
		return dateOfBirth;





	}

	public double getAnnualSalary() 
	{
		return annualSalary;





	}

	public Address getAddress() 
	{
		return address;





	}

	public List<PaymentMethod> getPaymentType() 
	{
		return paymentType;
	}






	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}


	public String toString() 
	{
		return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + "]";
	}

	public String printCustomerWithAllData() 
	{
		return "Customer [customerId=" + customerId + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", annualSalary=" + annualSalary + ", address=" + address + ", paymentType=" + paymentType + "]";
	}

}
