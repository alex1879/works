package com.syed.service;

import java.util.List;

import com.syed.entity.Customer;
import com.syed.entity.PaymentMethod;

public interface CustomerService 
{








	Customer createCustomer(Customer customer);



	Customer findCustomerFullData(Long custId);

	void createPaymentMethods(List<PaymentMethod> paymentMethods);



	Customer findCustomer(Long customerId);


}

