package com.syed.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


import com.syed.entity.Address;
import com.syed.entity.Customer;
import com.syed.entity.PaymentMethod;
import com.syed.service.CustomerService;
import com.syed.service.CustomerServiceImpl;

public class OrderLibApp 
{
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	private static CustomerService customerService = new CustomerServiceImpl();



	public static void main(String[] args) throws ParseException 
	{
		displayOptions();
	}

	private static void displayOptions() 
	{




		int array_length = 9;
		int current_option = 0;
		int options[] = new int[array_length];
		options[0] = 1; // Create customer
		options[1] = 2; // Add payment method









		int option = 0;
		String input = "";
		do 
		{



			option = options[current_option++]; // LAFHIS			



			{





				switchOptions(option, 1);










			}









			input = "y"; // LAFHIS 




		} while (input.equalsIgnoreCase("y") && current_option < array_length);







		String slicingVariable7 = tempSlicingVariable7;







		System.out.println("Slicing value is: " + slicingVariable7);

	}

















	static String tempSlicingVariable7 = "";








	private static void switchOptions(int option, long predefinedId) 
	{
		switch (option) 
		{
			case 1:
				Customer customer = null;
				try 
				{
					customer = populateCustInfo();
				} 
				catch (ParseException e) 
				{

				}
				Customer savedCustomer = customerService.createCustomer(customer);











				break;
			case 2:


				Customer checkedCustomer = customerService.findCustomer(predefinedId); // LAFHIS





				{
					List<PaymentMethod> paymentMethods = null;
					try 
					{
						paymentMethods = populatePaymentMethods(checkedCustomer);
					} 
					catch (ParseException e) 
					{

					}
					customerService.createPaymentMethods(paymentMethods);































				} 






				Customer customerWithFullData = customerService.findCustomerFullData(predefinedId); // LAFHIS

				{
					String tmp = customerWithFullData.printCustomerWithAllData(); // LAFHIS: intermediate line for slicing with this criterion




					{
						tempSlicingVariable7 = tmp;


					}













































					PaymentMethod paymentMethod = new PaymentMethod();














					try 
					{

						paymentMethod.setDateFrom(sdf.parse("2025/05/01")); // LAFHIS
					} 
					catch (ParseException e) 








					{




					}




















				} 



















		}
	}

















	private static List<PaymentMethod> populatePaymentMethods(Customer checkedCustomer) throws ParseException 
	{




		PaymentMethod pm = null;
		List<PaymentMethod> pmList = new ArrayList<>();


		{
			pm = new PaymentMethod();


			pm.setCardName("VISA"); // LAFHIS


			pm.setCardNumber("4509790112345678"); // LAFHIS


			pm.setCardType("CREDIT"); // LAFHIS


			pm.setDateFrom(sdf.parse("2022/10/22")); // LAFHIS
			pm.setCustomer(checkedCustomer);
			pmList.add(pm);


		}
		return pmList;
	}

	private static Customer populateCustInfo() throws ParseException 
	{

		Customer customer = new Customer();


		customer.setName("Gervasio"); // LAFHIS


		customer.setDateOfBirth(sdf.parse("1991/01/06")); // LAFHIS


		customer.setAnnualSalary(100000); // LAFHIS

		Address address = new Address();


		address.setStreet("Int. Guiraldes 2160"); // LAFHIS


		address.setCity("Buenos Aires"); // LAFHIS


		address.setState("Argentina"); // LAFHIS


		address.setZip("A1416C"); // LAFHIS
		customer.setAddress(address);
		address.setCustomer(customer);
		return customer;
	}
}
