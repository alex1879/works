package com.syed.entity;

import java.io.Serializable;
import java.util.Date;










public class PaymentMethod implements Serializable 
{

	private static final long serialVersionUID = -8536345472128992332L;




	public Long paymentId;


	public String cardNumber;


	private String cardName;


	private Date dateFrom;


	private String dateType;


	private String cardType;



	private Customer customer;

	public Long getPaymentId() 
	{
		return paymentId;





	}

	public String getCardNumber() 
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) 
	{
		this.cardNumber = cardNumber;
	}

	public String getCardName() 
	{
		return cardName;





	}

	public Date getDateFrom() 
	{
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) 
	{

	}

	public String getDateType() 
	{
		return dateType;
	}






	public String getCardType() 
	{
		return cardType;





	}

	public Customer getCustomer() 
	{
		return customer;





	}

	public static long getSerialversionuid() 
	{
		return serialVersionUID;
	}


	public String toString() 
	{
		return "PaymentMethod [paymentId=" + paymentId + ", cardNumber=" + cardNumber + ", cardName=" + cardName + ", dateFrom=" + dateFrom + ", cardType=" + cardType +  "]";
	}

}
