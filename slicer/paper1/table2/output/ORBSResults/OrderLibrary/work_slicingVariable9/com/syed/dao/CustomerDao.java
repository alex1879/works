package com.syed.dao;

import java.util.List;

import com.syed.entity.Customer;

public interface CustomerDao 
{
	Customer saveCustomer(Customer customer);


	Customer findCustomer(Long customerId);

	Customer findCustomerFullData(Long custId);






	List<Customer> getAllCustomers();
}

