package model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity

public class Client 
{

	@Id
	@GeneratedValue
	public Long id;


	private String lastname;


	private String firstname;


	@ManyToOne
	Book bookPref;


	@Enumerated(EnumType.STRING)
	public Gender gender;


	@ManyToMany	
	List<Book> books = new LinkedList<Book>();

	public Client() 
	{

	}

	public Client(String lastname, String firstname, Gender gender) 
	{




	}

	public Long getId() 
	{
		return id;
	}






	public String getLastname() 
	{
		return lastname;
	}






	public String getFirstname() 
	{
		return firstname;
	}






	public Gender getGender() 
	{
		return gender;
	}






	public Book getBookPref() 
	{
		return bookPref;
	}

	public void setBookPref(Book bookPref) 
	{
		this.bookPref = bookPref;
	}

	public List<Book> getBooks() 
	{
		return books;
	}







	public String toString() 
	{
		return "Client [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", bookPref=" + bookPref + ", gender=" + gender + "]";
	}
}

