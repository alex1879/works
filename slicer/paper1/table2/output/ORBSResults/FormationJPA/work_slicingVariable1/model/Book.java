package model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book 
{
	@Id
	@GeneratedValue
	public Long id;


	private String title;


	private String author;

	@OneToMany(mappedBy= "bookPref")
	List<Client> clientsHavePref;

	@ManyToMany(mappedBy= "books")	
	List<Client> clients;

	public Book() 
	{

	}

	public Book(String title, String author) 
	{



	}

	public String getTitle()
	{
		return title;
	}






	public String getAuthor() 
	{
		return author;
	}

	public Long getId() 
	{
		return id;
	}











	public List<Client> getClientsHavePref() 
	{
		return clientsHavePref;
	}






	public List<Client> getClients() 
	{
		return clients;
	}







	public String toString() 
	{
		return "Livre [id=" + id + ", title=" + title + ", author=" + author + "]";
	}

}
