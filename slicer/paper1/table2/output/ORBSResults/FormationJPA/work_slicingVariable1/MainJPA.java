import javax.persistence.EntityManager;

import DAO.DatabaseHelper;
import DAO.ServiceJPA;
import model.Book;
import model.Client;
import model.Gender;

public class MainJPA 
{

	public static void start() 
	{

        EntityManager em = DatabaseHelper.createEntityManager();       
        DatabaseHelper.beginTx(em);

        Client c = new Client("Uzumaki", "Naruto", Gender.M);
        Client c2 = new Client("Haruno", "Sakura", Gender.F);
        Client c3 = new Client("Uchiha", "Madara", Gender.M);
        Book b =  new Book("L'art de la guerre", "Sun Tzu") ;
        Book b2 = new Book("Le vieil homme et la mer", "Ernest Hemingway");
        Book b3 = new Book("Le Seigneur des Anneaux","JRR Tolkien");

        em.persist(b);
        em.persist(b2);
        em.persist(b3);
        em.persist(c);
        em.persist(c2);
        em.persist(c3);






        c.setBookPref(b);

        c3.setBookPref(b3);

        DatabaseHelper.commitTxAndClose(em);

        em = DatabaseHelper.createEntityManager();
        DatabaseHelper.beginTx(em);


        String t1 = ServiceJPA.displayBooksByCustomer(em, c);

        ServiceJPA.displayBooksByCustomer(em, c2);

        ServiceJPA.displayBooksByCustomer(em, c3);

        String t2 = ServiceJPA.displayCustomersByBook(em, b);

        ServiceJPA.displayCustomersByBook(em, b2);

        ServiceJPA.displayCustomersByBook(em, b3);

        String t3 = ServiceJPA.displayPurchasedBook(em);


        DatabaseHelper.commitTxAndClose(em);


        EntityManager slicingVariable1 = em;









System.out.println("Slicing value is: " + slicingVariable1);
	}
}
