package DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class DatabaseHelper 
{
	private static EntityManagerFactory entityManagerFactory;

	public static EntityManager createEntityManager() 
	{

		{
			entityManagerFactory = Persistence.createEntityManagerFactory("FormationJPA");
		}
		return entityManagerFactory.createEntityManager();
	}

	public static void commitTxAndClose(EntityManager entityManager) 
	{


	}

	public static void rollbackTxAndClose(EntityManager entityManager) 
	{


	}

	public static void beginTx(EntityManager entityManager) 
	{
		entityManager.getTransaction().begin();
	}
}

