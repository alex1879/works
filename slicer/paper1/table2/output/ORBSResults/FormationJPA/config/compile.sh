#!/bin/sh

# This file is part of the ORBS distribution.
# See the file LICENSE.TXT for more information.

# The compilation script.
#
# This script is called from ORBS to actually compile (build) the system.
#
# The script must return a generated signature if compilation was
# successful and must return "FAIL" if compilation was not
# successful. The signature should be, for example, an md5 hash over
# the generated binary.

# Configure ORBS for this project.
#source config/config.sh

cd work

# cleanup from previous run (just to make sure)
#rm -f reader checker.class
rm -f JPA.class MainJPA.class DAO/ServiceJPA.class DAO/DatabaseHelper.class model/Client.class model/Book.class model/Gender.class

# The actual compilation.  This can be replaced by any mechanism to
# build the system, e.g. by invoking make.

javac -cp ".:../repos/*" JPA.java MainJPA.java DAO/ServiceJPA.java DAO/DatabaseHelper.java model/Client.java model/Book.java model/Gender.java 2>> compile.log #Build
#gcc $CFLAGS -o reader reader.c 2> compile.log
#javac checker.java 2>> compile.log

if [ -f JPA.class -a -f MainJPA.class -a -f DAO/ServiceJPA.class -a -f DAO/DatabaseHelper.class -a -f model/Client.class -a -f model/Book.class -a -f model/Gender.class ]; then #check build
    echo `md5sum JPA.class` `md5sum MainJPA.class` `md5sum DAO/ServiceJPA.class` `md5sum DAO/DatabaseHelper.class` `md5sum model/Client.class` `md5sum model/Book.class` `md5sum model/Gender.class`
else
    echo FAIL
fi


