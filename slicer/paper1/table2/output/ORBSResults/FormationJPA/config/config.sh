
# This file is part of the ORBS distribution.  
# See the file LICENSE.TXT for more information.

# ORBS configuration for the EXAMPLE project.

# ORBS itself
#export ORBS_SCRIPT="../../orbs/orbs.py"
export ORBS_SCRIPT="orbs.py"

# The ORBS working space
export ORBS="$PWD"

# The list of files to be sliced
export ORBS_FILES="work/JPA.java work/MainJPA.java work/DAO/ServiceJPA.java work/DAO/DatabaseHelper.java work/model/Client.java work/model/Book.java work/model/Gender.java"


