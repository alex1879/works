package model;

import java.util.List;

import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity

public class Book 
{
	@Id

	public Long id;


	private String title;


	private String author;

	@OneToMany(mappedBy= "bookPref")
	List<Client> clientsHavePref;

	@ManyToMany(mappedBy= "books")	
	List<Client> clients;













	public String getTitle()
	{
		return title;
	}






	public String getAuthor() 
	{
		return author;
	}

	public Long getId() 
	{
		return id;
	}











	public List<Client> getClientsHavePref() 
	{
		return clientsHavePref;
	}






	public List<Client> getClients() 
	{
		return clients;
	}







	public String toString() 
	{
		return "Livre [id=" + id + ", title=" + title + ", author=" + author + "]";
	}

}
