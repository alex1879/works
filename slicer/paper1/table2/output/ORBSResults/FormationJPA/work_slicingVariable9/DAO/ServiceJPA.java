package DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import model.Book;
import model.Client;

public class ServiceJPA 
{

	public static String displayBooksByCustomer(EntityManager em, Client c) 
	{
		String returnValue = "";






		return returnValue;
	}

	public static String displayCustomersByBook(EntityManager em, Book b) 
	{
		String returnValue = "";






		return returnValue;
	}

	public static String displayPurchasedBook(EntityManager em) 
	{
		String returnValue = "";
		TypedQuery<Book> query = em.createQuery("SELECT b" + " FROM Book b" + " INNER JOIN b.clients", Book.class);
		List<Book> lb = query.getResultList();


		for (Book b : lb) 
		{

			returnValue = b.getTitle();
		}
		return returnValue;
	}

	public static void buyBook(EntityManager em, Client c, Book b)
	{

		List<Book> lb = c.getBooks();
		lb.add(b);
	}

}
