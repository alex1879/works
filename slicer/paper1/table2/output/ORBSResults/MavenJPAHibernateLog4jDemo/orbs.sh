#!/bin/bash

# This file is part of the ORBS distribution.
# See the file LICENSE.TXT for more information.

# Invoke ORBS.

rm time.log

date >> time.log

# Configure ORBS for this project.
. config/config.sh

# Pass the arguments to ORBS together with the list of files to be sliced.
python  "$ORBS_SCRIPT" $* $ORBS_FILES

# python orbs.py "/home/lafhis/ORBS/HibernateTest2" "work/JPA.java MainJPA.java DAO/ServiceJPA.java DAO/DatabaseHelper.java model/Client.java model/Book.java model/Gender.java"

date >> time.log
