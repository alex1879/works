package com.danielme.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class EntityB
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	@Column(nullable=false, length=50)
	public String name;


	@ManyToOne(optional=false)
	public EntityA entityA;


	public Long getId()
	{
		return id;
	}






	public String getName()
	{
		return name;





	}

	public EntityA getEntityA()
	{
		return entityA;





	}	

}
