#!/bin/sh

# This file is part of the ORBS distribution.
# See the file LICENSE.TXT for more information.

# The compilation script.
#
# This script is called from ORBS to actually compile (build) the system.
#
# The script must return a generated signature if compilation was
# successful and must return "FAIL" if compilation was not
# successful. The signature should be, for example, an md5 hash over
# the generated binary.

# Configure ORBS for this project.
#source config/config.sh

cd work

# cleanup from previous run (just to make sure)
#rm -f reader checker.class
rm -f com/danielme/demo/Main.class com/danielme/demo/model/EntityA.class com/danielme/demo/model/EntityB.class

# The actual compilation.  This can be replaced by any mechanism to
# build the system, e.g. by invoking make.

javac -cp ".:../repos/*" com/danielme/demo/Main.java com/danielme/demo/model/EntityA.java com/danielme/demo/model/EntityB.java 2>> compile.log #Build
#gcc $CFLAGS -o reader reader.c 2> compile.log
#javac checker.java 2>> compile.log

if [ -f com/danielme/demo/Main.class -a -f com/danielme/demo/model/EntityA.class -a -f com/danielme/demo/model/EntityB.class ]; then #check build
    echo `md5sum com/danielme/demo/Main.class` `md5sum com/danielme/demo/model/EntityA.class` `md5sum com/danielme/demo/model/EntityB.class`
else
    echo FAIL
fi




