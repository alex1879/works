
# This file is part of the ORBS distribution.  
# See the file LICENSE.TXT for more information.

# ORBS configuration for the EXAMPLE project.

# ORBS itself
#export ORBS_SCRIPT="../../orbs/orbs.py"
export ORBS_SCRIPT="orbs.py"

# The ORBS working space
export ORBS="$PWD"

# The list of files to be sliced
export ORBS_FILES="work/com/danielme/demo/Main.java work/com/danielme/demo/model/EntityA.java work/com/danielme/demo/model/EntityB.java"

