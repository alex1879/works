package com.danielme.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class EntityA
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	@Column(nullable=false, length=50)
	public String name;

	@OneToMany(cascade=CascadeType.ALL, mappedBy = "entityA")
	public List<EntityB> entities;


	public List<EntityB> getEntities()
	{
		return entities;
	}

	public void setEntities(List<EntityB> entities)
	{
		this.entities = entities;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Long getId()
	{
		return id;
	}






}
