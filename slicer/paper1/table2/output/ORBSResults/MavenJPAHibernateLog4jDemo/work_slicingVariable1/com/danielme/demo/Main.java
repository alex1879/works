package com.danielme.demo;

import java.util.LinkedList;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.danielme.demo.model.EntityA;
import com.danielme.demo.model.EntityB;

import org.apache.log4j.Logger;

public class Main
{	
	private static final Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args)
	{
		logger.info("demo begins...");

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence-unit_demo");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();

        EntityA entityA= new EntityA();
        entityA.setName("parent");

        EntityB entityB1  = new EntityB();

        entityB1.setEntityA(entityA);

        EntityB entityB2 = new EntityB();

        entityB2.setEntityA(entityA);

        entityA.setEntities(new LinkedList<EntityB>());
        entityA.getEntities().add(entityB1);
        entityA.getEntities().add(entityB2);        

        try
        {

        	entityManager.persist(entityA);
        	transaction.commit();

        	 logger.info("===================");
             logger.info(entityA.getId() + " " + entityA.getName()+ " : ");
             for (EntityB entityB : entityA.getEntities())
             {
             	logger.info("  " + entityB.getId() + " " + entityB.getName());
             }
        }
        catch (Exception ex)
        {


        }

        {
        	entityManager.close();
        } 

        EntityManager slicingVariable1 = entityManager;








        System.out.println("Slicing value is: " + slicingVariable1);
	}

}
