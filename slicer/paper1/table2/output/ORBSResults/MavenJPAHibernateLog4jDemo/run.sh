#!/bin/bash

for i in $(seq 1 1 9)
do
   echo "Running slicing variable $i"
   sh config/set_slicing_variable.sh $i
   sh orbs.sh
   mv *.log work/
   mv "work" "work_slicingVariable"$i
done
