using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExecuteWithTimeout
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = new string[] {
            //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\output\CSharpCompilation\BH\BH.exe",
            //    "-b 30 -s 2",
            //    "10"
            //};

            var executableFile = args[0];
            var arguments = args[1];
            // ORBS pone una "s" al final
            var timeout = int.Parse(args[2].Substring(0, args[2].Length - 1)) + 1;
            var ID = Guid.NewGuid().ToString();

            try { 
                log($"Executing {ID} {executableFile} {arguments} {timeout}");

                Console.WriteLine("1");
                log($"Finish {ID}");

                //var start_time = DateTime.Now;
                //using (var process = new Process
                //{
                //    StartInfo = {
                //        FileName = executableFile,
                //        Arguments = arguments,
                //        WindowStyle = ProcessWindowStyle.Hidden,
                //        RedirectStandardOutput = false,
                //        /*UseShellExecute = false,
                //        CreateNoWindow = true*/ }
                //})
                //{ 
                //    process.Start();
                //    try
                //    { 
                //        process.WaitForExit(timeout * 1000);
                //    }
                //    catch(Win32Exception ex)
                //    {
                //        log($"Win32Exception {ID}");
                //        throw;
                //    }
                //    finally
                //    {
                //        process.Close();
                //    }
                //}
                //Console.WriteLine("1");
                //log($"Finish {ID}");
            }
            catch(Exception ex)
            {
                log($"Exception {ID} {ex.Message}");
            }
            return;

            #region OLD
            //while (!process.StandardOutput.EndOfStream)
            //    Console.WriteLine(process.StandardOutput.ReadLine());

            //try
            //{
            //    var log_interval = 1;
            //    var log_counter = 0;
            //    DateTime last_log = start_time;

            //    while (DateTime.Now.Subtract(start_time).TotalSeconds < timeout && !process.HasExited) {
            //        if (DateTime.Now.Subtract(last_log).TotalSeconds >= log_interval)
            //        {
            //            log($"Logging {ID} {++log_counter}");
            //            last_log = DateTime.Now;
            //        }
            //    }
            //    // Lo intento matar si o si
            //    log($"Kill {ID}");
            //    process.Kill();
            //    //process.Close();
            //}
            //catch (Win32Exception ex1)
            //{
            //    log($"Win32Exception {ID}");
            //}
            //catch(InvalidOperationException ex2)
            //{
            //    log($"InvalidOperationException {ID}");
            //    log($"Output {ID}");
            //    if (process.HasExited)
            //        while (!process.StandardOutput.EndOfStream)
            //            Console.WriteLine(process.StandardOutput.ReadLine());
            //}
            //catch(Exception ex_def)
            //{
            //    log($"Exception {ID} {ex_def.Message}");
            //}
            //finally
            //{

            //}

            //log($"Finish {ID}");
            //return;
            #endregion
        }

        static void log(string text)
        {
            var log_name = @"C:\temp\log.txt";
            System.IO.File.AppendAllLines(log_name, new string[] { text });
        }
    }
}
