﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORBSDynAbsComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            var originalFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\src\FNHMVC_Modified";
            var slicedFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\src\FNHMVC_ORBS";
            var reducedFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\ORBS\regression\Sliced";
            var fullORBSFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\ORBS\regression\Full";

            var resultPath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\output\FNHMVC_NETCORE";
            var summaryResultPath = Path.Combine(resultPath, "summary.txt");
            var diffsInSlicePath = Path.Combine(resultPath, "orbs_diffs_inSlice.txt");
            var diffsOutSlicePath = Path.Combine(resultPath, "orbs_diffs_outSlice.txt");
            var equInSlicePath = Path.Combine(resultPath, "orbs_equ_inSlice.txt");
            var equOutSlicePath = Path.Combine(resultPath, "orbs_equ_outSlice.txt");
            
            var slicePath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\output\Results\FNHMVC_NETCORE\Results.txt";
            var executedPath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\output\Results\FNHMVC_NETCORE\ExecutedLines.txt";
            var executedPathFull = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\output\Results\FNHMVC_NETCORE\Full\ExecutedLines.txt";
            
            var slicedLines = GetLines(slicePath);
            var executedLines = GetLines(executedPath);
            var executedLinesFull = GetLines(executedPathFull);

            var diffsInSlice = new StringBuilder();
            var diffsOutSlice = new StringBuilder();
            var equInSlice = new StringBuilder();
            var equOutSlice = new StringBuilder();

            // Cantidad de líneas en el path original
            var totalOriginalProject = 0;
            var totalFullORBSProject = 0;
            // Cantidad de líneas ejecutadas
            var totalExecuted = 0;
            var totalExecutedFull = 0;
            // Cantidad de líneas sliceadas
            var totalSliced = 0;
            // Cantidad de líneas agregadas sobre el original hacia el sliceado
            var totalExtraLines = 0;
            // Cantidad de líneas en el path sliceado (en teoría sliceadas + agregadas)
            var totalSlicedProject = 0;
            // Cantidad de líneas en el path reducido (ORBS)
            var totalReducedProject = 0;

            // Contamos todas las líneas de todos los proyectos
            var processedOriginalProject = GetSolutionInfo(originalFolder);
            var processedSlicedProject = GetSolutionInfo(slicedFolder);
            var processedReducedProject = GetSolutionInfo(reducedFolder);
            var processedfullORBSProject = GetSolutionInfo(fullORBSFolder);

            // Computamos los valores
            totalOriginalProject = processedOriginalProject.Sum(x => x.Value.Count);
            totalExecuted = processedOriginalProject.Where(x => executedLines.ContainsKey(x.Key)).Sum(x => executedLines[x.Key].Intersect(x.Value.Keys).Count());
            totalExecutedFull = processedOriginalProject.Where(x => executedLinesFull.ContainsKey(x.Key)).Sum(x => executedLinesFull[x.Key].Intersect(x.Value.Keys).Count());
            totalSliced = processedOriginalProject.Where(x => slicedLines.ContainsKey(x.Key)).Sum(x => slicedLines[x.Key].Intersect(x.Value.Keys).Count());
            totalExtraLines = processedSlicedProject.Sum(x => x.Value.Count - (slicedLines.ContainsKey(x.Key) ? slicedLines[x.Key].Intersect(x.Value.Keys).Count() : 0));
            totalSlicedProject = processedSlicedProject.Sum(x => x.Value.Count);
            totalReducedProject = processedReducedProject.Sum(x => x.Value.Count);
            totalFullORBSProject = processedfullORBSProject.Sum(x => x.Value.Count);

            foreach (var fileLines in processedSlicedProject)
            {
                var slicedProjectValues = fileLines.Value;
                var cleanedProjectValues = processedReducedProject[fileLines.Key];

                foreach (var kv in slicedProjectValues)
                {
                    if (kv.Value.Length <= 2)
                        continue;

                    var inSlice = slicedLines.ContainsKey(fileLines.Key) && slicedLines[fileLines.Key].Contains(kv.Key);
                    var inCleaned = cleanedProjectValues.ContainsKey(kv.Key);
                    var l = string.Format("{0}:{1} - {2}", fileLines.Key, kv.Key, kv.Value);

                    if (inSlice && inCleaned)
                        equInSlice.AppendLine(l);
                    if (!inSlice && inCleaned)
                        equOutSlice.AppendLine(l);
                    if (inSlice && !inCleaned)
                        diffsInSlice.AppendLine(l);
                    if (!inSlice && !inCleaned)
                        diffsOutSlice.AppendLine(l);
                }
            }

            // GENERAL:
            // Cantidad de líneas en el path original
            // Cantidad de líneas ejecutadas
            // Cantidad de líneas sliceadas
            // Cantidad de líneas agregadas sobre el original hacia el sliceado
            // Cantidad de líneas en el path sliceado (en teoría sliceadas + agregadas)
            // Cantidad de líneas en el path reducido (ORBS)
            // Cantidad de líneas de ORBS sin haber pasado por nuestro slicer
            var summaryLine = $"{totalOriginalProject}|{totalExecuted}|{totalSliced}|{totalExtraLines}|{totalSlicedProject}|{totalReducedProject}|{totalFullORBSProject}";
            System.IO.File.WriteAllText(summaryResultPath, summaryLine);

            // COMPARACIÓN:
            // Líneas que están en ambos (slice y agregadas)
            System.IO.File.WriteAllText(equInSlicePath, equInSlice.ToString());
            System.IO.File.WriteAllText(equOutSlicePath, equOutSlice.ToString());
            // Líneas que ya no están en el reducido (slice y agregadas)
            System.IO.File.WriteAllText(diffsInSlicePath, diffsInSlice.ToString());
            System.IO.File.WriteAllText(diffsOutSlicePath, diffsOutSlice.ToString());
            
        }

        static Dictionary<int, string> Process(string file)
        {
            var result = new Dictionary<int, string>();
            var lines = System.IO.File.ReadAllLines(file);
            var entroNamespace = false;
            var entroClase = false;
            var entroMetodo = false;
            var contadorLlaves = 0;
            var hasNamespace = lines.Any(x => x.Contains("namespace "));
            var currentLineNumber = 0;
            foreach (var line in lines)
            {
                var tLine = line.Trim();
                
                currentLineNumber++;
                if (string.IsNullOrWhiteSpace(tLine))
                {
                    //sbNewLines.AppendLine(line);
                    continue;
                }

                if (!entroNamespace && hasNamespace)
                {
                    entroNamespace = tLine.Length == 1 && tLine[0] == '{';
                    //sbNewLines.AppendLine(line);
                }
                else if (!entroClase)
                {
                    entroClase = tLine.Length == 1 && tLine[0] == '{';
                    if (!entroClase && tLine.Length > 1 && tLine.Substring(0, 2) != "//" && !tLine.Contains("using"))
                    {
                        // Contamos las líneas de properties (incluye la cabecera del class)
                        //if (line.Contains(";"))
                        //{
                        //    if (loc.Contains(currentLineNumber))
                        //        sbNewLines.AppendLine(line);
                        //    else
                        //        sbNewLines.AppendLine("//" + line);
                        //}
                        //else
                        //    sbNewLines.AppendLine(line);
                    }

                    if (!entroClase && tLine.Length == 1 && tLine[0] == '}')
                    {
                        entroNamespace = false;
                    }
                }
                else if (!entroMetodo)
                {
                    entroMetodo = tLine.Length == 1 && tLine[0] == '{';
                    if (entroMetodo)
                    {
                        //sbNewLines.AppendLine(line);
                    }
                    else if (tLine.Length > 1 && tLine.Substring(0, 2) != "//" && (tLine.Contains(";") /*|| line.Contains(":")*/))
                    {
                        //if (loc.Contains(currentLineNumber))
                        //{
                        //    sbNewLines.AppendLine(line);
                        //}
                        //else
                        //{
                        //    sbNewLines.AppendLine("//" + line);
                        //}
                    }
                    else
                    {
                        //sbNewLines.AppendLine(line);
                    }
                    if (!entroMetodo && tLine.Length == 1 && tLine[0] == '}')
                        entroClase = false;
                }
                else if (entroClase && entroMetodo)
                {
                    var cantidadLlavesIn = tLine.Count(x => x == '{');
                    var cantidadLlavesOut = tLine.Count(x => x == '}');
                    contadorLlaves = contadorLlaves + cantidadLlavesIn - cantidadLlavesOut;
                    // Se salió de un método
                    if (contadorLlaves < 0)
                    {
                        entroMetodo = false;
                        contadorLlaves = 0;
                        //sbNewLines.AppendLine(line);
                    }
                    else
                    {
                        if (!(tLine.Length > 1 && tLine.Substring(0, 2) == "//"))
                        {
                            if (tLine.Length > 3 && tLine != "get" && tLine != "set")
                                result.Add(currentLineNumber, tLine);
                        }
                    }
                }
            }
            return result;
        }

        static Dictionary<string, HashSet<int>> GetLines(string resultPath)
        {
            var retDicc = new Dictionary<string, HashSet<int>>();
            var str = File.ReadAllLines(resultPath);
            foreach (var line in str)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    var values = line.Split(':');
                    if (values.Length < 2)
                        continue;
                    var fileName = values[0].Trim();
                    if (!retDicc.ContainsKey(fileName))
                        retDicc[fileName] = new HashSet<int>();
                    retDicc[fileName].Add(int.Parse(values[1].Trim()));
                }
            }
            return retDicc;
        }

        static Dictionary<string, Dictionary<int, string>> GetSolutionInfo(string folder)
        {
            var ProyectosExcluidos = new string[] { "CleanDB", "Tracer", "FNHMVC.Web" };
            var Result = new Dictionary<string, Dictionary<int, string>>();
            foreach (var file in System.IO.Directory.GetFiles(folder, "*.cs", System.IO.SearchOption.AllDirectories))
            {
                if (ProyectosExcluidos.Any(x => file.Contains(x)))
                    continue;

                if (file.Contains("Debug") || file.Contains("AssemblyInfo.cs"))
                    continue;

                var fileName = System.IO.Path.GetFileName(file);
                
                if (fileName.Contains("CustomConnectionString"))
                    continue;

                var resProc = Process(file);

                if (!Result.ContainsKey(fileName))
                    Result.Add(fileName, resProc);
                else
                    foreach (var kv in resProc)
                        Result[fileName].Add(kv.Key, kv.Value);
            }
            return Result;
        }
    }
}
