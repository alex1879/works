﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis.Sarif;
using Newtonsoft.Json;
using ResultsProcessor;

namespace CodeQLResultProcessor
{
    public static class CodeQLQueries
    {
        public class SourceCodeLocation
        {
            public string Path;
            public int Line;
            public SourceCodeLocation(string path, int line)
            {
                Path = path;
                Line = line;
            }

            public override bool Equals(object obj)
            {
                return Path == ((SourceCodeLocation)obj).Path && Line == ((SourceCodeLocation)obj).Line;
            }

            public override int GetHashCode()
            {
                return Path.GetHashCode() * Line;
            }
        }

        public class AnalysisResults
        {
            public bool Guard = false;
            public bool Taint = false;
        }

        public static string MainResultsFolder = "";
        public static string MainResultsQueries = "";
        public static string DatabaseName = "";
        public static string BaseDataBaseFolder = @"C:\Users\alexd\Desktop\Slicer\CodeQL\codeql-home\databases\";
        public static string DynAbsResultsMainFolder = "C:\\Users\\alexd\\Desktop\\Slicer\\Olden\\output\\DynAbsResults\\Clusters";
        //public static string BaseDataBaseFolder = @"C:\Users\lafhis\Desktop\Utils\databases\";
        public static int TimeoutMax = 600;

        public static void Execute(string fileTo, int lineTo, string resultPath, bool recursively = true)
        {
            var doneTaint = new HashSet<SourceCodeLocation>();
            var pendingTaint = new HashSet<SourceCodeLocation>();
            var to = new SourceCodeLocation(fileTo, lineTo);
            pendingTaint.Add(to);

            var maxSimultaneously = 10;

            if (recursively)
            {
                while (pendingTaint.Count > 0)
                {
                    var toAnalyze = new HashSet<SourceCodeLocation>(pendingTaint.Count >= maxSimultaneously ? pendingTaint.Take(maxSimultaneously) : pendingTaint);
                    foreach (var toRemove in toAnalyze)
                        pendingTaint.Remove(toRemove);
                    var resultTainting = PerformDefaultTrackingQuery(toAnalyze);
                    doneTaint.UnionWith(toAnalyze);
                    pendingTaint.UnionWith(resultTainting.Except(doneTaint));
                }
            }
            else
            {
                doneTaint.UnionWith(PerformDefaultTrackingQuery(new HashSet<SourceCodeLocation>() { to }));
            }

            SaveResults(resultPath, doneTaint);
        }

        public static void ExecuteWithGuardQuery(string fileFrom, int lineFrom, string fileTo, int lineTo, string resultPath)
        {
            var doneTaint = new HashSet<SourceCodeLocation>();
            var doneGuard = new HashSet<SourceCodeLocation>();
            var pendingTaint = new HashSet<SourceCodeLocation>();
            var pendingGuard = new HashSet<SourceCodeLocation>();
            var from = new SourceCodeLocation(fileFrom, lineFrom);
            var to = new SourceCodeLocation(fileTo, lineTo);
            pendingTaint.Add(to);
            pendingGuard.Add(to);

            while (pendingTaint.Count > 0)
            {
                var toAnalyze = pendingTaint.First();
                pendingTaint.Remove(toAnalyze);

                var resultTainting = PerformTaintTrackingQuery(from, toAnalyze);
                doneTaint.UnionWith(resultTainting);
                pendingGuard.UnionWith(resultTainting.Except(doneGuard));

                while (pendingGuard.Count > 0)
                {
                    toAnalyze = pendingGuard.First();
                    pendingGuard.Remove(toAnalyze);

                    var resultGuard = PerformGuardQuery(toAnalyze);

                    doneGuard.Add(toAnalyze);

                    pendingTaint.UnionWith(resultGuard.Except(doneTaint));
                    pendingGuard.UnionWith(resultGuard.Except(doneGuard));
                }
            }

            // Deberían ser el mismo
            if (!doneTaint.Equals(doneGuard))
                ;

            SaveResults(resultPath, doneTaint);
        }

        public static void ExecuteOlden()
        {
            var OldenPrograms = new Dictionary<string, List<int>>();
            //OldenPrograms.Add("BH", new List<int>() { 84, 85, 86, 87, 88, 89 });
            OldenPrograms.Add("BiSort", new List<int>() { 86, 87, 88 });

            //OldenPrograms.Add("Em3d", new List<int>() { 74, 75, 76, 77, 78 });
            //OldenPrograms.Add("Health", new List<int>() { 78, 79, 80 });
            //OldenPrograms.Add("MST", new List<int>() { 56 });

            //OldenPrograms.Add("Perimeter", new List<int>() { 73, 74 });
            //OldenPrograms.Add("Power", new List<int>() { 74, 75, 76, 77 });
            //OldenPrograms.Add("TreeAdd", new List<int>() { 52 });
            //OldenPrograms.Add("TSP", new List<int>() { 60, 61, 62, 63, 64 });
            //OldenPrograms.Add("Voronoi", new List<int>() { 72, 73, 74, 75, 76, 77 });

            var UseRecursiveAlgorithm = false;
            var initialMainResultsFolder = MainResultsFolder;
            var initialMainResultsQueries = MainResultsQueries;

            foreach (var oldenProgram in OldenPrograms)
            {
                DatabaseName = oldenProgram.Key + "QL";

                if (!Directory.Exists($"C:\\Users\\alexd\\Desktop\\Slicer\\CodeQL\\codeql-home\\databases\\{DatabaseName}"))
                { 
                    var createDBQuery = $"codeql database create \"C:/Users/alexd/Desktop/Slicer/CodeQL/codeql-home/databases/{DatabaseName}\" --language=\"csharp\" --source-root=\"C:/Users/lafhis/Desktop/Utils/src/Olden/{oldenProgram.Key}\" --command='dotnet build /t:rebuild'";
                    bool timeout = false;
                    RunCommand(createDBQuery, out timeout);
                    if (timeout)
                        continue;
                }

                var localDynAbsFolder = System.IO.Directory.GetDirectories(DynAbsResultsMainFolder).Where(x => x.Contains(oldenProgram.Key)).First();
                var results = ResultsProcessor.Program.ProcessOLDNETSlicerResults(localDynAbsFolder, null);
                var executedLines = results.Last().executedLines;
                var execLinesConditions = Utils.GetCondition(executedLines);

                var i = 1;
                foreach (var line in oldenProgram.Value)
                {
                    var finalResults = Path.Combine(initialMainResultsFolder, oldenProgram.Key, i.ToString(), $"query_{oldenProgram.Key}_{i}.txt");
                    if (UseRecursiveAlgorithm)
                    {
                        CodeQLQueries.MainResultsFolder = Path.Combine(initialMainResultsFolder, oldenProgram.Key, i.ToString());
                        CodeQLQueries.MainResultsQueries = Path.Combine(initialMainResultsQueries, oldenProgram.Key);
                        CodeQLQueries.Execute(oldenProgram.Key + ".cs", line, finalResults);
                    }
                    else
                    {
                        //var query = GetCompleteQuery(new HashSet<SourceCodeLocation>() { new SourceCodeLocation(oldenProgram.Key + ".cs", line) });
                        //var query = GetQuery2022(new SourceCodeLocation(oldenProgram.Key + ".cs", line), execLinesConditions);
                        var query = GetQuery2022_OptMinimalBien(new FileLine(oldenProgram.Key + ".cs", line), executedLines);
                        var fileName = Path.Combine(MainResultsQueries, oldenProgram.Key, $"query_{oldenProgram.Key}_{i}.ql");
                        var fileResults = Path.Combine(MainResultsFolder, oldenProgram.Key, i.ToString(), $"query_{oldenProgram.Key}_{i}.bqrs");
                        var codeQlResult = PerformQuery(query, fileName, fileResults, false);
                        SaveResults(finalResults, codeQlResult);
                    }
                    i++;
                }
            }
        }

        public static int defaultItNumber = 1;
        public static ISet<SourceCodeLocation> PerformDefaultTrackingQuery(HashSet<SourceCodeLocation> to)
        {
            var taintQuery = GetCompleteQuery(to);
            var fileName = Path.Combine(MainResultsQueries, "defaultQ" + defaultItNumber + ".ql");
            var fileResults = Path.Combine(MainResultsFolder, "ResDefaultQ.res");
            defaultItNumber++;
            return PerformQuery(taintQuery, fileName, fileResults, true);
        }


        public static int taintItNumber = 1;
        public static ISet<SourceCodeLocation> PerformTaintTrackingQuery(SourceCodeLocation from, SourceCodeLocation to)
        {
            var taintQuery = GetTaintTrackingQuery(from.Path, from.Line, 0, to.Path, to.Line, 0);
            var fileName = Path.Combine(MainResultsQueries, "taintQ" + taintItNumber + ".ql");
            var fileResults = Path.Combine(MainResultsFolder, "taintQ" + taintItNumber + ".res");
            taintItNumber++;
            return PerformQuery(taintQuery, fileName, fileResults, false);
        }

        public static int guardItNumber = 1;
        public static ISet<SourceCodeLocation> PerformGuardQuery(SourceCodeLocation location)
        {
            var guardQuery = GetControlDependenciesQuery(location.Path, location.Line, 0);
            var fileName = Path.Combine(MainResultsQueries, "guardQ" + guardItNumber + ".ql");
            var fileResults = Path.Combine(MainResultsFolder, "guardQ" + guardItNumber + ".res");
            guardItNumber++;
            return PerformQuery(guardQuery, fileName, fileResults, true);
        }

        public static ISet<SourceCodeLocation> PerformQuery(string query, string fileName, string fileResults, bool manualParse)
        {
            var execStmt = GetExecuteQuery2022(BaseDataBaseFolder + DatabaseName, fileName, fileResults);

            // Guardo el archivo
            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            Directory.CreateDirectory(Path.GetDirectoryName(fileResults));

            File.WriteAllText(fileName, query);
            bool hasTimeout;

            Console.WriteLine($"Start {Path.GetFileName(fileName)} at {DateTime.Now.ToString("hh:mm")}");
            RunCommand(execStmt, out hasTimeout);
            if (hasTimeout)
                ;

            Console.WriteLine($"End {Path.GetFileName(fileName)} at {DateTime.Now.ToString("hh:mm")}, Timeout: {hasTimeout}");

            var dic = ReadSimpleQuery(fileResults); //manualParse ? ManualResToDictionary(fileResults) : ResToDictionary(fileResults);
            var returnVal = new HashSet<SourceCodeLocation>();
            foreach (var key in dic.Keys)
                foreach (var l in dic[key])
                    returnVal.Add(new SourceCodeLocation(key, l));
            return returnVal;
        }

        public static void RunCommand(string command, out bool timeout)
        {
            timeout = false;
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            Process p = Process.Start(processInfo);
            StringBuilder sb = new StringBuilder();
            while (!p.StandardOutput.EndOfStream)
            {
                sb.AppendLine(p.StandardOutput.ReadLine());
            }

            //if (timeoutMilliseconds.HasValue)
            if (!p.WaitForExit(TimeoutMax))
            {
                p.Kill();
                timeout = true;
            }

            //return (timeout, p.TotalProcessorTime);
        }

        public static void SaveResults(string resultPath, ISet<SourceCodeLocation> results)
        {
            var sb = new StringBuilder();
            foreach (var res in results.OrderBy(x => x.Path).ThenBy(y => y.Line))
                sb.AppendLine($"{res.Path}:{res.Line}");
            File.WriteAllText(resultPath, sb.ToString());
        }

        public static string GetExecuteQuery(string databasePath, string codeQlPath, string resultPath)
        {
            return $"codeql database analyze \"{databasePath}\" \"{codeQlPath}\" --format=sarifv2.1.0 --output=\"{resultPath}\" --max-paths=100 --rerun";
        }

        public static string GetExecuteQuery2022(string databasePath, string codeQlPath, string resultPath)
        {
            return $"codeql query run --database=\"{databasePath}\" --output=\"{resultPath}\" \"{codeQlPath}\" ";
        }

        public static string GetTaintTrackingQuery(string fileFrom, int lineFrom, int columnFrom, string fileTo, int lineTo, int columnTo)
        {
            return string.Format(@"
/**
 * @name Static slicing
 * @description A simple Taint Traking query
 * @id cs/static-slicing
 * @problem.severity error
 * @precision very-high
 * @tags security
 * @kind path-problem
 */

import csharp
import semmle.code.csharp.security.dataflow.flowsources.Remote
import semmle.code.csharp.commons.Util
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.ExprOrStmtParent

class TaintTrackingConfiguration extends TaintTracking::Configuration {{
  TaintTrackingConfiguration() {{ this = ""LafhisStaticSlice_TQ"" }}

        override predicate isSource(DataFlow::Node source)
        {{
            any()

            //source.asExpr().getLocation().getFile().getAbsolutePath().indexOf(""{0}"") > 0
            //and source.asExpr().getLocation().getStartLine() = {1}
        }}

        override predicate isSink(DataFlow::Node sink)
        {{
            sink.asExpr().getLocation().getFile().getAbsolutePath().indexOf(""{2}"") > 0
            and sink.asExpr().getLocation().getStartLine() = {3}
        }}
}}

from TaintTrackingConfiguration c, DataFlow::PathNode source, DataFlow::PathNode sink
where c.hasFlowPath(source, sink)
select sink.getNode(), source, sink, ""Msj1"", source.getNode(), ""Msj2""
            ", fileFrom, lineFrom, fileTo, lineTo);
        }

        public static string GetControlDependenciesQuery(string fileFrom, int lineFrom, int columnFrom)
        {
            return string.Format(@"
/**
 * @name Guard problem
 * @description A simple description
 * @kind problem
 * @problem.severity error
 * @precision very-high
 * @id cs/condition
 * @tags maintainability
 */

import csharp
import semmle.code.csharp.commons.Assertions
import semmle.code.csharp.commons.Constants
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.ExprOrStmtParent

from DataFlow::Node pred, DataFlow::Node succ
where exists (Guard g, BasicBlock b, AbstractValue v | 
  g.getAControlFlowNode() = pred.getControlFlowNode() and
  b.getANode() = succ.getControlFlowNode() and
  g.controlsBasicBlock(b, v)
) 
and succ.getLocation().getFile().getAbsolutePath().indexOf(""{0}"") > 0
and succ.getLocation().getStartLine() = {1}
select pred, ""Msj""", fileFrom, lineFrom);
        }

        public static string GetCompleteQuery(HashSet<SourceCodeLocation> locations)
        {
            var baseQuery = @"
/**
 * @name Static slicing
 * @description An attempt to perform a static slice
 * @id cs/static-slicing
 * @problem.severity error
 * @precision very-high
 * @tags security
 * @kind path-problem
 */

import csharp
import semmle.code.csharp.security.dataflow.flowsources.Remote
import semmle.code.csharp.commons.Util
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.ExprOrStmtParent

/**
 * kind path-problem
 * A taint-tracking configuration as a baseline.
 */
class TaintTrackingConfiguration extends TaintTracking::Configuration {{
  TaintTrackingConfiguration() {{ this = ""LafhisStaticSlice_General"" }}

  override predicate isSource(DataFlow::Node source) {{
    any()
  }}

  override predicate isSink(DataFlow::Node sink) {{
     {0}
  }}

  override predicate isAdditionalTaintStep(DataFlow::Node pred, DataFlow::Node succ) {{
    exists (Guard g, BasicBlock b, AbstractValue v | 
        g.getAControlFlowNode() = pred.getControlFlowNode() and
        b.getANode() = succ.getControlFlowNode() and
        g.controlsBasicBlock(b, v)
      )
  }}
}}

from TaintTrackingConfiguration c, DataFlow::PathNode source, DataFlow::PathNode sink
where c.hasFlowPath(source, sink)
select sink.getNode(), source, sink, ""Msj1"", source.getNode(), ""Msj2""";

            var baseCondition = @"
                    (sink.asExpr().getLocation().getFile().getAbsolutePath().indexOf(""{0}"") > 0
	                and sink.asExpr().getLocation().getStartLine() = {1})";
            var conditions = "";
            foreach (var location in locations)
                conditions = conditions + (string.Format(baseCondition + " or ", location.Path, location.Line));
            conditions = conditions.Substring(0, conditions.Length - 4);

            return string.Format(baseQuery, conditions);
        }

        public static string GetQuery2022(SourceCodeLocation location, string execLinesCondition)
        {
            var baseQuery = @"
import semmle.code.csharp.dataflow.DataFlow
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph

class TargetFileName extends string {
  TargetFileName() { this = ""{0}"" }
}

class TargetLineNumber extends int {
  TargetLineNumber() { this = {1} }
}

class GlobalTestConfiguration extends DataFlow::Configuration {
  TargetFileName targetFileName;
  TargetLineNumber targetLineNumber;

  GlobalTestConfiguration() { this = ""GlobalTestConfiguration"" }

  string getTargetFileName() { result = targetFileName }
  int getTargetLineNumber() { result = targetLineNumber }

  override predicate isSource(DataFlow::Node source) {
    1 = 1
  }

  override predicate isSink(DataFlow::Node sink) {
    sink.getLocation().getFile().getAbsolutePath().indexOf(targetFileName) > 1
    and sink.getLocation().getStartLine() = targetLineNumber
  }

  override predicate isAdditionalFlowStep(DataFlow::Node pred, DataFlow::Node succ) {
    EXEC_LINES_COND
    and 
    // Conditions
    ((pred.getControlFlowNode().isCondition()
    and pred.getControlFlowNode().strictlyDominates(succ.getControlFlowNode()))
    // Callers
    or 
      ((Call)(pred.asExpr().getParent())).getTarget() = succ.getEnclosingCallable()
    or
      ((Expr)pred.asExpr()).getEnclosingStmt() = ((Expr)succ.asExpr()).getEnclosingStmt())
  }
}

from Assignment assignment, DataFlow::Node src, GlobalTestConfiguration config
where assignment.getLocation().getStartLine() = config.getTargetLineNumber()
  and assignment.getLocation().getFile().getAbsolutePath().indexOf(config.getTargetFileName()) > 1
  and config.hasFlow(src, DataFlow::exprNode(assignment.getRValue()))
  and src.asExpr().fromSource()
select src.getLocation().getFile().getAbsolutePath() + "": "" + src.getLocation().getStartLine().toString()
";

            return baseQuery.Replace("{0}", location.Path).Replace("{1}", location.Line.ToString()).Replace("EXEC_LINES_COND", execLinesCondition);
        }

        public static string GetQuery2022_Long(FileLine location, List<FileLine> executed)
        {
            var baseQuery = @"
import semmle.code.csharp.dataflow.DataFlow
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph

class TargetFileName extends string {
  TargetFileName() { this = ""{0}"" }
}

class TargetLineNumber extends int {
  TargetLineNumber() { this = {1} }
}

class GlobalTestConfiguration extends DataFlow::Configuration {
  TargetFileName targetFileName;
  TargetLineNumber targetLineNumber;

  GlobalTestConfiguration() { this = ""GlobalTestConfiguration"" }

  string getTargetFileName() { result = targetFileName }
  int getTargetLineNumber() { result = targetLineNumber }

  override predicate isSource(DataFlow::Node source) {
    1 = 1
  }

  override predicate isSink(DataFlow::Node sink) {
    sink.getLocation().getFile().getAbsolutePath().indexOf(targetFileName) > 1
    and sink.getLocation().getStartLine() = targetLineNumber
  }

  override predicate isAdditionalFlowStep(DataFlow::Node pred, DataFlow::Node succ) {
    EXEC_LINES_COND
    and 
    // Conditions
    ((pred.getControlFlowNode().isCondition()
    and pred.getControlFlowNode().strictlyDominates(succ.getControlFlowNode()))
    // Callers
    or 
      ((Call)(pred.asExpr().getParent())).getTarget() = succ.getEnclosingCallable()
    or
      ((Expr)pred.asExpr()).getEnclosingStmt() = ((Expr)succ.asExpr()).getEnclosingStmt())
  }
}

from Call call, DataFlow::Node src, GlobalTestConfiguration config
where call.getLocation().getStartLine() = config.getTargetLineNumber()
  and call.getLocation().getFile().getAbsolutePath().indexOf(config.getTargetFileName()) > 1
  and EXEC_LINES_FROM
  and 
  ((call.getNumberOfArguments() = 1 and config.hasFlow(src, DataFlow::exprNode(call.getArgument(0))))
  or
  (call.getNumberOfArguments() = 2 and config.hasFlow(src, DataFlow::exprNode(call.getArgument(1)))))
  and src.asExpr().fromSource()
select src.getLocation().getFile().getAbsolutePath() + "": "" + src.getLocation().getStartLine().toString()
";

            var execLinesCondition = Utils.GetCondition(executed);
            var execLinesFrom = Utils.GetQuery(executed);

            return baseQuery
                .Replace("{0}", Path.GetFileName(location.File))
                .Replace("{1}", location.Line.ToString())
                .Replace("EXEC_LINES_COND", execLinesCondition)
                .Replace("EXEC_LINES_FROM", execLinesFrom);
        }

        public static string GetQuery2022_Opt(FileLine location, List<FileLine> executed)
        {
            var baseQuery = @"
import semmle.code.csharp.dataflow.DataFlow
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph

class GlobalTestConfiguration extends DataFlow::Configuration {

  GlobalTestConfiguration() { this = ""GlobalTestConfiguration"" }

  override predicate isSource(DataFlow::Node source) {
    1 = 1
  }

  override predicate isSink(DataFlow::Node sink) {
    sink.getLocation().getFile().getBaseName() = ""{0}""
    and sink.getLocation().getStartLine() = {1}
  }

  override predicate isAdditionalFlowStep(DataFlow::Node pred, DataFlow::Node succ) {
    succ.getLocation().getFile().getBaseName() in [EXEC_LINES]
    and 
    (pred.getControlFlowNode().isCondition()
    and pred.getControlFlowNode().strictlyDominates(succ.getControlFlowNode()))
  }
}

from Call call, int argNum, DataFlow::Node src, GlobalTestConfiguration config
where call.getLocation().getStartLine() = {1}
  and call.getLocation().getFile().getBaseName() = ""{0}""
  and src.getLocation().getFile().getBaseName() in [EXEC_LINES]
  and argNum >= 0 and argNum < call.getNumberOfArguments()
  and config.hasFlow(src, DataFlow::exprNode(call.getArgument(argNum)))
  and src.asExpr().fromSource()
select src.getLocation().getFile().getAbsolutePath() + "": "" + src.getLocation().getStartLine().toString()
";
            var getFileNamesexecLinesCondition = Utils.GetFileNames(executed);
            
            return baseQuery
                .Replace("{0}", Path.GetFileName(location.File))
                .Replace("{1}", location.Line.ToString())
                .Replace("EXEC_LINES", getFileNamesexecLinesCondition);
        }

        public static string GetQuery2022_NewConditions(FileLine location, List<FileLine> executed)
        {
            var baseQuery = @"
import semmle.code.csharp.dataflow.DataFlow
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph

class GlobalTestConfiguration extends DataFlow::Configuration {

  GlobalTestConfiguration() { this = ""GlobalTestConfiguration"" }

  override predicate isSource(DataFlow::Node source) {
    1 = 1
  }

  override predicate isSink(DataFlow::Node sink) {
    sink.getLocation().getFile().getBaseName() = ""{0}""
    and sink.getLocation().getStartLine() = {1}
  }

  override predicate isAdditionalFlowStep(DataFlow::Node pred, DataFlow::Node succ) {
    // Fijate solo ejecutadas:
    succ.getLocation().getFile().getBaseName() in [EXEC_LINES]
    and
    pred.getLocation().getFile().getBaseName() in [EXEC_LINES]
    and
    (
      // Intra control flow (pred y succ del mismo método)
      (
        pred.getEnclosingCallable() = succ.getEnclosingCallable() and        
        ((((IfStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getATrueSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode() or
        (((IfStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getAFalseSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode() or
        (((WhileStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getATrueSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode() or
        (((ForStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getATrueSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode() or
        (((ForeachStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getATrueSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode() or
        (((DoStmt)((Expr)pred.asExpr()).getEnclosingStmt()).getCondition()).getAControlFlowNode().getATrueSuccessor() = succ.getControlFlowNode().getBasicBlock().getANode())
      )

      // Interprocedural:
      or 
      ((Call)(pred.asExpr().getParent())).getTarget() = succ.getEnclosingCallable()
      // or ((Expr)pred.asExpr()).getEnclosingStmt() = ((Expr)succ.asExpr()).getEnclosingStmt()
    )
  }

}

from Expr expr, DataFlow::Node src, GlobalTestConfiguration config
where 
  src.getLocation().getFile().getBaseName() in [EXEC_LINES]
  and src.asExpr().fromSource()
  and config.hasFlow(src, DataFlow::exprNode(expr))
select src.getLocation().getFile().getBaseName() + "": "" + src.getLocation().getStartLine().toString()
";
            var getFileNamesexecLinesCondition = Utils.GetFileNames(executed);

            return baseQuery
                .Replace("{0}", Path.GetFileName(location.File))
                .Replace("{1}", location.Line.ToString())
                .Replace("EXEC_LINES", getFileNamesexecLinesCondition);
        }

        public static string GetQuery2022_OptMinimalBien(FileLine location, List<FileLine> executed)
        {
            var baseQuery = @"
import semmle.code.csharp.dataflow.DataFlow
import semmle.code.csharp.controlflow.Guards
import semmle.code.csharp.controlflow.BasicBlocks
import semmle.code.csharp.dataflow.DataFlow::DataFlow::PathGraph
import Ssa

class GlobalTestCConfiguration extends TaintTracking::Configuration {
  GlobalTestCConfiguration() { this = ""GlobalTestCConfiguration"" }

  override predicate isSink(DataFlow::Node sink) {
    sink.getLocation().getFile().getBaseName() = ""{0}""
    and sink.getLocation().getStartLine() = {1}
  }

  override predicate isSource(DataFlow::Node source) {
    1 = 1
  }

  override predicate isAdditionalTaintStep(DataFlow::Node pred, DataFlow::Node succ) {
    // Solo ejecutadas:
    succ.getLocation().getFile().getBaseName() in [EXEC_LINES]
        and
    pred.getLocation().getFile().getBaseName() in [EXEC_LINES]
    and (
        // Operaciones aritméticas
        (succ.asExpr().(ArithmeticOperation).getAnOperand() = pred.asExpr())
    
        or
        // Condicionales por control
        (
            pred.getEnclosingCallable() = succ.getEnclosingCallable() and
            (pred.asExpr().(Expr).getEnclosingStmt().(IfStmt).getThen() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(IfStmt).getThen() = succ.asExpr().(Expr).getEnclosingStmt() or
            pred.asExpr().(Expr).getEnclosingStmt().(IfStmt).getElse() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(IfStmt).getElse() = succ.asExpr().(Expr).getEnclosingStmt() or
            pred.asExpr().(Expr).getEnclosingStmt().(WhileStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(WhileStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt() or
            pred.asExpr().(Expr).getEnclosingStmt().(ForStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(ForStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt() or
            pred.asExpr().(Expr).getEnclosingStmt().(ForeachStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(ForeachStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt() or
            pred.asExpr().(Expr).getEnclosingStmt().(DoStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(CaseStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt().getParent() or
            pred.asExpr().(Expr).getEnclosingStmt().(CaseStmt).getBody() = succ.asExpr().(Expr).getEnclosingStmt() or
            succ.asExpr().(Expr).getEnclosingStmt().(CaseStmt) = pred.asExpr().(Expr).getEnclosingStmt().(SwitchStmt).getACase() or
            pred.asExpr().(ConditionalExpr).getThen() = succ.asExpr().(Expr) or
            pred.asExpr().(ConditionalExpr).getElse() = succ.asExpr().(Expr))
        )

        or
        // Agrega los usos de los base en los member access
        ((MemberAccess)succ.asExpr()) = pred.asExpr().getParent()
    )
  }
}

from DataFlow::Node source, DataFlow::Node sink, GlobalTestCConfiguration config
where 
      sink.getLocation().getFile().getBaseName() = ""{0}""
  and sink.getLocation().getStartLine() = {1}
  and source.getLocation().getFile().getBaseName() in [EXEC_LINES]
  and source.asExpr().fromSource()
  and config.hasFlow(source, sink)
select source.getLocation().getFile().getBaseName() + "": "" + source.getLocation().getStartLine().toString()
";
            var getFileNamesexecLinesCondition = Utils.GetFileNames(executed);

            return baseQuery
                .Replace("{0}", Path.GetFileName(location.File))
                .Replace("{1}", location.Line.ToString())
                .Replace("EXEC_LINES", getFileNamesexecLinesCondition);
        }

        public static Dictionary<string, ISet<int>> ResToDictionary(string inputPath)
        {
            string logText = File.ReadAllText(inputPath);
            SarifLog log = JsonConvert.DeserializeObject<SarifLog>(logText);

            var dic = new Dictionary<string, ISet<int>>();
            foreach (var run in log.Runs)
            {
                if (run.Results != null)
                    foreach (var result in run.Results)
                        if (result.CodeFlows != null)
                            foreach (var codeFlow in result.CodeFlows)
                                if (codeFlow.ThreadFlows != null)
                                    foreach (var threadFlow in codeFlow.ThreadFlows)
                                        if (threadFlow != null && threadFlow.Locations != null)
                                            foreach (var location in threadFlow.Locations)
                                            {
                                                var pysicalLocation = location.Location.PhysicalLocation;
                                                if (pysicalLocation.Region != null)
                                                {
                                                    var fileName = pysicalLocation.ArtifactLocation.Uri.GetFileName();
                                                    var line = pysicalLocation.Region.StartLine;

                                                    if (!dic.ContainsKey(fileName))
                                                        dic[fileName] = new HashSet<int>();
                                                    var set = dic[fileName];
                                                    set.Add(line);
                                                }
                                            }
            }

            return dic;
        }

        public static Dictionary<string, ISet<int>> ManualResToDictionary(string inputPath)
        {
            var started = false;
            StringBuilder internalBuffer = new StringBuilder();
            var dic = new Dictionary<string, ISet<int>>();
            var linesAdded = 0;
            using (StreamReader sw = new StreamReader(inputPath))
            {
                string line = null;
                while ((line = sw.ReadLine()) != null)
                {
                    if (!started)
                    {
                        if (line.Contains("\"physicalLocation\""))
                        {
                            started = true;
                        }
                    }
                    else if (line.Trim() != "}")
                    {
                        internalBuffer.AppendLine(line);
                        linesAdded++;

                        if (linesAdded > 20)
                            ;
                    }
                    else
                    {
                        internalBuffer.AppendLine(line);
                        linesAdded++;

                        var str = internalBuffer.ToString().Trim();
                        str = "{" + str + "}";

                        var art = JsonConvert.DeserializeObject<CustomPysicalLocation>(str);

                        if (art.artifactLocation != null && art.region != null)
                        {
                            var fileName = art.artifactLocation.uri;
                            var fileLine = art.region.startLine;
                            if (!dic.ContainsKey(fileName))
                                dic[fileName] = new HashSet<int>();
                            var set = dic[fileName];
                            set.Add(fileLine);
                        }
                        started = false;
                        linesAdded = 0;
                        internalBuffer = internalBuffer.Clear();
                    }
                }
            }

            return dic;
        }

        public static Dictionary<string, ISet<int>> ReadSimpleQuery(string inputPath)
        {
            Dictionary<string, ISet<int>> result = new Dictionary<string, ISet<int>>();

            string logText = File.ReadAllText(inputPath);

            string pattern = "([A-Za-z0-9_]*).cs: ([0-9]*)";
            var matches = Regex.Matches(logText, pattern);
            foreach (var match in matches)
            {
                var m = (Match)match;
                if (m.Groups.Count > 2)
                { 
                    var file = m.Groups[1] + ".cs";
                    var line = int.Parse(m.Groups[2].ToString());
                    if (!result.ContainsKey(file))
                        result.Add(file, new HashSet<int>());

                    result[file].Add(line);
                    Console.WriteLine(match);
                }
            }

            return result;
        }
    }
}
