﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ResultsProcessor;

namespace CodeQLResultProcessor
{
    public class ProgramPS
    {
        static void Main()
        {
            CodeQLQueries.DatabaseName = "powershell";
            var BaseQueriesFolder = @"C:\Users\alexd\Desktop\Slicer\CodeQL\codeql-home\vscode-codeql-starter\codeql-custom-queries-csharp\experiments\";
            var BasePowershellResults = @"C:\Users\alexd\Desktop\Slicer\CodeQL\query-results\Powershell";
            var SummaryPath = Path.Combine(BasePowershellResults, "Summary.txt");
            var BaseFolderResults = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\full";
            var BaseFolderConfig = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\tests";

            var ExecutedExperiments = new Dictionary<string, ISet<int>>();

            var results = PowershellUnitTest.FillSubjects(BaseFolderResults);
            foreach (var group in results)
            {
                var groupName = group.Key;

                //if (groupName != "FileSystemProviderTests")
                //    continue;

                foreach (var subject in group.Value.Subjects)
                {
                    var subjectName = subject.Key;

                    var i = 1;
                    foreach (var criterion in subject.Value.Criteria.Where(x => x.criterion.Iteration == 1))
                    {
                        try
                        {
                            if (!ExecutedExperiments.ContainsKey(criterion.criterion.FileLine.File))
                                ExecutedExperiments[criterion.criterion.FileLine.File] = new HashSet<int>();

                            var added = ExecutedExperiments[criterion.criterion.FileLine.File].Add(criterion.criterion.FileLine.Line);
                            if (!added)
                                continue;

                            var query = CodeQLQueries.GetQuery2022_OptMinimalBien(criterion.criterion.FileLine, criterion.executedLines);

                            var queryDirectory = Path.Combine(BaseQueriesFolder, groupName, subjectName);
                            var resultDirectory = Path.Combine(BasePowershellResults, groupName, subjectName, i.ToString());
                            if (!Directory.Exists(resultDirectory))
                                Directory.CreateDirectory(resultDirectory);

                            var fileName = Path.Combine(queryDirectory, $"query_{subjectName}_{i}.ql");
                            var fileResults = Path.Combine(resultDirectory, $"query_{subjectName}_{i}.bqrs");
                            var codeQlResult = CodeQLQueries.PerformQuery(query, fileName, fileResults, false);
                            var finalResults = Path.Combine(resultDirectory, $"query_{subjectName}_{i}.txt");
                            CodeQLQueries.SaveResults(finalResults, codeQlResult);

                            var codeQLLoadedResults = ResultsProcessor.Program.ParseOLDResultFile(File.ReadAllLines(finalResults).ToList()).ToList().First();

                            // Comparación
                            var onlyDynAbsPath = Path.Combine(resultDirectory, $"analysis_onlyDynAbs.txt");
                            var onlyCodeQLPath = Path.Combine(resultDirectory, $"analysis_onlyCodeQL.txt");
                            var onlyCodeQLExecPath = Path.Combine(resultDirectory, $"analysis_onlyCodeQL_exec.txt");
                            var bothPath = Path.Combine(resultDirectory, $"analysis_bothAnalysis.txt");

                            var onlyDynAbs = new List<FileLine>();
                            var onlyCodeQL = new List<FileLine>();

                            var onlyDA = ProgramV2.Substract(criterion.FilteredResult, codeQLLoadedResults);
                            ProgramV2.SaveResults(onlyDynAbsPath, onlyDA);
                            var onlyQL = ProgramV2.Substract(codeQLLoadedResults, criterion.FilteredResult);
                            ProgramV2.SaveResults(onlyCodeQLPath, onlyQL);
                            var intersection = ProgramV2.Intersect(codeQLLoadedResults, criterion.FilteredResult);
                            ProgramV2.SaveResults(bothPath, intersection);

                            // Executed intersection
                            var onlyQLExec = ProgramV2.Intersect(onlyQL, criterion.executedLines);
                            ProgramV2.SaveResults(onlyCodeQLExecPath, onlyQLExec);

                            // All CodeQL intersection executed
                            var allQLExec = ProgramV2.Intersect(codeQLLoadedResults, criterion.executedLines);

                            var targetFileName = Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File);
                            File.AppendAllLines(SummaryPath, new string[] {
                                $"{groupName};{subjectName};{targetFileName};{criterion.criterion.FileLine.Line};{criterion.executedLines.Count};{criterion.FilteredResult.Count};{allQLExec.Count};{codeQLLoadedResults.Count};{onlyDA.Count};{intersection.Count};{onlyQL.Count};{onlyQLExec.Count}"
                            });
                        }
                        catch (Exception ex)
                        {

                        }
                        i++;
                    }
                }
            }
        }
    }
}
