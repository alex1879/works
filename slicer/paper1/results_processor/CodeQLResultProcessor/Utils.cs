﻿using ResultsProcessor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CodeQLResultProcessor.CodeQLQueries;

namespace CodeQLResultProcessor
{
    public static class Utils
    {
        public static string GetCondition(List<ResultsProcessor.FileLine> lines)
        {
            var sb = new StringBuilder();

            sb.AppendLine("(");
            // PRED
            sb.AppendLine("(");
            foreach (var line in lines)
            {
                var fileName = Path.GetFileName(line.File);
                sb.AppendLine("(");
                sb.AppendLine($"pred.getLocation().getFile().getAbsolutePath().indexOf(\"{fileName}\") > 1");
                sb.AppendLine($"and pred.getLocation().getStartLine() = {line.Line}");
                sb.AppendLine(") or");
            }
            sb.AppendLine(" 1=0");
            sb.AppendLine(")");
            // SUCC
            sb.AppendLine("and");
            sb.AppendLine("(");
            foreach (var line in lines)
            {
                var fileName = Path.GetFileName(line.File);
                sb.AppendLine("(");
                sb.AppendLine($"succ.getLocation().getFile().getAbsolutePath().indexOf(\"{fileName}\") > 1");
                sb.AppendLine($"and succ.getLocation().getStartLine() = {line.Line}");
                sb.AppendLine(") or");
            }
            sb.AppendLine(" 1=0");
            sb.AppendLine(")");
            // Global ending
            sb.AppendLine(")");

            return sb.ToString();
        }

        public static string GetQuery(List<ResultsProcessor.FileLine> lines)
        {
            var sb = new StringBuilder();

            sb.AppendLine("(");
            foreach (var line in lines)
            {
                var fileName = Path.GetFileName(line.File);
                sb.AppendLine("(");
                sb.AppendLine($"src.getLocation().getStartLine() = {line.Line}");
                sb.AppendLine($"and src.getLocation().getFile().getAbsolutePath().indexOf(\"{fileName}\") > 1");
                sb.AppendLine(") or");
            }
            sb.AppendLine(" 1=0");
            sb.AppendLine(")");

            return sb.ToString();
        }

        public static string GetFileNames(List<ResultsProcessor.FileLine> lines)
        {
            return string.Join(", ", lines.Select(x => "\"" + x.File + "\"").Distinct());
        }
    }
}
