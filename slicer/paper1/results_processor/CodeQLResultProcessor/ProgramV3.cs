﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeQLResultProcessor
{
    public class ProgramV3
    {
        public static void Main(string[] args)
        {
            var executedLinesForUserPath = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\full\RunspaceTests\TestRunspaceWithPipeline\ExecutedLinesForUser.txt";
            var parsedLines = ResultsProcessor.Program.ParseExecutedLinesFile(File.ReadAllLines(executedLinesForUserPath));
            //var queryParsedLines = Utils.GetQuery(parsedLines);

            //var DynAbsResultsFolder = "C:\\Users\\alexd\\Desktop\\Slicer\\Olden\\output\\DynAbsResults\\Clusters";
            //var program = "BH -b 2 -s 2";
            //var localDynAbsFolder = System.IO.Directory.GetDirectories(DynAbsResultsFolder).Where(x => x.Contains(program)).First();
            //var results = ResultsProcessor.Program.ProcessOLDNETSlicerResults(localDynAbsFolder, null);
            //var query = Utils.GetCondition(results.First().executedLines);

            var temp = string.Join(", ", parsedLines.Select(x => "\"" + x.File + "\"").Distinct());

            return;
        }
    }
}
