﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Sarif;
using Newtonsoft.Json;

namespace CodeQLResultProcessor
{
    public class Program
    {
		static void Main()
        {
            //MainProcessResults();
			MainOlden();
		}

		static void MainProcessResults()
        {
			// Powershell y común
			//var MainFolder = @"C:\Users\lafhis\Desktop\Utils\results\";
			//var MainFileNameWithoutExtension = "MainQuery_Powershell";
			//var MainResultsFolder = MainFolder + MainFileNameWithoutExtension + "\\";
			//System.IO.Directory.CreateDirectory(MainResultsFolder);

			var BaseMainFolder = @"C:\Users\lafhis\Desktop\Utils\results\Olden\";
			var SlicerBasePath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table1\output\DynAbsResults\Clusters";
			var JavaslicerBasePath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table1\output\JavaSlicerResults\";

			var OldenPrograms = new Dictionary<string, List<int>>();
            OldenPrograms.Add("BH", new List<int>() { 84, 85, 86, 87, 88, 89 });
			OldenPrograms.Add("BiSort", new List<int>() { 86, 87, 88 });
			OldenPrograms.Add("Em3d", new List<int>() { 74, 75, 76, 77, 78 });
			OldenPrograms.Add("Health", new List<int>() { 78, 79, 80 });
			OldenPrograms.Add("MST", new List<int>() { 56 });
			OldenPrograms.Add("Perimeter", new List<int>() { 73, 74 });
			OldenPrograms.Add("Power", new List<int>() { 74, 75, 76, 77 });
			OldenPrograms.Add("TreeAdd", new List<int>() { 52 });
			OldenPrograms.Add("TSP", new List<int>() { 60, 61, 62, 63, 64 });
			OldenPrograms.Add("Voronoi", new List<int>() { 72, 73, 74, 75, 76, 77 });

			var sb = new StringBuilder();
			var SummaryPath = BaseMainFolder + "Summary.txt";

			foreach(var oldenProgram in OldenPrograms)
			{
				var i = 1;
				var BaseOldenProgram = BaseMainFolder + oldenProgram.Key + "\\";
				foreach (var line in oldenProgram.Value)
				{
					var MainFolder = BaseOldenProgram + i.ToString() + "\\";
					var MainFileNameWithoutExtension = "taintQ_" + oldenProgram.Key + "_" + i.ToString();
					var MainResultsFolder = MainFolder;
					var SourceFile = MainFolder + "ResDefaultQ.res";
					//var SourceFile = MainFolder + MainFileNameWithoutExtension + ".res";
					var CleanedOutputFile = MainResultsFolder + MainFileNameWithoutExtension + "_Cleaned.txt";
					var OutputFile = MainResultsFolder + MainFileNameWithoutExtension + ".txt";
					var IntersectedOutputFile = MainResultsFolder + MainFileNameWithoutExtension + "_Intersected.txt";
					var JavaslicerPath = Path.Combine(JavaslicerBasePath, oldenProgram.Key, i + ".result");

					bool ResToTxt = false;
					bool TxtToSlicerTxt = false;
					bool CompareWith = true;
					bool FullProcessing = false;

					if (ResToTxt)
					{
						var dict = CodeQLQueries.ResToDictionary(SourceFile);
						WriteTo(dict, OutputFile);
					}

					if (TxtToSlicerTxt)
						RemoveFullPath(OutputFile, CleanedOutputFile);

					var SlicerDir = Directory.GetDirectories(SlicerBasePath).Where(x => x.Contains(oldenProgram.Key)).Single();
					var SlicerPath = Path.Combine(SlicerDir, "FilteredResults.txt");
					var ExecutedPath = Path.Combine(SlicerDir, "ExecutedLines.txt");

					var dictExec = parseFile(ExecutedPath);
					var dynAbsResults = SplitDynAbsResults(SlicerPath);
					var dictSlicer = parseFile(dynAbsResults[i - 1].ToArray());
					var dictCodeQL = parseFile(OutputFile); // parseFile(CleanedOutputFile);
					var dictJavaSlicer = parseFile(JavaslicerPath);

					if (CompareWith)
					{
						// Sin intersecar, DynAbs VS QL
						var pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_COMUN.txt";
						var pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_SOLO_SLICER.txt";
						var pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_SOLO_CODEQL.txt";
						CompararDiccionarios(dictSlicer, dictCodeQL, pathEnComun, pathSoloA, pathSoloB);
						
						var dictIntersec = intersect(dictCodeQL, dictExec);
						WriteTo(dictIntersec, IntersectedOutputFile);

						// Intersecado, DynAbs VS QL
						pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_INTER_COMUN.txt";
						pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_INTER_SOLO_SLICER.txt";
						pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_INTER_SOLO_CODEQL.txt";
						CompararDiccionarios(dictSlicer, dictIntersec, pathEnComun, pathSoloA, pathSoloB);

						// JAVAZONE (Item1 = Wrong (No están en JS), Item2 = OK (Están en JS)), Item3 = No están
						var dictIntersectQLJava = splitDicts(dictIntersec, dictJavaSlicer);
						var dictIntersectDynAbsJava = splitDicts(dictSlicer, dictJavaSlicer);

						// Guardado de archivos (solo para tenerlos generados)
						// DynAbs VS JS
						pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_DYNABS_VS_JS_COMUN.txt";
						pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_DYNABS_VS_JS_SOLO_DYNABS.txt";
						pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_DYNABS_VS_JS_SOLO_JS.txt";
						CompararDiccionarios(dictSlicer, dictJavaSlicer, pathEnComun, pathSoloA, pathSoloB);

						// CodeQL intersecado VS JS
						pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_CODEQL_VS_JS_COMUN.txt";
						pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_CODEQL_VS_JS_SOLO_CODEQL.txt";
						pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_CODEQL_VS_JS_SOLO_JS.txt";
						CompararDiccionarios(dictIntersec, dictJavaSlicer, pathEnComun, pathSoloA, pathSoloB);

						// Programa|Criterio|Ejecutadas|TOTALES(JS|CodeQL|DynAbs)|
						// Diferencias: Todo se va a comparar contra JS, porque es lo que tenemos.
						// Orden: En común, de más, de menos
						// CodeQL VS JS: |{CountLines(dictIntersectQLJava.Item2)}|{CountLines(dictIntersectQLJava.Item1)}|{CountLines(dictIntersectQLJava.Item3)}
						// DynAbs VS JS: |{CountLines(dictIntersectDynAbsJava.Item2)}|{CountLines(dictIntersectDynAbsJava.Item1)}|{CountLines(dictIntersectDynAbsJava.Item3)}

						var totalJS = CountLines(dictJavaSlicer);
						var totalDynAbs = CountLines(dictSlicer);
						var totalCodeQL = CountLines(dictIntersec);

						var lineToAdd = $"{oldenProgram.Key}|{i}|{CountLines(dictExec)}|{totalJS}|{totalCodeQL}|{totalDynAbs}|";
						lineToAdd = lineToAdd + $"{CountLines(dictIntersectQLJava.Item2)}|{CountLines(dictIntersectQLJava.Item1)}|{CountLines(dictIntersectQLJava.Item3)}|";
						lineToAdd = lineToAdd + $"{CountLines(dictIntersectDynAbsJava.Item2)}|{CountLines(dictIntersectDynAbsJava.Item1)}|{CountLines(dictIntersectDynAbsJava.Item3)}";

						// Primer versión
						//sb.AppendLine($"{oldenProgram.Key}|{i}|{CountLines(dictExec)}|{CountLines(dictJavaSlicer)}|{CountLines(dictIntersectQLJava.Item3)}||{CountLines(dictIntersectQLJava.Item2)}||{CountLines(dictIntersectQLJava.Item1)}||{CountLines(dictIntersectDynAbsJava.Item3)}||{CountLines(dictIntersectDynAbsJava.Item2)}||{CountLines(dictIntersectDynAbsJava.Item1)}");
						sb.AppendLine(lineToAdd);


						pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_WRONG_COMUN.txt";
						pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_WRONG_SOLO_SLICER.txt";
						pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_WRONG_SOLO_CODEQL.txt";
						CompararDiccionarios(dictIntersectDynAbsJava.Item1, dictIntersectQLJava.Item1, pathEnComun, pathSoloA, pathSoloB);

						pathEnComun = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_OK_COMUN.txt";
						pathSoloA = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_OK_SOLO_SLICER.txt";
						pathSoloB = MainResultsFolder + MainFileNameWithoutExtension + "_JAVA_OK_SOLO_CODEQL.txt";
						CompararDiccionarios(dictIntersectDynAbsJava.Item2, dictIntersectQLJava.Item2, pathEnComun, pathSoloA, pathSoloB);
					}

					if (FullProcessing)
					{
						//var basePath = @"C:\Users\lafhis\Desktop\Slicer\Powershell\Powershell2020\";
						// Para powershell descomentar
						//involvedFiles = GetFiles(OutputFile, basePath);
						//RemoveFullPath(OutputFile, CleanedOutputFile);
						//CompararConSlicer("", SlicerPath, 1, CleanedOutputFile, pathEnComun, pathSoloSlicer, pathSoloCodeQL, GetExtraInfo);
					}
					i++;
				}
			}

			System.IO.File.AppendAllText(SummaryPath, sb.ToString());
		}
		
		static List<List<string>> SplitDynAbsResults(string dynAbsResultPath)
        {
			var result = new List<List<string>>();
			var allLines = File.ReadAllLines(dynAbsResultPath);

			var current = new List<string>();
			foreach(var line in allLines)
            {
				if (string.IsNullOrWhiteSpace(line))
					break;

				if (line != "---------------------")
					current.Add(line);
				else
                {
					result.Add(current);
					current = new List<string>();
                }
			}

			return result;
        }

		static IDictionary<string, List<string>> involvedFiles = null;
		static string GetExtraInfo(string file, int line)
		{
			if (involvedFiles == null)
				return "";

			var key = involvedFiles.Keys.FirstOrDefault(x => Path.GetFileName(x) == file);
			if (string.IsNullOrWhiteSpace(key))
				return "";

			var textLine = involvedFiles[key][line - 1];

			if (textLine.Contains("private ConsoleHostUserInterface parent = null;"))
				;

			if (isPropertyOrFieldInit(textLine))
				return " - P";
			else if (isPlusPlus(textLine))
				return " - PL";
			return "";
		}

		static bool isPlusPlus(string line)
        {
			var split = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			return split.Length == 1 && split[0].Contains("++");
		}

		static bool isPropertyOrFieldInit(string line)
        {
			if (line.Contains("{ get;"))
				return true;

			var split = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			// I expect: public/private const/static type name = val;
			// I'll assume that val does not contain empty spaces
			var minLen = 3;
			var maxLen = 6;
			if (split.Length < minLen || split.Length > maxLen)
				return false;

			var basicCommands = new string[] { "return", "if", "while", "lock", "do" };

			// has or not initialization
			if (split[split.Length - 2] == "=")
			{
				if (split.Length <= 3)
					return false;

				if (split.Length >= 3)
					return split[0] == "public" || split[0] == "private" || split[0] == "const" || split[0] == "static";

				// Por ahora...
				return true;
			}
			else
            {
				if (split.Length > 4)
					return false;

				if (split.Length >= 3)
					return split[0] == "public" || split[0] == "private" || split[0] == "const" || split[0] == "static";
				if (split.Length == 2)
					// Por ahora
					if (!basicCommands.Contains(split[0]))
						return true;
					else
						return false;
			}
			// No debería llegar acá
			return false;
        }

		static IDictionary<string, List<string>> GetFiles(string resultPath, string basePath)
		{
			var result = new Dictionary<string, List<string>>();
			var allLines = File.ReadAllLines(resultPath);
			foreach (var line in allLines.Where(x => !string.IsNullOrWhiteSpace(x)))
			{
				var spLine = line.Split(':');
				if (!result.ContainsKey(spLine[0]))
					result[spLine[0]] = new List<string>(File.ReadAllLines(Path.Combine(basePath, spLine[0])));
			}
			return result;
		}

		static void MainOlden()
        {
			var ProjectIdentifier = "Olden";

			var BaseResultsFolder = @"C:\Users\alexd\Desktop\Slicer\CodeQL\query-results\";
			var BaseQueriesFolder = @"C:\Users\alexd\Desktop\Slicer\CodeQL\codeql-home\vscode-codeql-starter\codeql-custom-queries-csharp\experiments\";
			var MainFolderResults = Path.Combine(BaseResultsFolder, ProjectIdentifier);
			var MainFolderQueries = Path.Combine(BaseQueriesFolder, ProjectIdentifier);
			System.IO.Directory.CreateDirectory(MainFolderResults);
			System.IO.Directory.CreateDirectory(MainFolderQueries);

			var MainResultPath = Path.Combine(MainFolderResults, ProjectIdentifier + ".txt");
			var DatabaseName = "Example2";
			var FileFrom = "Program.cs";
			var LineFrom = 7;
			var FileTo = "Program.cs";
			var LineTo = 12;

			CodeQLQueries.MainResultsFolder = MainFolderResults;
			CodeQLQueries.MainResultsQueries = MainFolderQueries;
			//CodeQLQueries.DatabaseName = DatabaseName;
			//CodeQLQueries.Execute(FileTo, LineTo, MainResultPath, true);
			CodeQLQueries.ExecuteOlden();
		}

		static void CompararDiccionarios(Dictionary<string, ISet<int>> dictA, Dictionary<string, ISet<int>> dictB, string pathEnComun, string pathSoloA, string pathSoloB, Func<string, int, string> extraInfo = null)
        {
			var dictEnComun = new Dictionary<string, ISet<int>>();
			var dictSoloA = new Dictionary<string, ISet<int>>();
			var dictSoloB = new Dictionary<string, ISet<int>>();

			foreach (var kv in dictA)
				foreach (var v in kv.Value)
					if (dictB.ContainsKey(kv.Key) && dictB[kv.Key].Contains(v))
						Add(dictEnComun, kv.Key, v);
					else
						Add(dictSoloA, kv.Key, v);

			foreach (var kv in dictB)
				foreach (var v in kv.Value)
					if (dictA.ContainsKey(kv.Key) && dictA[kv.Key].Contains(v))
						Add(dictEnComun, kv.Key, v);
					else
						Add(dictSoloB, kv.Key, v);

			WriteTo(dictEnComun, pathEnComun, extraInfo);
			WriteTo(dictSoloA, pathSoloA, extraInfo);
			WriteTo(dictSoloB, pathSoloB, extraInfo);
		}

		static void Add(Dictionary<string, ISet<int>> dict, string s, int i)
        {
			if (!dict.ContainsKey(s))
				dict[s] = new HashSet<int>();
			dict[s].Add(i);
		}

		static void WriteTo(Dictionary<string, ISet<int>> dic, string path, Func<string, int, string> extraInfo = null)
		{
			var sb = new StringBuilder();
			foreach (var fileName in dic.Keys.OrderBy(x => x))
				foreach (var line in dic[fileName].OrderBy(x => x))
				{
					string extra = "";
					if (extraInfo != null)
						extra = extraInfo(fileName, line);
					sb.AppendLine($"{fileName}:{line}" + extra);
				}
			File.WriteAllText(path, sb.ToString());
		}

		static Dictionary<string, ISet<int>> parseFile(string filePath)
        {
			var allLines = File.ReadAllLines(filePath);
			return parseFile(allLines);
        }

		static Dictionary<string, ISet<int>> parseFile(string[] allLines)
        {
			var result = new Dictionary<string, ISet<int>>();
			foreach (var line in allLines.Where(x => x.Contains(":")))
			{
				var spLine = line.Split(':');
				var fileName = spLine[0].Replace(".java", ".cs");
				if (!result.ContainsKey(fileName))
					result.Add(fileName, new HashSet<int>());
				var set = result[fileName];
				set.Add(int.Parse(spLine[1]));
			}
			return result;
		}

		static Dictionary<string, ISet<int>> intersect(Dictionary<string, ISet<int>> prev, Dictionary<string, ISet<int>> exec)
        {
			var ret = new Dictionary<string, ISet<int>>();

			foreach (var kv in prev)
            {
				if (exec.ContainsKey(kv.Key))
					ret[kv.Key] = new HashSet<int>();
				else
					continue;

				// Intersect es más directo pero me gusta verlo así porque pintó
				foreach (var l in kv.Value)
					if (exec[kv.Key].Contains(l))
						ret[kv.Key].Add(l);
            }

			return ret;
		}

		static Tuple<Dictionary<string, ISet<int>>, Dictionary<string, ISet<int>>, Dictionary<string, ISet<int>>> splitDicts(Dictionary<string, ISet<int>> prev, Dictionary<string, ISet<int>> ok)
        {
			// En t1 las que quedan, en t2 las OK, en t3 las que no están
			var t = new Tuple<Dictionary<string, ISet<int>>,
					Dictionary<string, ISet<int>>,
					Dictionary<string, ISet<int>>>(
				new Dictionary<string, ISet<int>>(),
				new Dictionary<string, ISet<int>>(),
				new Dictionary<string, ISet<int>>());

			foreach (var kv in prev)
				foreach (var l in kv.Value)
                {
					ISet<int> setToAdd = null;
					if (ok.ContainsKey(kv.Key) && ok[kv.Key].Contains(l))
                    {
						if (!t.Item2.ContainsKey(kv.Key))
							t.Item2[kv.Key] = new HashSet<int>();
						setToAdd = t.Item2[kv.Key];
					}
					else
                    {
						if (!t.Item1.ContainsKey(kv.Key))
							t.Item1[kv.Key] = new HashSet<int>();
						setToAdd = t.Item1[kv.Key];
					}
					setToAdd.Add(l);
                }

			foreach (var kv in ok)
				foreach (var l in kv.Value)
					if (!prev.ContainsKey(kv.Key) || !prev[kv.Key].Contains(l))
                    {
						if (!t.Item3.ContainsKey(kv.Key))
							t.Item3[kv.Key] = new HashSet<int>();
						t.Item3[kv.Key].Add(l);
					}
			
			return t;
		}

		static int CountLines (Dictionary<string, ISet<int>> dict)
        {
			return dict.SelectMany(x => x.Value).Count();
        }

		static void RemoveFullPath(string input, string output)
        {
			var codeQLAllLines = File.ReadAllLines(input);
			var sb = new StringBuilder();
			foreach(var codeQLLine in codeQLAllLines.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
				var spLine = codeQLLine.Split(':');
				var fileName = Path.GetFileName(spLine[0]);
				sb.AppendLine(fileName + ":" + spLine[1]);
            }
			File.WriteAllText(output, sb.ToString());
		}

        static void ResToSlicerFormat(string inputPath, string outputPath)
        {
            //var inputPath = @"C:\Users\lafhis\Desktop\Utils\results\custom_dominance.res";
            //var outputPath = @"C:\Users\lafhis\Desktop\Utils\results\custom_dominance.txt";

            string logText = File.ReadAllText(inputPath);
            SarifLog log = JsonConvert.DeserializeObject<SarifLog>(logText);

            var dic = new Dictionary<string, ISet<int>>();
			foreach (var run in log.Runs)
			{
				if (run.Results != null)
					foreach (var result in run.Results)
						if (result.CodeFlows != null)
							foreach (var codeFlow in result.CodeFlows)
								if (codeFlow.ThreadFlows != null)
									foreach (var threadFlow in codeFlow.ThreadFlows)
										if (threadFlow != null && threadFlow.Locations != null)
											foreach (var location in threadFlow.Locations)
											{
												var pysicalLocation = location.Location.PhysicalLocation;
												if (pysicalLocation.Region != null)
												{ 
													var fileName = pysicalLocation.ArtifactLocation.Uri.GetFileName();
													var line = pysicalLocation.Region.StartLine;

													if (!dic.ContainsKey(fileName))
														dic[fileName] = new HashSet<int>();
													var set = dic[fileName];
													set.Add(line);
												}
											}
			}

			var sb = new StringBuilder();
			foreach (var fileName in dic.Keys.OrderBy(x => x))
				foreach (var line in dic[fileName].OrderBy(x => x))
					sb.AppendLine($"{fileName}:{line}");
			File.WriteAllText(outputPath, sb.ToString());
		}
	}

	public class CustomPysicalLocation
	{
		public CustomArtifactLocation artifactLocation { get; set; }
		public CustomRegion region { get; set; }
	}
	public class CustomArtifactLocation
	{
		public string uri { get; set; }
		public string uriBaseId { get; set; }
		public int index { get; set; }
	}
	public class CustomRegion
	{
		public int startLine { get; set; }
		public int startColumn { get; set; }
		public int endColumn { get; set; }
	}

	// FORMATO:
	/*
	 "location" : {
  "physicalLocation" : {
	"artifactLocation" : {
	  "uri" : "src/System.Management.Automation/engine/MshCmdlet.cs",
	  "uriBaseId" : "%SRCROOT%",
	  "index" : 19
	},
	"region" : {
	  "startLine" : 321,
	  "startColumn" : 26,
	  "endColumn" : 134
	}
  },
  "message" : {
	"text" : "call to method LookupCommandInfo : CommandInfo"
  }
}
	 */

}
