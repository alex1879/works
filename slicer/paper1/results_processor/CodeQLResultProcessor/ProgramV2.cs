﻿using ResultsProcessor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CodeQLResultProcessor.CodeQLQueries;

namespace CodeQLResultProcessor
{
    class ProgramV2
    {
        static void Main()
        {
            var mainResultPath = "C:\\Users\\alexd\\Desktop\\Slicer\\CodeQL\\query-results\\Olden\\summary.txt";
            var CodeQLMainResultsFolder = "C:\\Users\\alexd\\Desktop\\Slicer\\CodeQL\\query-results\\Olden";
            var DynAbsResultsFolder = "C:\\Users\\alexd\\Desktop\\Slicer\\Olden\\output\\DynAbsResults\\Clusters";

            var OldenPrograms = new Dictionary<string, List<int>>();
            //OldenPrograms.Add("BH", new List<int>() { 84, 85, 86, 87, 88, 89 });
            OldenPrograms.Add("BiSort", new List<int>() { 86, 87, 88 });
            //OldenPrograms.Add("Em3d", new List<int>() { 74, 75, 76, 77, 78 });
            //OldenPrograms.Add("Health", new List<int>() { 78, 79, 80 });
            //OldenPrograms.Add("MST", new List<int>() { 56 });
            //OldenPrograms.Add("Perimeter", new List<int>() { 73, 74 });
            //OldenPrograms.Add("Power", new List<int>() { 74, 75, 76, 77 });
            //OldenPrograms.Add("TreeAdd", new List<int>() { 52 });
            //OldenPrograms.Add("TSP", new List<int>() { 60, 61, 62, 63, 64 });
            //OldenPrograms.Add("Voronoi", new List<int>() { 72, 73, 74, 75, 76, 77 });

            var summaryStr = new StringBuilder();
            foreach (var program in OldenPrograms)
            {
                var localDynAbsFolder = System.IO.Directory.GetDirectories(DynAbsResultsFolder).Where(x => x.Contains(program.Key)).First();
                var results = ResultsProcessor.Program.ProcessOLDNETSlicerResults(localDynAbsFolder, null);

                var codeQLList = new List<List<FileLine>>();
                for (var i = 1; i <= program.Value.Count; i++)
                {
                    var localQLResults = Path.Combine(CodeQLMainResultsFolder, program.Key, i.ToString(), $"query_{program.Key}_{i}.txt");
                    var codeQLResults = ResultsProcessor.Program.ParseOLDResultFile(File.ReadAllLines(localQLResults).ToList()).ToList();
                    codeQLList.Add(codeQLResults.First());
                }

                for (var i = 1; i <= program.Value.Count; i++)
                {
                    var localQLResultsBasePath = Path.Combine(CodeQLMainResultsFolder, program.Key, i.ToString());

                    var onlyDynAbsPath = Path.Combine(localQLResultsBasePath, $"analysis_onlyDynAbs.txt");
                    var onlyCodeQLPath = Path.Combine(localQLResultsBasePath, $"analysis_onlyCodeQL.txt");
                    var onlyCodeQLExecPath = Path.Combine(localQLResultsBasePath, $"analysis_onlyCodeQL_exec.txt");
                    var bothPath = Path.Combine(localQLResultsBasePath, $"analysis_bothAnalysis.txt");

                    var onlyDynAbs = new List<FileLine>();
                    var onlyCodeQL = new List<FileLine>();
                    
                    var onlyDA = Substract(results[i - 1].FilteredResult, codeQLList[i - 1]);
                    SaveResults(onlyDynAbsPath, onlyDA);
                    var onlyQL = Substract(codeQLList[i - 1], results[i - 1].FilteredResult);
                    SaveResults(onlyCodeQLPath, onlyQL);
                    var intersection = Intersect(codeQLList[i - 1], results[i - 1].FilteredResult);
                    SaveResults(bothPath, intersection);

                    // Executed intersection
                    var onlyQLExec = Intersect(onlyQL, results[i - 1].executedLines);
                    SaveResults(onlyCodeQLExecPath, onlyQLExec);

                    // All CodeQL intersection executed
                    var allQLExec = Intersect(codeQLList[i - 1], results[i - 1].executedLines);

                    summaryStr.AppendLine($"{program.Key};{i};{results[i - 1].executedLines.Count};{results[i - 1].FilteredResult.Count};{allQLExec.Count};{codeQLList[i - 1].Count};{onlyDA.Count};{intersection.Count};{onlyQL.Count};{onlyQLExec.Count}");
                }
            }
            File.WriteAllText(mainResultPath, summaryStr.ToString());
        }

        public static List<FileLine> Substract(List<FileLine> left, List<FileLine> right)
        {
            var result = new List<FileLine>();
            foreach (FileLine lLine in left)
            {
                if (!right.Contains(lLine))
                    result.Add(lLine);

            }
            return result;
        }

        public static List<FileLine> Intersect(List<FileLine> left, List<FileLine> right)
        {
            var result = new List<FileLine>();
            foreach (FileLine lLine in left)
            {
                if (right.Contains(lLine))
                    result.Add(lLine);

            }
            return result;
        }

        public static void SaveResults(string resultPath, List<FileLine> results)
        {
            var sb = new StringBuilder();
            foreach (var res in results.OrderBy(x => x.File).ThenBy(y => y.Line))
                sb.AppendLine($"{res.File}:{res.Line}");
            File.WriteAllText(resultPath, sb.ToString());
        }
    }
}
