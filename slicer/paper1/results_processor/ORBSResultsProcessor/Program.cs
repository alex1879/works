using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ORBSResultsProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = new string[]
            //{
            //    "gui.cs",
            //    "",
            //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\output\ORBSResults\gui.cs\regression\slicingVariable5",
            //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\output\final_lines_orbs.txt"
            //};

            //string res_folder, result_file, program_name, arguments;
            //if (args.Count() == 2)
            //{
            //    program_name = arguments = "";
            //    res_folder = args[0];
            //    result_file = args[1];
            //}
            //else if (args.Count() == 4)
            //{
            //    program_name = args[0];
            //    arguments = args[1];
            //    res_folder = args[2];
            //    result_file = args[3];
            //}
            //else
            //    throw new Exception("Invalid arguments");


            var baseFolder = @"D:\ORBS\";
            var programs = new string[] { /*"gui.cs",*/ /*"FormationJPA",*/ "MavenJPAHibernateLog4jDemo" };
            foreach (var program in programs)
            {
                foreach (var subdir in System.IO.Directory.GetDirectories(Path.Combine(baseFolder, program)).Where(x => x.Contains("slicingVariable")).OrderBy(x => x))
                {
                    string res_folder, result_file, program_name, arguments;
                    program_name = program;
                    arguments = subdir.Split(new string[] { "slicingVariable" }, StringSplitOptions.None).Last()[0].ToString();
                    res_folder = subdir;
                    result_file = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table2\output\final_lines_orbs.txt";

                    //arguments = Path.GetFileName(subdir);
                    var resume = GetPythonORBSLineByPath(res_folder);
                    var final_line = program_name + "|" + arguments + "|" + resume;
                    System.IO.File.AppendAllLines(result_file, new string[] { final_line });
                }
            }
                
            return;
        }

        public static string GetORBSLineByPath(string path)
        {
            var files_to_slice =
                    File.ReadAllLines(Path.Combine(path, @"ORBS_CONFIG\config\", "OrbsFramework.properties")).ToList()[1]
                    .Split('=')[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();
            int methods_counter = 0, lines_counter = 0, totalFiles = 0;
            LinesCounter.Program.CountCSharp(path, files_to_slice, ref lines_counter, ref methods_counter, ref totalFiles);


            //File.ReadAllText(Path.Combine(path, "time.log")).Split(' ')[2].Split('.')[0]
            var total_time = GetSecondsFromElapsedTime(File.ReadAllText(Path.Combine(path, "time.log")).Split('\t')[1]);
            var extra_info = File.ReadAllLines(Path.Combine(path, "iternationLevelStat.csv")).Where(x => !string.IsNullOrWhiteSpace(x)).Last().Split(',');
            var lines_deleted = int.Parse(extra_info[1]);
            var compilations = int.Parse(extra_info[2]);
            var executions = int.Parse(extra_info[3]);
            var cached_compilations = int.Parse(extra_info[4]);
            var cached_executions = int.Parse(extra_info[5]);
            return $"{total_time}|{lines_deleted}|{compilations}|{cached_compilations}|{executions}|{cached_executions}|{methods_counter + lines_counter}";
        }

        static int GetSecondsFromElapsedTime(string elapsedTime)
        {
            //.Split('m')[1].Split('.')[0]

            var minutes = int.Parse(elapsedTime.Split('m')[0]);
            var seconds = int.Parse(elapsedTime.Split('m')[1].Split('.')[0]);
            return minutes * 60 + seconds;

            //var data = elapsedTime.Split(':').Select(x => int.Parse(x)).ToList();
            //int seconds = data.Last();
            //int minutes = 0;
            //int hours = 0;
            //if (data.Count > 1)
            //    minutes = data[data.Count - 2];
            //if (data.Count > 2)
            //    hours = data[data.Count - 3];
            //return seconds + 60 * minutes + 60 * 60 * hours;
        }

        public static string GetPythonORBSLineByPath(string path)
        {
            var files_to_slice = path;
            int methods_counter = 0, lines_counter = 0;
            LinesCounter.Program.CountJava(path, null, ref lines_counter, ref methods_counter);

            var allTimeValues = File.ReadAllLines(Path.Combine(path, "time.log"));
            var startValues = allTimeValues[0].Split(' ');
            var endValues = allTimeValues[1].Split(' ');
            var startTime = new DateTime(2020, 01, 01, int.Parse(startValues[4].Split(':')[0]), int.Parse(startValues[4].Split(':')[1]), int.Parse(startValues[4].Split(':')[2]));
            var endTime = new DateTime(2020, 01, 01, int.Parse(endValues[4].Split(':')[0]), int.Parse(endValues[4].Split(':')[1]), int.Parse(endValues[4].Split(':')[2]));

            //File.ReadAllText(Path.Combine(path, "time.log")).Split(' ')[2].Split('.')[0]
            var total_time = endTime.Subtract(startTime).TotalSeconds;
            
            //var extra_info = File.ReadAllLines(Path.Combine(path, "iternationLevelStat.csv")).Where(x => !string.IsNullOrWhiteSpace(x)).Last().Split(',');
            //var lines_deleted = int.Parse(extra_info[1]);
            //var compilations = int.Parse(extra_info[2]);
            //var executions = int.Parse(extra_info[3]);
            //var cached_compilations = int.Parse(extra_info[4]);
            //var cached_executions = int.Parse(extra_info[5]);

            var lines_deleted = 0;
            var compilations = 0;
            var executions = 0;
            var cached_compilations = 0;
            var cached_executions = 0;

            return $"{total_time}|{lines_deleted}|{compilations}|{cached_compilations}|{executions}|{cached_executions}|{/*methods_counter +*/ lines_counter}";
        }
    }
}
