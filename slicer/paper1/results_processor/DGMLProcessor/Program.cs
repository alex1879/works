﻿using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using Utils;

namespace DGMLProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            string configPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\TypeTests\ImportedVersusSource.slc";

            var excluded = new List<string>()
            {
                "C:\\Users\\alexd\\Desktop\\Slicer\\Roslyn\\src\\src\\Compilers\\CSharp\\Portable\\Symbols",
                "C:\\Users\\alexd\\Desktop\\Slicer\\Roslyn\\src\\src\\Compilers\\CSharp\\Portable\\Emitter\\Model",
                "C:\\Users\\alexd\\Desktop\\Slicer\\Roslyn\\src\\src\\Compilers\\CSharp\\Portable\\Binder",
                "C:\\Users\\alexd\\Desktop\\Slicer\\Roslyn\\src\\src\\Compilers\\CSharp\\Portable\\SymbolDisplay"
            };

            var dgmlPath_reducido = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\AnalyzingErr\DG_RED.dgml";
            var newDgmlPath_reducido = @"";

            var dgmlPath_full = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\AnalyzingErr\DG_OPT.dgml";
            var newDgmlPath_full = @"";

            var intersectedDml = @"C:\temp\intersectedDgml.dgml";

            var dg_reducido = LoadDirectedGraph(dgmlPath_reducido);
            var nodeFrom_customAssert = "27001:[18515..18569).1";
            var subgraph_reducido = GetSubgraphFrom(dg_reducido, nodeFrom_customAssert);

            var nodeFrom_laQueFalta = "25005:[9812..9830).1";
            var dg_full = LoadDirectedGraph(dgmlPath_full);
            var subgraph_full = GetSubgraphTo(dg_full, nodeFrom_laQueFalta);
            var idsToExclude = GetIdsFromConfigurationAndFolders(configPath, excluded);
            var simplifiedDg_full = DeleteIds(subgraph_full, GetIdsFromFilesIds(subgraph_full, idsToExclude));

            // A partir de acá metemos la intersección
            // 1) Al Reducido (que está completo desde el assert) le borramos los IDs que el reducido no tiene
            var nodesFull = simplifiedDg_full.Nodes.Select(x => x.Id).ToList();
            var idsEnComun = subgraph_reducido.Nodes.Where(x => nodesFull.Contains(x.Id)).Select(x => x.Id).ToList();
            var idsQueElReducidoNoTiene = subgraph_reducido.Nodes.Where(x => !idsEnComun.Contains(x.Id)).Select(x => x.Id).ToList();
            //var reduced_red = DeleteIds(subgraph_reducido, new HashSet<string>(idsQueElReducidoNoTiene));
            // 2) Pintamos los que están en común y los que no
            
            var groupCommon = simplifiedDg_full.Nodes.Where(x => idsEnComun.Contains(x.Id)).Select(x => new Node() { 
                Id = x.Id,
                Label = x.Label.Substring(0, Math.Min(x.Label.Length, 20)),
                Category = "Commmon"
            }).ToList();
            var groupDistinct = simplifiedDg_full.Nodes.Where(x => !idsEnComun.Contains(x.Id)).Select(x => new Node()
            {
                Id = x.Id,
                Label = x.Label.Substring(0, Math.Min(x.Label.Length, 20)),
                Category = "Distinct"
            }).ToArray();
            simplifiedDg_full.Nodes = groupCommon.Union(groupDistinct).ToArray();

            simplifiedDg_full.Categories = new Category[]
            {
                new Category() { Id = "Common", Background = "White" },
                new Category() { Id = "Distinct", Background = "Yellow" },
            };

            Save(intersectedDml, simplifiedDg_full);
        }

        static void Save(string path, DirectedGraph dg)
        {
            var serializer = new XmlSerializer(typeof(DirectedGraph));
            var streamToSave = System.IO.File.OpenWrite(path);
            serializer.Serialize(streamToSave, dg);
            streamToSave.Close();
            ReplaceTitle(path);
        }

        static DirectedGraph LoadDirectedGraph(string path)
        {
            var stream = File.OpenRead(path);
            var serializer = new XmlSerializer(typeof(DirectedGraph));
            return (DirectedGraph)serializer.Deserialize(stream);
        }

        static void ReplaceTitle(string path)
        {
            // OLD HEADER: Title="DG" xmlns="http://schemas.microsoft.com/vs/2009/dgml"
            // NEW HEADER: xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            var allLines = File.ReadAllLines(path);
            allLines[1] = "<DirectedGraph Title=\"DG\" xmlns=\"http://schemas.microsoft.com/vs/2009/dgml\">";
            File.WriteAllLines(path, allLines);
        }

        static DirectedGraph GetSubgraphTo(DirectedGraph dg, string id)
        {
            var nodesBag = new HashSet<string>();
            nodesBag.Add(id);
            var links = GetFromTarget(dg, id);
            var nextLinks = new List<Link>();
            var added = false;
            do
            {
                added = false;
                foreach (var link in links)
                {
                    var idSource = link.Source;
                    if (nodesBag.Add(idSource))
                    {
                        added = true;
                        nextLinks.AddRange(GetFromTarget(dg, idSource));
                    }
                }
                links = nextLinks;
                nextLinks = new List<Link>();
            } while (added);

            var retDg = new DirectedGraph();
            retDg.Nodes = GetNodesFromId(dg, nodesBag);
            retDg.Links = GetFromTarget(dg, nodesBag);
            return retDg;
        }
        
        static DirectedGraph GetSubgraphFrom(DirectedGraph dg, string id)
        {
            var nodesBag = new HashSet<string>();
            nodesBag.Add(id);
            var links = GetFromSource(dg, id);
            var nextLinks = new List<Link>();
            var added = false;
            do
            {
                added = false;
                foreach (var link in links)
                {
                    var idTarget = link.Target;
                    if (nodesBag.Add(idTarget))
                    {
                        added = true;
                        nextLinks.AddRange(GetFromSource(dg, idTarget));
                    }
                }
                links = nextLinks;
                nextLinks = new List<Link>();
            } while (added);

            var retDg = new DirectedGraph();
            retDg.Nodes = GetNodesFromId(dg, nodesBag);
            retDg.Links = GetFromSource(dg, nodesBag);
            return retDg;
        }

        static DirectedGraph DeleteIds(DirectedGraph dg, HashSet<string> nodesToDelete)
        {
            var allNodes = dg.Nodes.Where(x => !nodesToDelete.Any(y => y == x.Id) || x.Equals("EXTERNAL")).ToArray();
            var allLinks = new HashSet<Link>(dg.Links);

            foreach (var idToDelete in nodesToDelete)
            {
                var nodesToId = allLinks.Where(x => x.Target == idToDelete);
                var nodesFromId = allLinks.Where(x => x.Source == idToDelete);
                var newLinks = new HashSet<Link>();
                
                foreach (var nodeFromId in nodesFromId)
                    foreach (var nodeToId in nodesToId)
                        newLinks.Add(new Link() { Source = nodeToId.Source, Target = nodeFromId.Target });

                var beforeNodes = allLinks.Count;
                var afterNodes = allLinks.Where(x => x.Target != idToDelete && x.Source != idToDelete).Count() + newLinks.Count;
                if (afterNodes > beforeNodes)
                    ;

                allLinks = new HashSet<Link>(allLinks.Where(x => x.Target != idToDelete && x.Source != idToDelete));
                allLinks.UnionWith(newLinks);
            }

            var newDg = new DirectedGraph();
            newDg.Nodes = allNodes;
            newDg.Links = allLinks.ToArray();
            return newDg;
        }

        static List<Link> GetFromTarget(DirectedGraph dg, string target)
        {
            return dg.Links.Where(x => x.Target == target).ToList();
        }

        static List<Link> GetFromSource(DirectedGraph dg, string source)
        {
            return dg.Links.Where(x => x.Source == source).ToList();
        }

        static Link[] GetFromTarget(DirectedGraph dg, HashSet<string> nodes)
        {
            return dg.Links.Where(x => nodes.Any(y => x.Target == y)).ToArray();
        }

        static Link[] GetFromSource(DirectedGraph dg, HashSet<string> nodes)
        {
            return dg.Links.Where(x => nodes.Any(y => x.Source == y)).ToArray();
        }

        static Node[] GetNodesFromId(DirectedGraph dg, HashSet<string> nodes)
        {
            return dg.Nodes.Where(x => nodes.Any(y => x.Id == y)).ToArray();
        }

        static HashSet<string> GetIdsFromFilesIds(DirectedGraph dg, HashSet<string> filesIdsToDelete)
        {
            return new HashSet<string>(dg.Nodes.Where(x => filesIdsToDelete.Any(y => x.Id.Split(":")[0].Equals(y) || x.Id == "EXTERNAL")).Select(x => x.Id));
        }

        static HashSet<string> GetIdsFromConfigurationAndFolders(string slcPath, List<string> excludedFolders)
        {
            var stream = System.IO.File.OpenRead(slcPath);
            var serializer = new XmlSerializer(typeof(UserConfiguration));
            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);

            var dict = GetFilesInfo(userConfiguration);
            var files = FilesFromFolders(excludedFolders);

            return new HashSet<string>(files.Select(x => dict[x].ToString()));
        }

        static List<string> FilesFromFolders(List<string> folders)
        {
            var retList = new List<string>();
            foreach (var folder in folders)
            {
                retList.AddRange(System.IO.Directory.GetFiles(folder, "*", SearchOption.AllDirectories));
            }
            return retList;
        }

        static Dictionary<string, int> GetFilesInfo(UserConfiguration userConfiguration)
        {
            var retDict = new Dictionary<string, int>();

            foreach (var proj in userConfiguration.targetProjects.excluded)
            {
                var currentProjectName = proj.name;
                foreach (var file in proj.files)
                {
                    if (file.id > 0 && !retDict.ContainsKey(file.name))
                        retDict.Add(file.name, file.id);
                }
            }

            return retDict;
        }
    }

    public class DirectedGraph
    {
        public Node[] Nodes { get; set; }
        public Link[] Links { get; set; }
        public Category[] Categories { get; set; }
    }

    public class Node
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }
        [XmlAttribute("Label")]
        public string Label { get; set; }
        [XmlAttribute("Category")]
        public string Category { get; set; }
    }

    public class Link
    {
        [XmlAttribute("Source")]
        public string Source { get; set; }
        [XmlAttribute("Target")]
        public string Target { get; set; }

        public override bool Equals(object obj)
        {
            return ((Link)obj).Source == Source && ((Link)obj).Target == Target;
        }

        public override int GetHashCode()
        {
            return Source.GetHashCode() + Target.GetHashCode();
        }
    }

    public class Category
    {
        [XmlAttribute("Id")]
        public string Id { get; set; }
        [XmlAttribute("Background")]
        public string Background { get; set; }
    }
}
