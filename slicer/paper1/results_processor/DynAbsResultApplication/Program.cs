﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynAbsResultApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var resultPath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\output\Results\FNHMVC_NETCORE\Results.txt";
            var solutionFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\src\FNHMVC_Modified";
            var cleanedFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table3\src\FNHMVC_ORBS";
            var whereToSave = @"C:\temp\processedFiles";

            var lines = GetLines(resultPath);

            var ProyectosExcluidos = new string[] { "CleanDB", "FNHMVC.Web", "Tracer"/*, "FNHMVC.CommandProcessor"*/ };

            foreach (var file in System.IO.Directory.GetFiles(solutionFolder, "*.cs", System.IO.SearchOption.AllDirectories))
            {
                if (ProyectosExcluidos.Any(x => file.Contains(x)))
                    continue;

                if (file.Contains("Debug") || file.Contains("AssemblyInfo.cs"))
                    continue;
                var fileName = System.IO.Path.GetFileName(file);
                HashSet<int> loc;
                if (!lines.TryGetValue(fileName, out loc))
                    loc = new HashSet<int>();
                var sb = Process(file, loc);
                //var finalPath = System.IO.Path.Combine(whereToSave, fileName);
                //var finalPath = file;
                var finalPath = file.Replace(solutionFolder, cleanedFolder);
                System.IO.File.WriteAllText(finalPath, sb.ToString());
            }
        }

        static Dictionary<string, HashSet<int>> GetLines(string resultPath)
        {
            var retDicc = new Dictionary<string, HashSet<int>>();
            var str = File.ReadAllLines(resultPath);
            foreach(var line in str)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    var values = line.Split(':');
                    if (values.Length < 2)
                        continue;
                    var fileName = values[0].Trim();
                    if (!retDicc.ContainsKey(fileName))
                        retDicc[fileName] = new HashSet<int>();
                    retDicc[fileName].Add(int.Parse(values[1].Trim()));
                }
            }
            return retDicc;
        }

        static StringBuilder Process(string file, HashSet<int> loc)
        {
            var sbNewLines = new StringBuilder();
            var lines = System.IO.File.ReadAllLines(file);
            var entroNamespace = false;
            var entroClase = false;
            var entroMetodo = false;
            var contadorLlaves = 0;
            var hasNamespace = lines.Any(x => x.Contains("namespace "));
            var currentLineNumber = 0;
            foreach (var line in lines)
            {
                var tLine = line.Trim();
                if (line.Contains("foreach (var val in validations)") || line.Contains("public void CategoryCreateTest()")
                    || line.Contains("public class CategoryTest"))
                    ;

                currentLineNumber++;
                if (string.IsNullOrWhiteSpace(tLine))
                {
                    sbNewLines.AppendLine(line);
                    continue;
                }
                
                if (!entroNamespace && hasNamespace)
                {
                    entroNamespace = tLine.Length == 1 && tLine[0] == '{';
                    sbNewLines.AppendLine(line);
                }
                else if (!entroClase)
                {
                    entroClase = tLine.Length == 1 && tLine[0] == '{';
                    bool appended = false;
                    if (!entroClase && tLine.Length > 1 && tLine.Substring(0, 2) != "//" && !tLine.Contains("using"))
                    {
                        // Contamos las líneas de properties (incluye la cabecera del class)
                        if (line.Contains(";"))
                        {
                            if (loc.Contains(currentLineNumber))
                                sbNewLines.AppendLine(line);
                            else
                                sbNewLines.AppendLine("//" + line);
                        }
                        else /*if (line[0] != '[')*/
                            sbNewLines.AppendLine(line);
                        appended = true;
                    }
                    
                    if (!entroClase && tLine.Length == 1 && tLine[0] == '}')
                    {
                        entroNamespace = false;
                        sbNewLines.AppendLine(line);
                        appended = true;
                    }

                    if (!appended)
                        sbNewLines.AppendLine(line);
                }
                else if (!entroMetodo)
                {
                    entroMetodo = tLine.Length == 1 && tLine[0] == '{';
                    if (entroMetodo)
                    {
                        sbNewLines.AppendLine(line);
                    }
                    else if (tLine.Length > 1 && tLine.Substring(0, 2) != "//" && (tLine.Contains(";") /*|| line.Contains(":")*/))
                    {
                        if (loc.Contains(currentLineNumber))
                        {
                            sbNewLines.AppendLine(line);
                        }
                        else
                        {
                            sbNewLines.AppendLine("//" + line);
                        }
                    }
                    else
                    {
                        sbNewLines.AppendLine(line);
                    }
                    if (!entroMetodo && tLine.Length == 1 && tLine[0] == '}')
                        entroClase = false;
                }
                else if (entroClase && entroMetodo)
                {
                    var cantidadLlavesIn = tLine.Count(x => x == '{');
                    var cantidadLlavesOut = tLine.Count(x => x == '}');
                    contadorLlaves = contadorLlaves + cantidadLlavesIn - cantidadLlavesOut;
                    // Se salió de un método
                    if (contadorLlaves < 0)
                    {
                        entroMetodo = false;
                        contadorLlaves = 0;
                        sbNewLines.AppendLine(line);
                    }
                    else
                    {
                        if (loc.Contains(currentLineNumber) || (tLine.Length > 1 && tLine.Substring(0, 2) == "//"))
                        {
                            sbNewLines.AppendLine(line);
                        }
                        else
                        {
                            sbNewLines.AppendLine("//" + line);
                        }
                    }
                }
            }
            return sbNewLines;
        }
    }
}
