﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesCounter
{
    public class Program
    {
        static void Main(string[] args)
        {
            #region Subjects
            var PowershellSubjects = new Dictionary<string, List<string>>()
            {
                // POWERSHELL
                {
                    @"FileSystemProvider", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\namespaces",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\SessionStateNavigation.cs"
                    }
                },
                {
                    @"MshSnapinInfo", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\singleshell"
                    }
                },
                {
                    @"NamedPipe", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\remoting"
                    }
                },
                {
                    @"CorePsPlatform", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\CoreCLR"
                    }
                },
                {
                    @"PowerShellAPI", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\hostifaces"
                    }
                },
                {
                    @"PSConfiguration", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\PSConfiguration.cs"
                    }
                },
                {
                    @"Binders", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\runtime"
                    }
                },
                {
                    @"PSObject", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\MshObject.cs",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\MshMemberInfo.cs",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\AutomationNull.cs"
                    }
                },
                {
                    @"ExtensionMethods", new List<string>()
                    {

                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\utils",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\utils.cs",
                    }
                },
                {
                    @"PSVersionInfo", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\PSVersionInfo.cs"
                    }
                },
                {
                    @"Runspace", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\hostifaces",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\runtime",
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\lang\scriptblock.cs"
                    }
                },
                {
                    @"SecuritySupport", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\security"
                    }
                },
                {
                    @"SessionState", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine"
                    }
                },
                {
                    @"Utils", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\utils.cs"
                    }
                },
                {
                    @"WildcardPattern", new List<string>()
                    {
                    
                        @"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine\regex.cs"
                    }
                }
            };

            #region Roslyn Folders
            var foldersParser = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Parser",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Syntax"
            };

            var bindingTests = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Binder"
            };

            var typeTests = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Binder",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Symbols",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\SymbolDisplay",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Compilation\CSharpCompilation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\TypeParameterSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SynthesizedPrivateImplementationDetailsStaticConstructor.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SourceAssemblySymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PropertySymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PointerTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ParameterSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\NamespaceSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\NamedTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\MethodSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\FunctionPointerTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\FieldSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\EventSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\CustomModifierAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\AttributeDataAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ArrayTypeSymbolAdapter.cs",
            };

            var flowTests = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\FlowAnalysis"
            };

            var emitterTest = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Compilation",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Compiler",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\EditAndContinue",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\NoPia",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\TypeMemberReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SpecializedNestedTypeReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SpecializedMethodReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SpecializedGenericNestedTypeInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SpecializedGenericMethodInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SpecializedFieldReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PENetModuleBuilder.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PEModuleBuilder.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PEAssemblyBuilder.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ParameterTypeInformation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\NamedTypeReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ModuleReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\MethodReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\GenericTypeInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\GenericNestedTypeInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\GenericNamespaceTypeInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\GenericMethodInstanceReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ExpandedVarargsMethodReference.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\AssemblyReference.cs",
            };

            var pdbTests = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Binder",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Symbols",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\SymbolDisplay",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Compilation\CSharpCompilation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\TypeParameterSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SynthesizedPrivateImplementationDetailsStaticConstructor.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SourceAssemblySymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PropertySymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\PointerTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ParameterSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\NamespaceSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\NamedTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\SymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\MethodSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\FunctionPointerTypeSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\FieldSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\EventSymbolAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\CustomModifierAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\AttributeDataAdapter.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter\Model\ArrayTypeSymbolAdapter.cs",
            };
            #endregion

            var RoslynSubjects = new Dictionary<string, string[]>() 
            {
                { "Parser", foldersParser },
                { "Types", typeTests },
                { "Binding", bindingTests },
                { "Flow", flowTests },
                { "Compilation", emitterTest },
                { "PDB", pdbTests }
            };
            #endregion

            var folders = new string[] {
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\BH",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\BiSort",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\Em3d",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\Health",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\MST",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\Perimeter",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\Power",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\TreeAdd",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\TSP",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\CSharp\Voronoi"

                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\CSharp\FormationJPA",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\CSharp\MavenJPAHibernateLog4jDemo",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\CSharp\OrderLibrary"

                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\EdgeDeflector\EdgeDeflector",

                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\gui.cs\Terminal.Gui",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\gui.cs\Example",

                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\LibLog\src\LibLog",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\LibLog\src\LibLog.Example.Library",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\LibLog\src\LibLog.Example.Log4Net",
                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\src\LibLog\src\LibLog.Example.NLog",

                //@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2_C\output\ORBSCompilations\LibLog\Log4NET_slicingVariable1"

                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Core\Portable",

                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Core\CodeAnalysisTest",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Test\PdbUtilities"

                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src"
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\Microsoft.PowerShell.ConsoleHost"
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\powershell"
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation"

                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\eng\config\test\Core\InstallTraceListener.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\CSharpTestSource.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\CSharpTestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\BasicCompilationUtils.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\CompilationTestUtils.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\Extensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\TestOptions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\UsesIsNullableVisitor.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\IOperation\IOperation\IOperationTests_IBinaryOperatorExpression.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\IOperation\IOperation\IOperationTests_ILiteralOperation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\IOperation\IOperation\IOperationTests_IIfStatement.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\CodeGen\CodeGenFunctionPointersTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\PDB\CSharpDeterministicBuildCompilationTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\Emit\CompilationEmitTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\Emit\EmitMetadataTestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Core\MSBuildTask\MvidReader.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\PDB\PDBTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Test\PdbUtilities\Reader\PdbValidation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Test\PdbUtilities\Reader\PdbValidationOptions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\AssertEx.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\AssertXml.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Diagnostics\DiagnosticExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Diagnostics\DiagnosticDescription.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TargetFrameworkUtil.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\CompilationExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TestHelpers.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\CustomAssert.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Extensions\OperationExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\TestOperationVisitor.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\ControlFlowGraphVerifier.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\DiffUtil.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Extensions\SymbolExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\FX\MonoHelpers.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\CommonTestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\CompilationVerifier.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\IRuntimeEnvironment.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Metadata\MetadataSignatureUnitTestHelper.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Metadata\MetadataReaderUtils.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\TestExceptionUtilities.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Metadata\PEModuleTestHelpers.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\CompilationTestDataExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Metadata\IlasmUtilities.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TempFiles\TempFile.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\FX\ProcessUtilities.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TempFiles\DisposableDirectory.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TempFiles\DisposableFile.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TempFiles\TempDirectory.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\TempFiles\TempRoot.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Parsing\ParsingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Parsing\StatementParsingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Parsing\SyntaxExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Generated\Syntax.Test.xml.Generated.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Symbol\Symbols\TypeTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Semantic\Semantics\BindingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Semantic\FlowAnalysis\FlowTests.cs",

                @"C:\Users\alexd\Desktop\Slicer\Powershell\src\test\xUnit\Asserts",
                @"C:\Users\alexd\Desktop\Slicer\Powershell\src\test\xUnit\csharp",

                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\VisualBasic\Portable"
            };

            //foreach (var subject in PowershellSubjects)
            //{ 
                var totalTotalFiles = 0;
                var totalTotalLines = 0;
                var totalTotalMethods = 0;
                var totalSliceable = 0;
                foreach (var folder in /*subject.Value*/ folders)
                {
                    int totalLines = 0;
                    int totalMethods = 0;
                    int totalFiles = 0;
                    CountCSharp(folder, null, ref totalLines, ref totalMethods, ref totalFiles);
                    int totalSliceableLines = totalLines + totalMethods;
                    totalTotalFiles += totalFiles;
                    totalTotalLines += totalLines;
                    totalTotalMethods += totalMethods;
                    totalSliceable += totalSliceableLines;
                    //Console.WriteLine($"{folder} {totalFiles} {totalLines} {totalMethods} {totalSliceableLines}");
                }
                Console.WriteLine($"TOTAL {totalTotalFiles} {totalTotalLines} {totalTotalMethods}");
                //Console.WriteLine($"{subject.Key} {totalTotalFiles} {totalTotalLines} {totalTotalMethods} {totalSliceable}");
            //}
            Console.ReadKey();
        }

        public static void CountCSharp(string folder, string[] files_to_slice, 
            ref int totalLinesCounter, ref int totalMethodsCounter, ref int totalFiles)
        {
            totalLinesCounter = 0;
            totalMethodsCounter = 0;
            var totalLines = new List<string>();
            totalFiles = 0;

            var files = new List<string>();
            if (folder.EndsWith(".cs"))
                files.Add(folder);
            else
                files.AddRange(System.IO.Directory.GetFiles(folder, "*.cs", System.IO.SearchOption.AllDirectories) // TopDirectoryOnly
                .Where(x => files_to_slice == null || files_to_slice.Contains(Path.GetFileNameWithoutExtension(x))));

            foreach (var file in files)
            {
                totalFiles++;
                var lines = System.IO.File.ReadAllLines(file);
                var entroNamespace = false;
                var entroClase = false;
                var entroMetodo = false;
                var contadorLlaves = 0;
                var localLinesCounter = 0;
                var localMethodsCounter = 0;
                var linesToAnalyze = lines.Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x));
                var hasNamespace = linesToAnalyze.Any(x => x.Contains("namespace "));
                foreach (var line in linesToAnalyze)
                {
                    if (!entroNamespace && hasNamespace)
                        entroNamespace = line.Length == 1 && line[0] == '{';
                    else if (!entroClase)
                    //if (!entroClase)
                    {
                        entroClase = line.Length == 1 && line[0] == '{';
                        // Si solo contamos las que terminan, tienen ";" sino también hasta contaría cabeceras de métodos (line.Contains(";"))
                        if (!entroClase && line.Length > 1 && line.Substring(0, 2) != "//" && !line.Contains("using"))
                        {
                            // Contamos las líneas de properties (incluye la cabecera del class)
                            if (line.Contains(";"))
                            {
                                localLinesCounter++;
                                totalLines.Add(line);
                            }
                            // Cabecera de clase
                            else if (line[0] != '[')
                            {
                                localMethodsCounter++;
                            }
                        }
                        if (!entroClase && line.Length == 1 && line[0] == '}')
                            entroNamespace = false;
                    }
                    else if (!entroMetodo)
                    {
                        entroMetodo = line.Length == 1 && line[0] == '{';
                        if (entroMetodo)
                            localMethodsCounter++;
                        // Si solo contamos las que terminan, tienen ";" sino también hasta contaría cabeceras de métodos (line.Contains(";"))
                        else if (line.Length > 1 && line.Substring(0, 2) != "//" && (line.Contains(";") || line.Contains(":"))) 
                        {
                            // Contamos las líneas de properties
                            localLinesCounter++;
                            totalLines.Add(line);
                        }
                        if (!entroMetodo && line.Length == 1 && line[0] == '}')
                            entroClase = false;
                    }
                    else if (entroClase && entroMetodo)
                    {
                        var cantidadLlavesIn = line.Count(x => x == '{');
                        var cantidadLlavesOut = line.Count(x => x == '}');
                        contadorLlaves = contadorLlaves + cantidadLlavesIn - cantidadLlavesOut;
                        // Se salió de un método
                        if (contadorLlaves < 0)
                        {
                            entroMetodo = false;
                            contadorLlaves = 0;
                        }
                        else if (line.Length > 1 && 
                            line.Substring(0, 2) != "//"
                            /*&& line != "get" && line != "set" && line != "else"*/)
                        {
                            localLinesCounter++;
                            totalLines.Add(line);
                        }
                    }
                }
                totalMethodsCounter += localMethodsCounter;
                totalLinesCounter += localLinesCounter;
                //Console.WriteLine($"{file}={localMethodsCounter + localLinesCounter}");
            }
            return;
            //Console.WriteLine(string.Format("MÉTODOS: {0}, PROM: {4:N0}. LÍNEAS: {1}, PROM: {5:N0}. ARCHIVOS: {2}, PATH: {3}", totalMethodsCounter, totalLinesCounter, totalFiles, folder, totalMethodsCounter / totalFiles, totalLinesCounter / totalFiles));
            //return totalLinesCounter;
        }

        public static void CountJava(string folder, string[] files_to_slice,
            ref int totalLinesCounter, ref int totalMethodsCounter)
        {
            totalLinesCounter = 0;
            totalMethodsCounter = 0;
            var totalLines = new List<string>();
            var totalFiles = 0;
            foreach (var file in System.IO.Directory.GetFiles(folder, "*.java", System.IO.SearchOption.AllDirectories) // TopDirectoryOnly
                .Where(x => files_to_slice == null || files_to_slice.Contains(Path.GetFileNameWithoutExtension(x))))
            {
                totalFiles++;
                var lines = System.IO.File.ReadAllLines(file);
                var entroNamespace = false;
                var entroClase = false;
                var entroMetodo = false;
                var contadorLlaves = 0;
                var localLinesCounter = 0;
                var localMethodsCounter = 0;
                var linesToAnalyze = lines.Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x));
                var hasNamespace = false; //linesToAnalyze.Any(x => x.Contains("namespace "));
                foreach (var line in linesToAnalyze)
                {
                    if (!entroNamespace && hasNamespace)
                        entroNamespace = line.Length == 1 && line[0] == '{';
                    else if (!entroClase)
                    //if (!entroClase)
                    {
                        entroClase = line.Length == 1 && line[0] == '{';
                        // Si solo contamos las que terminan, tienen ";" sino también hasta contaría cabeceras de métodos (line.Contains(";"))
                        if (!entroClase && line.Length > 1 && line.Substring(0, 2) != "//" && !line.Contains("using"))
                        {
                            // Contamos las líneas de properties (incluye la cabecera del class)
                            if (line.Contains(";"))
                            {
                                //localLinesCounter++;
                                //totalLines.Add(line);
                            }
                            // Cabecera de clase
                            else if (line[0] != '[')
                            {
                                localMethodsCounter++;
                            }
                        }
                        if (!entroClase && line.Length == 1 && line[0] == '}')
                            entroNamespace = false;
                    }
                    else if (!entroMetodo)
                    {
                        entroMetodo = line.Length == 1 && line[0] == '{';
                        if (entroMetodo)
                            localMethodsCounter++;
                        // Si solo contamos las que terminan, tienen ";" sino también hasta contaría cabeceras de métodos (line.Contains(";"))
                        else if (line.Length > 1 && line.Substring(0, 2) != "//" && (line.Contains(";") || line.Contains(":")))
                        {
                            // Contamos las líneas de properties
                            //localLinesCounter++;
                            //totalLines.Add(line);
                        }
                        if (!entroMetodo && line.Length == 1 && line[0] == '}')
                            entroClase = false;
                    }
                    else if (entroClase && entroMetodo)
                    {
                        var cantidadLlavesIn = line.Count(x => x == '{');
                        var cantidadLlavesOut = line.Count(x => x == '}');
                        contadorLlaves = contadorLlaves + cantidadLlavesIn - cantidadLlavesOut;
                        // Se salió de un método
                        if (contadorLlaves < 0)
                        {
                            entroMetodo = false;
                            contadorLlaves = 0;
                        }
                        else if (line.Length > 1 &&
                            line.Substring(0, 2) != "//"
                            /*&& line != "get" && line != "set" && line != "else"*/)
                        {
                            localLinesCounter++;
                            totalLines.Add(line);
                        }
                    }
                }
                totalMethodsCounter += localMethodsCounter;
                totalLinesCounter += localLinesCounter;
                //Console.WriteLine($"{file}={localMethodsCounter + localLinesCounter}");
            }

            foreach (var l in totalLines)
                Console.WriteLine(l);

            return;

            //Console.WriteLine(string.Format("MÉTODOS: {0}, PROM: {4:N0}. LÍNEAS: {1}, PROM: {5:N0}. ARCHIVOS: {2}, PATH: {3}", totalMethodsCounter, totalLinesCounter, totalFiles, folder, totalMethodsCounter / totalFiles, totalLinesCounter / totalFiles));
            //return totalLinesCounter;
        }
    }
}
