﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ResultsProcessor
{
    public class RecolectDataFromMultipleCriteria
    {
        static void Main()
        {
            var mainPath = @"C:\Users\lafhis\Desktop\Slicer\roslyn-slicer\results\InvalidUnicodeString_Separated";
            var diccSet = GetInstanceInfo(mainPath);
            SetStats(diccSet);
            SaveStats(diccSet);
        }

        static Dictionary<SliceInstance, ResultData> GetInstanceInfo(string mainPath)
        {
            var diccSet = new Dictionary<SliceInstance, ResultData>();
            foreach (var directory in Directory.GetDirectories(mainPath).Select(x => Path.Combine(x, "InvalidUnicodeString_Asserts_WithSummaries_Clusters_Temp")))
            {
                var executed_lines_path = Path.Combine(directory, "ExecutedLinesForUser.txt");
                var results_path = Path.Combine(directory, "IResults.txt");
                if (!File.Exists(executed_lines_path))
                    continue;

                // Obtenemos el nombre original de todo
                var file_name = Path.GetFileName(Directory.GetParent(directory).Name);
                var summaries = file_name.Split('_')[0] == "WithSummaries";
                var criteria = int.Parse(file_name.Split('_')[1]);
                var iterationNumber = int.Parse(file_name.Split('_')[2]);
                if (iterationNumber > 2)
                    iterationNumber -= 2;

                var instance = new SliceInstance(summaries, criteria, iterationNumber);

                #region OLD
                //var original_name = file_name.Substring(0, file_name.Length - 5);

                // Solo mostramos los tests relevantes
                //if (!relevant_tests.ContainsKey(original_name))
                //    continue;

                // Inputs del programa:
                //var inputs = File.ReadAllLines(Path.Combine(pathTrazas, original_name + "_TEXT.txt")).First();

                // Archivo de traza original
                //var original_trace_file = File.ReadAllLines(Path.Combine(pathTrazas, original_name + "_ORIG.txt")).Count() - 1;

                // Levantamos todos los archivos de trazas
                //var total_ext_traces = Directory.GetFiles(pathTrazas)
                //    .Where(x => x.Contains(original_name) && !x.EndsWith("_TEXT.txt") && !x.EndsWith("_ORIG.txt"))
                //    .Sum(x => File.ReadAllLines(x).Count() - 1);
                #endregion

                var tempExecuted = new HashSet<string>(File.ReadAllLines(executed_lines_path).Where(x => !string.IsNullOrWhiteSpace(x)));
                var tempSilced = new HashSet<string>(File.ReadAllLines(results_path).Where(x => !string.IsNullOrWhiteSpace(x) && x != "---------------------"));

                // Es importante esto porque a veces una línea de código está escrita en muchas líneas. 
                // El slice muestra todas las líneas de un statement mientras que el executed solo la 1era
                // Para poder evaluar nuestras métricas nos quedamos con la 1er línea de un statement
                // DISCLAIM: Ya están trabajados
                // tempSilced.IntersectWith(tempExecuted);

                diccSet.Add(instance, new ResultData()
                {
                    ExecutedLines = tempExecuted,
                    ResultLines = tempSilced,
                    //input = inputs,
                    //trace_orig = original_trace_file,
                    //trace_ext = total_ext_traces,
                    //group_id = relevant_tests[original_name]
                });

                //mainSetExec.UnionWith(tempExecuted);
                //mainSetSlice.UnionWith(tempSilced);
                //var summary_file_path = Path.Combine(directory, "Summary.txt");
                //foreach (var line in File.ReadAllLines(summary_file_path))
                //{
                //    // Dejo un espacio en el medio con "||" para poder poner el % sliceado ahí en la tabla
                //    sbGeneral.AppendLine(inputs + "|" + line + tempSilced.Count.ToString() + $"||{original_trace_file}|{total_ext_traces}");
                //    //Console.WriteLine(inputs + "|" + line + tempSilced.Count.ToString() + $"||{original_trace_file}|{total_ext_traces}");
                //}
            }
            return diccSet;
        }

        static void SetStats(Dictionary<SliceInstance, ResultData> diccSet)
        {
            // Intersección, unión, etc. Tabla comparativa de ejecuciones entre si
            foreach (var x in diccSet.OrderBy(x => x.Key))
            {
                x.Value.Stats = new Dictionary<SliceInstance, IntersectionAndUnionData>();
                foreach (var y in diccSet.OrderBy(y => y.Key))
                {
                    var iud = new IntersectionAndUnionData();
                    //iud.group_id = y.Value.group_id;
                    iud.UnionExec = x.Value.ExecutedLines.Union(y.Value.ExecutedLines).Count();
                    iud.InterExec = x.Value.ExecutedLines.Intersect(y.Value.ExecutedLines).Count();
                    iud.UnionSlice = x.Value.ResultLines.Union(y.Value.ResultLines).Count();
                    iud.InterSlice = x.Value.ResultLines.Intersect(y.Value.ResultLines).Count();

                    var t1 = x.Value.ResultLines.Intersect(y.Value.ExecutedLines);
                    var t2 = x.Value.ResultLines.Intersect(y.Value.ResultLines);
                    var t3 = t1.Except(t2);

                    iud.slice_interseccion_ejecutadas = t1.ToList();
                    iud.interseccion_slices = t2.ToList();
                    iud.slice_interseccion_ejecutadas_menos_interseccion_slices = t3.ToList();

                    if (t3.Count() < 0)
                        ;
                    if (t1.Count() == 0)
                        iud.SpecialCoef = -1;
                    else
                        iud.SpecialCoef = t3.Count() * 100 / t1.Count();

                    x.Value.Stats.Add(y.Key, iud);
                }
            }
        }

        static void SaveStats(Dictionary<SliceInstance, ResultData> diccSet)
        {
            var sbGeneral = new StringBuilder();
            AddToSB(sbGeneral, diccSet, 1);
            AddToSB(sbGeneral, diccSet, 2);
            AddToSB(sbGeneral, diccSet, 3);
            AddToSB(sbGeneral, diccSet, 4);
            AddToSB(sbGeneral, diccSet, 5);
            sbGeneral.AppendLine("");
            
            File.AppendAllText($@"C:\temp\ResultsStats.txt", sbGeneral.ToString());
        }

        static void AddToSB(StringBuilder sbGeneral, Dictionary<SliceInstance, ResultData> diccSet, int prop)
        {
            sbGeneral.AppendLine("");
            switch(prop)
            {
                case 1:
                    sbGeneral.AppendLine("TABLA UNIÓN EXEC");
                    break;
                case 2:
                    sbGeneral.AppendLine("TABLA UNIÓN SLICE");
                    break;
                case 3:
                    sbGeneral.AppendLine("TABLA INTER SLICE");
                    break;
                case 4:
                    sbGeneral.AppendLine("TABLA SPECIAL COEF");
                    break;
                case 5:
                    sbGeneral.AppendLine("SPECIAL COEF MAX");
                    break;
            }
            
            foreach (var x in diccSet.OrderBy(x => x.Key))
            {
                var sb = new StringBuilder();
                sb.Append($"{x.Key.ToString()} ");
                foreach (var y in x.Value.Stats.OrderBy(y => y.Key))
                {
                    switch (prop)
                    {
                        case 1:
                            sb.Append($"{y.Value.UnionExec} ");
                            break;
                        case 2:
                            sb.Append($"{y.Value.UnionSlice} ");
                            break;
                        case 3:
                            sb.Append($"{y.Value.InterSlice} ");
                            break;
                        case 4:
                            if (y.Key == x.Key)
                                sb.Append($" ");
                            else
                                sb.Append($"{y.Value.SpecialCoef} ");
                            break;
                        case 5:
                            if (y.Key == x.Key)
                                sb.Append($" ");
                            else
                            {
                                var max_al_reves = diccSet[y.Key].Stats[x.Key].SpecialCoef;
                                var max = Math.Max(y.Value.SpecialCoef, max_al_reves);

                                sb.Append($"{max} ");
                            }
                            break;
                    }
                }
                    
                sbGeneral.AppendLine(sb.ToString());
            }
        }

        #region Classes
        public class OutputResult
        {
            public int ExecStmt { get; set; }
            public int Slice { get; set; }
            public int SliceOPT { get; set; }
            public int TotalTraza { get; set; }
            public int TrazaSalteada { get; set; }
        }

        public class SliceInstance : IComparable
        {
            public bool summaries { get; set; }
            public int line{ get; set; }
            public int iterationNumber { get; set; }

            public SliceInstance(bool summaries, int line, int iterationNumber)
            {
                this.summaries = summaries;
                this.line = line;
                this.iterationNumber = iterationNumber;
            }

            public override bool Equals(object obj)
            {
                var other = (SliceInstance)obj;
                return other.summaries == this.summaries && other.line == this.line && other.iterationNumber == this.iterationNumber;
            }

            public override int GetHashCode()
            {
                return (summaries ? 1 : 0 + 1) * 33 + line * 13 + iterationNumber * 15;
            }

            public override string ToString()
            {
                var summaries_text = summaries ? "Yes" : "No";
                return $"{summaries_text}_{line}_{iterationNumber}";
            }

            public int CompareTo(object obj)
            {
                if (obj == null) 
                    return 1;

                SliceInstance other = obj as SliceInstance;
                if (other != null)
                {
                    if (this.Equals(other))
                        return 0;
                    if (this < other)
                        return -1;
                    return 1;
                }
                else
                    throw new Exception();
            }

            public static bool operator <(SliceInstance a, SliceInstance b)
            {
                if (!a.summaries && b.summaries)
                    return true;
                if (a.summaries && !b.summaries)
                    return false;

                if (a.line < b.line)
                    return true;
                if (a.line > b.line)
                    return false;

                return a.iterationNumber < b.iterationNumber;
            }
            public static bool operator >(SliceInstance a, SliceInstance b)
            {
                var isLess = a < b;
                var isEquals = a.Equals(b);
                return !isEquals && !isLess;
            }
        }

        public class ResultData
        {
            public string input { get; set; }

            public int trace_orig { get; set; }

            public int trace_ext { get; set; }

            public int group_id { get; set; }

            public HashSet<string> ExecutedLines { get; set; }
            public HashSet<string> ResultLines { get; set; }

            public Dictionary<SliceInstance, IntersectionAndUnionData> Stats { get; set; }
        }

        public class IntersectionAndUnionData
        {
            public int group_id { get; set; }

            public int UnionExec { get; set; }
            public int InterExec { get; set; }
            public int UnionSlice { get; set; }
            public int InterSlice { get; set; }

            public double SpecialCoef { get; set; }

            public List<string> slice_interseccion_ejecutadas { get; set; }
            public List<string> interseccion_slices { get; set; }
            public List<string> slice_interseccion_ejecutadas_menos_interseccion_slices { get; set; }
        }
        #endregion

        #region OLD

        /*
         // Cómputo de información especial (relevancia de slices)
            // 1) Intersección ejecución en el 100% de los casos
            // 2) Intersección ejecución en al menos el 90% de los casos
            // 3) Intersección slice en el 100% de los casos
            // 4) Intersección slice en al menos 90% de los casos

            //var set1_exec_100 = new HashSet<string>();
            //var set1_exec_90 = new HashSet<string>();
            //var set1_slice_100 = new HashSet<string>();
            //var set1_slice_90 = new HashSet<string>();
            //foreach (var execLine in mainSetExec)
            //{
            //    var cantExecLine = diccSet.Values.Where(x => x.ExecutedLines.Contains(execLine)).Count();
            //    var cantSliceLine = diccSet.Values.Where(x => x.ResultLines.Contains(execLine)).Count();

            //    if (cantExecLine == diccSet.Count)
            //        set1_exec_100.Add(execLine);
            //    if (cantExecLine >= diccSet.Count * 0.9)
            //        set1_exec_90.Add(execLine);

            //    if (cantSliceLine == diccSet.Count)
            //        set1_slice_100.Add(execLine);
            //    if (cantSliceLine >= diccSet.Count * 0.9)
            //        set1_slice_90.Add(execLine);
            //}
         */

        /*
         // Archivo con detalles
            var nsb = new StringBuilder();
            foreach (var x in diccSet.OrderBy(x => x.Value.group_id))
            {
                foreach (var y in x.Value.Stats)
                {
                    if (y.Key != x.Key)
                    {
                        nsb.AppendLine("");
                        nsb.AppendLine($"GRUPO {x.Value.group_id} VS {y.Value.group_id} ");
                        nsb.AppendLine("SLICE INTERSECCIÓN EJECUTADAS");
                        nsb.AppendLine(String.Join(",", y.Value.slice_interseccion_ejecutadas.Select(t => t.ToString())));
                        nsb.AppendLine("INTERSECCIÓN DE SLICES");
                        nsb.AppendLine(String.Join(",", y.Value.interseccion_slices.Select(t => t.ToString())));
                        nsb.AppendLine("DIFERENCIA COEF. ESPECIAL");
                        nsb.AppendLine(String.Join(",", y.Value.slice_interseccion_ejecutadas_menos_interseccion_slices.Select(t => t.ToString())));
                    }
                }
            }
            File.AppendAllText($@"C:\temp\Deglose_{lastDirectoryName}.txt", nsb.ToString());

            //Console.WriteLine("");
            //Console.WriteLine("Cantidad de líneas distintas ejecutadas " + mainSetExec.Count.ToString());
            //Console.WriteLine("Cantidad de líneas distintas sliceadas " + mainSetSlice.Count.ToString());
            //Console.WriteLine("");
            //Console.WriteLine("Intersecciones");
            //Console.WriteLine("Intersección ejecutadas (100%): " + set1_exec_100.Count.ToString());
            //Console.WriteLine("Intersección ejecutadas (90%): " + set1_exec_90.Count.ToString());
            //Console.WriteLine("Intersección sliceadas (100%): " + set1_slice_100.Count.ToString());
            //Console.WriteLine("Intersección sliceadas (90%): " + set1_slice_90.Count.ToString());
         */

        /* 
         else
            {

                var mainPath = @"C:\Users\Camilo\Desktop\GitHub\PowerShellResults";
                foreach (var directory in Directory.GetDirectories(mainPath).Where(x => !x.EndsWith("_OPT")))
                {
                    Console.WriteLine(Path.GetFileName(directory));
                    var summary_file_path = Path.Combine(directory, "Powershell", "Summary.txt");
                    var result_list = new List<OutputResult>();
                    foreach (var l in File.ReadAllLines(summary_file_path))
                    {
                        // Nombre|Inputs|#Stmt|#UniqueStmt|#Slice|T. Instr|T. traza|T. Ejecución|Size Traza|Líneas Traza|Líneas Salteadas|%Salteado
                        var data = l.Split('|');
                        result_list.Add(new OutputResult()
                        {
                            ExecStmt = int.Parse(data[3]),
                            Slice = int.Parse(data[4]),
                            SliceOPT = 0,
                            TotalTraza = int.Parse(data[9]),
                            TrazaSalteada = 0
                        });
                    }

                    summary_file_path = Path.Combine(directory + "_OPT", "Powershell", "Summary.txt");
                    var i = 0;
                    foreach (var l in File.ReadAllLines(summary_file_path))
                    {
                        // Nombre|Inputs|#Stmt|#UniqueStmt|#Slice|T. Instr|T. traza|T. Ejecución|Size Traza|Líneas Traza|Líneas Salteadas|%Salteado
                        var data = l.Split('|');
                        result_list[i].SliceOPT = int.Parse(data[4]);
                        result_list[i].TotalTraza = int.Parse(data[9]);
                        result_list[i].TrazaSalteada = int.Parse(data[10]);
                        i++;
                    }

                    // Lo que queremos mostrar: Exec Stmt |	Slice |	% Total (se deja vacío) | Slice OPT	| % Total | Total Traza | Traza Salteada | % Salteado (se deja vacío)
                    foreach (var d in result_list)
                        Console.WriteLine($"{d.ExecStmt}|{d.Slice}||{d.SliceOPT}||{d.TotalTraza}|{d.TrazaSalteada}|");
                }
            }
         */

        #endregion
    }
}
