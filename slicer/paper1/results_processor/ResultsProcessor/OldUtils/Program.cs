﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class Program
    {
        static void Main(string[] args)
        {
            var parent_path = @"C:\Users\lafhis\Desktop\Slicer\roslyn-slicer\results\InvalidUnicodeString_Separated";
            var newSummary = @"C:\Users\lafhis\Desktop\Slicer\roslyn-slicer\results\InvalidUnicodeString_Separated\MergedSummary.txt";
            var paths = System.IO.Directory.GetDirectories(parent_path).Select(x => System.IO.Path.Combine(x, "InvalidUnicodeString_Asserts_WithSummaries_Clusters_Temp")).ToArray();

            var diccSet = new Dictionary<RecolectDataFromMultipleCriteria.SliceInstance, string>();
            foreach (var path in paths)
            {
                var file_name = Path.GetFileName(Directory.GetParent(path).Name);
                var summaries = file_name.Split('_')[0] == "WithSummaries";
                var criteria = int.Parse(file_name.Split('_')[1]);
                var iterationNumber = int.Parse(file_name.Split('_')[2]);
                if (iterationNumber > 2)
                    iterationNumber -= 2;

                var instance = new RecolectDataFromMultipleCriteria.SliceInstance(summaries, criteria, iterationNumber);
                var iSummary = Path.Combine(path, "ISummary.txt");
                
                // Last lines
                var allSummaryLines = File.ReadAllLines(iSummary).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

                diccSet.Add(instance, allSummaryLines.Single());
            }

            var sb = new StringBuilder();
            foreach (var y in diccSet.OrderBy(y => y.Key))
            {
                var allVals = y.Value.Split('|').ToList();
                var newList = new List<string>();
                newList.Add(y.Key.ToString());
                newList.AddRange(allVals.GetRange(1, allVals.Count - 1));
                sb.AppendLine(string.Join("|", newList));
            }
            File.AppendAllText(newSummary, sb.ToString());
        }

        static void Main_ORIG(string[] args)
        {
            var temp_path = @"C:\Users\lafhis\Desktop\Slicer\roslyn-slicer\results\InvalidUnicodeString_Separated";

            var paths = new string[] {
                //@"C:\Users\lafhis\Desktop\Slicer\roslyn-slicer\results\InvalidUnicodeString_Asserts_WithSummaries_Clusters_Asserts",
            };
            paths = System.IO.Directory.GetDirectories(temp_path).Select(x => System.IO.Path.Combine(x, "InvalidUnicodeString_Asserts_WithSummaries_Clusters_Temp")).ToArray();

            foreach (var path in paths)
            {
                var currentSummary = Path.Combine(path, "Summary.txt");
                var newSummary = Path.Combine(path, "ISummary.txt");
                var executedLinesForUser = Path.Combine(path, "ExecutedLinesForUser.txt");
                var results = Path.Combine(path, "Results.txt");
                var iResults = Path.Combine(path, "IResults.txt");
                var filteredResults = Path.Combine(path, "FilteredResults.txt");
                var iFilteredResults = Path.Combine(path, "IFilteredResults.txt");

                var intersectedResults = IntersectResultWithExecuted(executedLinesForUser, results, iResults);
                var intersectedFilteredResults = IntersectResultWithExecuted(executedLinesForUser, filteredResults, iFilteredResults);

                // Last lines
                var allSummaryLines = File.ReadAllLines(currentSummary).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                var lastSummaryLines = allSummaryLines.GetRange(allSummaryLines.Count - intersectedResults.Count, intersectedResults.Count);

                var sb = new StringBuilder();
                for (var i = 0; i < intersectedResults.Count; i++)
                {
                    var currentLine = lastSummaryLines[i].Split('|').ToList();
                    currentLine[4] = intersectedResults[i].ToString();
                    sb.AppendLine(string.Join("|", currentLine));
                }
                File.AppendAllText(newSummary, sb.ToString());
            }
        }

        #region OLD
        //static void Main(string[] args)
        //{
        //    var basepath = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\output\";
        //    foreach (var p in new string[] { "BH", "BiSort", "Em3d", "Health", "MST", "Power", "TreeAdd", "TSP", "Voronoi" })
        //    {
        //        args = new string[] {
        //            p,
        //            "999",
        //            "888",
        //            @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\src\Java\" + p,
        //            System.IO.Directory.GetDirectories(basepath + @"DynAbsResults\Default").Where(x => x.Contains(p)).First(),
        //            System.IO.Directory.GetDirectories(basepath + @"DynAbsResults\Opt").Where(x => x.Contains(p)).First(),
        //            $@"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\output\JavaSlicerResults\{p}\SummaryResultsFile",
        //            @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table1\output\final_lines.txt"
        //        };
        //        ExMain(args);
        //    }
        //}

        //static void Main(string[] args)
        //{

        //    //return;

        //    var arr_args = new string[][] {
        //    //    new string[] {
        //    //    "FormationJPA",
        //    //    "0",
        //    //    "0",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\Java\FormationJPA\src",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Default\FormationJPA\",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Opt\FormationJPA\",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\JavaSlicerResults\FormationJPA\SummaryResultsFile",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\final_lines.txt"
        //    //},
        //    //    new string[] {
        //    //    "MavenJPAHibernateLog4jDemo",
        //    //    "0",
        //    //    "0",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\Java\MavenJPAHibernateLog4jDemo\src",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Default\MavenJPAHibernateLog4jDemo\",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Opt\MavenJPAHibernateLog4jDemo\",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\JavaSlicerResults\MavenJPAHibernateLog4jDemo\SummaryResultsFile",
        //    //    @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\final_lines.txt"
        //    //},
        //    new string[] {
        //        "OrderLibrary",
        //        "0",
        //        "0",
        //        @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\src\Java\OrderLibrary\src",
        //        @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Default\OrderLibrary\",
        //        @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\Opt\OrderLibrary\",
        //        @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\JavaSlicerResults\OrderLibrary\SummaryResultsFile",
        //        @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\final_lines.txt"
        //    }
        //    };

        //    foreach(var a in arr_args)
        //    { 

        //    args = a;

        //    // Que reciba: 
        //    // 1) Programa
        //    // 2) Input
        //    // 3) Tiempo ejecución externo
        //    // 4) Carpeta del código Java (para filtrar archivos)
        //    // 5) Carpeta de default results
        //    // 6) Carpeta de opt results
        //    // 7) Path de Javaslicer summary
        //    // Que devuelva:
        //    // Por cada criterio una línea final (deberían coincidir todos los números), los guarda en:
        //    // 8) Path del resultado

        //    var programa = args[0];
        //    var input = args[1];
        //    // Tiempo de ejecución por fuera del slicer
        //    var tiempo_exec_default = Double.Parse(args[2]);
        //    var folder_java_src = args[3];
        //    var folder_dynabs_default = args[4];
        //    var folder_dynabs_opt = args[5];
        //    var summary_javaslicer = args[6];
        //    var results_path = args[7];

        //    // 1) Levantamos todo lo de DynAbs Default
        //    var summaries_default = ProcessNETSlicerResults(folder_dynabs_default, null);
        //    // 2) Levantamos todo lo de DynAbs OPT
        //    var summaries_opt = ProcessNETSlicerResults(folder_dynabs_opt, null);
        //    // 3) Levantamos los resultados de JavaSlicer
        //    var summaries_javaslicer = GetSummaryInfoJavaSlicer(folder_java_src, summary_javaslicer);
        //    // 4) Por cada uno agregamos una línea final
        //    for (var i = 0; i < summaries_default.Count; i++)
        //    {
        //        /*
        //        Lista de datos que tenemos que tener:

        //        General:
        //        * Nombre del programa (Input)
        //        * Input (Input)
        //        * Criteria (Input)
        //        * Executed Lines (Output de DynAbs)
        //        * Java Exec. Time (Output de PASO 2)
        //        * .NET Exec. Time (Output de PASO 1)

        //        DynAbs (general):
        //        * Instr. time
        //        * Generation trace time
        //        * Trace size (KB)
        //        * Trace lines
        //        * #Stmt

        //        DynAbs (sin compresión):
        //        * Analysis Time
        //        * #Small Slice
        //        * % Total
        //        * #Slice
        //        * % Total

        //        DynAbs (con compresión):
        //        * Analysis Time
        //        * #Traza salteada
        //        * % Salteado
        //        * #Small Slice
        //        * % Total
        //        * #Slice
        //        * % Total

        //        Javaslicer:
        //        * Exec. time
        //        * Trace Size (KB)
        //        * Analysis Time
        //        * #Slice
        //        * % Total    
        //        */

        //        var javaslicer_percentage = Math.Round((double)summaries_javaslicer[i].sliced_lines * 100 / (double)summaries_default[i].distinctStatementLines);
        //        File.AppendAllLines(results_path, new string[] { $"{programa}|{input}|{i+1}|{summaries_default[i].distinctStatementLines}|{summaries_javaslicer[i].tiempoOriginal}|" +
        //            $"{tiempo_exec_default}|{summaries_default[i].totalSecondsInstrumentation.ToString("N0")}|{summaries_default[i].generationTraceTime.ToString("N0")}|{summaries_default[i].traceSize}|{summaries_default[i].totalTraceLines}|{summaries_default[i].totalStatementsLines}|" +
        //            $"{summaries_default[i].executionSeconds.ToString("N0")}|{summaries_default[i].computed_filtered_statements}|{summaries_default[i].computed_filtered_percentage}|{summaries_default[i].computed_sliced_statements}|{summaries_default[i].computed_sliced_percentage}|" +
        //            $"{summaries_opt[i].executionSeconds.ToString("N0")}|{summaries_opt[i].skippedTrace}|{summaries_opt[i].skippedLinesPercent}|{summaries_opt[i].computed_filtered_statements}|{summaries_opt[i].computed_filtered_percentage}|{summaries_opt[i].computed_sliced_statements}|{summaries_opt[i].computed_sliced_percentage}|" +
        //            $"{summaries_javaslicer[i].tiempoTraza}|{summaries_javaslicer[i].tamaño_traza}|{summaries_javaslicer[i].tiempoSlice}|{summaries_javaslicer[i].sliced_lines}|{javaslicer_percentage}"
        //        });
        //    }

        //        // El objetivo es producir la tabla final, necesitamos los 3 paths
        //        //var ORBSPathResults = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\ORBS\Results";
        //        //var NETPathResults = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\NET\RESULTS";
        //        //var JavaslicerPathResults = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Java\SummaryResultsFile";

        //        //var ORBSPathResults = @"C:\Users\Camilo\Desktop\Slicer\experiments\slicer\paper1\table1\Results\ORBS";
        //        //var NETPathResults = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\NET\RESULTS"; //_DEFAULT
        //        //var JavaslicerPathResults = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Java\SummaryResultsFile";

        //        //var ORBSResults = ProcessORBSResults(ORBSPathResults);
        //        //var NETResults = ProcessNETSlicerResults(NETPathResults, null /*ORBSResults*/);
        //        //var JavaResults = ProcessJavaslicerResults(JavaslicerPathResults, ORBSResults);

        //        //NETResults.ForEach(x => Console.WriteLine($"{x.Name}|{x.Arguments}|{x.DistinctExecutedLines}|{x.ExecTime}|{x.SlicedLines}|{x.FilteredSlicedLines}|{x.OPT}"));
        //    }
        //    return;
        //}
        #endregion

        public class Info
        {
            public string Name { get; set; }
            public string Arguments { get; set; }
            public string Data { get; set; }
            public string[] FilesToSlice { get; set; }
            public int DistinctExecutedLines { get; set; }
            public int SlicedLines { get; set; }
            public int FilteredSlicedLines { get; set; }
            public double ExecTime { get; set; }
            public int OPT { get; set; }
        }

        public static List<int> IntersectResultWithExecuted(string executedLinesPath, string resultPath, string newResultPath)
        {
            var toReturn = new List<int>();
            var executed_lines = File.ReadAllLines(executedLinesPath).Where(x => x.Contains(".cs")).ToList();
            var sliced_lines_all = SplitLines(File.ReadAllLines(resultPath).ToList(), (string x) => !x.Contains(":"));

            var sb = new StringBuilder();
            for (var i = 0; i < sliced_lines_all.Count; i++)
            {
                var sliced_lines = sliced_lines_all[i].Where(x => x.Contains(".cs")).ToList();
                var final_sliced_lines = sliced_lines.Intersect(executed_lines).ToList();

                foreach (var line in final_sliced_lines)
                    sb.AppendLine(line);

                sb.AppendLine("---------------------");

                toReturn.Add(final_sliced_lines.Count);
            }

            File.WriteAllText(newResultPath, sb.ToString());
            return toReturn;
        }

        public static List<SummaryInfoDynAbs> ProcessNETSlicerResults(string path, List<Info> extra_info)
        {
            //var summaryLines = new List<Info>();
            //foreach (var directory in Directory.GetDirectories(path))...

            var skipCriteriaDict = new Dictionary<string, List<Tuple<string, int>>>()
            {
                { "TestSwitchWithMultipleCases", new List<Tuple<string, int>>() 
                    {
                        new Tuple<string, int>("StatementParsingTests", 2201),
                        new Tuple<string, int>("StatementParsingTests", 2212),
                    } 
                },
                { "TestSwitchWithMultipleLabelsOnOneCase", new List<Tuple<string, int>>()
                    {
                        new Tuple<string, int>("StatementParsingTests", 2281),
                        new Tuple<string, int>("StatementParsingTests", 2287),
                    }
                },
                { "TestSwitchWithMultipleStatementsOnOneCase", new List<Tuple<string, int>>()
                    {
                        new Tuple<string, int>("StatementParsingTests", 2323),
                    }
                }
            };

            //var executedLinesForUserFile = "ExecutedLinesWithoutHeadersForUser.txt";
            var executedLinesForUserFile = "ExecutedLinesForUser.txt";

            /*ExecutedStatmentsCounter|PropertiesOrFieldsInitializationCounter|MethodsCounter|ClassesCounter*/
            var executionCounters = GetExecutionCounters(Path.Combine(path, "ExecutionCounters.txt"));
            var stmtMethodsAndClassesCounter = executionCounters != null ?
                executionCounters[0] + executionCounters[2] + executionCounters[3] : (int?)null;
            var stmtCounter = executionCounters != null ?
                executionCounters[0] : (int?)null;

            var summaries = GetSummaryInfoDynAbs(path);
            var executedLines = 
                File.Exists(Path.Combine(path, executedLinesForUserFile)) ?
                    ParseExecutedLinesFile(File.ReadAllLines(Path.Combine(path, executedLinesForUserFile))) :
                    ParseExecutedLinesFile(File.ReadAllLines(Path.Combine(path, "ExecutedLines.txt")));
            var slices = summaries.Count > 0 ? ParseResultFile(File.ReadAllLines(Path.Combine(path, "Results.txt")).ToList()).ToList() : new List<KeyValuePair<Criterion, List<FileLine>>>();
            var filteredSlices = summaries.Count > 0 ? ParseResultFile(File.ReadAllLines(Path.Combine(path, "FilteredResults.txt")).ToList()).ToList() : new List<KeyValuePair<Criterion, List<FileLine>>>();

            summaries = summaries.GetRange(summaries.Count - slices.Count, slices.Count).ToList();

            for (var i = 0; i < slices.Count; i++)
            {
                var summary = summaries[i];

                var distinctLines = executedLines.Distinct().Count();
                var finalSlicedLines = slices[i].Value.Intersect(executedLines).ToArray();
                var finalFilteredSlicedLines = filteredSlices[i].Value.Intersect(executedLines).ToArray();

                summary.distinctStatementLines = distinctLines;
                summary.computed_sliced_statements = finalSlicedLines.Count();
                summary.computed_sliced_percentage = Math.Round((double)summary.computed_sliced_statements * 100 / (double)summary.distinctStatementLines);
                summary.computed_filtered_statements = finalFilteredSlicedLines.Count();
                summary.computed_filtered_percentage = Math.Round((double)summary.computed_filtered_statements * 100 / (double)summary.distinctStatementLines);

                summary.FilteredResult = finalFilteredSlicedLines.ToList();
                summary.executedLines = executedLines;

                // Full execution
                summary.fullExecutionTotalLines = stmtMethodsAndClassesCounter;
                summary.fullExecutionTotalStatementsLines = stmtCounter;

                summary.criterion = slices[i].Key;
            }

            foreach(var kv in skipCriteriaDict)
                if (path.Contains(kv.Key))
                {
                    var indexToRemove = new List<int>();
                    for (var i = summaries.Count - 1; i >= 0 ; i--)
                    {
                        var s = summaries[i];
                        var toRemove = kv.Value.Any(x => x.Item2 == s.criterion.FileLine.Line && x.Item1 + ".cs" == s.criterion.FileLine.File);
                        if (toRemove)
                            indexToRemove.Add(i);
                    }
                    foreach (var j in indexToRemove)
                        summaries.RemoveAt(j);
                }

            return summaries;

            #region OLD
            /* ANTERIOR:
             if (extra_info != null)
                {
                    var files_to_slice = extra_info.Where(x => x.Name == summary.name && x.Arguments == summary.arguments).Single().FilesToSlice;
                    sliced_lines = File.ReadAllLines(Path.Combine(path, "Results.txt"))
                        .Where(x => x.Contains(".cs") && files_to_slice.Contains(Path.GetFileNameWithoutExtension(x.Split(':')[0]))).ToArray();
                    executed_lines = File.ReadAllLines(Path.Combine(path, "ExecutedLines.txt"))
                        .Where(x => x.Contains(".cs") && files_to_slice.Contains(Path.GetFileNameWithoutExtension(x.Split(':')[0]))).ToArray();
                }

            // Y por otro lado..
            // Levantamos el filtered result y lo filtramos:
                var files_to_exclude = new string[] { "Program.cs" };
                //executed_lines = executed_lines_file
                //.Where(x => x.Contains(".cs") && !files_to_exclude.Contains(x.Split(':')[0])).ToArray();

            // El filtro se aplica a todos los conjuntos si está.
             */
            #endregion
        }

        public static List<SummaryInfoDynAbs> ProcessOLDNETSlicerResults(string path, List<Info> extra_info)
        {
            var summaries = GetSummaryInfoDynAbs(path);
            var executedLines = ParseExecutedLinesFile(File.ReadAllLines(Path.Combine(path, "ExecutedLines.txt")));
            var slices = summaries.Count > 0 ? ParseOLDResultFile(File.ReadAllLines(Path.Combine(path, "Results.txt")).ToList()).ToList() : new List<List<FileLine>>();
            var filteredSlices = summaries.Count > 0 ? ParseOLDResultFile(File.ReadAllLines(Path.Combine(path, "FilteredResults.txt")).ToList()).ToList() : new List<List<FileLine>>();

            summaries = summaries.GetRange(summaries.Count - slices.Count, slices.Count).ToList();

            for (var i = 0; i < slices.Count; i++)
            {
                var summary = summaries[i];

                var finalSlicedLines = slices[i].Intersect(executedLines).ToArray();
                var finalFilteredSlicedLines = filteredSlices[i].Intersect(executedLines).ToArray();

                summary.computed_sliced_statements = finalSlicedLines.Count();
                summary.computed_sliced_percentage = Math.Round((double)summary.computed_sliced_statements * 100 / (double)summary.distinctStatementLines);
                summary.computed_filtered_statements = finalFilteredSlicedLines.Count();
                summary.computed_filtered_percentage = Math.Round((double)summary.computed_filtered_statements * 100 / (double)summary.distinctStatementLines);

                summary.criterion = new Criterion() { Iteration = i };

                summary.FilteredResult = finalFilteredSlicedLines.ToList();
                summary.executedLines = executedLines.ToList();
            }

            return summaries;
        }

        public static List<FileLine> ParseExecutedLinesFile(string[] lines)
        {
            return lines.Select(x => new FileLine(x)).ToList();
        }

        static Dictionary<Criterion, List<FileLine>> ParseResultFile(List<string> list)
        {
            var result = new Dictionary<Criterion, List<FileLine>>();
            var result_WithoutCriterion = new List<List<FileLine>>();
            var currentList = new List<FileLine>();
            foreach (var line in list.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                if (line.Contains("----"))
                {
                    if (line.Contains(":"))
                    { 
                        // ---- Last slice: [StatementParsingTests.cs - 956]
                        var s1 = line.Split('[');
                        var file = s1.Last().Split(' ').First();
                        var s2 = s1.Last().Split('-').Last();
                        var lineNumber = int.Parse(s2.Substring(1, s2.Length - 2));
                        var initialIteration = 1;
                        var nextCriterion = new Criterion()
                        {
                            FileLine = new FileLine(file, lineNumber),
                            Iteration = initialIteration
                        };
                        while (result.ContainsKey(nextCriterion))
                            nextCriterion.Iteration++;
                        result.Add(nextCriterion, currentList);
                        currentList = new List<FileLine>();
                    }
                    else
                    {
                        result_WithoutCriterion.Add(currentList);
                        currentList = new List<FileLine>();
                    }
                }   
                else
                    currentList.Add(new FileLine(line));
            }
            return result;
        }

        public static List<List<FileLine>> ParseOLDResultFile(List<string> list)
        {
            var result = new List<List<FileLine>>();
            var currentList = new List<FileLine>();
            foreach (var line in list.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                if (line.Contains("----"))
                {
                    result.Add(currentList);
                    currentList = new List<FileLine>();
                }
                else
                    currentList.Add(new FileLine(line));
            }
            if (currentList.Count > 0)
                result.Add(currentList);
            return result;
        }

        static List<List<string>> SplitLines(List<string> list, Func<string, bool> criteria)
        {
            var result = new List<List<string>>();
            result.Add(new List<string>());
            foreach (var line in list.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                if (criteria(line))
                    result.Add(new List<string>());
                else
                    result.Last().Add(line);
            }
            if (result.Last().Count == 0)
                result.RemoveAt(result.Count - 1);
            return result;
        }

        public static List<SummaryInfoDynAbs> GetSummaryInfoDynAbs(string directory)
        {
            var listResult = new List<SummaryInfoDynAbs>();
            var fileName = Path.Combine(directory, "Summary.txt");
            if (!System.IO.File.Exists(fileName))
                return listResult;
            var lines = File.ReadAllLines(Path.Combine(directory, "Summary.txt"));
            foreach (var line in lines.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                var values = line.Split('|');
                var result = new SummaryInfoDynAbs();
                result.name = values[0];
                result.arguments = values[1];
                result.summaryTotalStatementsLines = Int32.Parse(values[2]);
                result.distinctStatementLines = Int32.Parse(values[3]);
                result.slicedStatements = Int32.Parse(values[4]);
                result.totalSecondsInstrumentation = Double.Parse(values[5].Replace(',', '.'));
                result.generationTraceTime = string.IsNullOrWhiteSpace(values[6]) ? 1 : Double.Parse(values[6].Replace(',', '.'));
                result.executionSeconds = Double.Parse(values[7].Replace(',', '.'));
                result.traceSize = Double.Parse(values[8]);
                result.totalTraceLines = Int32.Parse(values[9]);
                result.skippedTrace = values[10] == "" ? 0 : Int32.Parse(values[10]);
                result.skippedTracePercent = values[11] == "" ? 0 : Double.Parse(values[11]);
                result.totalTime = result.totalSecondsInstrumentation + result.generationTraceTime + result.executionSeconds;
                listResult.Add(result);
            }
            return listResult;
        }

        public static List<SummaryInfoJavaSlicer> GetSummaryInfoJavaSlicer(string javaSourcePath, string path)
        {
            var javaFiles = Directory.GetFiles(javaSourcePath, "*.java", SearchOption.AllDirectories).Select(x => Path.GetFileName(x)).ToList();

            var listResult = new List<SummaryInfoJavaSlicer>();
            var values = File.ReadAllLines(path);
            foreach (var line in values.Where(x => !string.IsNullOrWhiteSpace(x)))
            {
                var liner = line.Split('|');
                var result = new SummaryInfoJavaSlicer();
                result.name = liner[0];
                result.arguments = liner[1];
                result.tiempoOriginal = Double.Parse(liner[2]);
                result.tiempoTraza = Double.Parse(liner[3]);
                result.tiempoSlice = Double.Parse(liner[4]);
                result.total_time = Double.Parse(liner[5]);
                result.resultFile = liner[6];
                result.tamaño_traza = Double.Parse(liner[7]);

                //var basePath = @"C:\Users\Camilo\Desktop\Slicer\complete_configuration_example\Table2\output\JavaSlicerResults\FormationJPA";
                //var resFile = result.resultFile.Replace(
                //    @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table1",
                //    @"C:\Users\alexd\Desktop\Slicer\Olden");
                var resFile = result.resultFile.Replace(
                    @"C:\Users\alexd\Desktop\Slicer\Olden\",
                    @"C:\Users\alexd\Desktop\Slicer\Olden\output\Javaslicer\");

                var filteredLines = File.ReadAllLines(resFile).Where(x => x.Contains(".java") && javaFiles.Contains(x.Split(':')[0]));
                //File.WriteAllLines(Path.Combine(basePath, Path.GetFileName(result.resultFile)), filteredLines.ToArray());
                result.sliced_lines = filteredLines.Count();
                listResult.Add(result);
            }
            return listResult;
        }

        static List<Info> ProcessJavaslicerResults(string path, List<Info> extra_info)
        {
            // En este caso el path tiene todo, solo hay que contar las líneas que están en el archivo del campo "lines"
            var summaryLines = new List<Info>();
            var pathDirectory = Path.GetDirectoryName(path);
            var lines = File.ReadAllLines(path);
            foreach (var line in lines)
            {
                var allData = line.Split('|');
                var name = allData.First();
                var files_to_slice = extra_info.Where(x => x.Name == name && x.Arguments == allData[1]).Single().FilesToSlice;
                var correctLines = File.ReadAllLines(Path.Combine(pathDirectory, allData[6]))
                    .Where(x => x.Contains(".java") && files_to_slice.Contains(Path.GetFileNameWithoutExtension(x.Split(':')[0])));
                var correctLinesCounter = correctLines.Count();
                allData[6] = correctLinesCounter.ToString();
                summaryLines.Add(new Info()
                {
                    Name = name,
                    Arguments = allData[1],
                    Data = string.Join("|", allData)
                });
            }
            return summaryLines.OrderBy(x => x.Name).ThenBy(x => x.Arguments).ToList();
        }

        static List<Info> ProcessORBSResults(string path)
        {
            var summaryLines = new List<Info>();
            // Recibe el path donde están las carpetas resultado
            foreach (var directory in Directory.GetDirectories(path))
            {
                // Del path de ORBS podemos obtener los archivos que queremos slicear, importante para filtrar los slices de Java y NET
                var files_to_slice =
                    File.ReadAllLines(Path.Combine(directory, @"ORBS_CONFIG\config\", "OrbsFramework.properties")).ToList()[1]
                    .Split('=')[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();

                // El caso de estudio tiene un archivo "instance.txt" con el nombre y el input
                var instance_info = File.ReadAllLines(Path.Combine(directory, @"ORBS_CONFIG\config\", "instance.txt")).First().Split('|');
                var name = instance_info[0];
                var arguments = instance_info[1];
                var total_time = GetSecondsFromElapsedTime(File.ReadAllText(Path.Combine(directory, "time.log")).Split(' ')[2].Split('.')[0]);
                int methods_counter = 0, lines_counter = 0, total_lines = 0;
                LinesCounter.Program.CountCSharp(directory, files_to_slice, ref lines_counter, ref methods_counter, ref total_lines);
                var lines = methods_counter + lines_counter;
                var extra_info = File.ReadAllLines(Path.Combine(directory, "iternationLevelStat.csv")).Where(x => !string.IsNullOrWhiteSpace(x)).Last().Split(',');
                var compilations = int.Parse(extra_info[2]);
                var executions = int.Parse(extra_info[3]);
                var cached_compilations = int.Parse(extra_info[4]);
                var cached_executions = int.Parse(extra_info[5]);

                summaryLines.Add(new Info()
                {
                    Name = name,
                    Arguments = arguments,
                    Data = $"{name}|{arguments}|-|{total_time}|{lines}|{compilations}|{executions}",
                    FilesToSlice = files_to_slice
                });
            }
            return summaryLines.OrderBy(x => x.Name).ThenBy(x => x.Arguments).ToList();
        }

        public static string GetORBSLineByPath(string path)
        {
            var files_to_slice =
                    File.ReadAllLines(Path.Combine(path, @"ORBS_CONFIG\config\", "OrbsFramework.properties")).ToList()[1]
                    .Split('=')[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Path.GetFileNameWithoutExtension(x)).ToArray();
            int methods_counter = 0, lines_counter = 0, total_files = 0;
            LinesCounter.Program.CountCSharp(path, files_to_slice, ref lines_counter, ref methods_counter, ref total_files);
            var total_time = GetSecondsFromElapsedTime(File.ReadAllText(Path.Combine(path, "time.log")).Split(' ')[2].Split('.')[0]);
            var extra_info = File.ReadAllLines(Path.Combine(path, "iternationLevelStat.csv")).Where(x => !string.IsNullOrWhiteSpace(x)).Last().Split(',');
            var lines_deleted = int.Parse(extra_info[1]);
            var compilations = int.Parse(extra_info[2]);
            var executions = int.Parse(extra_info[3]);
            var cached_compilations = int.Parse(extra_info[4]);
            var cached_executions = int.Parse(extra_info[5]);
            return $"{total_time}|{lines_deleted}|{compilations}|{cached_compilations}|{executions}|{cached_executions}|{methods_counter+lines_counter}";
        }

        static int GetSecondsFromElapsedTime(string elapsedTime)
        {
            var data = elapsedTime.Split(':').Select(x => int.Parse(x)).ToList();
            int seconds = data.Last();
            int minutes = 0;
            int hours = 0;
            if (data.Count > 1)
                minutes = data[data.Count - 2];
            if (data.Count > 2)
                hours = data[data.Count - 3];
            return seconds + 60 * minutes + 60 * 60 * hours;
        }

        static List<int> GetExecutionCounters(string path)
        {
            if (File.Exists(path))
            {
                try
                { 
                    return File.ReadAllLines(path)[0].Split('|').Select(x => int.Parse(x)).ToList();
                }
                catch(Exception ex)
                {
                    throw;
                }
            }
            return null;
        }
    }
}
