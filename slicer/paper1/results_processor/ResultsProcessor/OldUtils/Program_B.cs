﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    class Program_B
    {
        public static void Main_Javaslicer()
        {
            var mainSb = new StringBuilder();
            //var results_path = @"C:\Users\alexd\Desktop\Slicer\Olden\output\final_lines_javaslicer.txt";
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output\JavaSlicerResults";
            var baseFolderSrc = @"C:\Users\alexd\Desktop\Slicer\Olden\src\Java\";
            var programs = new string[] { "BH", "BiSort", "Em3d", "Health", "MST", "Perimeter", "Power", "TreeAdd", "TSP", "Voronoi" };
            foreach (var program in programs)
            {
                var folder_java_src = Path.Combine(baseFolderSrc, program);
                var summary_javaslicer = Path.Combine(baseFolder, program, "SummaryResultsFile");
                var summaries_javaslicer = Program.GetSummaryInfoJavaSlicer(folder_java_src, summary_javaslicer);

                // Deglose
                if (true)
                { 
                    for (var i = 0; i < summaries_javaslicer.Count; i++)
                    {
                        mainSb.AppendLine($"{summaries_javaslicer[i].sliced_lines};;{summaries_javaslicer[i].tiempoTraza};{summaries_javaslicer[i].tiempoSlice};{summaries_javaslicer[i].tamaño_traza}");
                      
                        //new string[] { $"{program}|-l 12|{i+1}|204|{summaries_javaslicer[i].tiempoOriginal}|" +
                        //$"{summaries_javaslicer[i].tiempoTraza}|{summaries_javaslicer[i].tamaño_traza}|{summaries_javaslicer[i].tiempoSlice}|{summaries_javaslicer[i].sliced_lines}" });
                    }
                }
            }

            mainSb.AppendLine("================");

            foreach (var program in programs)
            {
                var folder_java_src = Path.Combine(baseFolderSrc, program);
                var summary_javaslicer = Path.Combine(baseFolder, program, "SummaryResultsFile");
                var summaries_javaslicer = Program.GetSummaryInfoJavaSlicer(folder_java_src, summary_javaslicer);

                // Agrupado
                if (true)
                {
                    // AVG Stmt. Slice	AVG %	MIN %	MAX %	Exec. time	Análisis	Traza (KB)
                    var avg_stmt_slice = summaries_javaslicer.Average(x => x.sliced_lines);
                    var min_stmt_slice = summaries_javaslicer.Min(x => x.sliced_lines);
                    var max_stmt_slice = summaries_javaslicer.Max(x => x.sliced_lines);
                    var avg_exec_time = summaries_javaslicer.Average(x => x.tiempoTraza);
                    var avg_analysis_time = summaries_javaslicer.Average(x => x.tiempoSlice);
                    var avg_traza = summaries_javaslicer.Max(x => x.tamaño_traza);

                    mainSb.AppendLine($"{avg_stmt_slice};;{min_stmt_slice};{max_stmt_slice};{avg_exec_time};{avg_analysis_time};{avg_traza}");
                }
            }

            //results_path, 
            Console.WriteLine(mainSb.ToString());
        }

        public static void Main_Javaslicer_Generalized()
        {
            var mainSb = new StringBuilder();
            //var results_path = @"C:\Users\alexd\Desktop\Slicer\Olden\output\final_lines_javaslicer.txt";
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output\Javaslicer";
            var baseFolderSrc = @"C:\Users\alexd\Desktop\Slicer\Olden\src\Java\";
            var programs = new string[] { "BH", "BiSort", "Em3d", "Health", "MST", "Perimeter", "Power", "TreeAdd", "TSP", "Voronoi" };

            #region Deglose
            if (false)
                foreach (var program in programs)
                {
                    var folder_java_src = Path.Combine(baseFolderSrc, program);
                    var summary_javaslicer = Path.Combine(baseFolder, program, "SummaryResultsFile");
                    var summaries_javaslicer = Program.GetSummaryInfoJavaSlicer(folder_java_src, summary_javaslicer);

                    // Deglose
                    if (false)
                    {
                        for (var i = 0; i < summaries_javaslicer.Count; i++)
                        {
                            mainSb.AppendLine($"{summaries_javaslicer[i].sliced_lines};;{summaries_javaslicer[i].tiempoTraza};{summaries_javaslicer[i].tiempoSlice};{summaries_javaslicer[i].tamaño_traza}");

                            //new string[] { $"{program}|-l 12|{i+1}|204|{summaries_javaslicer[i].tiempoOriginal}|" +
                            //$"{summaries_javaslicer[i].tiempoTraza}|{summaries_javaslicer[i].tamaño_traza}|{summaries_javaslicer[i].tiempoSlice}|{summaries_javaslicer[i].sliced_lines}" });
                        }
                    }
                }
            #endregion

            var dicAcums = new Dictionary<string, JavaslicerCounters>();
            foreach (var program in programs)
            {
                dicAcums[program] = new JavaslicerCounters();
                for (var i = 1; i <= 10; i++)
                { 
                    var folder_java_src = Path.Combine(baseFolderSrc, program);
                    var summary_javaslicer = Path.Combine(baseFolder, i.ToString(), "output", "JavaSlicerResults", program, "SummaryResultsFile");
                    var summaries_javaslicer = Program.GetSummaryInfoJavaSlicer(folder_java_src, summary_javaslicer);

                    // Agrupado
                    if (true)
                    {
                        // AVG Stmt. Slice	AVG %	MIN %	MAX %	Exec. time	Análisis	Traza (KB)
                        dicAcums[program].counter_stmt_slice += summaries_javaslicer.Count();
                        dicAcums[program].acum_stmt_slice += summaries_javaslicer.Sum(x => x.sliced_lines);
                        dicAcums[program].min_stmt_slice = Math.Min(dicAcums[program].min_stmt_slice, summaries_javaslicer.Min(x => x.sliced_lines));
                        dicAcums[program].max_stmt_slice = Math.Max(dicAcums[program].max_stmt_slice, summaries_javaslicer.Max(x => x.sliced_lines));
                        dicAcums[program].acum_exec_time += summaries_javaslicer.Sum(x => x.tiempoTraza);
                        dicAcums[program].acum_analysis_time += summaries_javaslicer.Sum(x => x.tiempoSlice);
                        dicAcums[program].acum_traza += summaries_javaslicer.Max(x => x.tamaño_traza);
                    }
                }
            }

            foreach (var program in programs)
                mainSb.AppendLine($"{dicAcums[program].avg_stmt_slice};;{dicAcums[program].min_stmt_slice};{dicAcums[program].max_stmt_slice};{dicAcums[program].avg_exec_time};{dicAcums[program].avg_analysis_time};{dicAcums[program].avg_traza}");

            Console.WriteLine(mainSb.ToString());
        }

        public static void Main()
        {
            // TABLA 1
            //var basePath = @"C:\Users\alexd\Desktop\Slicer\Olden";
            //var results_path = @"C:\Users\alexd\Desktop\Slicer\Olden\output\DynAbsResults\feb_2021.txt";
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output\DynAbsResults\";
            var programs = new string[] { "BH", "BiSort", "Em3d", "Health", "MST", "Perimeter", "Power", "TreeAdd", "TSP", "Voronoi" };

            // TABLA 2
            //var basePath = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table2";
            //var results_path = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults\feb_2021.txt";
            //var baseFolder = @"C:\Users\lafhis\Desktop\Slicer\complete_configuration_example\Table2\output\DynAbsResults";
            //var programs = new string[] { "MavenJPAHibernateLog4jDemo", "OrderLibrary", "FormationJPA", "Health", "MST", "Power", "TreeAdd", "TSP", "Voronoi" };

            var modes = new string[] { /*"Basic", "Annotations", "Speed", "Mixed",*/ "Clusters" };
            //{ "GuiCS", "LibLog_Log4Net", "LibLog_NLog", "EdgeDeflector" };

            var globalLines = new StringBuilder();
            foreach (var program in programs)
            {
                //// 1) Levantamos todo lo de DynAbs Default /*@"\Default"*/
                var path = System.IO.Directory.GetDirectories(baseFolder + "Clusters").Where(x => x.Contains(program)).First();
                var summaries_default = Program.ProcessOLDNETSlicerResults(path, null);
                var input = path.Length > path.IndexOf(program) + program.Length + 2 ? path.Substring(path.IndexOf(program) + program.Length + 1) : "";
                //// 2) Levantamos todo lo de DynAbs OPT
                //var summaries_opt = Program.ProcessNETSlicerResults(System.IO.Directory.GetDirectories(baseFolder + @"\Opt").Where(x => x.Contains(program)).First(), null);

                //var results = new List<List<SummaryInfoDynAbs>>();
                //var input = "";
                //var tiempo_exec_default = 0;
                //foreach (var mode in modes)
                //{
                //    var config = Path.Combine(baseFolder, System.IO.Directory.GetDirectories(baseFolder + @"\" + mode).Where(x => x.Contains(program)).First());
                //    input = config.Length > config.IndexOf(program) + program.Length + 2 ? config.Substring(config.IndexOf(program) + program.Length + 1) : "";
                //    var summaries = Program.ProcessOLDNETSlicerResults(config, null);
                //    results.Add(summaries);
                //}

                //for (var j = 0; j < results[0].Count; j++)
                //{
                //    //var localLines = new StringBuilder();
                //    for (var i = 0; i < results.Count; i++)
                //    {
                //        if (i == 0)
                //        {
                //            globalLines.Append($"{program}|{input}|{i + 1}|{results[i][j].distinctStatementLines}|" +
                //    $"{tiempo_exec_default}|{results[i][j].totalSecondsInstrumentation.ToString("N0")}|{results[i][j].generationTraceTime.ToString("N0")}|{results[i][j].traceSize}|{results[i][j].totalTraceLines}|{results[i][j].totalStatementsLines}|");
                //        }
                //        globalLines.Append($"{results[i][j].executionSeconds.ToString("N0")}|{results[i][j].computed_filtered_statements}|{results[i][j].computed_filtered_percentage}|{results[i][j].computed_sliced_statements}|{results[i][j].computed_sliced_percentage}|");
                //    }
                //    globalLines.AppendLine();
                //}

                //    var input = "";
                //    var tiempo_exec_default = 0;
                //    for (var i = 0; i < summaries_default.Count; i++)
                //    {
                //        File.AppendAllLines(results_path, new string[] { $"{program}|{input}|{i+1}|{summaries_default[i].distinctStatementLines}|" +
                //    $"{tiempo_exec_default}|{summaries_default[i].totalSecondsInstrumentation.ToString("N0")}|{summaries_default[i].generationTraceTime.ToString("N0")}|{summaries_default[i].traceSize}|{summaries_default[i].totalTraceLines}|{summaries_default[i].totalStatementsLines}|" +

                //    $"{summaries_default[i].executionSeconds.ToString("N0")}|{summaries_default[i].computed_filtered_statements}|{summaries_default[i].computed_filtered_percentage}|{summaries_default[i].computed_sliced_statements}|{summaries_default[i].computed_sliced_percentage}|" +
                //    $"{summaries_opt[i].executionSeconds.ToString("N0")}|{summaries_opt[i].skippedTrace}|{summaries_opt[i].skippedLinesPercent}|{summaries_opt[i].computed_filtered_statements}|{summaries_opt[i].computed_filtered_percentage}|{summaries_opt[i].computed_sliced_statements}|{summaries_opt[i].computed_sliced_percentage}"
                //});
                //    }

                // DEGLOSE
                if (true)
                { 
                    for (var i = 0; i < summaries_default.Count; i++)
                    {
                        globalLines.AppendLine($"{program};{input};{i+1};{summaries_default[i].totalStatementsLines};{summaries_default[i].distinctStatementLines};{summaries_default[i].computed_sliced_statements};{summaries_default[i].computed_sliced_percentage};{summaries_default[i].computed_filtered_statements};{summaries_default[i].computed_filtered_percentage};{summaries_default[i].totalSecondsInstrumentation};{summaries_default[i].executionSeconds};{summaries_default[i].totalTraceLines};{summaries_default[i].traceSize}");
                    };
                }
            }

            globalLines.AppendLine("================");

            foreach (var program in programs)
            {
                //// 1) Levantamos todo lo de DynAbs Default /*@"\Default"*/
                var path = System.IO.Directory.GetDirectories(baseFolder + "Clusters").Where(x => x.Contains(program)).First();
                var summaries_default = Program.ProcessOLDNETSlicerResults(path, null);
                var input = path.Length > path.IndexOf(program) + program.Length + 2 ? path.Substring(path.IndexOf(program) + program.Length + 1) : "";
                //// 2) Levantamos todo lo de DynAbs OPT
                
                // AGRUPADO
                if (true)
                {
                    var avg_stmt_exec = summaries_default.Average(x => x.totalStatementsLines);
                    var avg_stmt_distintos = summaries_default.Average(x => x.distinctStatementLines);
                    // Primer slice
                    var avg_slice = summaries_default.Average(x => x.computed_sliced_statements);
                    var avg_slice_perc = summaries_default.Average(x => x.computed_sliced_percentage);
                    var min_slice_perc = summaries_default.Min(x => x.computed_sliced_percentage);
                    var max_slice_perc = summaries_default.Max(x => x.computed_sliced_percentage);
                    // Segundo slice
                    var avg_filtered = summaries_default.Average(x => x.computed_filtered_statements);
                    var avg_filtered_perc = summaries_default.Average(x => x.computed_filtered_percentage);
                    var min_filtered_perc = summaries_default.Min(x => x.computed_filtered_percentage);
                    var max_filtered_perc = summaries_default.Max(x => x.computed_filtered_percentage);
                    // Tiempos y traza
                    var avg_instr = summaries_default.First().totalSecondsInstrumentation;
                    var avg_analisis = summaries_default.Average(x => x.executionSeconds);
                    var avg_traza_lineas = summaries_default.Average(x => x.totalTraceLines);
                    var avg_traza_size = summaries_default.Average(x => x.traceSize);

                    globalLines.AppendLine($"{program};{input};{summaries_default.Count};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size}");
                }
            }

            Console.WriteLine(globalLines.ToString());
            //File.AppendAllText(results_path, globalLines.ToString());
        }

        public static void MainGeneralized()
        {
            // TABLA 1
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output\DynAbsResults\";
            var programs = new string[] { "BH", "BiSort", "Em3d", "Health", "MST", "Perimeter", "Power", "TreeAdd", "TSP", "Voronoi" };
            var modes = new string[] { "Clusters" };

            var globalLines = new StringBuilder();

            #region Deglose
            if (false)
                foreach (var program in programs)
                {
                    var paths = System.IO.Directory.GetDirectories(baseFolder + "Clusters")
                        .Where(x => x.Contains(program)).ToList();
                    foreach (var path in paths)
                    { 
                        var summaries_default = Program.ProcessOLDNETSlicerResults(path, null);
                        var input = path.Length > path.IndexOf(program) + program.Length + 2 ? path.Substring(path.IndexOf(program) + program.Length + 1) : "";
                
                        // DEGLOSE
                        if (true)
                        {
                            for (var i = 0; i < summaries_default.Count; i++)
                            {
                                globalLines.AppendLine($"{program};{input};{i + 1};{summaries_default[i].totalStatementsLines};{summaries_default[i].distinctStatementLines};{summaries_default[i].computed_sliced_statements};{summaries_default[i].computed_sliced_percentage};{summaries_default[i].computed_filtered_statements};{summaries_default[i].computed_filtered_percentage};{summaries_default[i].totalSecondsInstrumentation};{summaries_default[i].executionSeconds};{summaries_default[i].totalTraceLines};{summaries_default[i].traceSize}");
                            };
                        }
                    }
                }
            //globalLines.AppendLine("================");
            #endregion

            foreach (var program in programs)
            {
                // 1) Levantamos todo lo de DynAbs Default /*@"\Default"*/
                var paths = System.IO.Directory.GetDirectories(baseFolder + "Clusters")
                    .Where(x => x.Contains(program));

                #region Acums
                var criteria_num = 0;
                var counter = 0;
                var sum_stmt_exec = 0d;
                var sum_stmt_distintos = 0d;
                // Primer slice
                var sum_slice = 0d;
                var sum_slice_perc = 0d;
                var min_slice_perc = 0d;
                var max_slice_perc = 0d;
                // Segundo slice
                var sum_filtered = 0d;
                var sum_filtered_perc = 0d;
                var min_filtered_perc = 0d;
                var max_filtered_perc = 0d;
                // Tiempos y traza
                var sum_instr = 0d;
                var sum_analisis = 0d;
                var sum_traza_lineas = 0d;
                var sum_traza_size = 0d;
                #endregion

                foreach (var path in paths)
                {
                    var summaries_default = Program.ProcessOLDNETSlicerResults(path, null);
                    criteria_num = summaries_default.Count;
                    var input = path.Length > path.IndexOf(program) + program.Length + 2 ? path.Substring(path.IndexOf(program) + program.Length + 1) : "";
                    // 2) Levantamos todo lo de DynAbs OPT

                    // AGRUPADO
                    if (true)
                    {
                        counter += summaries_default.Count;
                        sum_stmt_exec += summaries_default.Sum(x => x.totalStatementsLines);
                        sum_stmt_distintos += summaries_default.Sum(x => x.distinctStatementLines);
                        // Primer slice
                        sum_slice += summaries_default.Sum(x => x.computed_sliced_statements);
                        sum_slice_perc += summaries_default.Sum(x => x.computed_sliced_percentage);
                        min_slice_perc = Math.Min(min_slice_perc, summaries_default.Min(x => x.computed_sliced_percentage));
                        max_slice_perc = Math.Max(max_slice_perc, summaries_default.Max(x => x.computed_sliced_percentage));
                        // Segundo slice
                        sum_filtered += summaries_default.Sum(x => x.computed_filtered_statements);
                        sum_filtered_perc += summaries_default.Sum(x => x.computed_filtered_percentage);
                        min_filtered_perc = Math.Min(min_filtered_perc, summaries_default.Min(x => x.computed_filtered_percentage));
                        max_filtered_perc = Math.Max(max_filtered_perc, summaries_default.Max(x => x.computed_filtered_percentage));
                        // Tiempos y traza
                        sum_instr += summaries_default.First().totalSecondsInstrumentation;
                        sum_analisis += summaries_default.Sum(x => x.executionSeconds);
                        sum_traza_lineas += summaries_default.Sum(x => x.totalTraceLines);
                        sum_traza_size += summaries_default.Sum(x => x.traceSize);

                        //globalLines.AppendLine($"{program};{input};{summaries_default.Count};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size}");
                    }
                }

                #region Acums
                var avg_stmt_exec = sum_stmt_exec / counter;
                var avg_stmt_distintos = sum_stmt_distintos / counter;
                // Primer slice
                var avg_slice = sum_slice / counter;
                var avg_slice_perc = sum_slice_perc / counter;
                // Segundo slice
                var avg_filtered = sum_filtered / counter;
                var avg_filtered_perc = sum_filtered_perc / counter;
                // Tiempos y traza
                var avg_instr = sum_instr / paths.Count();
                var avg_analisis = sum_analisis / counter;
                var avg_traza_lineas = sum_traza_lineas / counter;
                var avg_traza_size = sum_traza_size / counter;
                #endregion

                globalLines.AppendLine($"{program};RANDOM;{criteria_num};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size}");
            }

            Console.WriteLine(globalLines.ToString());
            //File.AppendAllText(results_path, globalLines.ToString());
        }

        #region JavaslicerCounters
        class JavaslicerCounters
        {
            public double counter_stmt_slice = 0;
            public double acum_stmt_slice = 0;
            public double min_stmt_slice = 0;
            public double max_stmt_slice = 0;
            public double acum_exec_time = 0;
            public double acum_analysis_time = 0;
            public double acum_traza = 0;
            public double avg_stmt_slice { get { return acum_stmt_slice / counter_stmt_slice; } }
            public double avg_exec_time { get { return acum_exec_time / counter_stmt_slice; } }
            public double avg_analysis_time { get { return acum_analysis_time / counter_stmt_slice; } }
            public double avg_traza { get { return acum_traza / 10; } }
        }
        #endregion
    }
}
