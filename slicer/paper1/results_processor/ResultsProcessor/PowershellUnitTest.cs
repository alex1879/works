﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ResultsProcessor
{
    public class PowershellUnitTest
    {
        public static void Main()
        {
            //MovePS(@"C:\Users\alexd\Desktop\Slicer\Powershell\results\tests");
            //return;

            //var pathDefault = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\tests_WithoutSummaries_Normal";
            //var pathNew = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\tests_NewDG";

            //var originalResultPath = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\tests";

            var originalResultPath = @"C:\Users\alexd\Desktop\Slicer\Powershell\results_04072023\full";
            //var half = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\half";
            //var sinSym = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\mid";
            //var sinCore = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\sinCore";
            var oneFile = @"C:\Users\alexd\Desktop\Slicer\Powershell\results_04072023\oneFile";

            var groupsDefault = FillSubjects(originalResultPath);
            //var groupsHalf = FillSubjects(half, true);
            //var groupsSinSym = FillSubjects(sinSym, true);
            //var groupsSinCore = FillSubjects(sinCore, true);
            var groupsOneFile = FillSubjects(oneFile, true);
            //CompareWithOriginal(groupsHalf, groupsDefault);
            //CompareWithOriginal(groupsSinSym, groupsDefault);
            //CompareWithOriginal(groupsSinCore, groupsDefault);
            CompareWithOriginal(groupsOneFile, groupsDefault);

            PrintAll(groupsDefault, true);
            Console.WriteLine("===============================");
            Console.ReadKey();
            //PrintAll(groupsHalf);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(groupsSinSym);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(groupsSinCore);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(groupsOneFile);

            Console.ReadKey();
        }

        static void MovePS(string path, bool skippingFiles = false)
        {
            var groups = Directory.GetDirectories(path).Select(x => Path.GetFileName(x)).ToArray();
            var groupsDict = new Dictionary<string, GroupInfo>();

            foreach (var group in groups)
            {
                var mainResultPath = Path.Combine(path, group);
                var subjects = new Dictionary<string, Roslyn.SubjectInfo>();
                foreach (var dir in Directory.GetDirectories(mainResultPath))
                {
                    var subjectName = Path.GetFileName(dir).Replace("DynAbsTrace_", "").Replace("_ORIG", "");
                    if (!SelectedSubjectNames.Contains(subjectName))
                        continue;

                    var subject = new Roslyn.SubjectInfo(subjectName);

                    var pathToCopy = dir;
                    var localResultPathC = Path.Combine(dir, group + "_" + subject.Name);

                    foreach (var file in Directory.GetFiles(localResultPathC))
                        System.IO.File.Move(file, Path.Combine(pathToCopy, Path.GetFileName(file)));
                }
            }
        }

        public static Dictionary<string, GroupInfo> FillSubjects(string path, bool skippingFiles = false)
        {
            var groups = Directory.GetDirectories(path).Select(x => Path.GetFileName(x)).ToArray();
            var groupsDict = new Dictionary<string, GroupInfo>();

            //var excludedGroups = new string[] { "PlatformTests", "PSTypeExtensionsTests", "PSEnumerableBinderTests" };

            foreach (var group in groups)
            {
                //if (excludedGroups.Contains(group))
                //    continue;

                var mainResultPath = Path.Combine(path, group);
                var subjects = new Dictionary<string, Roslyn.SubjectInfo>();
                foreach (var dir in Directory.GetDirectories(mainResultPath))
                {
                    var subjectName = Path.GetFileName(dir).Replace("DynAbsTrace_", "").Replace("_ORIG", "");
                    //if (!SelectedSubjectNames.Contains(subjectName))
                    //    continue;

                    var subject = new Roslyn.SubjectInfo(subjectName);

                    var pathToCopy = dir;
                    var localResultPathC = Path.Combine(dir, group + "_" + subject.Name);

                    //var t = localResultPathC.Replace(
                    //        @"C:\Users\alexd\Desktop\Slicer\Powershell\results_04072023",
                    //        @"C:\Users\alexd\Desktop\Slicer\Powershell\results\tests");
                    //System.IO.File.Copy(
                    //    Path.Combine(t, "ExecutionCounters.txt"),
                    //    Path.Combine(pathToCopy, "ExecutionCounters.txt"), true);
                    //continue;

                    //foreach (var file in Directory.GetFiles(localResultPathC))
                    //    System.IO.File.Move(file, Path.Combine(pathToCopy, Path.GetFileName(file)));
                    //continue;

                    var localResultPath = dir;
                    //var localResultPath = localResultPathC;

                    // Files:
                    var executedMethodPath = Path.Combine(localResultPath, "ExecutedMethods.txt");
                    var executedCallbacksPath = Path.Combine(localResultPath, "ExecutedCallbacks.txt");
                    var skippedFilesPath = Path.Combine(localResultPath, "SkippedFilesInfo.txt");
                    
                    if (!File.Exists(executedMethodPath))
                        continue;

                    // Load each file:
                    subject.Criteria = Program.ProcessNETSlicerResults(localResultPath, null);
                    // Load methods info
                    var methodsInfo = Roslyn.ParseExecutedMethodInfoFile(executedMethodPath);
                    Roslyn.ParseCallbackInfo(executedCallbacksPath, methodsInfo);
                    subject.MethodsInfo = methodsInfo;
                    // Skipped info
                    if (skippingFiles)
                        subject.SkippedInfo = Roslyn.ParseSkippedFilesInfo(skippedFilesPath);

                    if (subject.Criteria.Count > 0)
                    {
                        // Compute statistics
                        subject.avg_stmt_exec = subject.Criteria.Average(x => x.totalStatementsLines);
                        subject.avg_stmt_distintos = subject.Criteria.Average(x => x.distinctStatementLines);
                        // Primer slice
                        subject.avg_slice = subject.Criteria.Average(x => x.computed_sliced_statements);
                        subject.avg_slice_perc = Roslyn.GetPercentage(subject.avg_slice, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_sliced_percentage);
                        subject.min_slice_perc = subject.Criteria.Min(x => x.computed_sliced_percentage);
                        subject.max_slice_perc = subject.Criteria.Max(x => x.computed_sliced_percentage);
                        // Segundo slice
                        subject.avg_filtered = subject.Criteria.Average(x => x.computed_filtered_statements);
                        subject.avg_filtered_perc = Roslyn.GetPercentage(subject.avg_filtered, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_filtered_percentage);
                        subject.min_filtered_perc = subject.Criteria.Min(x => x.computed_filtered_percentage);
                        subject.max_filtered_perc = subject.Criteria.Max(x => x.computed_filtered_percentage);
                        // Tiempos y traza
                        subject.avg_instr = subject.Criteria.First().totalSecondsInstrumentation;
                        subject.avg_analisis = subject.Criteria.Average(x => x.executionSeconds);
                        subject.avg_traza_lineas = subject.Criteria.Average(x => x.totalTraceLines);
                        subject.avg_traza_size = subject.Criteria.Average(x => x.traceSize);
                    }
                    subjects.Add(subjectName, subject);
                }
                groupsDict.Add(group, new GroupInfo() { Name = group, Subjects = subjects });
            }

            var emptyGroups = new List<string>();
            foreach (var kv in groupsDict)
                if (kv.Value.Subjects.Count == 0)
                    emptyGroups.Add(kv.Key);

            foreach (var emptyGroup in emptyGroups)
                groupsDict.Remove(emptyGroup);

            return groupsDict;
        }

        static void PrintAll(Dictionary<string, GroupInfo> subjects, bool sonOriginales = false)
        {
            PrintByCriteria(subjects, sonOriginales);
            //PrintSubjects(subjects, sonOriginales);
            //PrintGroups(subjects, sonOriginales);
        }

        static void PrintByCriteria(Dictionary<string, GroupInfo> subjects, bool sonOriginales = false)
        {
            var sb = new StringBuilder();
            foreach (var subjectKv in subjects)
                foreach (var subject in subjectKv.Value.Subjects.Values)
                {
                    var avg_methods = subject.MethodsInfo.Sum(y => y.Invocations);
                    var avg_callbacks = subject.MethodsInfo.First().Callbacks.Sum(y => y.Invocations);

                    if (!sonOriginales && subject.hayOriginal)
                        foreach (var criterion in subject.Criteria)
                            Console.WriteLine($"{subjectKv.Key};{subject.Name};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
                                $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
                                $"{criterion.computed_sliced_statements};{criterion.computed_sliced_percentage};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
                                $"{criterion.totalSecondsInstrumentation};{criterion.executionSeconds};{criterion.totalTraceLines};{criterion.traceSize};" +
                                $"{criterion.traceDiff};{criterion.traceDiffPercentage};{criterion.secDiff};{criterion.secDiffPercentage};{criterion.cantNewLines};{criterion.cantNewLinesPercentage};" +
                                $"{subject.SkippedInfo.TotalFilesWithLines};{subject.SkippedInfo.TotalFilesExcluded};{subject.SkippedInfo.TotalFilesExcludedPercentage}" +
                                $"{avg_methods};{avg_callbacks}");
                    else
                        foreach (var criterion in subject.Criteria)
                            Console.WriteLine($"{subjectKv.Key};{subject.Name};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
                                $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
                                $"{criterion.computed_sliced_statements};{criterion.computed_sliced_percentage};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
                                $"{criterion.totalSecondsInstrumentation};{criterion.executionSeconds};{criterion.totalTraceLines};{criterion.traceSize};" +
                                $"{criterion.traceDiff};{criterion.traceDiffPercentage};;;;;" +
                                $"{subject.SkippedInfo?.TotalFilesWithLines};{subject.SkippedInfo?.TotalFilesExcluded};{subject.SkippedInfo?.TotalFilesExcludedPercentage}" +
                                $"{avg_methods};{avg_callbacks}");
                }
            Console.WriteLine(sb.ToString());
        }

        static void PrintSubjects(Dictionary<string, GroupInfo> subjects, bool sonOriginales = false)
        {
            var sb = new StringBuilder();
            foreach (var subjectKv in subjects)
                foreach (var subject in subjectKv.Value.Subjects.Values)
                {
                    var avg_methods = subject.MethodsInfo.Sum(y => y.Invocations);
                    var avg_callbacks = subject.MethodsInfo.First().Callbacks.Sum(y => y.Invocations);

                    // Grupo	Subject	# Criterios	AVG Stmt. Exec	AVG Stmt. Distintos	AVG Slice	AVG %	MIN %	MAX %	AVG Slice F.	AVG %	MIN %	MAX %	Instr. (seg)	AVG Análisis (seg)	AVG #Traza	AVG Traza (KB)
                    if (!sonOriginales && subject.hayOriginal)
                        Console.WriteLine($"{subjectKv.Key};{subject.Name};{subject.Criteria.Count};{subject.avg_stmt_exec};{subject.avg_stmt_distintos};" +
                            $"{subject.avg_slice};{subject.avg_slice_perc};{subject.min_slice_perc};{subject.max_slice_perc};{subject.avg_filtered};" +
                            $"{subject.avg_filtered_perc};{subject.min_filtered_perc};{subject.max_filtered_perc};{subject.avg_instr};{subject.avg_analisis};" +
                            $"{subject.avg_traza_lineas};{subject.avg_traza_size};{subject.avg_traceDiff};{subject.avg_traceDiffPercentage};" +
                            $"{subject.avg_secDiff};{subject.avg_secDiffPercentage};{subject.avg_cantNewLines};{subject.avg_cantNewLinesPercentage};" +
                            $"{subject.avg_traceDiffPercentage};{subject.SkippedInfo.TotalFilesExcluded};{subject.SkippedInfo.TotalFilesExcludedPercentage}" +
                            $"{avg_methods};{avg_callbacks}");
                    else
                        Console.WriteLine($"{subjectKv.Key};{subject.Name};{subject.Criteria.Count};{subject.avg_stmt_exec};{subject.avg_stmt_distintos};" +
                            $"{subject.avg_slice};{subject.avg_slice_perc};{subject.min_slice_perc};{subject.max_slice_perc};{subject.avg_filtered};" +
                            $"{subject.avg_filtered_perc};{subject.min_filtered_perc};{subject.max_filtered_perc};{subject.avg_instr};{subject.avg_analisis};" +
                            $"{subject.avg_traza_lineas};{subject.avg_traza_size};{subject.avg_traceDiff};{subject.avg_traceDiffPercentage};" +
                            $";;;;" +
                            $"{subject.SkippedInfo?.TotalFilesWithLines};{subject.SkippedInfo?.TotalFilesExcluded};{subject.SkippedInfo?.TotalFilesExcludedPercentage}" +
                            $"{avg_methods};{avg_callbacks}");
                }
            Console.WriteLine(sb.ToString());
        }

        static void PrintGroups(Dictionary<string, GroupInfo> subjects, bool sonOriginales = false)
        {
            foreach (var subjectKv in subjects)
            {
                // Voy a seleccionar los 20 que más criterios tengan
                var subSeleccion = subjectKv.Value.Subjects.ToList();

                var cant_casos = subSeleccion.Count();
                var cant_criterias = subSeleccion.Sum(x => x.Value.Criteria.Count);

                var avg_stmt_exec = subSeleccion.Average(x => x.Value.avg_stmt_exec);
                var avg_stmt_distintos = subSeleccion.Average(x => x.Value.avg_stmt_distintos);
                // Primer slice
                var avg_slice = subSeleccion.Average(x => x.Value.avg_slice);
                var avg_slice_perc = Roslyn.GetPercentage(avg_slice, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_slice_perc);
                var min_slice_perc = subSeleccion.Min(x => x.Value.min_slice_perc);
                var max_slice_perc = subSeleccion.Max(x => x.Value.max_slice_perc);
                // Segundo slice
                var avg_filtered = subSeleccion.Average(x => x.Value.avg_filtered);
                var avg_filtered_perc = Roslyn.GetPercentage(avg_filtered, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_filtered_perc);
                var min_filtered_perc = subSeleccion.Min(x => x.Value.min_filtered_perc);
                var max_filtered_perc = subSeleccion.Max(x => x.Value.max_filtered_perc);
                // Tiempos y traza
                var avg_instr = subSeleccion.Average(x => x.Value.avg_instr);
                var avg_analisis = subSeleccion.Average(x => x.Value.avg_analisis);
                var min_analisis = subSeleccion.Min(x => x.Value.Criteria.Min(y => y.executionSeconds));
                var max_analisis = subSeleccion.Max(x => x.Value.Criteria.Max(y => y.executionSeconds));
                var avg_traza_lineas = subSeleccion.Average(x => x.Value.avg_traza_lineas);
                var avg_traza_size = subSeleccion.Average(x => x.Value.avg_traza_size);

                // Datos Sebas (# de veces que un criteria full está mejor que un criteria no full)
                var v1 = subSeleccion.Sum(x => x.Value.Criteria.Sum(y => y.cantNewLines > 0 ? 1 : 0));
                var v1_max = subSeleccion.Min(x => x.Value.Criteria.Min(y => y.perdidaDePrecisionPercentage));
                var temp = subSeleccion.SelectMany(x =>
                             x.Value.Criteria.Where(y => y.cantNewLines > 0)
                                             .Select(y => y.perdidaDePrecisionPercentage));
                var v1_avg_new = temp.Count() > 0 ? temp.Average() : 0;

                var temp2 = subSeleccion.SelectMany(x =>
                             x.Value.Criteria.Where(y => y.cantNewLines > 0)
                                             .Select(y => y.speedUp));
                var s1_max_new = temp2.Count() > 0 ? temp2.Max() : 0;
                var s1_avg_new = temp2.Count() > 0 ? temp2.Average() : 0;

                // Calls a métodos externos y callbacks
                var avg_methods = subSeleccion.Average(x => x.Value.MethodsInfo.Sum(y => y.Invocations));
                var avg_callbacks = subSeleccion.Average(x => x.Value.MethodsInfo.First().Callbacks.Sum(y => y.Invocations));

                //Console.WriteLine($"{subjectKv.Key};{v1};{v1_max};{v1_avg_new}");
                //Console.WriteLine($"{subjectKv.Key};{s1_max_new}");

                //Console.WriteLine($"{subjectKv.Key} - {min_analisis} - {avg_analisis} - {max_analisis}");

                Console.WriteLine($"{subjectKv.Key};{cant_casos};{cant_criterias};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};" +
                    $"{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};" +
                    $"{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size};" +
                    $"{subjectKv.Value.avg_traceDiff};{subjectKv.Value.avg_traceDiffPercentage};{subjectKv.Value.avg_secDiff};{subjectKv.Value.avg_secDiffPercentage};{subjectKv.Value.avg_cantNewLines};" +
                    $"{subjectKv.Value.avg_cantNewLinesPercentage};{subjectKv.Value.avg_totalFilesWithLines};{subjectKv.Value.avg_totalFilesExcluded};{subjectKv.Value.avg_totalFilesExcludedPercentage};{avg_methods};{avg_callbacks}");
            }
        }

        static void CompareWithOriginal(Dictionary<string, GroupInfo> reduced, Dictionary<string, GroupInfo> original)
        {
            var lessLinesGeneral = new HashSet<FileLine>();
            var differentCriterionSize = new List<string>();
            var lessLinesSubjects = new Dictionary<string, HashSet<FileLine>>();

            foreach (var group in reduced)
            {
                foreach (var subjectInfo in group.Value.Subjects.Values)
                {
                    var anotherSubjectInfo = original[group.Key].Subjects.Values.FirstOrDefault(x => x.Name.Equals(subjectInfo.Name));
                    if (anotherSubjectInfo == null)
                    {
                        subjectInfo.hayOriginal = false;
                        continue;
                    }

                    if (anotherSubjectInfo.Criteria.Count != subjectInfo.Criteria.Count)
                    {
                        differentCriterionSize.Add(group + " - " + subjectInfo.Name);
                        continue;
                    }

                    if (subjectInfo.Name.Contains("Return_Void1"))
                        ;

                    // Cálculo por criterion
                    for (var i = 0; i < subjectInfo.Criteria.Count; i++)
                    {
                        var anotherCriterion = anotherSubjectInfo.Criteria[i];

                        subjectInfo.Criteria[i].traceDiff = anotherCriterion.totalTraceLines - subjectInfo.Criteria[i].totalTraceLines;
                        subjectInfo.Criteria[i].traceDiffPercentage = Roslyn.GetPercentage(subjectInfo.Criteria[i].traceDiff, anotherCriterion.totalTraceLines);
                        subjectInfo.Criteria[i].secOrig = anotherCriterion.executionSeconds;
                        subjectInfo.Criteria[i].secDiff = anotherCriterion.executionSeconds - subjectInfo.Criteria[i].executionSeconds;
                        subjectInfo.Criteria[i].secDiffPercentage = Roslyn.GetPercentage(subjectInfo.Criteria[i].secDiff, anotherCriterion.executionSeconds);

                        // Puede fallar... (hay nombres de archivos repetidos)
                        var newLines = subjectInfo.Criteria[i].FilteredResult
                            .Where(x => !anotherCriterion.FilteredResult.Any(y => x.File.Equals(y.File))).ToList();

                        var excludedFiles = subjectInfo.SkippedInfo.SkippedFiles.Select(x => Path.GetFileName(x)).ToList();
                        var lessLines = new HashSet<FileLine>(anotherCriterion.FilteredResult
                            .Where(x => !subjectInfo.Criteria[i].FilteredResult.Any(y => x.File.Equals(y.File)) &&
                                        !excludedFiles.Any(y => y.Equals(x.File))));

                        if (lessLines.Count > 0)
                        {
                            lessLinesGeneral.UnionWith(lessLines);
                            lessLinesSubjects.Add(group.Key + " - " + subjectInfo.Name + " - Criterion: " + (i + 1).ToString(), lessLines);
                        }

                        if (group.Key == "PSTypeExtensionsTests")
                            ;

                        var newLinesPercentage = Roslyn.GetPercentage(newLines.Count, subjectInfo.Criteria[i].computed_filtered_statements);

                        subjectInfo.Criteria[i].cantNewLines = newLines.Count;
                        subjectInfo.Criteria[i].cantNewLinesPercentage = newLinesPercentage;
                        subjectInfo.Criteria[i].perdidaDePrecisionPercentage = 100 -
                            (double)subjectInfo.Criteria[i].computed_filtered_statements * (double)100 / ((double)subjectInfo.Criteria[i].computed_filtered_statements - (double)newLines.Count);
                    }

                    // Cálculo por subject
                    subjectInfo.avg_traceDiff = subjectInfo.Criteria.Average(x => x.traceDiff);
                    var anotherAvg_trace = anotherSubjectInfo.Criteria.Average(x => x.totalTraceLines);
                    subjectInfo.avg_traceDiffPercentage = Roslyn.GetPercentage(subjectInfo.avg_traceDiff, anotherAvg_trace); //subjectInfo.Criteria.Average(x => x.traceDiffPercentage);
                    subjectInfo.avg_secDiff = subjectInfo.Criteria.Average(x => x.secDiff);
                    var anotherAvg_sec = anotherSubjectInfo.Criteria.Average(x => x.executionSeconds);
                    subjectInfo.avg_secDiffPercentage = Roslyn.GetPercentage(subjectInfo.avg_secDiff, anotherAvg_sec); //subjectInfo.Criteria.Average(x => x.secDiffPercentage);
                    subjectInfo.avg_cantNewLines = subjectInfo.Criteria.Average(x => x.cantNewLines);
                    subjectInfo.avg_cantNewLinesPercentage = subjectInfo.Criteria.Average(x => x.cantNewLinesPercentage);
                }

                // Cálculo por grupo
                var anotherGroup = original.Single(x => x.Key == group.Key);
                group.Value.avg_traceDiff = group.Value.Subjects.Average(x => x.Value.avg_traceDiff);
                var anotherAvg_TraceGroup = anotherGroup.Value.Subjects.Average(x => x.Value.avg_traza_lineas);
                group.Value.avg_traceDiffPercentage = anotherAvg_TraceGroup > 1 ? Roslyn.GetPercentage(group.Value.avg_traceDiff, anotherAvg_TraceGroup) : 0; //group.Value.Subjects.Average(x => x.avg_traceDiffPercentage);
                group.Value.avg_secDiff = group.Value.Subjects.Average(x => x.Value.avg_secDiff);
                var anotherAvg_SecGroup = anotherGroup.Value.Subjects.Average(x => x.Value.avg_analisis);
                group.Value.avg_secDiffPercentage = anotherAvg_SecGroup > 1 ? Roslyn.GetPercentage(group.Value.avg_secDiff, anotherAvg_SecGroup) : 0; // group.Value.Subjects.Average(x => x.avg_secDiffPercentage);
                group.Value.avg_cantNewLines = group.Value.Subjects.Average(x => x.Value.avg_cantNewLines);
                group.Value.avg_cantNewLinesPercentage = group.Value.Subjects.Average(x => x.Value.avg_cantNewLinesPercentage);
                group.Value.avg_totalFilesWithLines = group.Value.Subjects.Average(x => x.Value.SkippedInfo.TotalFilesWithLines);
                group.Value.avg_totalFilesExcluded = group.Value.Subjects.Average(x => x.Value.SkippedInfo.TotalFilesExcluded);
                group.Value.avg_totalFilesExcludedPercentage = group.Value.Subjects.Average(x => x.Value.SkippedInfo.TotalFilesExcludedPercentage);
            }

            //var sb = new StringBuilder();
            //foreach (var ln in lessLinesGeneral)
            //    sb.AppendLine(ln.File + ":" + ln.Line.ToString());
            //foreach (var ln in lessLinesSubjects)
            //{
            //    sb.AppendLine(ln.Key + ":");
            //    foreach (var newl in ln.Value)
            //        sb.AppendLine("     " + newl.File + ":" + newl.Line);
            //}
            //Console.WriteLine(sb.ToString());
        }

        static string[] SelectedSubjectNames = new string[]
        {
            "TestSetProperty",
            "TestReadCoreEngineSnapIn",
            "TestCustomPipeNameCreation",
            "TestCustomPipeNameCreationTooLongOnNonWindows",
            "TestStartJobThrowTerminatingException",
            "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesEmpty",
            "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesNotEmpty",
            "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesNotExist",
            "PowerShellConfig_GetPowerShellPolicies_EmptySystemConfig",
            "PowerShellConfig_GetPowerShellPolicies_EmptyUserConfig",
            "Utils_GetPolicySetting_BothConfigFilesEmpty",
            "Utils_GetPolicySetting_BothConfigFilesNotEmpty",
            "Utils_GetPolicySetting_BothConfigFilesNotExist",
            "Utils_GetPolicySetting_EmptySystemConfig",
            "Utils_GetPolicySetting_EmptyUserConfig",
            "TestAdaptedMember",
            "TestCimInstanceProperty",
            "TestEmptyObjectHasNoProperty",
            "TestMemberSet",
            "TestMemberSetIsNotProperty",
            "TestShadowedMember",
            "TestWrappedDateTimeHasReflectedMember",
            "TextXmlAttributeMember",
            "TextXmlElementMember",
            "TestVersions",
            "TestRunspaceWithPipeline",
            "TestRunspaceWithPowerShell",
            "TestRunspaceWithPowerShellAndInitialSessionState",
            "TestScanContent",
            "TestDrives",
            "TestBoundedStack",
            "TestConvertToJsonBasic",
            "TestConvertToJsonCancellation",
            "TestConvertToJsonWithEnum",
            "TestConvertToJsonWithoutCompress",
            "TestHistoryStack",
            "TestIsWinPEHost",
            "TestEscape_Empty",
            "TestEscape_Null",
            "TestEscape_String_A",
            "TestEscape_String_B",
            "TestEscape_String_C",
            "TestEscape_String_NotEscape_A",
            "TestEscape_String_NotEscape_B",
            "TestEscape_String_NotEscape_C",
            "TestUnescape_Empty",
            "TestUnescape_Null",
            "TestUnescape_String_A",
            "TestUnescape_String_B",
            "TestUnescape_String_C",
            "TestIsCoreCLR",
            "TestIsNumeric",
            "TestIsStaticTypePossiblyEnumerable",
            "TestClearContent",
            "TestCreateJunctionFails",
            "TestGetContentReader",
            "TestGetContentWriter",
            "TestGetHelpMaml",
            "TestGetProperty",
            "TestMode",
        };

        public class GroupInfo
        {
            public string Name { get; set; }
            public Dictionary<string, Roslyn.SubjectInfo> Subjects = new Dictionary<string, Roslyn.SubjectInfo>();

            public double avg_traceDiff { get; set; }
            public double avg_traceDiffPercentage { get; set; }
            public double avg_secDiff { get; set; }
            public double avg_secDiffPercentage { get; set; }
            public double avg_cantNewLines { get; set; }
            public double avg_cantNewLinesPercentage { get; set; }

            public double avg_totalFilesWithLines { get; set; }
            public double avg_totalFilesExcluded { get; set; }
            public double avg_totalFilesExcludedPercentage { get; set; }
        }

        #region Imprimir comparando
        //static void PrintDeglose(Dictionary<string, GroupInfo> groupsDefault, Dictionary<string, GroupInfo> groupsNew)
        //{
        //    var sb = new StringBuilder();
        //    foreach (var group in groupsDefault)
        //        foreach (var subjectKv in group.Value.Subjects)
        //        {
        //            var subject = subjectKv.Value;
        //            var i = 0;
        //            foreach (var criterion in subject.Criteria)
        //            {
        //                var criterionNew = groupsNew[group.Key].Subjects[subjectKv.Key].Criteria[i];

        //                // Subject	# Criterios	AVG Stmt. Exec	AVG Stmt. Distintos	AVG Slice	AVG %	MIN %	MAX %	AVG Slice F.	AVG %	MIN %	MAX %	Instr. (seg)	AVG Análisis (seg)	AVG #Traza	AVG Traza (KB)
        //                Console.WriteLine($"{subjectKv.Key};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
        //                                    $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
        //                                    $"{criterion.executionSeconds};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
        //                                    $"{criterionNew.executionSeconds};{criterionNew.computed_filtered_statements};{criterionNew.computed_filtered_percentage}");
        //                i++;
        //            }
        //        }
        //    Console.WriteLine(sb.ToString());
        //}
        #endregion
    }
}
