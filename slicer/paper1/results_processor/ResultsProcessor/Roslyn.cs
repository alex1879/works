﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class Roslyn
    {
        public static void Main()
        {
            // Paths
            var fullCoreResultPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\full - julio2023";
            //var fullCoreResultPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\full-a3";
            var csCoreResultPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\original-a3";
            var sinSym = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\sinSym";
            var sinCore = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\sinCore";
            var newClient = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\half";
            var tracesBasePath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\traces\aut";

            var fullCoreSubjects = Initialize20Subjects();
            //var csCoreSubjects = Initialize20Subjects();
            //var sinSymSubjects = InitializeSubjects();
            //var sinCoreSubjects = InitializeSubjects();
            //var newClientSubjects = Initialize20Subjects();

            FillSubjects(fullCoreResultPath, tracesBasePath, fullCoreSubjects);
            //FillSubjects(csCoreResultPath, tracesBasePath, csCoreSubjects, true);
            //FillSubjects(sinSym, tracesBasePath, sinSymSubjects, true);
            //FillSubjects(sinCore, tracesBasePath, sinCoreSubjects, true);
            //FillSubjects(newClient, tracesBasePath, newClientSubjects, true);
            //CompareWithOriginal(csCoreSubjects, fullCoreSubjects);
            //CompareWithOriginal(sinSymSubjects, csCoreSubjects);
            //CompareWithOriginal(sinCoreSubjects, csCoreSubjects);
            //CompareWithOriginal(newClientSubjects, csCoreSubjects);

            PrintAll(fullCoreSubjects, true);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(csCoreSubjects, false);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(sinSymSubjects);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(sinCoreSubjects);
            //Console.WriteLine("===============================");
            //Console.ReadKey();
            //PrintAll(newClientSubjects);

            Console.ReadKey();
        }

        static void PrintAll(Dictionary<string, GroupInfo> subjects, bool sonOriginales = false)
        {
            //var cant_resultados_mostrar = 10;
            var modoFinal = false;
            var modoPresentacion = false;

            var callsExternos = false;
            var checkSubjects = false;
            var tablaDeglose = false; // modoPresentacion || !modoFinal;
            var printTablaPorSubject = false; // (modoPresentacion || !modoFinal);
            var tablaPorGrupo = true; // true; //tablaPorSubject && (modoPresentacion || modoFinal);
            //var filtrarCantidad = false; // modoPresentacion || modoFinal;

            //if (filtrarCantidad)
            //{
            //    var keys = subjects.Keys.ToList();
            //    foreach (var subjectKey in keys)
            //    {
            //        subjects[subjectKey].Subjects = subjects[subjectKey].Subjects.Where(x => x.SeleccionFinal)
            //            .OrderByDescending(x => x.Criteria.Count).Take(cant_resultados_mostrar).ToList();
            //    }
            //}

            // Quiero ver todos los calls externos dentro de la librería conocida
            if (callsExternos)
            {
                var knownLibrary = "Microsoft.CodeAnalysis.CSharp";
                var notLibrary = "Microsoft.CodeAnalysis.CSharp.Syntax";
                var filteredMethodInfo = new HashSet<ExecutedMethodInfo>();
                foreach (var subjectKv in subjects)
                    foreach (var subject in subjectKv.Value.Subjects)
                    {
                        var selectedMethods = subject.MethodsInfo.Where(x => x.NamespaceName.Contains(knownLibrary) && !x.NamespaceName.Contains(notLibrary)).ToList();
                        if (selectedMethods.Any(x => x.MethodName.Contains("WithFeatures")))
                            ;
                        filteredMethodInfo.UnionWith(selectedMethods);
                    }

                foreach (var methodInfo in filteredMethodInfo)
                    Console.WriteLine($"{methodInfo.NamespaceName}.{methodInfo.ClassName}.{methodInfo.MethodName}");
            }

            // Quiero saber si todos tienen al menos 1 slice
            if (checkSubjects)
            {
                foreach (var subjectKv in subjects)
                    foreach (var subject in subjectKv.Value.Subjects)
                        if (subject.Criteria.Count < 1)
                            Console.WriteLine($"No lines: {subjectKv.Key} - {subject.Name}");
            }

            // Imprimir resultados planilla por criterio
            if (tablaDeglose)
            {
                foreach (var subjectKv in subjects)
                    foreach (var subject in subjectKv.Value.Subjects)
                    {
                        if (!sonOriginales && subject.hayOriginal)
                            foreach (var criterion in subject.Criteria)
                                Console.WriteLine($"{subjectKv.Key};{subject.Name};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
                                    $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
                                    $"{criterion.computed_sliced_statements};{criterion.computed_sliced_percentage};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
                                    $"{criterion.totalSecondsInstrumentation};{criterion.executionSeconds};{criterion.totalTraceLines};{criterion.traceSize};" +
                                    $"{criterion.traceDiff};{criterion.traceDiffPercentage};{criterion.secDiff};{criterion.secDiffPercentage};{criterion.cantNewLines};{criterion.cantNewLinesPercentage};" +
                                    $"{subject.SkippedInfo.TotalFilesWithLines};{subject.SkippedInfo.TotalFilesExcluded};{subject.SkippedInfo.TotalFilesExcludedPercentage}");
                        else
                            foreach (var criterion in subject.Criteria)
                                Console.WriteLine($"{subjectKv.Key};{subject.Name};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
                                    $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
                                    $"{criterion.computed_sliced_statements};{criterion.computed_sliced_percentage};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
                                    $"{criterion.totalSecondsInstrumentation};{criterion.executionSeconds};{criterion.totalTraceLines};{criterion.traceSize};" +
                                    $"{criterion.traceDiff};{criterion.traceDiffPercentage};;;;;" +
                                    $"{subject.SkippedInfo?.TotalFilesWithLines};{subject.SkippedInfo?.TotalFilesExcluded};{subject.SkippedInfo?.TotalFilesExcludedPercentage}");
                    }
                Console.WriteLine("=======================================================");
            }

            // Imprimir resultados por subject
            if (printTablaPorSubject)
            {
                foreach (var subjectKv in subjects)
                    foreach (var subject in subjectKv.Value.Subjects)
                    {
                        // Grupo	Subject	# Criterios	AVG Stmt. Exec	AVG Stmt. Distintos	AVG Slice	AVG %	MIN %	MAX %	AVG Slice F.	AVG %	MIN %	MAX %	Instr. (seg)	AVG Análisis (seg)	AVG #Traza	AVG Traza (KB)
                        if (printTablaPorSubject)
                            if (!sonOriginales && subject.hayOriginal)
                                Console.WriteLine($"{subjectKv.Key};{subject.Name};{subject.Criteria.Count};{subject.avg_stmt_exec};{subject.avg_stmt_distintos};" +
                                    $"{subject.avg_slice};{subject.avg_slice_perc};{subject.min_slice_perc};{subject.max_slice_perc};{subject.avg_filtered};" +
                                    $"{subject.avg_filtered_perc};{subject.min_filtered_perc};{subject.max_filtered_perc};{subject.avg_instr};{subject.avg_analisis};" +
                                    $"{subject.avg_traza_lineas};{subject.avg_traza_size};{subject.avg_traceDiff};{subject.avg_traceDiffPercentage};" +
                                    $"{subject.avg_secDiff};{subject.avg_secDiffPercentage};{subject.avg_cantNewLines};{subject.avg_cantNewLinesPercentage};" +
                                    $"{subject.avg_traceDiffPercentage};{subject.SkippedInfo.TotalFilesExcluded};{subject.SkippedInfo.TotalFilesExcludedPercentage}");
                            else
                                Console.WriteLine($"{subjectKv.Key};{subject.Name};{subject.Criteria.Count};{subject.avg_stmt_exec};{subject.avg_stmt_distintos};" +
                                    $"{subject.avg_slice};{subject.avg_slice_perc};{subject.min_slice_perc};{subject.max_slice_perc};{subject.avg_filtered};" +
                                    $"{subject.avg_filtered_perc};{subject.min_filtered_perc};{subject.max_filtered_perc};{subject.avg_instr};{subject.avg_analisis};" +
                                    $"{subject.avg_traza_lineas};{subject.avg_traza_size};{subject.avg_traceDiff};{subject.avg_traceDiffPercentage};" +
                                    $";;;;" +
                                    $"{subject.SkippedInfo?.TotalFilesWithLines};{subject.SkippedInfo?.TotalFilesExcluded};{subject.SkippedInfo?.TotalFilesExcludedPercentage}");
                    }

                Console.WriteLine("=======================================================");
            }

            if (tablaPorGrupo)
            {
                foreach (var subjectKv in subjects)
                {
                    // Voy a seleccionar los 20 que más criterios tengan
                    var subSeleccion = subjectKv.Value.Subjects.ToList();

                    var cant_casos = subSeleccion.Count();
                    var cant_criterias = subSeleccion.Sum(x => x.Criteria.Count);

                    var avg_stmt_exec = subSeleccion.Average(x => x.avg_stmt_exec);
                    var avg_stmt_distintos = subSeleccion.Average(x => x.avg_stmt_distintos);
                    // Primer slice
                    var avg_slice = subSeleccion.Average(x => x.avg_slice);
                    var avg_slice_perc = GetPercentage(avg_slice, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_slice_perc);
                    var min_slice_perc = subSeleccion.Min(x => x.min_slice_perc);
                    var max_slice_perc = subSeleccion.Max(x => x.max_slice_perc);
                    // Segundo slice
                    var avg_filtered = subSeleccion.Average(x => x.avg_filtered);
                    var avg_filtered_perc = GetPercentage(avg_filtered, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_filtered_perc);
                    var min_filtered_perc = subSeleccion.Min(x => x.min_filtered_perc);
                    var max_filtered_perc = subSeleccion.Max(x => x.max_filtered_perc);
                    // Tiempos y traza
                    var avg_instr = subSeleccion.Average(x => x.avg_instr);
                    var avg_analisis = subSeleccion.Average(x => x.avg_analisis);
                    var min_analisis = subSeleccion.Min(x => x.Criteria.Min(y => y.executionSeconds));
                    var max_analisis = subSeleccion.Max(x => x.Criteria.Max(y => y.executionSeconds));
                    var avg_traza_lineas = subSeleccion.Average(x => x.avg_traza_lineas);
                    var avg_traza_size = subSeleccion.Average(x => x.avg_traza_size);

                    // INICIO DATOS ESPECIALES (GUARDAR)
                    // Datos Sebas (# de veces que un criteria full está mejor que un criteria no full)
                    //var v1 = subSeleccion.Sum(x => x.Criteria.Sum(y => y.cantNewLines > 0 ? 1 : 0));
                    //var v1_max = subSeleccion.Min(x => x.Criteria.Min(y => y.perdidaDePrecisionPercentage));
                    //var temp = subSeleccion.SelectMany(x =>
                    //                        x.Criteria.Where(y => y.cantNewLines > 0)
                    //                         .Select(y => y.perdidaDePrecisionPercentage));
                    //var v1_avg_new = temp.Count() > 0 ? temp.Average() : 0;

                    //var temp2 = subSeleccion.SelectMany(x =>
                    //         x.Criteria.Where(y => y.cantNewLines > 0)
                    //                         .Select(y => y.speedUp));
                    //var s1_max_new = temp2.Count() > 0 ? temp2.Max() : 0;
                    //var s1_avg_new = temp2.Count() > 0 ? temp2.Average() : 0;

                    //Console.WriteLine($"{subjectKv.Key};{s1_avg_new};{s1_max_new}");

                    // Methods
                    var avg_methods = subSeleccion.Average(x => x.MethodsInfo.Sum(y => y.Invocations));
                    var avg_callbacks = subSeleccion.Average(x => x.MethodsInfo.First().Callbacks.Sum(y => y.Invocations));

                    //Console.WriteLine($"{subjectKv.Key};{v1};{v1_max};{v1_avg_new}");

                    //Console.WriteLine($"{subjectKv.Key} - {min_analisis} - {avg_analisis} - {max_analisis}");
                    // FIN DATOS ESPECIALES (GUARDAR)

                    Console.WriteLine($"{subjectKv.Key};{cant_casos};{cant_criterias};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};" +
                        $"{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};" +
                        $"{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size};" +
                        $"{subjectKv.Value.avg_traceDiff};{subjectKv.Value.avg_traceDiffPercentage};{subjectKv.Value.avg_secDiff};{subjectKv.Value.avg_secDiffPercentage};{subjectKv.Value.avg_cantNewLines};" +
                        $"{subjectKv.Value.avg_cantNewLinesPercentage};{subjectKv.Value.avg_totalFilesWithLines};{subjectKv.Value.avg_totalFilesExcluded};{subjectKv.Value.avg_totalFilesExcludedPercentage};{avg_methods};{avg_callbacks};");
                }
            }

            if (false)
            {
                var sbOrdering = new StringBuilder();
                foreach (var kvSubjects in subjects)
                {
                    sbOrdering.AppendLine("Grupo: " + kvSubjects.Key);
                    foreach (var e in kvSubjects.Value.Subjects.OrderBy(x => x.SkippedInfo.TotalLines).Select(x => x.Name + " - " + x.SkippedInfo.TotalLines.ToString()))
                        sbOrdering.AppendLine(e);
                    sbOrdering.AppendLine("");
                }
                Console.Write(sbOrdering.ToString());
            }
        }

        static void FillSubjects(string mainResultPath, string tracesBasePath, Dictionary<string, GroupInfo> subjects, bool skippingFiles = false)
        {
            // Load results
            foreach (var subjectKv in subjects)
            {
                var groupName = subjectKv.Key;
                foreach (var subject in subjectKv.Value.Subjects)
                {
                    var localResultPath = Path.Combine(mainResultPath, groupName, subject.Name, $"{groupName}_{subject.Name}");
                    // Files:
                    var executedMethodPath = Path.Combine(localResultPath, "ExecutedMethods.txt");
                    var executedCallbacksPath = Path.Combine(localResultPath, "ExecutedCallbacks.txt");
                    var skippedFilesPath = Path.Combine(localResultPath, "SkippedFilesInfo.txt");
                    // Load each file:
                    subject.Criteria = Program.ProcessNETSlicerResults(localResultPath, null);
                    // Load methods info
                    var methodsInfo = ParseExecutedMethodInfoFile(executedMethodPath);
                    ParseCallbackInfo(executedCallbacksPath, methodsInfo);
                    subject.MethodsInfo = methodsInfo;
                    // Skipped info
                    if (skippingFiles)
                    {
                        subject.SkippedInfo = new SkippedFilesInfo()
                        {
                            SkippedFolders = new List<SkippedFilesInfo.SkippedFile>()
                            {
                                new SkippedFilesInfo.SkippedFile() 
                                {
                                    Path = @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Core\Portable",
                                    Files =
                        FilesFromFolders(new List<string>() { @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Core\Portable" })
                            .Select(x =>  Path.GetFileName(x)).ToList()
                    }
                            }
                        };

                        //subject.SkippedInfo = ParseSkippedFilesInfo(skippedFilesPath);
                    }

                    // Duration
                    //var resFileName = $"{groupName}.{subject.Name}.res";
                    //var resFile = Path.Combine(tracesBasePath, groupName, subject.Name, resFileName);
                    //var duration = Roslyn.GetDurationFromResFile(resFile);
                    //subject.TestDuration = duration;
                    //Console.WriteLine($"{groupName};{subject.Name};{duration}");

                    if (subject.Criteria.Count > 0)
                    { 
                        // Compute statistics
                        subject.avg_stmt_exec = subject.Criteria.Average(x => x.totalStatementsLines);
                        subject.avg_stmt_distintos = subject.Criteria.Average(x => x.distinctStatementLines);
                        // Primer slice
                        subject.avg_slice = subject.Criteria.Average(x => x.computed_sliced_statements);
                        subject.avg_slice_perc = GetPercentage(subject.avg_slice, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_sliced_percentage);
                        subject.min_slice_perc = subject.Criteria.Min(x => x.computed_sliced_percentage);
                        subject.max_slice_perc = subject.Criteria.Max(x => x.computed_sliced_percentage);
                        // Segundo slice
                        subject.avg_filtered = subject.Criteria.Average(x => x.computed_filtered_statements);
                        subject.avg_filtered_perc = GetPercentage(subject.avg_filtered, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_filtered_percentage);
                        subject.min_filtered_perc = subject.Criteria.Min(x => x.computed_filtered_percentage);
                        subject.max_filtered_perc = subject.Criteria.Max(x => x.computed_filtered_percentage);
                        // Tiempos y traza
                        subject.avg_instr = subject.Criteria.First().totalSecondsInstrumentation;
                        subject.avg_analisis = subject.Criteria.Average(x => x.executionSeconds);
                        subject.avg_traza_lineas = subject.Criteria.Average(x => x.totalTraceLines);
                        subject.avg_traza_size = subject.Criteria.Average(x => x.traceSize);
                    }
                }
            }

            //foreach (var subjectKv in subjects)
            //    Console.WriteLine(subjectKv.Key + ": " + string.Join(", ", subjectKv.Value.Subjects.OrderBy(x => x.Criteria.Count).Select(x => x.Name + " (" + x.Criteria.Count.ToString() + ")")));
        }

        static Dictionary<string, GroupInfo> InitializeSubjects()
        {
            var subjects = new Dictionary<string, GroupInfo>();

            // StatementParsingTests
            var lStatementParsingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ParseCreateNullableTuple_01"),
                new SubjectInfo("TestConstLocalDeclarationStatement"),
                new SubjectInfo("TestEmptyStatement"),
                new SubjectInfo("TestFixedVarStatement"),
                new SubjectInfo("TestFor"),
                new SubjectInfo("TestForEachAfterOffset"),
                new SubjectInfo("TestForWithIncrementor"),
                new SubjectInfo("TestForWithMultipleVariableInitializers"),
                new SubjectInfo("TestForWithVarDeclaration"),
                new SubjectInfo("TestForWithVariableDeclaration"),
                new SubjectInfo("TestForWithVariableInitializer"),
                new SubjectInfo("TestLocalDeclarationStatement"),
                new SubjectInfo("TestLocalDeclarationStatementWithDynamic"),
                new SubjectInfo("TestLocalDeclarationStatementWithMixedType"),
                new SubjectInfo("TestLock"),
                new SubjectInfo("TestRefLocalDeclarationStatement"),
                new SubjectInfo("TestReturnExpression"),
                new SubjectInfo("TestSwitchWithMultipleCases"),
                new SubjectInfo("TestSwitchWithMultipleLabelsOnOneCase"),
                new SubjectInfo("TestSwitchWithMultipleStatementsOnOneCase"),
                new SubjectInfo("TestThrow"),
                new SubjectInfo("TestTryCatchWithMultipleCatches"),
                new SubjectInfo("TestUsingSpecialCase2"),
                new SubjectInfo("TestUsingSpecialCase3"),
                new SubjectInfo("TestUsingVarRefTree"),
                new SubjectInfo("TestUsingVarSpecialCase2"),
                new SubjectInfo("TestUsingVarWithDeclarationTree"),
                new SubjectInfo("TestUsingWithVarDeclaration"),
                new SubjectInfo("TestYieldBreakExpression"),
            };
            // TypeTests
            var lTypeTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ArrayTypeGetHashCode"),
                new SubjectInfo("ArrayTypeInterfaces"),
                new SubjectInfo("ArrayTypes"),
                new SubjectInfo("BuiltInTypeNullableTest01"),
                new SubjectInfo("ClassWithMultipleConstr"),
                new SubjectInfo("DuplicateType"),
                new SubjectInfo("EmptyNameErrorSymbolErr"),
                new SubjectInfo("EnumFields"),
                new SubjectInfo("ErrorTypeSymbolWithArity"),
                new SubjectInfo("ExplicitlyImplementGenericInterface"),
                new SubjectInfo("ImplementNestedInterface_02"),
                new SubjectInfo("ImportedVersusSource"),
                new SubjectInfo("InaccessibleTypesSkipped"),
                new SubjectInfo("InheritedTypesCrossTrees"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_01"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_02"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_08"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_09"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_15"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_17"),
                new SubjectInfo("SimpleGeneric"),
                new SubjectInfo("SimpleNullable"),
            };
            // BindingTests
            var lBindingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AmbiguousAndBadArgument"),
                new SubjectInfo("AssignStaticEventToLocalVariable"),
                new SubjectInfo("DelegatesFromOverloads"),
                new SubjectInfo("EnumNotMemberInConstructor"),
                new SubjectInfo("GenericMethodName"),
                new SubjectInfo("GenericTypeName"),
                new SubjectInfo("InaccessibleAndAccessibleValid"),
                new SubjectInfo("InterfaceWithPartialMethodExplicitImplementation"),
                new SubjectInfo("NamespaceQualifiedGenericTypeName"),
                new SubjectInfo("NamespaceQualifiedGenericTypeNameWrongArity"),
                new SubjectInfo("NonMethodsWithArgs"),
                new SubjectInfo("ParenthesizedDelegate"),
                new SubjectInfo("RefReturningDelegateCreation"),
                new SubjectInfo("RefReturningDelegateCreationBad"),
                new SubjectInfo("SimpleDelegates"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario04"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario05"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario11"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation13"),
                new SubjectInfo("UseSiteErrorViaAliasTest06"),
            };
            // FlowTests
            var lFlowTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AssignedInFinallyUsedInTry"),
                new SubjectInfo("IfConditionalAnd"),
                new SubjectInfo("IsPatternBadValueConversion"),
                new SubjectInfo("IsPatternConversion"),
                new SubjectInfo("OutVarConversion"),
                new SubjectInfo("UnreachableDoWhileCondition"),
                new SubjectInfo("UseDefViolationInDelegateInSwitchWithGoto"),
                new SubjectInfo("UseDefViolationInUnreachableDelegate"),
                new SubjectInfo("UseDef_CondAccess02"),
                new SubjectInfo("UseDef_CondAccess03"),
                new SubjectInfo("UseDef_ExceptionFilters2"),
                new SubjectInfo("UseDef_ExceptionFilters5"),
                new SubjectInfo("IrrefutablePattern_1"),
                new SubjectInfo("UseDef_ExceptionFilters1"),
                new SubjectInfo("UseDef_CondAccess"),
                new SubjectInfo("UseDef_ExceptionFilters3"),
                new SubjectInfo("UseDef_ExceptionFilters6"),
                new SubjectInfo("ThrowStatement"),
                new SubjectInfo("WhidbeyBug467493_WithSuppression"),
                new SubjectInfo("ForEachStatement"),
            };
            // CompilationEmitTests
            var lCompilationEmitTests = new List<SubjectInfo>()
            {
                new SubjectInfo("Bug769741"),
                new SubjectInfo("CheckCOFFAndPEOptionalHeaders32"),
                new SubjectInfo("CheckRuntimeMDVersion"),
                new SubjectInfo("CompilationEmitWithQuotedMainType"),
                new SubjectInfo("EmitMetadataOnly_DisallowPdbs"),
                new SubjectInfo("EmitRefAssembly_PrivatePropertySetter"),
                new SubjectInfo("EmitAlwaysFalseExpression"),
                new SubjectInfo("EmitInvocationExprInIfStatementNestedInsideCatch"),
                new SubjectInfo("EmitLambdaInConstructorBody"),
                new SubjectInfo("EmitLambdaInConstructorInitializerAndBody"),
                new SubjectInfo("EmitNestedLambdaWithAddPlusOperator"),
                new SubjectInfo("IllegalNameOverride"),
                new SubjectInfo("PlatformMismatch_05"),
                new SubjectInfo("RefAssembly_CryptoHashFailedIsOnlyReportedOnce"),
                new SubjectInfo("RefAssembly_ReferenceAssemblyAttributeAlsoInSource"),
                new SubjectInfo("RefAssembly_StrongNameProvider_Arm64"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnStruct"),
                new SubjectInfo("RefAssemblyClient_EmitAllTypes"),
                new SubjectInfo("RefAssemblyClient_EmitVariance_InSuccess"),
                new SubjectInfo("WarnAsErrorDoesNotEmit_SpecificDiagnosticOption"),
            };
            // PDBTests
            var lPDBTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AnonymousType_Empty"),
                new SubjectInfo("DisabledLineDirective"),
                new SubjectInfo("DoStatement"),
                new SubjectInfo("ExpressionBodiedConstructor"),
                new SubjectInfo("ExpressionBodiedMethod"),
                new SubjectInfo("Return_ExpressionBodied1"),
                new SubjectInfo("Return_FromExceptionHandler1"),
                new SubjectInfo("Return_Void1"),
                new SubjectInfo("SequencePointsForConstructorWithHiddenInitializer"),
                new SubjectInfo("SyntaxOffset_IsPattern"),
                new SubjectInfo("ForStatement3"),
                new SubjectInfo("ForEachStatement_Enumerator"),
                new SubjectInfo("BranchToStartOfTry"),
                new SubjectInfo("FixedStatementSingleString"),
                new SubjectInfo("TestDeconstruction"),
                new SubjectInfo("Patterns_SwitchStatement_Constant"),
                new SubjectInfo("ExceptionHandling_Filter_Debug3"),
                new SubjectInfo("TestPartialClassFieldInitializersWithLineDirectives"),
                new SubjectInfo("FixedStatementSingleArray"),
                new SubjectInfo("ConditionalInAsyncMethodWithExplicitReturn"),
            };

            subjects.Add("StatementParsingTests", new GroupInfo() { Name = "StatementParsingTests", Subjects = lStatementParsingTests });
            subjects.Add("TypeTests", new GroupInfo() { Name = "TypeTests", Subjects = lTypeTests });
            subjects.Add("BindingTests", new GroupInfo() { Name = "BindingTests", Subjects = lBindingTests });
            subjects.Add("FlowTests", new GroupInfo() { Name = "FlowTests", Subjects = lFlowTests });
            subjects.Add("CompilationEmitTests", new GroupInfo() { Name = "CompilationEmitTests", Subjects = lCompilationEmitTests });
            subjects.Add("PDBTests", new GroupInfo() { Name = "PDBTests", Subjects = lPDBTests });
            return subjects;
        }

        static Dictionary<string, GroupInfo> Initialize20Subjects()
        {
            var subjects = new Dictionary<string, GroupInfo>();

            // StatementParsingTests
            var lStatementParsingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ParseCreateNullableTuple_01"),
                new SubjectInfo("TestConstLocalDeclarationStatement"),
                //new SubjectInfo("TestEmptyStatement"),
                new SubjectInfo("TestFixedVarStatement"),
                new SubjectInfo("TestFor"),
                new SubjectInfo("TestForEachAfterOffset"),
                new SubjectInfo("TestForWithIncrementor"),
                new SubjectInfo("TestForWithMultipleVariableInitializers"),
                new SubjectInfo("TestForWithVarDeclaration"),
                new SubjectInfo("TestForWithVariableDeclaration"),
                new SubjectInfo("TestForWithVariableInitializer"),
                new SubjectInfo("TestLocalDeclarationStatement"),
                new SubjectInfo("TestLocalDeclarationStatementWithDynamic"),
                //new SubjectInfo("TestLocalDeclarationStatementWithMixedType"),
                //new SubjectInfo("TestLock"),
                new SubjectInfo("TestRefLocalDeclarationStatement"),
                //new SubjectInfo("TestReturnExpression"),
                new SubjectInfo("TestSwitchWithMultipleCases"),
                new SubjectInfo("TestSwitchWithMultipleLabelsOnOneCase"),
                new SubjectInfo("TestSwitchWithMultipleStatementsOnOneCase"),
                //new SubjectInfo("TestThrow"),
                new SubjectInfo("TestTryCatchWithMultipleCatches"),
                //new SubjectInfo("TestUsingSpecialCase2"),
                //new SubjectInfo("TestUsingSpecialCase3"),
                new SubjectInfo("TestUsingVarRefTree"),
                //new SubjectInfo("TestUsingVarSpecialCase2"),
                new SubjectInfo("TestUsingVarWithDeclarationTree"),
                new SubjectInfo("TestUsingWithVarDeclaration"),
                //new SubjectInfo("TestYieldBreakExpression"),
            };
            // TypeTests
            var lTypeTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ArrayTypeGetHashCode"),
                new SubjectInfo("ArrayTypeInterfaces"),
                new SubjectInfo("ArrayTypes"),
                new SubjectInfo("BuiltInTypeNullableTest01"),
                new SubjectInfo("ClassWithMultipleConstr"),
                new SubjectInfo("DuplicateType"),
                new SubjectInfo("EmptyNameErrorSymbolErr"),
                new SubjectInfo("EnumFields"),
                new SubjectInfo("ErrorTypeSymbolWithArity"),
                new SubjectInfo("ExplicitlyImplementGenericInterface"),
                new SubjectInfo("ImplementNestedInterface_02"),
                new SubjectInfo("ImportedVersusSource"),
                new SubjectInfo("InaccessibleTypesSkipped"),
                new SubjectInfo("InheritedTypesCrossTrees"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_01"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_02"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_08"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_09"),
                new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_15"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_17"),
                new SubjectInfo("SimpleGeneric"),
                new SubjectInfo("SimpleNullable"),
            };
            // BindingTests
            var lBindingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AmbiguousAndBadArgument"),
                new SubjectInfo("AssignStaticEventToLocalVariable"),
                new SubjectInfo("DelegatesFromOverloads"),
                new SubjectInfo("EnumNotMemberInConstructor"),
                new SubjectInfo("GenericMethodName"),
                new SubjectInfo("GenericTypeName"),
                new SubjectInfo("InaccessibleAndAccessibleValid"),
                new SubjectInfo("InterfaceWithPartialMethodExplicitImplementation"),
                new SubjectInfo("NamespaceQualifiedGenericTypeName"),
                new SubjectInfo("NamespaceQualifiedGenericTypeNameWrongArity"),
                new SubjectInfo("NonMethodsWithArgs"),
                new SubjectInfo("ParenthesizedDelegate"),
                new SubjectInfo("RefReturningDelegateCreation"),
                new SubjectInfo("RefReturningDelegateCreationBad"),
                new SubjectInfo("SimpleDelegates"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario04"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario05"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario11"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation13"),
                new SubjectInfo("UseSiteErrorViaAliasTest06"),
            };
            // FlowTests
            var lFlowTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AssignedInFinallyUsedInTry"),
                new SubjectInfo("IfConditionalAnd"),
                new SubjectInfo("IsPatternBadValueConversion"),
                new SubjectInfo("IsPatternConversion"),
                new SubjectInfo("OutVarConversion"),
                new SubjectInfo("UnreachableDoWhileCondition"),
                new SubjectInfo("UseDefViolationInDelegateInSwitchWithGoto"),
                new SubjectInfo("UseDefViolationInUnreachableDelegate"),
                new SubjectInfo("UseDef_CondAccess02"),
                new SubjectInfo("UseDef_CondAccess03"),
                new SubjectInfo("UseDef_ExceptionFilters2"),
                new SubjectInfo("UseDef_ExceptionFilters5"),
                new SubjectInfo("IrrefutablePattern_1"),
                new SubjectInfo("UseDef_ExceptionFilters1"),
                new SubjectInfo("UseDef_CondAccess"),
                new SubjectInfo("UseDef_ExceptionFilters3"),
                new SubjectInfo("UseDef_ExceptionFilters6"),
                new SubjectInfo("ThrowStatement"),
                new SubjectInfo("WhidbeyBug467493_WithSuppression"),
                new SubjectInfo("ForEachStatement"),
            };
            // CompilationEmitTests
            var lCompilationEmitTests = new List<SubjectInfo>()
            {
                new SubjectInfo("Bug769741"),
                new SubjectInfo("CheckCOFFAndPEOptionalHeaders32"),
                new SubjectInfo("CheckRuntimeMDVersion"),
                new SubjectInfo("CompilationEmitWithQuotedMainType"),
                new SubjectInfo("EmitMetadataOnly_DisallowPdbs"),
                new SubjectInfo("EmitRefAssembly_PrivatePropertySetter"),
                new SubjectInfo("EmitAlwaysFalseExpression"),
                new SubjectInfo("EmitInvocationExprInIfStatementNestedInsideCatch"),
                new SubjectInfo("EmitLambdaInConstructorBody"),
                new SubjectInfo("EmitLambdaInConstructorInitializerAndBody"),
                new SubjectInfo("EmitNestedLambdaWithAddPlusOperator"),
                new SubjectInfo("IllegalNameOverride"),
                new SubjectInfo("PlatformMismatch_05"),
                new SubjectInfo("RefAssembly_CryptoHashFailedIsOnlyReportedOnce"),
                new SubjectInfo("RefAssembly_ReferenceAssemblyAttributeAlsoInSource"),
                new SubjectInfo("RefAssembly_StrongNameProvider_Arm64"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnStruct"),
                new SubjectInfo("RefAssemblyClient_EmitAllTypes"),
                new SubjectInfo("RefAssemblyClient_EmitVariance_InSuccess"),
                new SubjectInfo("WarnAsErrorDoesNotEmit_SpecificDiagnosticOption"),
            };
            // PDBTests
            var lPDBTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AnonymousType_Empty"),
                new SubjectInfo("DisabledLineDirective"),
                new SubjectInfo("DoStatement"),
                new SubjectInfo("ExpressionBodiedConstructor"),
                new SubjectInfo("ExpressionBodiedMethod"),
                new SubjectInfo("Return_ExpressionBodied1"),
                new SubjectInfo("Return_FromExceptionHandler1"),
                new SubjectInfo("Return_Void1"),
                new SubjectInfo("SequencePointsForConstructorWithHiddenInitializer"),
                new SubjectInfo("SyntaxOffset_IsPattern"),
                new SubjectInfo("ForStatement3"),
                new SubjectInfo("ForEachStatement_Enumerator"),
                new SubjectInfo("BranchToStartOfTry"),
                new SubjectInfo("FixedStatementSingleString"),
                new SubjectInfo("TestDeconstruction"),
                new SubjectInfo("Patterns_SwitchStatement_Constant"),
                new SubjectInfo("ExceptionHandling_Filter_Debug3"),
                new SubjectInfo("TestPartialClassFieldInitializersWithLineDirectives"),
                new SubjectInfo("FixedStatementSingleArray"),
                new SubjectInfo("ConditionalInAsyncMethodWithExplicitReturn"),
            };

            subjects.Add("StatementParsingTests", new GroupInfo() { Name = "StatementParsingTests", Subjects = lStatementParsingTests });
            subjects.Add("TypeTests", new GroupInfo() { Name = "TypeTests", Subjects = lTypeTests });
            subjects.Add("BindingTests", new GroupInfo() { Name = "BindingTests", Subjects = lBindingTests });
            //subjects.Add("FlowTests", new GroupInfo() { Name = "FlowTests", Subjects = lFlowTests });
            //subjects.Add("CompilationEmitTests", new GroupInfo() { Name = "CompilationEmitTests", Subjects = lCompilationEmitTests });
            //subjects.Add("PDBTests", new GroupInfo() { Name = "PDBTests", Subjects = lPDBTests });
            return subjects;
        }

        static Dictionary<string, GroupInfo> InitializeNewSubjects()
        {
            var subjects = new Dictionary<string, GroupInfo>();

            // StatementParsingTests
            var lStatementParsingTests = new List<SubjectInfo>()
            {
                
            };
            // TypeTests
            var lTypeTests = new List<SubjectInfo>()
            {
                
            };
            // BindingTests
            var lBindingTests = new List<SubjectInfo>()
            {
                
            };
            // FlowTests
            var lFlowTests = new List<SubjectInfo>()
            {
                new SubjectInfo("General"),
                new SubjectInfo("ThrowStatement"),
                new SubjectInfo("IrrefutablePattern_1"),
                new SubjectInfo("UseDef_CondAccess"),
                new SubjectInfo("UsingAndLockStatements"),
                new SubjectInfo("UseDef_ExceptionFilters1"),
                new SubjectInfo("UseDef_ExceptionFilters3"),
                new SubjectInfo("UseDef_ExceptionFilters6"),
                new SubjectInfo("ForEachStatement"),
                new SubjectInfo("TryCatchStatement"),
                new SubjectInfo("WhileStatement"),
                new SubjectInfo("ForStatement"),
                new SubjectInfo("WhidbeyBug479106"),
                new SubjectInfo("WhidbeyBug467493"),
                new SubjectInfo("WhidbeyBug467493_WithSuppression"),
            };
            // CompilationEmitTests
            var lCompilationEmitTests = new List<SubjectInfo>()
            {
                new SubjectInfo("EmitInvocationExprInIfStatementNestedInsideCatch"),
                new SubjectInfo("EmitNestedLambdaWithAddPlusOperator"),
                new SubjectInfo("EmitLambdaInConstructorInitializerAndBody"),
                new SubjectInfo("EmitLambdaInConstructorBody"),
                new SubjectInfo("EmitAlwaysFalseExpression"),
                new SubjectInfo("RefAssemblyClient_EmitArgumentNames"),
                new SubjectInfo("RefAssemblyClient_EmitTupleNames"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnExplicitlyImplementedEvent"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnExplicitlyImplementedIndexer"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnExplicitlyImplementedProperty"),
            };
            // PDBTests
            var lPDBTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ConditionalInAsyncMethodWithExplicitReturn"),
                new SubjectInfo("BranchToStartOfTry"),
                new SubjectInfo("ExceptionHandling_Filter_Debug3"),
                new SubjectInfo("FixedStatementMultipleArrays"),
                new SubjectInfo("FixedStatementSingleString"),
                new SubjectInfo("ForStatement3"),
                new SubjectInfo("HeadingHiddenSequencePointsPickUpDocumentFromVisibleSequencePoint"),
                new SubjectInfo("FixedStatementSingleArray"),
                new SubjectInfo("MethodsWithDebuggerAttributes"),
                new SubjectInfo("SyntaxOffsetInPresenceOfTrivia_Initializers"),
                new SubjectInfo("TestPartialClassFieldInitializersWithLineDirectives"),
                new SubjectInfo("ImportsInIterator"),
                new SubjectInfo("Patterns_SwitchStatement_Constant"),
                new SubjectInfo("SwitchWithConstantGenericPattern_02"),
                new SubjectInfo("SyntaxOffset_OutVarInQuery_01"),
                new SubjectInfo("TestDeconstruction"),
                new SubjectInfo("ForEachStatement_Enumerator"),
                new SubjectInfo("ForEachStatement_Deconstruction"),
            };

            subjects.Add("FlowTests", new GroupInfo() { Name = "FlowTests", Subjects = lFlowTests });
            subjects.Add("CompilationEmitTests", new GroupInfo() { Name = "CompilationEmitTests", Subjects = lCompilationEmitTests });
            subjects.Add("PDBTests", new GroupInfo() { Name = "PDBTests", Subjects = lPDBTests });
            return subjects;
        }

        static void CompareWithOriginal(Dictionary<string, GroupInfo> reduced, Dictionary<string, GroupInfo> original)
        {
            var lessLinesGeneral = new HashSet<FileLine>();
            var differentCriterionSize = new List<string>();
            var lessLinesSubjects = new Dictionary<string, HashSet<FileLine>>();

            foreach (var group in reduced)
            { 
                foreach (var subjectInfo in group.Value.Subjects)
                {
                    var anotherSubjectInfo = original[group.Key].Subjects.FirstOrDefault(x => x.Name.Equals(subjectInfo.Name));
                    if (anotherSubjectInfo == null) 
                    {
                        subjectInfo.hayOriginal = false;
                        continue;
                    }

                    if (anotherSubjectInfo.Criteria.Count != subjectInfo.Criteria.Count)
                    {
                        differentCriterionSize.Add(group + " - " + subjectInfo.Name);
                        continue;
                    }

                    if (subjectInfo.Name.Contains("Return_Void1"))
                        ;

                    // Cálculo por criterion
                    for (var i = 0; i < subjectInfo.Criteria.Count; i++)
                    {
                        var anotherCriterion = anotherSubjectInfo.Criteria[i];

                        subjectInfo.Criteria[i].traceDiff = anotherCriterion.totalTraceLines - subjectInfo.Criteria[i].totalTraceLines;
                        subjectInfo.Criteria[i].traceDiffPercentage = GetPercentage(subjectInfo.Criteria[i].traceDiff, anotherCriterion.totalTraceLines);
                        subjectInfo.Criteria[i].secOrig = anotherCriterion.executionSeconds;
                        subjectInfo.Criteria[i].secDiff = anotherCriterion.executionSeconds - subjectInfo.Criteria[i].executionSeconds;
                        subjectInfo.Criteria[i].secDiffPercentage = GetPercentage(subjectInfo.Criteria[i].secDiff, anotherCriterion.executionSeconds);

                        // Puede fallar... (hay nombres de archivos repetidos)
                        var newLines = subjectInfo.Criteria[i].FilteredResult
                            .Where(x => !anotherCriterion.FilteredResult.Any(y => x.File.Equals(y.File))).ToList();

                        var excludedFiles = subjectInfo.SkippedInfo.SkippedFiles.Select(x => Path.GetFileName(x)).ToList();
                        var lessLines = new HashSet<FileLine>(anotherCriterion.FilteredResult
                            .Where(x => !subjectInfo.Criteria[i].FilteredResult.Any(y => x.File.Equals(y.File)) && 
                                        !excludedFiles.Any(y => y.Equals(x.File))));
                        
                        if (lessLines.Count > 0) { 
                            lessLinesGeneral.UnionWith(lessLines);
                            lessLinesSubjects.Add(group.Key + " - " + subjectInfo.Name + " - Criterion: " + (i+1).ToString(), lessLines);
                        }

                        var newLinesPercentage = GetPercentage(newLines.Count, subjectInfo.Criteria[i].slicedStatements);

                        subjectInfo.Criteria[i].newLines = newLines.ToList();
                        subjectInfo.Criteria[i].cantNewLines = newLines.Count;
                        subjectInfo.Criteria[i].cantNewLinesPercentage = newLinesPercentage;
                        subjectInfo.Criteria[i].perdidaDePrecisionPercentage = 100 -
                            (double)subjectInfo.Criteria[i].computed_filtered_statements * (double)100 / ((double)subjectInfo.Criteria[i].computed_filtered_statements - (double)newLines.Count);
                    }

                    // Cálculo por subject
                    subjectInfo.avg_traceDiff = subjectInfo.Criteria.Average(x => x.traceDiff);
                    var anotherAvg_trace = anotherSubjectInfo.Criteria.Average(x => x.totalTraceLines);
                    subjectInfo.avg_traceDiffPercentage = GetPercentage(subjectInfo.avg_traceDiff, anotherAvg_trace); //subjectInfo.Criteria.Average(x => x.traceDiffPercentage);
                    subjectInfo.avg_secDiff = subjectInfo.Criteria.Average(x => x.secDiff);
                    var anotherAvg_sec = anotherSubjectInfo.Criteria.Average(x => x.executionSeconds);
                    subjectInfo.avg_secDiffPercentage = GetPercentage(subjectInfo.avg_secDiff, anotherAvg_sec); //subjectInfo.Criteria.Average(x => x.secDiffPercentage);
                    subjectInfo.avg_cantNewLines = subjectInfo.Criteria.Average(x => x.cantNewLines);
                    subjectInfo.avg_cantNewLinesPercentage = subjectInfo.Criteria.Average(x => x.cantNewLinesPercentage);
                }

                // Cálculo por grupo
                var anotherGroup = original.Single(x => x.Key == group.Key);
                group.Value.avg_traceDiff = group.Value.Subjects.Average(x => x.avg_traceDiff);
                var anotherAvg_TraceGroup = anotherGroup.Value.Subjects.Average(x => x.avg_traza_lineas);
                group.Value.avg_traceDiffPercentage = anotherAvg_TraceGroup > 1 ? GetPercentage(group.Value.avg_traceDiff, anotherAvg_TraceGroup) : 0; //group.Value.Subjects.Average(x => x.avg_traceDiffPercentage);
                group.Value.avg_secDiff = group.Value.Subjects.Average(x => x.avg_secDiff);
                var anotherAvg_SecGroup = anotherGroup.Value.Subjects.Average(x => x.avg_analisis);
                group.Value.avg_secDiffPercentage = anotherAvg_SecGroup > 1 ? GetPercentage(group.Value.avg_secDiff, anotherAvg_SecGroup) : 0; // group.Value.Subjects.Average(x => x.avg_secDiffPercentage);
                group.Value.avg_cantNewLines = group.Value.Subjects.Average(x => x.avg_cantNewLines);
                group.Value.avg_cantNewLinesPercentage = group.Value.Subjects.Average(x => x.avg_cantNewLinesPercentage);
                group.Value.avg_totalFilesWithLines = group.Value.Subjects.Average(x => x.SkippedInfo.TotalFilesWithLines);
                group.Value.avg_totalFilesExcluded = group.Value.Subjects.Average(x => x.SkippedInfo.TotalFilesExcluded);
                group.Value.avg_totalFilesExcludedPercentage = group.Value.Subjects.Average(x => x.SkippedInfo.TotalFilesExcludedPercentage);
            }

            //var sb = new StringBuilder();
            //foreach (var ln in lessLinesGeneral)
            //    sb.AppendLine(ln.File + ":" + ln.Line.ToString());
            //foreach (var ln in lessLinesSubjects)
            //{
            //    sb.AppendLine(ln.Key + ":");
            //    foreach (var newl in ln.Value)
            //        sb.AppendLine("     " + newl.File + ":" + newl.Line);
            //}
            //Console.WriteLine(sb.ToString());
        }

        public static HashSet<ExecutedMethodInfo> ParseExecutedMethodInfoFile(string path)
        {
            var retList = new HashSet<ExecutedMethodInfo>();
            var allLines = File.ReadAllLines(path);

            foreach (var line in allLines)
                // |System.Runtime.CompilerServices|RuntimeHelpers|RunModuleConstructor|System.ModuleHandle module|METHOD|4|FULL HAVOC
                retList.Add(ParseExecutedMethodInfo(line));

            return retList;
        }

        public static string GetDurationFromResFile(string path)
        {
            try
            { 
                var lastLine = File.ReadAllLines(path).Where(x => !string.IsNullOrWhiteSpace(x)).Last();
                var h = lastLine.Split(new string[] { "Duration: " }, StringSplitOptions.RemoveEmptyEntries)[1];
                var f = h.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries)[0];
                return f;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static void ParseCallbackInfo(string path, HashSet<ExecutedMethodInfo> executedMethods)
        {
            var allLines = File.ReadAllLines(path);
            foreach (var line in allLines)
            {
                var tup = ParseCallbackMethodInfo(line);
                var instance = executedMethods.Single(x => x.Equals(tup.Item1));
                instance.Callbacks.Add(tup.Item2);
            }
        }

        public static ExecutedMethodInfo ParseExecutedMethodInfo(string str)
        {
            var s = str.Split('|');
            var emi = new ExecutedMethodInfo()
            {
                NamespaceName = s[1],
                ClassName = s[2],
                MethodName = s[3],
                Parameters = s[4],
                Type = s[5],
                Invocations = int.Parse(s[6]),
                Annotation = s[7],
                Callbacks = new List<ExecutedMethodInfo>()
            };
            return emi;
        }

        public static SkippedFilesInfo ParseSkippedFilesInfo(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return null;

            var retObj = new SkippedFilesInfo();
            var allLines = File.ReadAllLines(path);
            retObj.TotalLines = int.Parse(allLines[0].Split(':')[1].Trim());
            retObj.SkippedLines = int.Parse(allLines[1].Split(':')[1].Trim());
            retObj.SkippedLinesPercentage = GetPercentage(retObj.SkippedLines, retObj.TotalLines);
            retObj.TotalFilesWithLines = int.Parse(allLines[3].Split(':')[1].Trim());
            retObj.TotalFilesExcluded = int.Parse(allLines[4].Split(':')[1].Trim());
            retObj.TotalFilesExcludedPercentage = GetPercentage(retObj.TotalFilesExcluded, retObj.TotalFilesWithLines);
            for (var i = 7; i < allLines.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(allLines[i]) && !allLines[i].Contains("==="))
                {
                    var s = allLines[i].Split('|');
                    var newFileInfo = new SkippedFilesInfo.SkippedFile();
                    newFileInfo.Path = s[0].Trim();
                    newFileInfo.Count = int.Parse(s[1].Trim());
                    newFileInfo.Percentage = GetPercentage(newFileInfo.Count, retObj.TotalLines);
                    newFileInfo.Files = 
                        FilesFromFolders(new List<string>() { newFileInfo.Path })
                            .Select(x =>  Path.GetFileName(x)).ToList();
                    retObj.SkippedFolders.Add(newFileInfo);
                }
            }

            return retObj;
        }

        static Dictionary<string, string[]> Cache = new Dictionary<string, string[]>();

        static List<string> FilesFromFolders(List<string> folders)
        {
            var retList = new List<string>();
            foreach (var folder in folders)
            {
                if (folder.EndsWith(".cs"))
                    continue;
                if (!Cache.ContainsKey(folder))
                    Cache[folder] = System.IO.Directory.GetFiles(folder, "*", SearchOption.AllDirectories);
                retList.AddRange(Cache[folder]);
            }
            return retList;
        }

        static double GetPercentage(int part, int total)
        {
            return Math.Round((double)part * (double)100 / (double)total, 2);
        }
        public static double GetPercentage(double part, double total)
        {
            return Math.Round((double)part * (double)100 / (double)total, 2);
        }

        static Tuple<ExecutedMethodInfo, ExecutedMethodInfo> ParseCallbackMethodInfo(string str)
        {
            str = str.Replace("Fresh.{Many{|", "Fresh.{Many{");
            var s = str.Split('|');
            var emi = new ExecutedMethodInfo()
            {
                NamespaceName = s[1],
                ClassName = s[2],
                MethodName = s[3],
                Parameters = s[4],
                Type = s[5],
                Invocations = int.Parse(s[6]),
                Annotation = s[7],
                Callbacks = new List<ExecutedMethodInfo>()
            };

            var call = new ExecutedMethodInfo()
            {
                NamespaceName = s[9],
                ClassName = s[10],
                MethodName = s[11],
                Parameters = s[12],
                Type = s[13],
                Invocations = int.Parse(s[14]),
                Annotation = "",
            };

            return new Tuple<ExecutedMethodInfo, ExecutedMethodInfo>(emi, call);
        }

        static Dictionary<string, GroupInfo> FakeSubjects()
        {
            var subjects = new Dictionary<string, GroupInfo>();

            // StatementParsingTests
            var lStatementParsingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("ParseCreateNullableTuple_01"),
                new SubjectInfo("TestConstLocalDeclarationStatement"),
                new SubjectInfo("TestEmptyStatement"),
                new SubjectInfo("TestFixedVarStatement"),
                new SubjectInfo("TestFor"),
                new SubjectInfo("TestForEachAfterOffset"),
                new SubjectInfo("TestForWithIncrementor"),
                new SubjectInfo("TestForWithMultipleVariableInitializers"),
                new SubjectInfo("TestForWithVarDeclaration"),
                new SubjectInfo("TestForWithVariableDeclaration"),
                new SubjectInfo("TestForWithVariableInitializer"),
                new SubjectInfo("TestLocalDeclarationStatement"),
                new SubjectInfo("TestLocalDeclarationStatementWithDynamic"),
                new SubjectInfo("TestLocalDeclarationStatementWithMixedType"),
                new SubjectInfo("TestLock"),
                new SubjectInfo("TestRefLocalDeclarationStatement"),
                new SubjectInfo("TestReturnExpression"),
                new SubjectInfo("TestSwitchWithMultipleCases"),
                new SubjectInfo("TestSwitchWithMultipleLabelsOnOneCase"),
                new SubjectInfo("TestSwitchWithMultipleStatementsOnOneCase"),
                new SubjectInfo("TestThrow"),
                new SubjectInfo("TestTryCatchWithMultipleCatches"),
                new SubjectInfo("TestUsingSpecialCase2"),
                new SubjectInfo("TestUsingSpecialCase3"),
                new SubjectInfo("TestUsingVarRefTree"),
                new SubjectInfo("TestUsingVarSpecialCase2"),
                new SubjectInfo("TestUsingVarWithDeclarationTree"),
                new SubjectInfo("TestUsingWithVarDeclaration"),
                new SubjectInfo("TestYieldBreakExpression"),
            };
            // TypeTests
            var lTypeTests = new List<SubjectInfo>()
            {
                //new SubjectInfo("ArrayTypeGetHashCode"),
                //new SubjectInfo("ArrayTypeInterfaces"),
                new SubjectInfo("ArrayTypes"),
                //new SubjectInfo("BuiltInTypeNullableTest01"),
                //new SubjectInfo("ClassWithMultipleConstr"),
                //new SubjectInfo("DuplicateType"),
                //new SubjectInfo("EmptyNameErrorSymbolErr"),
                //new SubjectInfo("EnumFields"),
                //new SubjectInfo("ErrorTypeSymbolWithArity", false),
                //new SubjectInfo("ExplicitlyImplementGenericInterface"),
                //new SubjectInfo("ImplementNestedInterface_02"),
                //new SubjectInfo("ImportedVersusSource"),
                //new SubjectInfo("InaccessibleTypesSkipped"),
                //new SubjectInfo("InheritedTypesCrossTrees"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_01"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_02"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_08"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_09"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_15"),
                //new SubjectInfo("IsExplicitDefinitionOfNoPiaLocalType_17"),
                //new SubjectInfo("SimpleGeneric"),
                //new SubjectInfo("SimpleNullable"),
            };
            // BindingTests
            var lBindingTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AmbiguousAndBadArgument"),
                new SubjectInfo("AssignStaticEventToLocalVariable"),
                new SubjectInfo("DelegatesFromOverloads"),
                new SubjectInfo("EnumNotMemberInConstructor"),
                new SubjectInfo("GenericMethodName"),
                new SubjectInfo("GenericTypeName"),
                new SubjectInfo("InaccessibleAndAccessibleValid"),
                new SubjectInfo("InterfaceWithPartialMethodExplicitImplementation"),
                new SubjectInfo("NamespaceQualifiedGenericTypeName"),
                new SubjectInfo("NamespaceQualifiedGenericTypeNameWrongArity"),
                new SubjectInfo("NonMethodsWithArgs"),
                new SubjectInfo("ParenthesizedDelegate"),
                new SubjectInfo("RefReturningDelegateCreation"),
                new SubjectInfo("RefReturningDelegateCreationBad"),
                new SubjectInfo("SimpleDelegates"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario04"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario05"),
                new SubjectInfo("UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario11"),
                new SubjectInfo("UseSiteErrorViaAliasTest06"),
            };
            // FlowTests
            var lFlowTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AssignedInFinallyUsedInTry"),
                new SubjectInfo("IfConditionalAnd"),
                new SubjectInfo("IsPatternBadValueConversion"),
                new SubjectInfo("IsPatternConversion"),
                new SubjectInfo("OutVarConversion"),
                new SubjectInfo("UnreachableDoWhileCondition"),
                new SubjectInfo("UseDefViolationInDelegateInSwitchWithGoto"),
                new SubjectInfo("UseDefViolationInUnreachableDelegate"),
                new SubjectInfo("UseDef_CondAccess02"),
                new SubjectInfo("UseDef_CondAccess03"),
                new SubjectInfo("UseDef_ExceptionFilters2"),
                new SubjectInfo("UseDef_ExceptionFilters5"),
            };
            // CompilationEmitTests
            var lCompilationEmitTests = new List<SubjectInfo>()
            {
                new SubjectInfo("Bug769741"),
                new SubjectInfo("CheckCOFFAndPEOptionalHeaders32"),
                new SubjectInfo("CheckRuntimeMDVersion"),
                new SubjectInfo("CompilationEmitWithQuotedMainType"),
                new SubjectInfo("EmitMetadataOnly_DisallowPdbs"),
                new SubjectInfo("EmitRefAssembly_PrivatePropertySetter"),
                new SubjectInfo("IllegalNameOverride"),
                new SubjectInfo("PlatformMismatch_05"),
                new SubjectInfo("RefAssembly_CryptoHashFailedIsOnlyReportedOnce"),
                new SubjectInfo("RefAssembly_ReferenceAssemblyAttributeAlsoInSource"),
                new SubjectInfo("RefAssembly_StrongNameProvider_Arm64"),
                new SubjectInfo("RefAssembly_VerifyTypesAndMembersOnStruct"),
                new SubjectInfo("RefAssemblyClient_EmitAllTypes"),
                new SubjectInfo("RefAssemblyClient_EmitVariance_InSuccess"),
                new SubjectInfo("WarnAsErrorDoesNotEmit_SpecificDiagnosticOption"),
            };
            // PDBTests
            var lPDBTests = new List<SubjectInfo>()
            {
                new SubjectInfo("AnonymousType_Empty"),
                new SubjectInfo("DisabledLineDirective"),
                new SubjectInfo("DoStatement"),
                new SubjectInfo("ExpressionBodiedConstructor"),
                new SubjectInfo("ExpressionBodiedMethod"),
                new SubjectInfo("Return_ExpressionBodied1"),
                new SubjectInfo("Return_FromExceptionHandler1"),
                new SubjectInfo("Return_Void1"),
                new SubjectInfo("SequencePointsForConstructorWithHiddenInitializer"),
                new SubjectInfo("SyntaxOffset_IsPattern"),
            };

            //subjects.Add("StatementParsingTests", new GroupInfo() { Name = "StatementParsingTests", Subjects = lStatementParsingTests });
            subjects.Add("TypeTests", new GroupInfo() { Name = "TypeTests", Subjects = lTypeTests });
            //subjects.Add("BindingTests", new GroupInfo() { Name = "BindingTests", Subjects = lBindingTests });
            //subjects.Add("FlowTests", new GroupInfo() { Name = "FlowTests", Subjects = lFlowTests });
            //subjects.Add("CompilationEmitTests", new GroupInfo() { Name = "CompilationEmitTests", Subjects = lCompilationEmitTests });
            //subjects.Add("PDBTests", new GroupInfo() { Name = "PDBTests", Subjects = lPDBTests });
            return subjects;
        }

        #region Classes
        public class SkippedFilesInfo
        {
            public int TotalLines { get; set; }
            public int SkippedLines { get; set; }
            public double SkippedLinesPercentage { get; set; }
            public int TotalFilesWithLines { get; set; }
            public int TotalFilesExcluded { get; set; }
            public double TotalFilesExcludedPercentage { get; set; }

            public List<SkippedFile> SkippedFolders { get; set; } = new List<SkippedFile>();
            
            public List<string> _SkippedFiles = null;
            public List<string> SkippedFiles { 
                get 
                {
                    if (_SkippedFiles == null)
                        _SkippedFiles = SkippedFolders.SelectMany(x => x.Files).ToList();
                    return _SkippedFiles;
                } 
            }

            public class SkippedFile
            {
                public string Path { get; set; }
                public int Count { get; set; }
                public double Percentage { get; set; }
                public List<string> Files { get; set; } = new List<string>();
            }
        }

        public class SubjectInfo
        {
            public SubjectInfo(string name, bool seleccionFinal = true) { Name = name; SeleccionFinal = seleccionFinal; }

            public string Name { get; set; }
            public bool SeleccionFinal = true;
            public List<SummaryInfoDynAbs> Criteria { get; set; } = new List<SummaryInfoDynAbs>();
            public HashSet<ExecutedMethodInfo> MethodsInfo { get; set; } = new HashSet<ExecutedMethodInfo>();

            public SkippedFilesInfo SkippedInfo { get; set; }
            public string TestDuration { get; set; }

            // Comparación contra el original

            public bool hayOriginal { get; set; } = true;
            public double avg_traceDiff { get; set; }
            public double avg_traceDiffPercentage { get; set; }
            public double avg_secDiff { get; set; }
            public double avg_secDiffPercentage { get; set; }
            public double avg_cantNewLines { get; set; }
            public double avg_cantNewLinesPercentage { get; set; }

            // Summaries varios

            public double avg_stmt_exec;
            public double avg_stmt_distintos;

            public double avg_slice;
            public double avg_slice_perc;
            public double min_slice_perc;
            public double max_slice_perc;

            public double avg_filtered;
            public double avg_filtered_perc;
            public double min_filtered_perc;
            public double max_filtered_perc;

            public double avg_instr;
            public double avg_analisis;
            public double avg_traza_lineas;
            public double avg_traza_size;
        }

        class GroupInfo
        {
            public List<SubjectInfo> Subjects { get; set; } = new List<SubjectInfo>();

            public string Name { get; set; }

            public double avg_traceDiff { get; set; }
            public double avg_traceDiffPercentage { get; set; }
            public double avg_secDiff { get; set; }
            public double avg_secDiffPercentage { get; set; }
            public double avg_cantNewLines { get; set; }
            public double avg_cantNewLinesPercentage { get; set; }

            public double avg_totalFilesWithLines { get; set; }
            public double avg_totalFilesExcluded { get; set; }
            public double avg_totalFilesExcludedPercentage { get; set; }
        }
        #endregion
    }
}
