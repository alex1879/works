﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ResultsProcessor
{
    public class Powershell
    {
        public static void Main()
        {
            var path = @"C:\Users\alexd\Desktop\Slicer\Powershell\results\end\sinCore";
            var tracesBasePath = @"C:\Users\alexd\Desktop\Slicer\Powershell\traces\tests";
            var groups = FillSubjects(path, tracesBasePath);
            PrintDeglose(groups);
            Console.WriteLine("============");
            PrintGroups(groups);
            Console.ReadLine();
        }

        static Dictionary<string, GroupInfo> FillSubjects(string path, string tracesBasePath, bool skippingFiles = false)
        {
            var groups = Directory.GetDirectories(path).Select(x => Path.GetFileName(x)).ToArray();
            var groupsDict = new Dictionary<string, GroupInfo>();

            foreach (var group in groups)
            {
                var mainResultPath = Path.Combine(path, group);
                var subjects = new Dictionary<string, Roslyn.SubjectInfo>();
                foreach (var dir in Directory.GetDirectories(mainResultPath))
                {
                    var subjectName = Path.GetFileName(dir).Replace("DynAbsTrace_", "").Replace("_ORIG", "");

                    var subject = new Roslyn.SubjectInfo(subjectName);
                    var localResultPath = dir;
                    // Files:
                    var executedMethodPath = Path.Combine(localResultPath, "ExecutedMethods.txt");
                    var executedCallbacksPath = Path.Combine(localResultPath, "ExecutedCallbacks.txt");
                    var skippedFilesPath = Path.Combine(localResultPath, "SkippedFilesInfo.txt");

                    if (!File.Exists(executedMethodPath))
                        continue;

                    var groupName = group.Replace("Tests", "");
                    if (groupName == "PowerShellPolicy")
                        groupName = "PSConfiguration";
                    if (groupName == "Platform")
                        groupName = "CorePsPlatform";
                    if (groupName == "PSEnumerableBinder")
                        groupName = "Binders";

                    var resFileName = $"{groupName}.{subjectName}.res";
                    var resFile = Path.Combine(tracesBasePath, groupName, subjectName, resFileName);
                    var duration = Roslyn.GetDurationFromResFile(resFile);
                    subject.TestDuration = duration;

                    // Console.WriteLine($"{group};{subjectName};{duration}");

                    // Load each file:
                    subject.Criteria = Program.ProcessNETSlicerResults(localResultPath, null);
                    // Load methods info
                    var methodsInfo = Roslyn.ParseExecutedMethodInfoFile(executedMethodPath);
                    Roslyn.ParseCallbackInfo(executedCallbacksPath, methodsInfo);
                    subject.MethodsInfo = methodsInfo;
                    // Skipped info
                    if (skippingFiles)
                        subject.SkippedInfo = Roslyn.ParseSkippedFilesInfo(skippedFilesPath);

                    if (subject.Criteria.Count > 0)
                    {
                        // Compute statistics
                        subject.avg_stmt_exec = subject.Criteria.Average(x => x.totalStatementsLines);
                        subject.avg_stmt_distintos = subject.Criteria.Average(x => x.distinctStatementLines);
                        // Primer slice
                        subject.avg_slice = subject.Criteria.Average(x => x.computed_sliced_statements);
                        subject.avg_slice_perc = Roslyn.GetPercentage(subject.avg_slice, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_sliced_percentage);
                        subject.min_slice_perc = subject.Criteria.Min(x => x.computed_sliced_percentage);
                        subject.max_slice_perc = subject.Criteria.Max(x => x.computed_sliced_percentage);
                        // Segundo slice
                        subject.avg_filtered = subject.Criteria.Average(x => x.computed_filtered_statements);
                        subject.avg_filtered_perc = Roslyn.GetPercentage(subject.avg_filtered, subject.avg_stmt_distintos); //subject.Criteria.Average(x => x.computed_filtered_percentage);
                        subject.min_filtered_perc = subject.Criteria.Min(x => x.computed_filtered_percentage);
                        subject.max_filtered_perc = subject.Criteria.Max(x => x.computed_filtered_percentage);
                        // Tiempos y traza
                        subject.avg_instr = subject.Criteria.First().totalSecondsInstrumentation;
                        subject.avg_analisis = subject.Criteria.Average(x => x.executionSeconds);
                        subject.avg_traza_lineas = subject.Criteria.Average(x => x.totalTraceLines);
                        subject.avg_traza_size = subject.Criteria.Average(x => x.traceSize);
                    }
                    subjects.Add(subjectName, subject);
                }
                groupsDict.Add(group, new GroupInfo() { Name = group, Subjects = subjects });
            }

            return groupsDict;
        }

        static void PrintDeglose(Dictionary<string, GroupInfo> groups)
        {
            var sb = new StringBuilder();
            foreach (var group in groups)
                foreach (var subjectKv in group.Value.Subjects)
                {
                    var subject = subjectKv.Value;
                    var criterion = subject.Criteria.Single();
                    // Subject	# Criterios	AVG Stmt. Exec	AVG Stmt. Distintos	AVG Slice	AVG %	MIN %	MAX %	AVG Slice F.	AVG %	MIN %	MAX %	Instr. (seg)	AVG Análisis (seg)	AVG #Traza	AVG Traza (KB)
                    Console.WriteLine($"{group.Key};{subjectKv.Key};{Path.GetFileNameWithoutExtension(criterion.criterion.FileLine.File)};{criterion.criterion.FileLine.Line};" +
                                        $"{criterion.criterion.Iteration};{criterion.totalStatementsLines};{criterion.distinctStatementLines};" +
                                        $"{criterion.computed_sliced_statements};{criterion.computed_sliced_percentage};{criterion.computed_filtered_statements};{criterion.computed_filtered_percentage};" +
                                        $"{criterion.totalSecondsInstrumentation};{criterion.executionSeconds};{criterion.totalTraceLines};{criterion.traceSize};" +
                                        $"{criterion.traceDiff};{criterion.traceDiffPercentage}");
                }
            Console.WriteLine(sb.ToString());
        }

        static void PrintGroups(Dictionary<string, GroupInfo> groups)
        {
            var sb = new StringBuilder();
            foreach (var group in groups)
            {
                var subSeleccion = group.Value.Subjects.Values.ToList();

                var cant_casos = subSeleccion.Count();
                var cant_criterias = subSeleccion.Sum(x => x.Criteria.Count);

                var avg_stmt_exec = subSeleccion.Average(x => x.avg_stmt_exec);
                var avg_stmt_distintos = subSeleccion.Average(x => x.avg_stmt_distintos);
                // Primer slice
                var avg_slice = subSeleccion.Average(x => x.avg_slice);
                var avg_slice_perc = Roslyn.GetPercentage(avg_slice, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_slice_perc);
                var min_slice_perc = subSeleccion.Min(x => x.min_slice_perc);
                var max_slice_perc = subSeleccion.Max(x => x.max_slice_perc);
                // Segundo slice
                var avg_filtered = subSeleccion.Average(x => x.avg_filtered);
                var avg_filtered_perc = Roslyn.GetPercentage(avg_filtered, avg_stmt_distintos); //subSeleccion.Average(x => x.avg_filtered_perc);
                var min_filtered_perc = subSeleccion.Min(x => x.min_filtered_perc);
                var max_filtered_perc = subSeleccion.Max(x => x.max_filtered_perc);
                // Tiempos y traza
                var avg_instr = subSeleccion.Average(x => x.avg_instr);
                var avg_analisis = subSeleccion.Average(x => x.avg_analisis);
                var avg_traza_lineas = subSeleccion.Average(x => x.avg_traza_lineas);
                var avg_traza_size = subSeleccion.Average(x => x.avg_traza_size);

                Console.WriteLine($"{group.Key};{cant_casos};{cant_criterias};{avg_stmt_exec};{avg_stmt_distintos};{avg_slice};" +
                    $"{avg_slice_perc};{min_slice_perc};{max_slice_perc};{avg_filtered};{avg_filtered_perc};{min_filtered_perc};" +
                    $"{max_filtered_perc};{avg_instr};{avg_analisis};{avg_traza_lineas};{avg_traza_size}");
            }
            Console.WriteLine(sb.ToString());
        }

        class GroupInfo
        {
            public string Name { get; set; }
            public Dictionary<string, Roslyn.SubjectInfo> Subjects = new Dictionary<string, Roslyn.SubjectInfo>();
        }
    }
}
