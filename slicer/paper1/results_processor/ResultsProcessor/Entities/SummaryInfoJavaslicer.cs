﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class SummaryInfoJavaSlicer
    {
        // La línea producida por el ResultsProcessor es:
        // $"{programName}|{sb.ToString().Trim()}|{tiempoOriginal}|{tiempoTraza}|{tiempoSlice}|{total_time}|{result}|{tamañoTraza}"
        public string name { get; set; }
        public string arguments { get; set; }
        public double tiempoOriginal { get; set; }
        public double tiempoTraza { get; set; }
        public double tiempoSlice { get; set; }
        public double total_time { get; set; }
        public string resultFile { get; set; }
        public double tamaño_traza { get; set; }
        // Computed
        public int sliced_lines { get; set; }
    }
}
