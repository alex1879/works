﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class ExecutedMethodInfo
    {
        public string NamespaceName { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string Parameters { get; set; }
        public string Type { get; set; } // METHOD OR PROPERTY
        public int Invocations { get; set; }
        public string Annotation { get; set; }
        public List<ExecutedMethodInfo> Callbacks { get; set; } = new List<ExecutedMethodInfo>();

        public override bool Equals(object obj)
        {
            if (obj is ExecutedMethodInfo executedMethodInfo)
                return this.NamespaceName.Equals(executedMethodInfo.NamespaceName) &&
                    this.ClassName.Equals(executedMethodInfo.ClassName) &&
                    this.MethodName.Equals(executedMethodInfo.MethodName) &&
                    this.Parameters.Equals(executedMethodInfo.Parameters);
            return false;
        }

        public override int GetHashCode()
        {
            return this.NamespaceName.GetHashCode() +
                this.ClassName.GetHashCode() +
                this.MethodName.GetHashCode() +
                this.Parameters.GetHashCode();
        }
    }
}
