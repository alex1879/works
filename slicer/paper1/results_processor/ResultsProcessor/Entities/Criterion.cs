﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class Criterion
    {
        public FileLine FileLine { get; set; }
        public int Iteration { get; set; }
        public override bool Equals(object obj)
        {
            if (obj is Criterion criterion)
                return criterion.Equals(this.FileLine) && criterion.Iteration.Equals(this.Iteration);
            return false;
        }
        public override int GetHashCode()
        {
            return FileLine.GetHashCode() + Iteration * 13;
        }
    }

    public class FileLine
    {
        public string File { get; set; }
        public int Line { get; set; }

        public FileLine(string file, int line)
        {
            File = file;
            Line = line;
        }

        public FileLine(string fileLine)
        {
            var s = fileLine.Split(':');
            File = s.First();
            Line = int.Parse(s.Last());
        }

        public override bool Equals(object obj)
        {
            if (obj is FileLine fileLine)
                return fileLine.File.Equals(this.File) && fileLine.Line.Equals(this.Line);
            return false;
        }
        public override int GetHashCode()
        {
            return File.GetHashCode() + Line * 17;
        }
    }
}
