﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    public class SummaryInfoDynAbs
    {
        // La línea producida en el slicer es: 
        // $"{name}|{arguments}|{totalStatementsLines}|{distinctStatementLines}|{slicedStatements}|{totalSecondsInstrumentation}|" +
        // $"{generationTraceTime}|{executionSeconds}|{traceSize}|{totalTraceLines}|{skippedTrace}|{skippedLinesPercent}"

        public string name { get; set; }
        public string arguments { get; set; }
        public int totalStatementsLines 
        {
            get
            {
                return fullExecutionTotalLines ?? summaryTotalStatementsLines;
            }
            set
            {
                throw new Exception();
            }
        }
        public int distinctStatementLines { get; set; }
        public int slicedStatements { get; set; }
        public double totalSecondsInstrumentation { get; set; }
        public double generationTraceTime { get; set; }
        public double executionSeconds { get; set; }
        public double traceSize { get; set; }
        public int totalTraceLines { get; set; }
        public int skippedTrace { get; set; }
        public double skippedTracePercent { get; set; }
        // Computados
        public double totalTime { get; set; }
        public int computed_sliced_statements { get; set; }
        public int computed_filtered_statements { get; set; }
        public double computed_sliced_percentage { get; set; }
        public double computed_filtered_percentage { get; set; }
        // Full exec values
        public int summaryTotalStatementsLines { get; set; }
        public int? fullExecutionTotalStatementsLines { get; set; }
        public int? fullExecutionTotalLines { get; set; }
        // Criterion
        public Criterion criterion { get; set; }
        // Comparación contra otro del mismo tipo
        public double traceDiff { get; set; }
        public double traceDiffPercentage { get; set; }
        public double secOrig { get; set; }
        public double secDiff { get; set; }
        public double secDiffPercentage { get; set; }
        public int cantNewLines { get; set; }
        public double cantNewLinesPercentage { get; set; }
        public double perdidaDePrecisionPercentage { get; set; }
        public int cantLessLines { get; set; } // Tiene que dar 0
        public List<FileLine> newLines { get; set; }

        public List<FileLine> FilteredResult { get; set; } = new List<FileLine>();

        public List<FileLine> executedLines { get; set; }

        public double speedUp
        {
            get
            {
                return Math.Max(secOrig / executionSeconds, 1);
            }
        }
    }
}
