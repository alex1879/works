﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultsProcessor
{
    static class PowershellUnitTestsGroups
    {
        public static List<Group> FillGroups()
        {
            var list = new List<Group>();

            var group1 = new Group()
            {
                Name = "Binders",
                Path = "test_Binders.cs",
                ClassName = "PSEnumerableBinderTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestIsStaticTypePossiblyEnumerable",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 16 }
                    }
                }
            };
            list.Add(group1);

            var group2 = new Group()
            {
                Name = "CorePsPlatform",
                Path = "test_CorePsPlatform.cs",
                ClassName = "PlatformTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestIsCoreCLR",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 17 }
                    }
                }
            };
            list.Add(group2);

            var group3 = new Group()
            {
                Name = "ExtensionMethods",
                Path = "test_ExtensionMethods.cs",
                ClassName = "PSTypeExtensionsTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestIsNumeric",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 15 }
                    }
                }
            };
            list.Add(group3);

            var group4 = new Group()
            {
                Name = "FileSystemProvider",
                Path = "test_FileSystemProvider.cs",
                ClassName = "FileSystemProviderTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestCreateJunctionFails",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 78 }
                    },
                    new TestMethod()
                    {
                        Name = "TestGetHelpMaml",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 86, 87, 88 }
                    },
                    new TestMethod()
                    {
                        Name = "TestMode",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 94, 112, 113, 114 }
                    },
                    new TestMethod()
                    {
                        Name = "TestGetProperty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 131 }
                    },
                    new TestMethod()
                    {
                        Name = "TestSetProperty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 146 }
                    },
                    //new TestMethod()
                    //{
                    //    Name = "TestClearProperty",
                    //    Mode = TestMode.Normal,
                    //    DefaultAssertLines = new List<int>() { }
                    //},
                    new TestMethod()
                    {
                        Name = "TestGetContentReader",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 168 }
                    },
                    new TestMethod()
                    {
                        Name = "TestGetContentWriter",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 183 }
                    },
                    new TestMethod()
                    {
                        Name = "TestClearContent",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 194 }
                    },
                }
            };
            list.Add(group4);

            var group5 = new Group()
            {
                Name = "MshSnapinInfo",
                Path = "test_MshSnapinInfo.cs",
                ClassName = "MshSnapinInfoTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    //new TestMethod()
                    //{
                    //    Name = "TestReadRegistryInfo",
                    //    Mode = TestMode.Normal,
                    //    DefaultAssertLines = new List<int>() {  }
                    //},
                    new TestMethod()
                    {
                        Name = "TestReadCoreEngineSnapIn",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 29 }
                    }
                }
            };
            list.Add(group5);

            var group6 = new Group()
            {
                Name = "NamedPipe",
                Path = "test_NamedPipe.cs",
                ClassName = "NamedPipeTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestCustomPipeNameCreation",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 21, 25, 28 }
                    },
                    new TestMethod()
                    {
                        Name = "TestCustomPipeNameCreationTooLongOnNonWindows",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 44 }
                    }
                }
            };
            list.Add(group6);

            var group7 = new Group()
            {
                Name = "PowerShellAPI",
                Path = "test_PowerShellAPI.cs",
                ClassName = "PowerShellHostingScenario",
                Namespace = "PSTests.Sequential",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestStartJobThrowTerminatingException",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 18, 19, 20 }
                    }
                }
            };
            list.Add(group7);

            var group8 = new Group()
            {
                Name = "PSConfiguration",
                Path = "test_PSConfiguration.cs",
                ClassName = "PowerShellPolicyTests",
                Namespace = "PSTests.Sequential",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesNotEmpty",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233, 393, 394 }
                    },
                    new TestMethod()
                    {
                        Name = "PowerShellConfig_GetPowerShellPolicies_EmptyUserConfig",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233, 409, 410 }
                    },
                    new TestMethod()
                    {
                        Name = "PowerShellConfig_GetPowerShellPolicies_EmptySystemConfig",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233, 424, 425 }
                    },
                    new TestMethod()
                    {
                        Name = "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesEmpty",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233, 439, 440 }
                    },
                    new TestMethod()
                    {
                        Name = "PowerShellConfig_GetPowerShellPolicies_BothConfigFilesNotExist",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 452, 453 }
                    },
                    new TestMethod()
                    {
                        Name = "Utils_GetPolicySetting_BothConfigFilesNotEmpty",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233 }
                    },
                    new TestMethod()
                    {
                        Name = "Utils_GetPolicySetting_EmptyUserConfig",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233 }
                    },
                    new TestMethod()
                    {
                        Name = "Utils_GetPolicySetting_EmptySystemConfig",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233 }
                    },
                    new TestMethod()
                    {
                        Name = "Utils_GetPolicySetting_BothConfigFilesEmpty",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233 }
                    },
                    new TestMethod()
                    {
                        Name = "Utils_GetPolicySetting_BothConfigFilesNotExist",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 128, 132, 133, 141, 145, 146, 154, 158, 161, 165, 168, 178, 182, 185, 189, 192, 202, 206, 207, 208, 216, 220, 228, 232, 233 }
                    },
                }
            };
            list.Add(group8);

            var group9 = new Group()
            {
                Name = "PSObject",
                Path = "test_PSObject.cs",
                ClassName = "PSObjectTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestEmptyObjectHasNoProperty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 24 }
                    },
                    new TestMethod()
                    {
                        Name = "TestWrappedDateTimeHasReflectedMember",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 32, 33 }
                    },
                    new TestMethod()
                    {
                        Name = "TestAdaptedMember",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 42, 43 }
                    },
                    new TestMethod()
                    {
                        Name = "TestShadowedMember",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 52, 53, 54 }
                    },
                    new TestMethod()
                    {
                        Name = "TestMemberSetIsNotProperty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 66 }
                    },
                    new TestMethod()
                    {
                        Name = "TestMemberSet",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 78, 79 }
                    },
                    new TestMethod()
                    {
                        Name = "TextXmlElementMember",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 94 }
                    },
                    new TestMethod()
                    {
                        Name = "TextXmlAttributeMember",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 108 }
                    },
                    new TestMethod()
                    {
                        Name = "TestCimInstanceProperty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 121, 123 }
                    }
                }
            };
            list.Add(group9);

            var group10 = new Group()
            {
                Name = "PSVersionInfo",
                Path = "test_PSVersionInfo.cs",
                ClassName = "PSVersionInfoTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestVersions",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 17 }
                    }
                }
            };
            list.Add(group10);

            var group11 = new Group()
            {
                Name = "Runspace",
                Path = "test_Runspace.cs",
                ClassName = "RunspaceTests",
                Namespace = "PSTests.Sequential",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestRunspaceWithPipeline",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 32, 35 }
                    },
                    new TestMethod()
                    {
                        Name = "TestRunspaceWithPowerShell",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 59, 62 }
                    },
                    new TestMethod()
                    {
                        Name = "TestRunspaceWithPowerShellAndInitialSessionState",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 95, 98 }
                    },
                }
            };
            list.Add(group11);

            var group12 = new Group()
            {
                Name = "SecuritySupport",
                Path = "test_SecuritySupport.cs",
                ClassName = "SecuritySupportTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestScanContent",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 17 }
                    }
                }
            };
            list.Add(group12);

            var group13 = new Group()
            {
                Name = "SessionState",
                Path = "test_SessionState.cs",
                ClassName = "SessionStateTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestDrives",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 31 }
                    }
                }
            };
            list.Add(group13);

            var group14 = new Group()
            {
                Name = "Utils",
                Path = "test_Utils.cs",
                ClassName = "UtilsTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestIsWinPEHost",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 22 }
                    },
                    new TestMethod()
                    {
                        Name = "TestHistoryStack",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 29, 30, 34, 35, 37, 38, 39, 40, 42, 43, 44, 48, 49, 52, 55, 56 }
                    },
                    new TestMethod()
                    {
                        Name = "TestBoundedStack",
                        Mode = TestMode.AtEndWithCriteria,
                        DefaultAssertLines = new List<int>() { 64, 74, 77 }
                    },
                    new TestMethod()
                    {
                        Name = "TestConvertToJsonBasic",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 90, 95 }
                    },
                    new TestMethod()
                    {
                        Name = "TestConvertToJsonWithEnum",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 107, 112 }
                    },
                    new TestMethod()
                    {
                        Name = "TestConvertToJsonWithoutCompress",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 126 }
                    },
                    new TestMethod()
                    {
                        Name = "TestConvertToJsonCancellation",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 147 }
                    }
                }
            };
            list.Add(group14);

            var group15 = new Group()
            {
                Name = "WildcardPattern",
                Path = "test_WildcardPattern.cs",
                ClassName = "WildcardPatternTests",
                Namespace = "PSTests.Parallel",
                TestMethods = new List<TestMethod>()
                {
                    new TestMethod()
                    {
                        Name = "TestEscape_Null",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 15 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_Empty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 21 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_A",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 30 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_B",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 38 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_C",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 46 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_NotEscape_A",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 54 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_NotEscape_B",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 62 }
                    },
                    new TestMethod()
                    {
                        Name = "TestEscape_String_NotEscape_C",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 70 }
                    },
                    new TestMethod()
                    {
                        Name = "TestUnescape_Null",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 76 }
                    },
                    new TestMethod()
                    {
                        Name = "TestUnescape_Empty",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 82 }
                    },
                    new TestMethod()
                    {
                        Name = "TestUnescape_String_A",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 90 }
                    },
                    new TestMethod()
                    {
                        Name = "TestUnescape_String_B",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 98 }
                    },
                    new TestMethod()
                    {
                        Name = "TestUnescape_String_C",
                        Mode = TestMode.Normal,
                        DefaultAssertLines = new List<int>() { 106 }
                    }
                }
            };
            list.Add(group15);

            return list;
        }

        public class Group
        {
            public string Name { get; set; }
            public string ClassName { get; set; }
            public string Namespace { get; set; }
            public string Path { get; set; }

            public List<TestMethod> TestMethods { get; set; }
        }

        public class TestMethod
        {
            public string Name { get; set; }

            public TestMode Mode { get; set; }

            public List<int> DefaultAssertLines { get; set; }
        }

        public enum TestMode
        {
            Normal = 1,
            AtEndWithCriteria = 2,
        }
    }
}
