using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QuickGraph;

namespace DGBuilder
{
    class Node
    {
        public string NamespaceAndClass;
        public string Method;
        public int Line;
        public IDictionary<Node, RelationPath> OwnRelations = null;
        public List<RelationNode> Relations = new List<RelationNode>();
    }

    class RelationPath
    {
        public List<RelationNode> Path = new List<RelationNode>();
    }

    class RelationNode
    {
        public Relation Relation;
        public Node Node;
    }

    enum RelationKind
    {
        Control, 
        Data
    }

    class Relation
    {
        public string Info;
        public RelationKind Kind;
    }

    class Program
    {
        static Dictionary<string, Node> Nodes = new Dictionary<string, Node>();
        static void Main(string[] args)
        {
            var file3 = @"C:\Users\Camilo\Desktop\DG_OrderLibrary_Variable3.txt";
            var resultFile3 = @"C:\Users\Camilo\Desktop\Filtered_DG_OrderLibrary_Variable3.txt";

            var file5 = @"C:\Users\Camilo\Desktop\DG_OrderLibrary_Variable5.txt";
            var resultFile5 = @"C:\Users\Camilo\Desktop\Filtered_DG_OrderLibrary_Variable5.txt";

            var var3 = LoadFile(file3, resultFile3);
            var var5 = LoadFile(file5, resultFile5);

            var commonKeys = var5.Keys.Intersect(var3.Keys);
            foreach (var k in commonKeys)
                if (var3[k].OwnRelations.Count != var5[k].OwnRelations.Count)
                { 
                    Console.WriteLine(k + ":");
                    Console.WriteLine("In 3: ");
                    foreach (var r in var3[k].OwnRelations)
                        if (!var5[k].OwnRelations.ContainsKey(r.Key))
                            Console.WriteLine(r.Key.NamespaceAndClass + ":" + r.Key.Method + ":" + r.Key.Line);
                    Console.WriteLine("In 5: ");
                    foreach (var r in var5[k].OwnRelations)
                        if (!var3[k].OwnRelations.ContainsKey(r.Key))
                            Console.WriteLine(r.Key.NamespaceAndClass + ":" + r.Key.Method + ":" + r.Key.Line);
                    Console.WriteLine("");
                }

            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }

        static Dictionary<string, Node> LoadFile(string file, string resultFile)
        {
            Nodes = new Dictionary<string, Node>();

            var allLines = System.IO.File.ReadAllLines(file);

            foreach (var l in allLines)
            {
                var l_parts = l.Split(' ');
                Node f_node = null;
                if (Nodes.ContainsKey(l_parts[0]))
                    f_node = Nodes[l_parts[0]];
                else
                {
                    var f_parts = l_parts[0].Split(':');
                    f_node = new Node()
                    {
                        NamespaceAndClass = f_parts[0],
                        Method = f_parts[1],
                        Line = int.Parse(f_parts[2])
                    };
                    Nodes.Add(l_parts[0], f_node);
                }

                Node t_node = null;
                if (Nodes.ContainsKey(l_parts[2]))
                    t_node = Nodes[l_parts[2]];
                else
                {
                    var t_parts = l_parts[2].Split(':');
                    t_node = new Node()
                    {
                        NamespaceAndClass = t_parts[0],
                        Method = t_parts[1],
                        Line = int.Parse(t_parts[2])
                    };
                    Nodes.Add(l_parts[2], t_node);
                }

                var relation = new Relation();
                relation.Kind = l_parts[3] == "data" ? RelationKind.Data : RelationKind.Control;
                relation.Info = l_parts[4];

                var relationNode = new RelationNode()
                {
                    Relation = relation,
                    Node = t_node
                };
                f_node.Relations.Add(relationNode);
            }

            foreach (var n in Nodes)
            {
                if (n.Value.NamespaceAndClass.Contains("com.syed"))
                    n.Value.OwnRelations = BFS(n.Value);
                else
                    n.Value.OwnRelations = new Dictionary<Node, RelationPath>();
            }

            var sb = new StringBuilder();
            foreach (var n in Nodes.Where(x => x.Key.Contains("com.syed")))
            {
                sb.AppendLine(n.Key + " -> ");
                if (n.Value.OwnRelations != null)
                    foreach (var r in n.Value.OwnRelations)
                        sb.AppendLine("         " + r.Key.NamespaceAndClass + ":" + r.Key.Method + ":" + r.Key.Line + " ");
                sb.AppendLine("");
            }
            System.IO.File.WriteAllText(resultFile, sb.ToString());

            return Nodes;
        }

        static IDictionary<Node, RelationPath> BFS(Node initVertex)
        {
            string userNamespace = "com.syed";
            
            var visited = new Dictionary<Node, RelationPath>();
            var toVisit = new Dictionary<Node, RelationPath>();
            foreach (var r in initVertex.Relations.Where(x => !x.Node.NamespaceAndClass.Contains(userNamespace)))
                toVisit.Add(r.Node, new RelationPath() { Path = new List<RelationNode>() { r } });

            var result = new Dictionary<Node, RelationPath>();
            foreach (var r in initVertex.Relations.Where(x => x.Node.NamespaceAndClass.Contains(userNamespace)))
                result.Add(r.Node, new RelationPath() { Path = new List<RelationNode>() { r } });

            while (toVisit.Count > 0)
            {
                var t = toVisit.First();
                toVisit.Remove(t.Key);
                if (visited.ContainsKey(t.Key))
                    continue;

                visited.Add(t.Key, t.Value);

                foreach (var r in t.Key.Relations.Where(x => !x.Node.NamespaceAndClass.Contains(userNamespace)))
                {
                    if (!toVisit.ContainsKey(r.Node))
                    { 
                        t.Value.Path.Add(r);
                        toVisit.Add(r.Node, t.Value);
                    }
                }

                foreach (var r in t.Key.Relations.Where(x => x.Node.NamespaceAndClass.Contains(userNamespace)))
                {
                    if (!result.ContainsKey(r.Node))
                    {
                        t.Value.Path.Add(r);
                        result.Add(r.Node, t.Value);
                    }
                }
            }

            return result;
        }

        static void PrintNode(int a, int b)
        {
            PrintPath(Nodes.ToList()[a].Value.OwnRelations.ToList()[b].Value);
        }

        static void PrintPath(RelationPath rp)
        {
            var sb = new StringBuilder();
            foreach (var r in rp.Path)
            {
                sb.AppendLine(r.Node.NamespaceAndClass + ":" + r.Node.Method + ":" + r.Node.Line + " - " + (r.Relation.Kind == RelationKind.Control ? "Control" : "Data") + " " + r.Relation.Info);
            }
            System.IO.File.WriteAllText(@"C:\Users\Camilo\Desktop\temp.txt", sb.ToString());
        }
    }
}
