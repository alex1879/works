﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Utils
{
    class CheckFileLarge
    {
        static void Main()
        {
            //ReplaceRealFiles();
            ReplaceWrong();
            //CheckWrong();
            //UpdateConfigFiles();
        }

        static void ReplaceRealFiles()
        {
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\StatementParsingTests";
            var files = Directory.GetFiles(ConfigMainFolder, "*.slc", SearchOption.AllDirectories);

            var toChange = new string[] 
            {
                "AnonymousType_Empty",
                "TestDeconstruction",
                "Patterns_SwitchStatement_Constant",
                "ExceptionHandling_Filter_Debug3",
                "ConditionalInAsyncMethodWithExplicitReturn",
                "BranchToStartOfTry",
                "DisabledLineDirective",
                "DoStatement",
                "ExpressionBodiedConstructor",
                "_DLL",
                "ExpressionBodiedMethod",
                "FixedStatementMultipleArrays"
            };

            var desiredStartWith = new int[] { 25014, 25015 };
            var changed = new List<string>();
            var notChanged = new List<string>();

            foreach (var file in files)
            {
                //if (toChange.Any(x => file.Contains(x)))
                //    continue;

                var stream = File.OpenRead(file);
                var serializer = new XmlSerializer(typeof(UserConfiguration));
                var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                stream.Close();

                var currentFile = userConfiguration.criteria.fileTraceInputPath;
                var currentCode = Path.GetFileNameWithoutExtension(currentFile).Split("_")[1];

                #region Check between...
                //var currentStarthWith = StartWith(currentFile);
                //if (currentStarthWith >= notWithFrom && currentStarthWith < notWithTo)
                //{ 
                //    wrong.Add(Path.GetFileName(Path.GetDirectoryName(currentFile)));

                //    // TODO: From here...

                //}
                //continue;
                #endregion

                var currentFolder = Path.GetDirectoryName(currentFile);
                var candidates = new List<string>();
                foreach (var traceFile in Directory.GetFiles(currentFolder).Where(x => x.Contains(currentCode)))
                {
                    if (desiredStartWith.Any(x => x == StartWith(traceFile)))
                        candidates.Add(traceFile);
                }

                if (candidates.Count < 2)
                {
                    notChanged.Add(file);
                    continue;
                }

                // Just change:
                var newSubject = candidates.Except(new string[] { currentFile }).Single();
                File.WriteAllText(newSubject, File.ReadAllText(newSubject).Replace("#", ""));

                userConfiguration.criteria.fileTraceInputPath = newSubject;

                //var chosenCandidate =
                //    candidates.Select(x => new { file = x, length = new FileInfo(x).Length })
                //    .OrderByDescending(y => y.length).First().file;

                var error = false;
                Stream newStream = null;
                //do
                //{
                    try
                    {
                        var streamToSave = System.IO.File.OpenWrite(file);
                        serializer.Serialize(streamToSave, userConfiguration);
                        streamToSave.Flush();
                        streamToSave.Close();

                        newStream = File.OpenRead(file);
                        var temp = (UserConfiguration)serializer.Deserialize(newStream);
                        error = false;
                    }
                    catch (Exception ex)
                    {
                        newStream.Flush();
                        newStream.Close();

                        error = true;
                    }

                if (error)
                {
                    var lines = File.ReadAllLines(file);
                    var lastIndex = lines.Length - 1;
                    if (string.IsNullOrWhiteSpace(lines[lastIndex]))
                        lastIndex--;

                    lines[lastIndex] = "</UserConfiguration>";
                    File.WriteAllLines(file, lines);
                    error = false;
                }

                try
                {
                    newStream = File.OpenRead(file);
                    var temp = (UserConfiguration)serializer.Deserialize(newStream);
                    error = false;
                }
                catch (Exception ex)
                {
                    newStream.Flush();
                    newStream.Close();

                    error = true;
                }

                if (error)
                    ;

                //} while (error);

                changed.Add(file);
            }

            Console.ReadKey();
        }

        static void ReplaceWrong()
        {
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\";
            var files = Directory.GetFiles(ConfigMainFolder, "*.slc", SearchOption.AllDirectories);

            var changed = new List<string>();
            var notChanged = new List<string>();

            foreach (var file in files)
            {
                Stream stream = null;
                bool error = false;
                var serializer = new XmlSerializer(typeof(UserConfiguration));
                try
                {
                    stream = File.OpenRead(file);
                    var temp = (UserConfiguration)serializer.Deserialize(stream);
                    error = false;
                }
                catch (Exception ex)
                {
                    stream.Flush();
                    stream.Close();

                    error = true;
                }

                if (error)
                {
                    var lines = File.ReadAllLines(file);
                    var lastIndex = lines.Length - 1;
                    if (string.IsNullOrWhiteSpace(lines[lastIndex]))
                        lastIndex--;

                    lines[lastIndex] = "</UserConfiguration>";
                    File.WriteAllLines(file, lines);
                    error = false;
                }

                try
                {
                    stream = File.OpenRead(file);
                    var temp = (UserConfiguration)serializer.Deserialize(stream);
                    error = false;
                }
                catch (Exception ex)
                {
                    stream.Flush();
                    stream.Close();

                    error = true;
                }

                if (error)
                    ;

                //} while (error);

                changed.Add(file);
            }

            Console.ReadKey();
        }

        static int StartWith(string path)
        {
            var valToRet = 0;

            var lines = File.ReadAllLines(path);
            if (lines.Length > 0)
            {
                var firstLine = lines.First().Replace("#", "");
                var firstVal = firstLine.Split(',');
                var firstInt = 0;
                if (int.TryParse(firstVal[0], out firstInt))
                {
                    return firstInt;
                }
            }

            return valToRet;
        }

        static void CheckWrong()
        {
            //var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\tests";
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var files = Directory.GetFiles(ConfigMainFolder, "*.slc", SearchOption.AllDirectories);

            var wrong = new List<string>();
            foreach (var file in files)
            {
                var stream = File.OpenRead(file);
                var serializer = new XmlSerializer(typeof(UserConfiguration));

                try
                {
                    var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    wrong.Add(file);
                }
            }

            Console.ReadKey();
        }

        static void CheckFiles()
        {
            var Tests = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";
            //var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\tests";
            var classesToAnalyze = new string[] { "StatementParsingTests", /*"BindingTests", "TypeTests",*/ /*"FlowTests", "CompilationEmitTests", "PDBTests"*/ };

            var res = new List<FileMethodInfo>();

            //var lines = File.ReadAllLines(Tests).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            //foreach (var line in lines)
            //{
            //    var className = line.Split(';')[0];
            //    if (!classesToAnalyze.Contains(className))
            //        continue;

            //    var methods = line.Split(';')[1].Split(',').ToList();
            //    foreach (var method in methods)
            //    {
                  foreach (var file in System.IO.Directory.GetFiles(ConfigMainFolder, "*.slc", SearchOption.AllDirectories))
                  {

                #region Filtering
                //var ok = new string[] {
                //    //"IsPatternBadValueConversion",
                //    //"IsPatternConversion",
                //    //"ThrowStatement",
                //    //"UseDefViolationInDelegateInSwitchWithGoto",
                //    //"UseDef_CondAccess",
                //    //"UseDef_ExceptionFilters1",
                //    //"UseDef_ExceptionFilters3",
                //    //"UseDef_ExceptionFilters6",

                //    // Desde acá hay que terminar
                //    //"BranchToStartOfTry",
                //    //"FixedStatementMultipleArrays",
                //    //"FixedStatementSingleString",
                //    //"FixedStatementSingleArray",
                //    //"ForStatement3",
                //    //"ForEachStatement_Deconstruction",
                //    //"Patterns_SwitchStatement_Constant",
                //    //"SequencePointsForConstructorWithHiddenInitializer",
                //    //"SwitchWithConstantGenericPattern_02",
                //    //"SyntaxOffset_IsPattern",
                //    //"TestDeconstruction",
                //};
                //if (!ok.Any(x => method.Contains(x)))
                //    continue;
                #endregion

                    //var currentConfigFolder = ConfigMainFolder + string.Format(@"\{0}", className, method);
                    //var slcFile = Path.Combine(currentConfigFolder, method + ".slc");
                    var slcFile = file;
                    var stream = File.OpenRead(slcFile);
                    var serializer = new XmlSerializer(typeof(UserConfiguration));

                    //try
                    //{
                    //    var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                    //    res.Add(new FileMethodInfo(className, method, 1));
                    //}
                    //catch (Exception ex)
                    //{
                    //    res.Add(new FileMethodInfo(className, method, 0));
                    //}

                        var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                        var txtLines = File.ReadAllLines(userConfiguration.criteria.fileTraceInputPath);
                        //res.Add(new FileMethodInfo(className, method, txtLines.Length));

                        var splitted = file.Split('\\');

                        res.Add(new FileMethodInfo(splitted[splitted.Length - 2], splitted[splitted.Length-1].Replace(".slc", ""), txtLines.Length));
                    }
            //    }
            //}

            foreach (var x in res.OrderBy(x => x.TotalLines))
                Console.WriteLine($"{x.Group} - {x.Method} - {x.TotalLines}");
            Console.ReadKey();
        }

        static void UpdateConfigFiles()
        {
            var originalSlc = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn.slc";
            var configMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var projectName = "Microsoft.CodeAnalysis.Test.Utilities(netcoreapp3.1)";

            // Get original config file
            var original_stream = File.OpenRead(originalSlc);
            var original_serializer = new XmlSerializer(typeof(UserConfiguration));
            var originalConfig = (UserConfiguration)original_serializer.Deserialize(original_stream);
            // What I want to copy
            var codeAnalysis = originalConfig.targetProjects.excluded.Where(x => x.name.Equals(projectName)).Single();

            foreach (var file in System.IO.Directory.GetFiles(configMainFolder, "*.slc", SearchOption.AllDirectories))
            {
                var stream = File.OpenRead(file);
                var serializer = new XmlSerializer(typeof(UserConfiguration));
                var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                stream.Close();

                var projectList = userConfiguration.targetProjects.excluded.ToList();
                var removed = projectList.Remove(projectList.Single(x => x.name == projectName));
                if (!removed)
                    ;

                var newCodeAnalysis = new UserConfiguration.ExcludedProject()
                {
                    mode = UserConfiguration.ExcludedMode.None,
                    name = codeAnalysis.name,
                    initialFileID = codeAnalysis.initialFileID,
                    skipDefault = true,
                    files = codeAnalysis.files.Select(x => new UserConfiguration.ExcludedFile()
                    {
                        id = x.id,
                        name = x.name,
                        skip = true
                    }).ToArray()
                };

                // Where to add
                var currElem = projectList.Where(x => x.name == "Microsoft.CodeAnalysis.CSharp.Syntax.UnitTests(netcoreapp3.1)").Single();
                var index = projectList.IndexOf(currElem);

                projectList.Insert(index, newCodeAnalysis);
                userConfiguration.targetProjects.excluded = projectList.ToArray();

                var streamToSave = System.IO.File.OpenWrite(file);
                serializer.Serialize(streamToSave, userConfiguration);
                streamToSave.Close();
            }
            
            Console.ReadKey();
        }

        class FileMethodInfo
        {
            public string Group { get; set; }
            public string Method { get; set; }
            public int TotalLines { get; set; }
            public FileMethodInfo(string group, string method, int totalLines)
            {
                Group = group;
                Method = method;
                TotalLines = totalLines;
            }
        }
    }
}
