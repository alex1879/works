﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils
{
    public class RandomInputGenerator
    {
        public static void Main()
        {
            var electionResultPath = @"C:\Users\alexd\Desktop\Slicer\Olden\oldenExecutions.csv";
            var targetOldenExecutions = GetOldenExecutions();
            var elections = GetOldenElections(targetOldenExecutions);
            PrintResult(electionResultPath, elections);
        }

        #region Classes
        public class OldenExecution
        {
            public string Name { get; set; }
            public List<OldenInput> Inputs { get; set; } = new List<OldenInput>();

            public override bool Equals(object x) =>
                ((OldenExecution)x).Name == this.Name && 
                ((OldenExecution)x).Inputs.TrueForAll(y => this.Inputs.Any(z => y.Equals(z)));

            public override int GetHashCode() => this.Inputs.Sum(x => x.GetHashCode()/1000);
        }
        public class OldenInput
        {
            public string Name { get; set; }
            public int Value { get; set; }
            public int MinValue { get; set; }
            public int MaxValue { get; set; }

            public override bool Equals(object x) => ((OldenInput)x).Name == this.Name && ((OldenInput)x).Value == this.Value;

            public override int GetHashCode() => this.Name.GetHashCode() + this.Value.GetHashCode();
        }
        public class OldenElection
        {
            public HashSet<OldenExecution> Executions { get; set; } = new HashSet<OldenExecution>();
        }
        #endregion

        static List<OldenExecution> GetOldenExecutions()
        {
            List<OldenExecution> TargetOldenExecutions = new List<OldenExecution>();
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "BH",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "b", MinValue = 2, MaxValue = 26 },
                    new OldenInput(){ Name = "s", MinValue = 2, MaxValue = 15 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "BiSort",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "s", MinValue = 4, MaxValue = 1024 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Em3d",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "n", MinValue = 2, MaxValue = 50 },
                    new OldenInput(){ Name = "d", MinValue = 2, MaxValue = 25 },
                    new OldenInput(){ Name = "i", MinValue = 1, MaxValue = 10 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Health",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 6 },
                    new OldenInput(){ Name = "t", MinValue = 2, MaxValue = 10 },
                    new OldenInput(){ Name = "s", MinValue = 1, MaxValue = 1 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "MST",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "v", MinValue = 4, MaxValue = 144 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Perimeter",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 11 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Power",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "f", MinValue = 1, MaxValue = 4 },
                    new OldenInput(){ Name = "l", MinValue = 1, MaxValue = 4 },
                    new OldenInput(){ Name = "b", MinValue = 1, MaxValue = 2 },
                    new OldenInput(){ Name = "e", MinValue = 1, MaxValue = 2 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "TreeAdd",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 16 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "TSP",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "c", MinValue = 4, MaxValue = 512 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Voronoi",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "n", MinValue = 4, MaxValue = 256 }
                }
            });
            return TargetOldenExecutions;
        }

        static List<OldenElection> GetOldenElections(List<OldenExecution> oldenExecutions)
        {
            List<OldenElection> results = new List<OldenElection>();
            foreach (var oldenExecution in oldenExecutions)
            {
                var oldenElection = new OldenElection();
                for (var i = 0; i < 10; i++)
                {
                    var exec = MakeChoise(oldenExecution);
                    var added = oldenElection.Executions.Add(exec);
                    if (!added)
                        i--;
                }
                results.Add(oldenElection);
            }
            return results;
        }

        static void PrintResult(string path, List<OldenElection> results)
        {
            var sb = new StringBuilder();
            foreach (var result in results)
            {
                foreach (var x in result.Executions)
                {
                    Append(sb, x.Name);
                    foreach (var i in x.Inputs)
                        Append(sb, "-" + i.Name + " " + i.Value);
                    sb.AppendLine();
                }
            }
            System.IO.File.WriteAllText(path, sb.ToString());
        }

        static void Append(StringBuilder sb, string val)
        {
            sb.Append(val);
            sb.Append(";");
        }

        static OldenExecution MakeChoise(OldenExecution execution)
        {
            var r = new OldenExecution();
            r.Name = execution.Name;
            foreach (var i in execution.Inputs)
            {
                var newInput = new OldenInput();
                newInput.Name = i.Name;
                newInput.Value = new Random().Next(i.MinValue, i.MaxValue+1);
                r.Inputs.Add(newInput);
            }
            return r;
        }
    }
}
