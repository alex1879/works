﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    class ManualResultsCollector
    {
        public static void Main()
        {
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output_large\ManualResults";
            var sb = new StringBuilder();

            var controlDep = 0;
            var thisDep = 0;
            var otherDep = 0;

            var folders = Directory.GetDirectories(baseFolder);
            foreach (var folder in folders)
            {
                var files = Directory.GetFiles(folder);
                foreach (var file in files)
                {
                    var counter = 0;
                    var lines = File.ReadAllLines(file);
                    foreach (var line in lines)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            if (line.Contains("//"))
                            {
                                var comment = line.Split("//")[1].Trim();
                                switch (comment)
                                {
                                    case "Control":
                                        controlDep++;
                                        break;
                                    case "This":
                                        thisDep++;
                                        break;
                                    default:
                                        otherDep++;
                                        break;
                                }
                            }
                            else
                                counter++;
                        }
                    }

                    var programName = Path.GetFileName(folder);
                    var fileName = Path.GetFileNameWithoutExtension(file);
                    //sb.AppendLine($"{programName};{fileName};{counter}");
                    sb.AppendLine($"{counter}");
                }
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
