﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Utils
{
    class MoveMethodFiles
    {
        static void Main()
        {
            var folderFrom = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\aut";
            var folderTo = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\lastest\half";

            foreach (var file in Directory.GetFiles(folderFrom, @"*ExecutedMethods.txt", SearchOption.AllDirectories))
            {
                var newpath = file.Replace(folderFrom, folderTo);
                File.Move(file, newpath, true);
            }

            foreach (var file in Directory.GetFiles(folderFrom, @"*ExecutedCallbacks.txt", SearchOption.AllDirectories))
            {
                var newpath = file.Replace(folderFrom, folderTo);
                File.Move(file, newpath, true);
            }
        }
    }
}
