﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Utils
{
    public class SkipUpdateConfig
    {
        public static void Main()
        {
            var outputPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\CompilationEmitTests\EmitInvocationExprInIfStatementNestedInsideCatch_OUTPUT.slc";
            var slcFile = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\CompilationEmitTests\EmitInvocationExprInIfStatementNestedInsideCatch.slc";
            var stream = File.OpenRead(slcFile);
            var serializer = new XmlSerializer(typeof(UserConfiguration));
            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);

            var pathToExclude = @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Symbols\Metadata\PE\";
            foreach (var targetProject in userConfiguration.targetProjects.excluded)
                for (var i = 0; i < targetProject.files.Length; i++)
                    if (targetProject.files[i].name.Contains(pathToExclude))
                        targetProject.files[i].skip = true;

            var streamToSave = System.IO.File.OpenWrite(outputPath);
            serializer.Serialize(streamToSave, userConfiguration);
        }
    }
}
