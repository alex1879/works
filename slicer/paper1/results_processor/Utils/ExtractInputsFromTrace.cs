﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    class ExtractInputsFromTrace
    {
        static void Main()
        {
            var folder = @"C:\Users\alexd\Desktop\Slicer\Powershell\traces\host_d";
            var outputs = new List<string>();

            foreach (var file in System.IO.Directory.GetFiles(folder))
            {
                var lines = System.IO.File.ReadAllLines(file);
                foreach (var line in lines)
                    if (line.Contains("####"))
                    {
                        outputs.Add(line);
                        break;
                    }
            }

            foreach (var output in outputs)
                Console.WriteLine(output.Replace("####:", ""));
        }
    }
}
