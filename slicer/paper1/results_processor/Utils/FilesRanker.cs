﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Utils
{
    class FilesRanker
    {
        static void Main()
        {
            //SkipUpdateConfig.Main();
            //return;

            //var slcFile = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\tests\FileSystemProvider\TestClearContent.slc";
            //var slcFile = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\tests\PowerShellAPI\TestStartJobThrowTerminatingException.slc";
            var slcFile = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut\TypeTests\ArrayTypeInterfaces.slc";

            Dictionary<int, FileInfo> fileInfoDict = null;
            var counterDictionary = new Dictionary<int, int>();
            
            var stream = File.OpenRead(slcFile);
            var serializer = new XmlSerializer(typeof(UserConfiguration));
            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
            if (fileInfoDict == null)
                fileInfoDict = GetFilesInfo(userConfiguration);
            var txtLines = File.ReadAllLines(userConfiguration.criteria.fileTraceInputPath);
            var newLines = new List<string>();
            foreach (var oldLine in txtLines)
            {
                var trimmed = oldLine.Split(',');
                if (trimmed.Length > 1)
                {
                    var fileId = int.Parse(trimmed[0]);
                    if (counterDictionary.ContainsKey(fileId))
                        counterDictionary[fileId]++;
                    else
                        counterDictionary[fileId] = 1;
                }
            }

            var total = counterDictionary.Sum(x => x.Value);
            Console.WriteLine("Total: " + total);
            foreach (var kv in counterDictionary.Select(x => new { FileId = x.Key, Count = x.Value }).OrderByDescending(x => x.Count))
                Console.WriteLine(kv.FileId.ToString() + " - " + kv.Count.ToString() + " - " +
                    Math.Round((double)kv.Count * (double)100 / (double)total) + "% - " +
                    (fileInfoDict.ContainsKey(kv.FileId) ? 
                        fileInfoDict[kv.FileId].Path
                            .Replace(@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\", "") 
                            : kv.FileId.ToString()));

            Console.Write("==================");

            // Ahora puedo agrupar por todas las carpetas distintas y ver que onda.
            // Por ejemplo, tengamos todas las carpetas:
            var allFolders = fileInfoDict.Select(x => Path.GetDirectoryName(x.Value.Path)).Distinct();
            var dictFolder = new Dictionary<string, int>();
            foreach (var folder in allFolders)
            {
                var count = 0;
                foreach (var idCount in counterDictionary)
                {
                    if (fileInfoDict.ContainsKey(idCount.Key) &&
                        fileInfoDict[idCount.Key].Path.Contains(folder))
                    {
                        count += idCount.Value;
                    }
                }
                dictFolder[folder.Replace(@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\", "")] = count;
            }

            foreach (var kv in dictFolder.OrderByDescending(x => x.Value))
                Console.WriteLine(kv.Key + " - " + kv.Value + " - " + Math.Round((double)kv.Value * (double)100 / (double)total) + "%");

            Console.ReadKey();
        }

        static void Main_ROSLYN()
        {
            //SkipUpdateConfig.Main();
            //return;

            var Tests = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var classesToAnalyze = new string[] { "StatementParsingTests", "BindingTests", "TypeTests",
                "FlowTests", "CompilationEmitTests", "PDBTests" };

            Dictionary<int, FileInfo> fileInfoDict = null;
            var counterDictionary = new Dictionary<int, int>();
            var lines = File.ReadAllLines(Tests).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach (var line in lines)
            {
                var className = line.Split(';')[0];
                if (!classesToAnalyze.Contains(className))
                    continue;

                var methods = line.Split(';')[1].Split(',').ToList();
                foreach (var method in methods)
                {
                    #region Filtering
                    var ok = new string[] { 
                        // PDB
                            "ForEachStatement"
                    };
                    if (!ok.Any(x => method == x))
                        continue;
                    #endregion

                    // CARPETA: C:\Users\alexd\Desktop..
                    var currentConfigFolder = ConfigMainFolder + string.Format(@"\{0}", className, method);
                    // ARCHIVO: C:\Users\alexd\Desktop..\TestSwitchWithMultipleStatementsOnOneCase.slc
                    var slcFile = Path.Combine(currentConfigFolder, method + ".slc");
                    var stream = File.OpenRead(slcFile);
                    var serializer = new XmlSerializer(typeof(UserConfiguration));
                    var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                    if (fileInfoDict == null)
                        fileInfoDict = GetFilesInfo(userConfiguration);
                    var txtLines = File.ReadAllLines(userConfiguration.criteria.fileTraceInputPath);
                    var newLines = new List<string>();
                    foreach (var oldLine in txtLines)
                    {
                        var trimmed = oldLine.Split(',');
                        if (trimmed.Length > 1)
                        {
                            var fileId = int.Parse(trimmed[0]);
                            if (counterDictionary.ContainsKey(fileId))
                                counterDictionary[fileId]++;
                            else
                                counterDictionary[fileId] = 1;
                        }
                    }
                }
            }



            var total = counterDictionary.Sum(x => x.Value);
            Console.WriteLine("Total: " + total);
            foreach (var kv in counterDictionary.Select(x => new { FileId = x.Key, Count = x.Value }).OrderByDescending(x => x.Count))
                Console.WriteLine(kv.FileId.ToString() + " - " + kv.Count.ToString() + " - " +
                    Math.Round((double)kv.Count * (double)100 / (double)total) + "% - " +
                    fileInfoDict[kv.FileId].Path.Replace(@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\", ""));

            Console.Write("==================");

            // Ahora puedo agrupar por todas las carpetas distintas y ver que onda.
            // Por ejemplo, tengamos todas las carpetas:
            var allFolders = fileInfoDict.Select(x => Path.GetDirectoryName(x.Value.Path)).Distinct();
            var dictFolder = new Dictionary<string, int>();
            foreach (var folder in allFolders)
            {
                var count = 0;
                foreach (var idCount in counterDictionary)
                {
                    if (fileInfoDict[idCount.Key].Path.Contains(folder))
                    {
                        count += idCount.Value;
                    }
                }
                dictFolder[folder.Replace(@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\", "")] = count;
            }

            foreach (var kv in dictFolder.OrderByDescending(x => x.Value))
                Console.WriteLine(kv.Key + " - " + kv.Value + " - " + Math.Round((double)kv.Value * (double)100 / (double)total) + "%");

            Console.ReadKey();
        }

        static Dictionary<int, FileInfo> GetFilesInfo(UserConfiguration userConfiguration)
        {
            var retDict = new Dictionary<int, FileInfo>();

            foreach (var proj in userConfiguration.targetProjects.excluded)
            {
                var currentProjectName = proj.name;
                if (proj.files != null)
                    foreach (var file in proj.files)
                    {
                        var fileInfo = new FileInfo();
                        fileInfo.FileId = file.id;
                        fileInfo.ProjectName = currentProjectName;
                        fileInfo.Path = file.name;
                        if (fileInfo.FileId > 0)
                            retDict.Add(fileInfo.FileId, fileInfo);
                    }
            }

            return retDict;
        }

        class FileInfo
        {
            public int FileId { get; set; }
            public string ProjectName { get; set; }
            public string Path { get; set; }
        }
    }
}
