﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    class Program
    {
        static void Main(string[] args)
        {
            var clasified = new Dictionary<string, ISet<string>>();

            var mainFolder = @"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\TestHost\";
            var allFiles = System.IO.Directory.GetFiles(mainFolder);
            foreach (var file in allFiles)
            {
                //trace_5d4d68ba-6527-493c-914d-aa9e0d5bc12c_ORIG.txt
                var fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                var key = fileName.Substring(12, fileName.LastIndexOf('_')-12);
                if (!clasified.ContainsKey(key))
                    clasified[key] = new HashSet<string>();
                var set = clasified[key];
                set.Add(file);
            }

            var toProcess = new HashSet<string>() {
                        "7010a393-0fe8-41d2-ad30-878a69e54af8"
                        //"8588c91b-9827-4141-93c7-eff1f91a23d6",
                        //"fb5ca824-3b6e-42dd-918a-c77b92a9e1d9"

                        //"a62c06d0-e5c9-414e-8ef8-9926ceeb8a22", //--> MUY GRANDE
                        //"06bb29b8-89de-4295-a771-087c7cd07c08",
                        //"09d93cfa-71d4-4c9f-afea-8687c2266a40",
                        //"13d67c1a-5a22-40fd-b791-562885677c39",
                        //"1548fb4e-64d0-4c0d-9e2d-7f5bcbf35ae2",
                        //"211febf2-d7e2-4133-9089-41b92417404d",
                        //"323070b8-3b2c-4f08-90bc-b6d5ca75735d",
                        //"3f841995-981c-4451-ba66-7605b72cb783",
                        //"48e4b4d2-580e-4181-8ed5-c7199586ce4d",
                        //"4b849faf-115f-46fa-abeb-946176c3cdc2", --> Corregido a mano por 1 thread llama a otro
                        //"6af3cefb-7fc5-48b0-a816-d4522965d6a7",
                        //"6ff5914f-bf66-486c-8faa-045402ef4d9a",
                        //"739531e7-3c4e-4522-8618-12164899b9ff",
                        //"75a3868f-f197-41b0-920c-25330a03ce37",
                        //"96f836c1-9c2f-4a67-8e70-8d5cf28663f3",
                        //"9da4c4a3-c333-45c1-b18c-f431abbcf36e",
                        //"dbf70692-cf72-4e62-8fcf-46468f847a7d",
                        //"f460c27e-a218-4997-b12a-e85a651b2f96",
                        //"fc3cda9f-7775-442c-807f-5cc9ee7f0902",
                        //"fed1e2e3-033e-4681-ab8d-f3cb294c6ecc",
                    };

            foreach (var kv in clasified)
            {
                if (!toProcess.Contains(kv.Key))
                    continue;

                var outputPath = @"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\TestHost\Unified\" + kv.Key + ".txt";
                DeltaMerge(kv.Value, outputPath);
            }

            //var files = new List<string>();
            //files.Add(@"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\HotsM_LS\trace_5d4d68ba-6527-493c-914d-aa9e0d5bc12c_ORIG.txt");
            //files.Add(@"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\HotsM_LS\trace_5d4d68ba-6527-493c-914d-aa9e0d5bc12c_11.txt");
            //files.Add(@"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\HotsM_LS\trace_5d4d68ba-6527-493c-914d-aa9e0d5bc12c_15.txt");

            //var output = @"C:\Users\lafhis\Desktop\Slicer\Powershell\TodoPowershell\Trazas2020\HotsM_LS\trace.txt";
            //Merge(files, output);
        }

        static void Merge(IEnumerable<string> files, string outputPath)
        {
            var allLines = new List<Line>();
            foreach(var file in files)
            {
                var allFileLines = System.IO.File.ReadAllLines(file);
                foreach(var line in allFileLines.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    var data = (line[0] == '#' ? line.Substring(1) : line).Split(',');
                    var fileId = Int32.Parse(data[0]);
                    var traceType = Int32.Parse(data[1]);
                    var spanStart = Int32.Parse(data[2]);
                    var spanEnd = Int32.Parse(data[3]);
                    var order = Int32.Parse(data[4]);
                    allLines.Add(new Line(fileId, traceType, spanStart, spanEnd, order));
                }
            }
            allLines.Sort((x, y) => x.order.CompareTo(y.order));

            var sb = new StringBuilder();
            foreach (var line in allLines)
                sb.AppendLine(LineToString(line));
            System.IO.File.WriteAllText(outputPath, sb.ToString());
        }

        static void DeltaMerge(IEnumerable<string> files, string outputPath)
        {
            // 1) Leemos todas las líneas en listas de lines por separados
            var diccLines = new Dictionary<string, List<Line>>();
            foreach (var file in files)
            {
                var allFileLines = System.IO.File.ReadAllLines(file);
                var parsedLines = new List<Line>();
                foreach (var line in allFileLines.Where(x => !string.IsNullOrWhiteSpace(x)))
                {
                    var data = (line[0] == '#' ? line.Substring(1) : line).Split(',');
                    var fileId = Int32.Parse(data[0]);
                    var traceType = Int32.Parse(data[1]);
                    var spanStart = Int32.Parse(data[2]);
                    var spanEnd = Int32.Parse(data[3]);
                    var order = Int32.Parse(data[4]);
                    parsedLines.Add(new Line(fileId, traceType, spanStart, spanEnd, order));
                }
                diccLines[file] = parsedLines;
            }

            // **: Para un Guid en específico limpiamos limpiamos las entradas que empiecen con 4198,4368
            var tempDict = new Dictionary<string, List<Line>>();
            foreach (var kv in diccLines)
                if (!(kv.Value.First().fileId == 115 && kv.Value.First().spanStart == 4198))
                    tempDict[kv.Key] = kv.Value;
            diccLines = tempDict;

            // 2) Empezamos a leer y sacar líneas desde el ORIG (vamos a suponer que los threads no se llaman entre si)
            var allLines = new List<Line>();
            var internalDelta = 15;
            var originalDelta = 1;
            var startFile = files.First(x => x.Contains("ORIG"));
            var listStartFile = diccLines[startFile];
            var lastLine = listStartFile.First();
            var nextLineToDebug = 3455;
            while (listStartFile.Count > 0)
            {
                var nextLine = listStartFile.First();
                // Si la siguiente supera por el delta a la actual consideramos salto.
                var currentDelta = nextLine.order - lastLine.order;

                if (nextLine.order == nextLineToDebug)
                    ;

                if (originalDelta < currentDelta /*|| diccLines.Any(x => x.Value.Count == currentDelta-1 && x.Value.First().order == lastLine.order + 1)*/)
                {
                    // Buscamos el thread:
                    var nextFileAndLines = diccLines.FirstOrDefault(x => x.Value.Count > 0 && x.Value.First().order == lastLine.order + 1);
                    if (!string.IsNullOrWhiteSpace(nextFileAndLines.Key))
                    {
                        var internalLastLine = nextFileAndLines.Value.First();
                        // Consumimos este archivo mientras el delta lo amerite
                        while (nextFileAndLines.Value.Count > 0)
                        {
                            var internalNextLine = nextFileAndLines.Value.First();
                            // Si la siguiente supera por el delta a la actual consideramos salto.
                            if (internalLastLine.order + internalDelta < internalNextLine.order)
                            {
                                break;
                            }
                            else
                            {
                                allLines.Add(internalNextLine);
                                internalLastLine = internalNextLine;
                                nextFileAndLines.Value.RemoveAt(0);
                            }
                        }
                    }
                    else
                        ;
                }
                allLines.Add(nextLine);
                lastLine = nextLine;
                listStartFile.RemoveAt(0);
            }

            if (diccLines.Any(x => x.Value.Count > 0))
                Console.WriteLine(startFile);

            var sb = new StringBuilder();
            foreach (var line in allLines)
                sb.AppendLine(LineToString(line));
            System.IO.File.WriteAllText(outputPath, sb.ToString());
        }

        static string LineToString(Line line)
        {
            return line.fileId + "," +
                line.traceType + "," +
                line.spanStart + "," +
                line.spanEnd + "," +
                line.order;
        }

        class Line
        {
            public int fileId;
            public int traceType;
            public long spanStart;
            public long spanEnd;
            public long order;

            public Line(int fileId, int traceType, long spanStart, long spanEnd, long order)
            {
                this.fileId = fileId;
                this.traceType = traceType;
                this.spanStart = spanStart;
                this.spanEnd = spanEnd;
                this.order = order;
            }
        }
    }
}
