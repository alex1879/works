﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    class BuildFileLineFromFiles
    {
        public static void Main()
        {
            var outputPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\generalLineNumbers_2.txt";
            var files = new string[] {
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Diagnostics\DiagnosticExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\CommonTestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Test\PdbUtilities\Reader\PdbValidation.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\CompilationExtensions.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\CSharpTestBase.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Compilation\ControlFlowGraphVerifier.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\AssertEx.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core\Assert\AssertXml.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Parsing\ParsingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\CompilationTestUtils.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Utilities\CSharp\Extensions.cs",
                // OUR SUBJECTS
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Syntax\Parsing\StatementParsingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Semantic\Semantics\BindingTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Symbol\Symbols\TypeTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\Emit\CompilationEmitTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Emit\PDB\PDBTests.cs",
                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Test\Semantic\FlowAnalysis\FlowTests.cs",
            };

            var resDict = new Dictionary<string, ISet<int>>();
            foreach(var file in files)
            {
                var newHashSet = new HashSet<int>();

                var allLines = new List<string>(System.IO.File.ReadAllLines(file));
                for (var i = 0; i < allLines.Count; i++)
                {
                    if (allLines[i].Contains("CustomAssert"))
                    {
                        newHashSet.Add(i + 1);
                    }
                }
                resDict[file] = newHashSet;
            }

            var sb = new StringBuilder();
            foreach (var kv in resDict)
                foreach (var l in kv.Value)
                {
                    sb.AppendLine("<FileLine>");
                    sb.AppendLine(string.Format(" <file>{0}</file>", kv.Key));
                    sb.AppendLine(string.Format(" <line>{0}</line>", l));
                    sb.AppendLine("</FileLine>");
                }

            System.IO.File.WriteAllText(outputPath, sb.ToString());
        }
    }
}
