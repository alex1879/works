﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Utils
{
    public class ConfigEditor
    {
        static void Main()
        {
            var path = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn.slc";
            var outputPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn_2.slc";
            var projectNameToAdd = "Microsoft.CodeAnalysis.Test.Utilities(netcoreapp3.1)";

            //var path = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\Powershell.slc";
            //var outputPath = @"C:\Users\alexd\Desktop\Slicer\Powershell\config\Powershell_2.slc";
            //var projectNameToAdd = "System.Management.Automation";

            var foldersToAdd = new string[] {
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Compilation",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\DocumentationComments",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Emitter",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\Operations",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\SymbolDisplay"
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\CodeGen",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\CommandLine",
                //@"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\CSharp\Portable\SourceGeneration",

                @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\src\Compilers\Test\Core"

                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\utils"

                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\cimSupport",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\CoreCLR",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\DscSupport",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\FormatAndOutput",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\help",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\logging",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\namespaces",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\security",
                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\singleshell",

                //@"C:\Users\alexd\Desktop\Slicer\Powershell\src\src\System.Management.Automation\engine"
            };

            var stream = System.IO.File.OpenRead(path.Trim());
            var serializer = new XmlSerializer(typeof(UserConfiguration));
            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);

            var projectToAdd = userConfiguration.targetProjects.excluded.Single(x => x.name == projectNameToAdd);

            //var projects = userConfiguration.targetProjects.excluded.Where(x => x.name != projectNameToAdd && x.name != project2);
            //foreach (var project in projects.Where(x => x.files != null))
            //    foreach (var file in project.files)
            //        Console.WriteLine(file.name);
            //return;

            var currentFiles = projectToAdd.files.ToList();

            var nextId = 25030;
            foreach (var folderToAdd in foldersToAdd)
                foreach (var file in System.IO.Directory.GetFiles(folderToAdd, "*.cs", System.IO.SearchOption.AllDirectories))
                {
                    if (file.Contains("Release"))
                        ;

                    if (file.Contains(@"\obj\"))
                        continue;

                    if (!currentFiles.Any(x => x.name == file))
                        currentFiles.Add(new UserConfiguration.ExcludedFile() { name = file, id = nextId++, skip = false });
                }

            projectToAdd.files = currentFiles.ToArray();

            var streamToSave = System.IO.File.OpenWrite(outputPath.Trim());
            serializer.Serialize(streamToSave, userConfiguration);
        }
    }
}
