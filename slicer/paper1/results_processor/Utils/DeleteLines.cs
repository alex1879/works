﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Utils
{
    class DeleteLines
    {
        static void Main()
        {
            var Tests = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var classesToAnalyze = new string[] { /*"StatementParsingTests", "BindingTests", "TypeTests","FlowTests",*/ "CompilationEmitTests", "PDBTests" };

            var lines = File.ReadAllLines(Tests).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach (var line in lines)
            {
                var className = line.Split(';')[0];
                if (!classesToAnalyze.Contains(className))
                    continue;

                var methods = line.Split(';')[1].Split(',').ToList();
                foreach (var method in methods)
                {
                    #region Filtering
                    var ok = new string[] { 
                        // StatementParsingTests
                            "ParseCreateNullableTuple_01.slc",
                            "TestConstLocalDeclarationStatement.slc",
                            "TestEmptyStatement.slc",
                            "TestFixedVarStatement.slc",
                            "TestFor.slc",
                            "TestForEachAfterOffset.slc",
                            "TestForWithIncrementor.slc",
                            "TestForWithMultipleVariableInitializers.slc",
                            "TestForWithVarDeclaration.slc",
                            "TestForWithVariableDeclaration.slc",
                            "TestForWithVariableInitializer.slc",
                            "TestLocalDeclarationStatement.slc",
                            "TestLocalDeclarationStatementWithDynamic.slc",
                            "TestLocalDeclarationStatementWithMixedType.slc",
                            "TestLock.slc",
                            "TestRefLocalDeclarationStatement.slc",
                            "TestReturnExpression.slc",
                            "TestSwitchWithMultipleCases.slc",
                            "TestSwitchWithMultipleLabelsOnOneCase.slc",
                            "TestSwitchWithMultipleStatementsOnOneCase.slc",
                            "TestThrow.slc",
                            "TestTryCatchWithMultipleCatches.slc",
                            "TestUsingSpecialCase2.slc",
                            "TestUsingSpecialCase3.slc",
                            "TestUsingVarRefTree.slc",
                            "TestUsingVarSpecialCase2.slc",
                            "TestUsingVarWithDeclarationTree.slc",
                            "TestUsingWithVarDeclaration.slc",
                            "TestYieldBreakExpression.slc",

                        // Type
                            "ArrayTypeGetHashCode.slc",
                            "ArrayTypeInterfaces.slc",
                            "ArrayTypes.slc",
                            "BuiltInTypeNullableTest01.slc",
                            "ClassWithMultipleConstr.slc",
                            "DuplicateType.slc",
                            "EmptyNameErrorSymbolErr.slc",
                            "EnumFields.slc",
                            "ErrorTypeSymbolWithArity.slc",
                            "ExplicitlyImplementGenericInterface.slc",
                            "ImplementNestedInterface_02.slc",
                            "ImportedVersusSource.slc",
                            "InaccessibleTypesSkipped.slc",
                            "InheritedTypesCrossTrees.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_01.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_02.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_08.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_09.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_15.slc",
                            "IsExplicitDefinitionOfNoPiaLocalType_17.slc",
                            "SimpleGeneric.slc",
                            "SimpleNullable.slc",

                        // Binding
                            "AmbiguousAndBadArgument.slc",
                            "AssignStaticEventToLocalVariable.slc",
                            "DelegatesFromOverloads.slc",
                            "EnumNotMemberInConstructor.slc",
                            "GenericMethodName.slc",
                            "GenericTypeName.slc",
                            "InaccessibleAndAccessibleValid.slc",
                            "InterfaceWithPartialMethodExplicitImplementation.slc",
                            "NamespaceQualifiedGenericTypeName.slc",
                            "NamespaceQualifiedGenericTypeNameWrongArity.slc",
                            "NonMethodsWithArgs.slc",
                            "ParenthesizedDelegate.slc",
                            "RefReturningDelegateCreation.slc",
                            "RefReturningDelegateCreationBad.slc",
                            "SimpleDelegates.slc",
                            "UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario04.slc",
                            "UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario05.slc",
                            "UnimplementedInterfaceSquiggleLocation_InterfaceInheritanceScenario11.slc",
                            "UseSiteErrorViaAliasTest06.slc",

                        // Flowtest
                            "AssignedInFinallyUsedInTry.slc",
                            "IfConditionalAnd.slc",
                            "IsPatternBadValueConversion.slc",
                            "IsPatternConversion.slc",
                            "OutVarConversion.slc",
                            "UnreachableDoWhileCondition.slc",
                            "UseDefViolationInDelegateInSwitchWithGoto.slc",
                            "UseDefViolationInUnreachableDelegate.slc",
                            "UseDef_CondAccess02.slc",
                            "UseDef_CondAccess03.slc",
                            "UseDef_ExceptionFilters2.slc",
                            "UseDef_ExceptionFilters5.slc",

                        // CompilationEmit
                            "Bug769741.slc",
                            "CheckCOFFAndPEOptionalHeaders32.slc",
                            "CheckRuntimeMDVersion.slc",
                            "RefAssemblyClient_EmitAllTypes.slc",
                            "CompilationEmitWithQuotedMainType.slc",
                            "EmitMetadataOnly_DisallowPdbs.slc",
                            "EmitRefAssembly_PrivatePropertySetter.slc",
                            "IllegalNameOverride.slc",
                            "PlatformMismatch_05.slc",
                            "RefAssembly_CryptoHashFailedIsOnlyReportedOnce.slc",
                            "RefAssembly_ReferenceAssemblyAttributeAlsoInSource.slc",
                            "RefAssembly_StrongNameProvider_Arm64.slc",
                            "RefAssembly_VerifyTypesAndMembersOnStruct.slc",
                            "RefAssemblyClient_EmitVariance_InSuccess.slc",
                            "WarnAsErrorDoesNotEmit_SpecificDiagnosticOption.slc",

                        // PDB
                            "AnonymousType_Empty.slc",
                            "DisabledLineDirective.slc",
                            "DoStatement.slc",
                            "ExpressionBodiedConstructor.slc",
                            "ExpressionBodiedMethod.slc",
                            "Return_ExpressionBodied1.slc",
                            "Return_FromExceptionHandler1.slc",
                            "Return_Void1.slc",
                            "SequencePointsForConstructorWithHiddenInitializer.slc",
                            "SyntaxOffset_IsPattern.slc",
                    };
                    //if (!ok.Any(x => method + ".slc" == x))
                    //    continue;
                    #endregion

                    try
                    { 

                        // CARPETA: C:\Users\alexd\Desktop..
                        var currentConfigFolder = ConfigMainFolder + string.Format(@"\{0}", className, method);
                        // ARCHIVO: C:\Users\alexd\Desktop..\TestSwitchWithMultipleStatementsOnOneCase.slc
                        var slcFile = Path.Combine(currentConfigFolder, method + ".slc");
                        var stream = File.OpenRead(slcFile);
                        var serializer = new XmlSerializer(typeof(UserConfiguration));
                        var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
                        var txtLines = File.ReadAllLines(userConfiguration.criteria.fileTraceInputPath);

                        var newLines = new List<string>();
                        foreach (var oldLine in txtLines)
                        {
                            // 25014,13,390,448,42
                            var trimmed = oldLine.Split(',');
                            if (trimmed.Length > 1 && trimmed[0] == "10732" && trimmed[2] == "353767" && trimmed[3] == "353806")
                                continue;

                            newLines.Add(oldLine);
                        }

                        var newFileName = userConfiguration.criteria.fileTraceInputPath.Replace(".txt", "_OLD.txt");
                        if (!File.Exists(newFileName))
                        { 
                            File.Move(userConfiguration.criteria.fileTraceInputPath, newFileName);
                            File.WriteAllLines(userConfiguration.criteria.fileTraceInputPath, newLines);
                        }
                    }
                    catch(Exception ex)
                    {
                        ;
                    }
                }
            }

            Console.ReadKey();
        }
    }
}
