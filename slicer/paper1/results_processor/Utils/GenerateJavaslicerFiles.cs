﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Utils
{
    public class GenerateJavaslicerFiles
    {
        public static void Main()
        {
            var resultFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\scripts";
            var shBasePath = @"C:\Users\alexd\Desktop\Slicer\Olden\scripts\base.sh";
            var path = @"C:\Users\alexd\Desktop\Slicer\Olden\oldenExecutions.csv";
            var programs = new Dictionary<string, int>()
                {
                    { "bh", 1 },
                    { "bisort", 2 },
                    { "em3d", 3 },
                    { "health", 4 },
                    { "mst", 5 },
                    { "perimeter", 6 },
                    { "power", 7 },
                    { "treeadd", 8 },
                    { "tsp", 9 },
                    { "voronoi", 10 }
                };

            var allLines = System.IO.File.ReadAllLines(path);
            foreach (var line in allLines)
            {
                var s = line.Split(";", StringSplitOptions.RemoveEmptyEntries);
                List<RandomInputGenerator.OldenExecution> l = null;
                if (!Executions.TryGetValue(s[0], out l))
                { 
                    l = new List<RandomInputGenerator.OldenExecution>();
                    Executions[s[0]] = l;
                }
                l.Add(GetFromLine(s));
            }

            var baseFile = File.ReadAllText(shBasePath);
            for (var i = 0; i < 10; i++)
            {
                var cTest = baseFile;
                foreach(var execution in Executions)
                {
                    var j = programs[execution.Key.ToLower()];
                    foreach (var input in execution.Value[i].Inputs)
                        cTest = cTest.Replace($"%{j}{input.Name}%", input.Value.ToString());
                }
                cTest = cTest.Replace("%varPage%", (i + 1).ToString());
                File.WriteAllText(Path.Combine(resultFolder, (i+1).ToString() + ".sh"), cTest);
            }
        }

        static RandomInputGenerator.OldenExecution GetFromLine(string[] s)
        {
            var l = new RandomInputGenerator.OldenExecution();
            l.Name = s[0];
            for (var i = 1; i < s.Length; i++)
            {
                var x = s[i].Substring(1);
                var y = x.Split(" ");
                var input = new RandomInputGenerator.OldenInput();
                input.Name = y[0];
                input.Value = Convert.ToInt32(y[1]);
                l.Inputs.Add(input);
            }
            return l;
        }

        public static Dictionary<string, List<RandomInputGenerator.OldenExecution>> Executions { get; set; } 
            = new Dictionary<string, List<RandomInputGenerator.OldenExecution>>();
    }
}
