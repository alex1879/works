﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace Utils
{
    class Latex
    {
        static void Main()
        {
            //Roslyn();
            //return;

            Powershell();
            return;

            var input = @"BH & -b 26 -s 2 & 6 & 130022 & 386 & 153 & 40 & 154 & 40 & 26 & 247196 & 166 & 43 & 12 & 4473 & 39 & 10 & 20 & 42863 & 441 & 114 & 499 & 7634 & 230 \\
BiSort & -s 12 & 3 & 1159 & 128 & 80 & 62 & 80 & 62 & 6 & 2029 & 79 & 61 & 11 & 2830 & 55 & 43 & 7 & 277 & 86 & 68 & 178 & 2245 & 171 \\
Em3d & -n 2 -d 2 -i 1 & 5 & 444 & 146 & 56 & 38 & 56 & 38 & 6 & 856 & 66 & 45 & 10 & 2872 & 47 & 32 & 7 & 167 & 95 & 65 & 197 & 2581 & 174 \\
Health & -l 4 -t 14 -s 1 & 3 & 115800 & 237 & 146 & 62 & 146 & 62 & 25 & 204442 & 97 & 41 & 10 & 3784 & 108 & 46 & 25 & 29147 & 181 & 76 & 362 & 4463 & 279 \\
MST & -v 4 & 1 & 802 & 187 & 111 & 59 & 111 & 59 & 6 & 1725 & 97 & 52 & 10 & 2864 & 84 & 45 & 7 & 205 & 173 & 93 & 449 & 5251 & 339 \\
Perimeter & -l 12 & 2 & 9194510 & 205 & 76 & 37 & 76 & 37 & 286790 & 23170098 & 40 & 20 & 43 & 63322 & 0 & 0 & 0 & 0 & 214 & 105 & 260 & 4907 & 51 \\
Power & -f 2 -l 2 -b 2 -l 2 & 4 & 227496 & 296 & 202 & 68 & 202 & 68 & 38 & 388971 & 192 & 65 & 15 & 5488 & 185 & 63 & 4699 & 101778 & 333 & 113 & 369 & 5462 & 197 \\
TreeAdd & -l 2 & 1 & 59 & 44 & 22 & 50 & 22 & 50 & 7 & 123 & 20 & 45 & 10 & 2790 & 20 & 45 & 6 & 17 & 32 & 73 & 112 & 1337 & 144 \\
TSP & -c 1024 & 5 & 1568678 & 263 & 106 & 40 & 106 & 40 & 813 & 3359519 & 111 & 42 & 28 & 28243 & 5 & 2 & 34 & 207078 & 103 & 39 & 399 & 3877 & 423 \\
Voronoi & -n 9 & 6 & 15880 & 352 & 204 & 58 & 205 & 58 & 14 & 35385 & 215 & 61 & 10 & 3062 & 32 & 9 & 10 & 3764 & 416 & 118 & 594 & 7648 & 234 \\";
			var lines = input.Split(Environment.NewLine);
			var sb = new StringBuilder();

			foreach (var line in lines)
			{
				var currentLine = "";
				var splittedLine = line.Replace(@"\\", "").Split('&').Select(x => x.Trim()).ToList();
				var isFirst = true;
                var boldLines = new int[] { 5, 7, 11, 15, 19 };
				for (var i = 0; i < splittedLine.Count; i++)
				{
                    var cell = splittedLine[i];
                    if (i > 1) //(!isFirst)
                    {
                        if (boldLines.Contains(i))
                            cell = $@"\textbf{{{cell}}}";

                        currentLine = currentLine + @"\multicolumn{1}{|r|}{" + cell + "}";
                    }
                    else
                    {
                        //var text = $"{cell} ({splittedLine[i + 1]})";
                        var text = $"{cell}";
                        //i++;

                        currentLine = currentLine + @"\multicolumn{1}{|l|}{" + text + "}";
                        isFirst = false;
                    }
					currentLine += " & ";
				}
				currentLine = currentLine.Substring(0, currentLine.Length - 2);
				currentLine += @"\\";
				sb.AppendLine(currentLine);
			}

			Console.Write(sb.ToString());
			Console.ReadKey();
		}

        static void Powershell()
        {
            var input = @"FileSystemProvider & 1 & 844 & 330 & 33 & 10.00 & 35 & 11.00 & 0.1 & 132 & 10.00 \\
PowerShellPolicy & 15 & 2732 & 317 & 50 & 15.53 & 92 & 29.07 & 0.1 & 582 & 150.60 \\
PSObject & 2 & 1344 & 509 & 52 & 11.50 & 169 & 33.00 & 0.1 & 263 & 238.50 \\
Utils & 2 & 2659 & 203 & 9 & 25.50 & 12 & 34.50 & 0.1 & 78 & 67.00 \\";
            var lines = input.Split(Environment.NewLine);
            var sb = new StringBuilder();

            foreach (var line in lines)
            {
                var currentLine = "";
                var splittedLine = line.Replace(@"\\", "").Split('&').Select(x => x.Trim());
                var isFirst = true;
                var boldLines = new int[] { 4, 6 };
                var i = 0; 
                foreach (var cell in splittedLine)
                {
                    var cellText = cell;
                    if (boldLines.Contains(i))
                        cellText = $@"\textbf{{{cellText}}}";

                    if (!isFirst)
                        currentLine = currentLine + @"\multicolumn{1}{|r|}{" + cellText + "}";
                    else
                    {
                        currentLine = currentLine + cellText;
                        isFirst = false;
                    }
                    currentLine += " & ";

                    i++;
                }
                currentLine = currentLine.Substring(0, currentLine.Length - 2);
                currentLine += @"\\";
                sb.AppendLine(currentLine);
            }

            Console.Write(sb.ToString());
            Console.ReadKey();
        }

        static void Roslyn()
        {
            var iTimeA = "175.0";
            var iTimeB = "190.2";
            //var iTimeA = "21.8";
            //var iTimeB = "44.9";

            //var input = @"StatementParsingTests & 517 & 1406 & 19756 & 428.1 & 30.46 & 3.4 & 38249 & 427.9 & 6.1 & 46.17 & x2 & -0.05 \\
            //            TypeTests & 169 & 6443 & 72236 & 2634.0 & 40.88 & 31.9 & 871926 & 2401.1 & 850.2 & 89.05 & x27 & -9.70 \\
            //            BindingTests & 67 & 9250 & 100178 & 2486.6 & 26.88 & 51.4 & 1100324 & 2483.8 & 1045.5 & 88.08 & x20 & -0.12 \\
            //            FlowTests & 40 & 9183 & 101699 & 2721.1 & 29.63 & 35.3 & 969848 & 2717.6 & 882.5 & 86.56 & x25 & -0.13 \\
            //            CompilationEmitTests & 104 & 7326 & 106165 & 2108.4 & 28.78 & 49.4 & 770085 & 1881.6 & 1081.8 & 81.96 & x22 & -12.05 \\";

            var input = @"FileSystemProvider & 13 & 220 & 3950 & 25.0 & 11.40 & 0.9 & 195641 & 25.0 & 70.9 & 31.04 & x79 & 0.00 \\
                            MshSnapinInfo & 1 & 102 & 112 & 6.0 & 5.88 & 0.1 & 654 & 6.0 & 0.1 & 10.00 & x1 & 0.00 \\
                            NamedPipe & 4 & 155 & 257 & 4.5 & 2.91 & 0.1 & 403 & 4.5 & 0.1 & 6.50 & x1 & 0.00 \\
                            CorePsPlatform & 1 & 13 & 13 & 2.0 & 15.38 & 0.1 & 13 & 2.0 & 0.1 & 3.00 & x1 & 0.00 \\
                            PowerShellAPI & 3 & 291 & 451 & 151.0 & 51.89 & 0.8 & 299897 & 151.0 & 274.9 & 180.00 & x352 & 0.00 \\
                            PSConfiguration & 286 & 187 & 844 & 55.4 & 29.55 & 0.1 & 1592 & 55.4 & 0.6 & 71.09 & x6 & 0.00 \\
                            Binders & 1 & 17 & 17 & 4.0 & 23.53 & 0.1 & 17 & 4.0 & 0.1 & 6.00 & x1 & 0.00 \\
                            PSObject & 15 & 167 & 269 & 72.3 & 43.38 & 0.1 & 11337 & 72.3 & 3.2 & 84.35 & x32 & 0.00 \\
                            ExtensionMethods & 1 & 8 & 8 & 3.0 & 37.50 & 0.1 & 4498 & 3.0 & 0.7 & 5.00 & x7 & 0.00 \\
                            PSVersionInfo & 1 & 128 & 222 & 11.0 & 8.59 & 0.1 & 240 & 11.0 & 0.1 & 12.00 & x1 & 0.00 \\
                            Runspace & 6 & 130 & 170 & 43.2 & 33.12 & 0.3 & 144997 & 43.2 & 30.7 & 49.50 & x92 & 0.00 \\
                            SecuritySupport & 1 & 48 & 48 & 20.0 & 41.67 & 0.1 & 122 & 20.0 & 0.1 & 23.00 & x1 & 0.00 \\
                            SessionState & 1 & 50 & 137 & 38.0 & 76.00 & 3.1 & 232528 & 8.0 & 319.9 & 43.00 & x105 & -375.00 \\
                            Utils & 45 & 11 & 31 & 5.4 & 49.95 & 0.1 & 2288 & 5.4 & 0.7 & 6.42 & x7 & 0.00 \\
                            WildcardPattern & 13 & 32 & 46 & 8.9 & 28.16 & 0.1 & 53 & 8.9 & 0.1 & 11.46 & x1 & 0.00 \\";

            var lines = input.Split(Environment.NewLine);
            var sb = new StringBuilder();

            foreach (var line in lines)
            {
                var currentLine = "";
                var splittedLine = line.Replace(@"\\", "").Split('&').Select(x => x.Trim());
                var isFirst = true;

                var colI = 0;
                foreach (var cell in splittedLine)
                {
                    //if (colI == 7)
                    //{
                    //    currentLine = currentLine + @"\multicolumn{1}{|r|}{" + iTimeA + "}" + " & ";
                    //}
                    //if (colI == 14)
                    //{
                    //    currentLine = currentLine + @"\multicolumn{1}{|r|}{" + iTimeB + "}" + " & ";
                    //}

                    //if (int.TryParse(cell, out int _))
                    if (!isFirst)
                        currentLine = currentLine + @"\multicolumn{1}{|r|}{" + cell + "}";
                    else
                    {
                        currentLine = currentLine + cell;
                        isFirst = false;
                    }
                    currentLine += " & ";

                    colI++;
                }
                currentLine = currentLine.Substring(0, currentLine.Length - 2);
                currentLine += @"\\";
                sb.AppendLine(currentLine);
            }

            Console.Write(sb.ToString());
            Console.ReadKey();
        }
    }
}
