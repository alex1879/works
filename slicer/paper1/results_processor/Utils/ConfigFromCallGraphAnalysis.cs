﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Utils
{
    class ConfigFromCallGraphAnalysis
    {
        public static void Main()
        {
            var mainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis";
            var set = new HashSet<string>();
            foreach (var file in Directory.GetFiles(mainFolder, "*", searchOption: SearchOption.AllDirectories))
            {
                if (file.Contains("RoslynAnalysisResults"))
                {
                    var allLines = File.ReadAllLines(file).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    set.UnionWith(allLines);
                }
            }

            // OLD
            //var analysisResult = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\RoslynAnalysisResults.txt";
            //var allLines = File.ReadAllLines(analysisResult).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            ChangeSlc(set.ToList());
        }

        public static void ChangeSlc(List<string> allLines)
        {
            var slcFile = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn.slc";
            var outputSlcFile = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn_WithAll.slc";

            // Open slc file
            var stream = File.OpenRead(slcFile);
            var serializer = new XmlSerializer(typeof(UserConfiguration));
            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);
            // Open analysis result --> Dictionary<Project, Files>
            var analysisDict = new Dictionary<string, HashSet<string>>();
            foreach (var line in allLines)
            {
                var data = line.Split(';');
                var projectName = data[0];
                var filePath = data[1];
                if (!analysisDict.ContainsKey(projectName))
                    analysisDict[projectName] = new HashSet<string>();

                analysisDict[projectName].Add(filePath);
            }
            // Foreach project in dictionary
            foreach (var kv in analysisDict)
            {
                // Get the project entry in the config file
                var currentProject = userConfiguration.targetProjects.excluded.SingleOrDefault(x => x.name.Replace("(netcoreapp3.1)", "") == kv.Key);
                if (currentProject == null)
                    continue;

                var maxID = currentProject.initialFileID + 1;
                if (currentProject.files != null)
                    maxID = currentProject.files.Max(x => x.id) + 1;
                if (currentProject.files == null)
                    currentProject.files = new UserConfiguration.ExcludedFile[0];
                var toAdd = new List<UserConfiguration.ExcludedFile>(currentProject.files);
                foreach (var fileToInstrument in kv.Value.Where(x => File.Exists(x)))
                    if (!currentProject.files.Any(x => x.name == fileToInstrument))
                        toAdd.Add(new UserConfiguration.ExcludedFile() { id = maxID++, name = fileToInstrument, skip = false });
                currentProject.files = toAdd.ToArray();
            }
            // Save the result to the output slc file
            var streamToSave = System.IO.File.OpenWrite(outputSlcFile);
            serializer.Serialize(streamToSave, userConfiguration);
        }
    }
}
