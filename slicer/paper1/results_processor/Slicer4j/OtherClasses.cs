﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Utils
{
    class ManualResultsCollector
    {
        public static void Main()
        {
            var baseFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\output_large\ManualResults";
            var sb = new StringBuilder();

            var controlDep = 0;
            var thisDep = 0;
            var otherDep = 0;

            var folders = Directory.GetDirectories(baseFolder);
            foreach (var folder in folders)
            {
                var files = Directory.GetFiles(folder);
                foreach (var file in files)
                {
                    var counter = 0;
                    var lines = File.ReadAllLines(file);
                    foreach (var line in lines)
                    {
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            if (line.Contains("//"))
                            {
                                var comment = line.Split(new string[] {"//" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim();
                                switch (comment)
                                {
                                    case "Control":
                                        controlDep++;
                                        break;
                                    case "This":
                                        thisDep++;
                                        break;
                                    default:
                                        otherDep++;
                                        break;
                                }
                            }
                            else
                                counter++;
                        }
                    }

                    var programName = Path.GetFileName(folder);
                    var fileName = Path.GetFileNameWithoutExtension(file);
                    //sb.AppendLine($"{programName};{fileName};{counter}");
                    sb.AppendLine($"{counter}");
                }
            }

            Console.WriteLine(sb.ToString());
        }
    }

    public class GenerateJavaslicerFiles
    {
        public static void Main()
        {
            var resultFolder = @"C:\Users\alexd\Desktop\Slicer\Olden\scripts";
            var shBasePath = @"C:\Users\alexd\Desktop\Slicer\Olden\scripts\base.sh";
            var path = @"C:\Users\alexd\Desktop\Slicer\Olden\oldenExecutions.csv";
            var programs = new Dictionary<string, int>()
                {
                    { "bh", 1 },
                    { "bisort", 2 },
                    { "em3d", 3 },
                    { "health", 4 },
                    { "mst", 5 },
                    { "perimeter", 6 },
                    { "power", 7 },
                    { "treeadd", 8 },
                    { "tsp", 9 },
                    { "voronoi", 10 }
                };

            var allLines = System.IO.File.ReadAllLines(path);
            foreach (var line in allLines)
            {
                var s = line.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                List<RandomInputGenerator.OldenExecution> l = null;
                if (!Executions.TryGetValue(s[0], out l))
                {
                    l = new List<RandomInputGenerator.OldenExecution>();
                    Executions[s[0]] = l;
                }
                l.Add(GetFromLine(s));
            }

            var baseFile = File.ReadAllText(shBasePath);
            for (var i = 0; i < 10; i++)
            {
                var cTest = baseFile;
                foreach (var execution in Executions)
                {
                    var j = programs[execution.Key.ToLower()];
                    foreach (var input in execution.Value[i].Inputs)
                        cTest = cTest.Replace($"%{j}{input.Name}%", input.Value.ToString());
                }
                File.WriteAllText(Path.Combine(resultFolder, (i + 1).ToString() + ".sh"), cTest);
            }
        }

        public static RandomInputGenerator.OldenExecution GetFromLine(string[] s)
        {
            var l = new RandomInputGenerator.OldenExecution();
            l.Name = s[0];
            for (var i = 1; i < s.Length; i++)
            {
                var x = s[i].Substring(1);
                var y = x.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                var input = new RandomInputGenerator.OldenInput();
                input.Name = y[0];
                input.Value = Convert.ToInt32(y[1]);
                l.Inputs.Add(input);
            }
            return l;
        }

        public static Dictionary<string, List<RandomInputGenerator.OldenExecution>> Executions { get; set; }
            = new Dictionary<string, List<RandomInputGenerator.OldenExecution>>();
    }

    public class RandomInputGenerator
    {
        public static void Main()
        {
            var electionResultPath = @"C:\Users\alexd\Desktop\Slicer\Olden\oldenExecutions.csv";
            var targetOldenExecutions = GetOldenExecutions();
            var elections = GetOldenElections(targetOldenExecutions);
            PrintResult(electionResultPath, elections);
        }

        #region Classes
        public class OldenExecution
        {
            public string Name { get; set; }
            public List<OldenInput> Inputs { get; set; } = new List<OldenInput>();

            public override bool Equals(object x) =>
                ((OldenExecution)x).Name == this.Name &&
                ((OldenExecution)x).Inputs.TrueForAll(y => this.Inputs.Any(z => y.Equals(z)));

            public override int GetHashCode() => this.Inputs.Sum(x => x.GetHashCode() / 1000);
        }
        public class OldenInput
        {
            public string Name { get; set; }
            public int Value { get; set; }
            public int MinValue { get; set; }
            public int MaxValue { get; set; }

            public override bool Equals(object x) => ((OldenInput)x).Name == this.Name && ((OldenInput)x).Value == this.Value;

            public override int GetHashCode() => this.Name.GetHashCode() + this.Value.GetHashCode();
        }
        public class OldenElection
        {
            public HashSet<OldenExecution> Executions { get; set; } = new HashSet<OldenExecution>();
        }
        #endregion

        static List<OldenExecution> GetOldenExecutions()
        {
            List<OldenExecution> TargetOldenExecutions = new List<OldenExecution>();
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "BH",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "b", MinValue = 2, MaxValue = 26 },
                    new OldenInput(){ Name = "s", MinValue = 2, MaxValue = 15 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "BiSort",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "s", MinValue = 4, MaxValue = 1024 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Em3d",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "n", MinValue = 2, MaxValue = 50 },
                    new OldenInput(){ Name = "d", MinValue = 2, MaxValue = 25 },
                    new OldenInput(){ Name = "i", MinValue = 1, MaxValue = 10 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Health",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 6 },
                    new OldenInput(){ Name = "t", MinValue = 2, MaxValue = 10 },
                    new OldenInput(){ Name = "s", MinValue = 1, MaxValue = 1 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "MST",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "v", MinValue = 4, MaxValue = 144 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Perimeter",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 11 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Power",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "f", MinValue = 1, MaxValue = 4 },
                    new OldenInput(){ Name = "l", MinValue = 1, MaxValue = 4 },
                    new OldenInput(){ Name = "b", MinValue = 1, MaxValue = 2 },
                    new OldenInput(){ Name = "e", MinValue = 1, MaxValue = 2 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "TreeAdd",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "l", MinValue = 2, MaxValue = 16 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "TSP",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "c", MinValue = 4, MaxValue = 512 }
                }
            });
            TargetOldenExecutions.Add(new OldenExecution()
            {
                Name = "Voronoi",
                Inputs = new List<OldenInput>()
                {
                    new OldenInput(){ Name = "n", MinValue = 4, MaxValue = 256 }
                }
            });
            return TargetOldenExecutions;
        }

        static List<OldenElection> GetOldenElections(List<OldenExecution> oldenExecutions)
        {
            List<OldenElection> results = new List<OldenElection>();
            foreach (var oldenExecution in oldenExecutions)
            {
                var oldenElection = new OldenElection();
                for (var i = 0; i < 10; i++)
                {
                    var exec = MakeChoise(oldenExecution);
                    var added = oldenElection.Executions.Add(exec);
                    if (!added)
                        i--;
                }
                results.Add(oldenElection);
            }
            return results;
        }

        static void PrintResult(string path, List<OldenElection> results)
        {
            var sb = new StringBuilder();
            foreach (var result in results)
            {
                foreach (var x in result.Executions)
                {
                    Append(sb, x.Name);
                    foreach (var i in x.Inputs)
                        Append(sb, "-" + i.Name + " " + i.Value);
                    sb.AppendLine();
                }
            }
            System.IO.File.WriteAllText(path, sb.ToString());
        }

        static void Append(StringBuilder sb, string val)
        {
            sb.Append(val);
            sb.Append(";");
        }

        static OldenExecution MakeChoise(OldenExecution execution)
        {
            var r = new OldenExecution();
            r.Name = execution.Name;
            foreach (var i in execution.Inputs)
            {
                var newInput = new OldenInput();
                newInput.Name = i.Name;
                newInput.Value = new Random().Next(i.MinValue, i.MaxValue + 1);
                r.Inputs.Add(newInput);
            }
            return r;
        }
    }
}
