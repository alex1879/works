﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace Slicer4j
{
    class Program
    {
        static string sourceCodePath = @"C:\Users\alexd\Desktop\Slicer\Slicer4JCases\src";
        static string slicer4jPath = @"C:\Users\alexd\Desktop\Slicer\Slicer4J\Slicer4JTool\scripts\slicer4j.py";
        static string outputFolderPath = @"C:\Users\alexd\Desktop\Slicer\Slicer4JCases\output";
        static string binFolderPath = @"C:\Users\alexd\Desktop\Slicer\Slicer4JCases\bin";

        static void Main(string[] args)
        {
            //RunCase("MST", 56, "-v 4");
            //RunCase("Voronoi", 79, "-n 2");
            //RunCase("NewObject", 15, "");
            //Olden(new string[] { });
            //RunCompiled("example");
            OldenRandom(args);
        }

        static void OldenRandom(string[] args)
        {
            #region LoadRandomInputs
            var path = @"C:\Users\alexd\Desktop\Slicer\Olden\oldenExecutions.csv";
            var programs = new Dictionary<string, int>()
                {
                    { "bh", 1 },
                    { "bisort", 2 },
                    { "em3d", 3 },
                    { "health", 4 },
                    { "mst", 5 },
                    { "perimeter", 6 },
                    { "power", 7 },
                    { "treeadd", 8 },
                    { "tsp", 9 },
                    { "voronoi", 10 }
                };

            var executions = new Dictionary<string, List<RandomInputGenerator.OldenExecution>>();

            var allLines = System.IO.File.ReadAllLines(path);
            foreach (var line in allLines)
            {
                var s = line.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                List<RandomInputGenerator.OldenExecution> l = null;
                if (!executions.TryGetValue(s[0], out l))
                {
                    l = new List<RandomInputGenerator.OldenExecution>();
                    executions[s[0]] = l;
                }
                l.Add(GenerateJavaslicerFiles.GetFromLine(s));
            }
            #endregion

            #region Lines
            var l2 = new Dictionary<string, Tuple<string, List<int>>>();
            l2.Add("BH", new Tuple<string, List<int>>("-b 26 -s 2", new List<int>() { 84, 85, 86, 87, 88, 89 }));
            l2.Add("BiSort", new Tuple<string, List<int>>("-s 12", new List<int>() { 86, 87, 88 }));
            l2.Add("Em3d", new Tuple<string, List<int>>("-n 2 -d 2 -i 1", new List<int>() { 74, 75, 76, 77, 78 }));
            l2.Add("Health", new Tuple<string, List<int>>("-l 4 -t 14 -s 1", new List<int>() { 78, 79, 80 }));
            l2.Add("MST", new Tuple<string, List<int>>("-v 4", new List<int>() { 56 }));
            l2.Add("Perimeter", new Tuple<string, List<int>>("-l 12", new List<int>() { 73, 74 }));
            //l2.Add("Power", new Tuple<string, List<int>>("", new List<int>() { 74, 75, 76, 77 }));
            l2.Add("Power", new Tuple<string, List<int>>("", new List<int>() { 90, 91, 92, 93 }));
            l2.Add("TreeAdd", new Tuple<string, List<int>>("-l 2", new List<int>() { 52 }));
            l2.Add("TSP", new Tuple<string, List<int>>("-c 1024", new List<int>() { 60, 61, 62, 63, 64 }));
            l2.Add("Voronoi", new Tuple<string, List<int>>("-n 9", new List<int>() { 72, 73, 74, 75, 76, 77 }));
            #endregion

            foreach (var execution in executions)
            {
                var i = 0;
                foreach (var e in execution.Value)
                {
                    var dic = new Dictionary<string, Tuple<string, List<int>>>();
                    dic[e.Name] = new Tuple<string, List<int>>(string.Join(" ", 
                        e.Inputs.Select(x => "-" + x.Name + " " + x.Value.ToString())), 
                        l2[e.Name].Item2);
                    Olden(dic, ++i);
                }
            }

            // Print random execution values
            var sb = new StringBuilder();
            foreach (var execution in executions)
            {
                var instances = Results[execution.Key].Count;
                var avgLines = Results[execution.Key].Values.Sum(x => x.Lines / 10) / instances;
                var avgTrace = Results[execution.Key].Values.Sum(x => x.TraceSize / 10) / instances;
                var avgSeconds = Results[execution.Key].Values.Sum(x => x.Seconds / 10) / instances;
                var nextLine = $"{execution.Key}|{instances}|{avgLines}|{avgSeconds}|{avgTrace}";
                sb.AppendLine(nextLine);
            }
            System.Console.WriteLine(sb);
        }

        static void Olden(Dictionary<string, Tuple<string, List<int>>> OldenPrograms, int? number)
        {
            var localOutputFolderPath = outputFolderPath;

            if (number != null)
                localOutputFolderPath = Path.Combine(localOutputFolderPath, number.ToString());

            var summaryPath = Path.Combine(localOutputFolderPath, "summary.txt");
            
            var collectResults = true;

            if (OldenPrograms == null)
                OldenPrograms = new Dictionary<string, Tuple<string, List<int>>>();

            #region Old inputs
            // Default inputs
            //OldenPrograms.Add("BH", new Tuple<string, List<int>>("-b 26 -s 2", new List<int>() { 84, 85, 86, 87, 88, 89 }));
            //OldenPrograms.Add("BiSort", new Tuple<string, List<int>>("-s 12", new List<int>() { 86, 87, 88 }));
            //OldenPrograms.Add("Em3d", new Tuple<string, List<int>>("-n 2 -d 2 -i 1", new List<int>() { 74, 75, 76, 77, 78 }));
            //OldenPrograms.Add("Health", new Tuple<string, List<int>>("-l 4 -t 14 -s 1", new List<int>() { 78, 79, 80 }));
            //OldenPrograms.Add("MST", new Tuple<string, List<int>>("-v 4", new List<int>() { 56 }));
            //OldenPrograms.Add("Perimeter", new Tuple<string, List<int>>("-l 12", new List<int>() { 73, 74 }));
            //OldenPrograms.Add("Power", new Tuple<string, List<int>>("", new List<int>() { 74, 75, 76, 77 }));
            //OldenPrograms.Add("TreeAdd", new Tuple<string, List<int>>("-l 2", new List<int>() { 52 }));
            //OldenPrograms.Add("TSP", new Tuple<string, List<int>>("-c 1024", new List<int>() { 60, 61, 62, 63, 64 }));
            //OldenPrograms.Add("Voronoi", new Tuple<string, List<int>>("-n 9", new List<int>() { 72, 73, 74, 75, 76, 77 }));

            // Large inputs
            //OldenPrograms.Add("BH", new Tuple<string, List<int>>("-b 26 -s 15", new List<int>() { 84, 85, 86, 87, 88, 89 }));
            //OldenPrograms.Add("BiSort", new Tuple<string, List<int>>("-s 1024", new List<int>() { 86, 87, 88 }));
            //OldenPrograms.Add("Em3d", new Tuple<string, List<int>>("-n 50 -d 25 -i 100", new List<int>() { 74, 75, 76, 77, 78 }));
            //OldenPrograms.Add("Health", new Tuple<string, List<int>>("-l 6 -t 7 -s 1", new List<int>() { 78, 79, 80 }));
            //OldenPrograms.Add("MST", new Tuple<string, List<int>>("-v 144", new List<int>() { 56 }));
            //OldenPrograms.Add("Perimeter", new Tuple<string, List<int>>("-l 11", new List<int>() { 73, 74 }));
            //OldenPrograms.Add("Power", new Tuple<string, List<int>>("", new List<int>() { 74, 75, 76, 77 }));
            //OldenPrograms.Add("TreeAdd", new Tuple<string, List<int>>("-l 16", new List<int>() { 52 }));
            //OldenPrograms.Add("TSP", new Tuple<string, List<int>>("-c 512", new List<int>() { 60, 61, 62, 63, 64 }));
            //OldenPrograms.Add("Voronoi", new Tuple<string, List<int>>("-n 256", new List<int>() { 72, 73, 74, 75, 76, 77 }));
            #endregion

            var summaryText = new StringBuilder();
            foreach (var oldenProgram in OldenPrograms)
            {
                var oldenProgramSourceCodePath = Path.Combine(sourceCodePath, oldenProgram.Key);
                if (!Results.ContainsKey(oldenProgram.Key))
                    Results[oldenProgram.Key] = new Dictionary<int, Slicer4JResult>();

                var i = 1;
                foreach (var slicingVariable in oldenProgram.Value.Item2)
                {
                    try
                    {
                        var localOutput = Path.Combine(localOutputFolderPath, oldenProgram.Key, i.ToString());
                        var localsrc = Path.Combine(localOutput, "src");
                        var localCompilation = Path.Combine(localOutput, "compilation");
                        var localSlicingResults = Path.Combine(localOutput, "output");
                        var slicePath = Path.Combine(localSlicingResults, "slice.log");
                        var timePath = Path.Combine(localSlicingResults, "time.log");
                        var tracePath = Path.Combine(localSlicingResults, "trace.log_icdg.log");

                        if (collectResults)
                        {
                            if (!Results[oldenProgram.Key].ContainsKey(i))
                                Results[oldenProgram.Key][i] = new Slicer4JResult();

                            var allLines = 0;
                            var totalSeconds = 0m;
                            var traceSize = 0;
                            if (File.Exists(slicePath))
                            {
                                allLines = File.ReadAllLines(slicePath)
                                    .Where(x => !string.IsNullOrWhiteSpace(x) && !x.Contains("-1")).Count();
                                var times = File.ReadAllLines(timePath).First().Split('-');
                                totalSeconds = decimal.Parse(times[0]) + decimal.Parse(times[1]);

                                traceSize = Convert.ToInt32(Math.Round((decimal)new FileInfo(tracePath).Length / (decimal)1024));
                            }
                            var nextLine = $"{oldenProgram.Key}|{i}|{allLines}|{totalSeconds}|{traceSize}";
                            summaryText.AppendLine(nextLine);

                            Results[oldenProgram.Key][i].Lines += allLines;
                            Results[oldenProgram.Key][i].TraceSize += traceSize;
                            Results[oldenProgram.Key][i].Seconds += (double)totalSeconds;
                        }
                        else
                        {
                            Directory.CreateDirectory(localOutput);
                            Directory.CreateDirectory(localsrc);
                            Directory.CreateDirectory(localCompilation);
                            Directory.CreateDirectory(localSlicingResults);

                            // Copiar código del otro lado
                            foreach (var file in Directory.GetFiles(oldenProgramSourceCodePath))
                                File.Copy(file, Path.Combine(localsrc, Path.GetFileName(file)), true);
                            // Reemplazar el done por el número de variable que estamos sliceando
                            var mainFile = Path.Combine(localsrc, oldenProgram.Key + ".java");
                            var allText = File.ReadAllText(mainFile).Replace("\"Done!\"", $"slicingVariable{i}");
                            File.WriteAllText(mainFile, allText);
                            // Compilar código
                            bool timeOut;
                            DateTime initTime = DateTime.Now;
                            var javaClassCompilation = $"javac {localsrc}\\*.java -g -d {localCompilation}";
                            RunCommand(javaClassCompilation, out timeOut);
                            var javaJarCompilation = $"jar cfe {localCompilation}\\{oldenProgram.Key}.jar {oldenProgram.Key} -C {localCompilation} .";
                            RunCommand(javaJarCompilation, out timeOut);
                            DateTime afterCompilationTime = DateTime.Now;
                            // Ejecutar Slicer4j
                            var outputLine = oldenProgram.Value.Item2.Last() + 1;
                            var slicerCommand = $"python3 {slicer4jPath} -j {localCompilation}\\{oldenProgram.Key}.jar -o {localSlicingResults} -b {oldenProgram.Key}:{outputLine} -m \"{oldenProgram.Key} {oldenProgram.Value.Item1}\"";
                            RunCommand(slicerCommand, out timeOut);
                            DateTime lastTime = DateTime.Now;
                            // Order the result:
                            if (File.Exists(slicePath))
                            {
                                var newSlicePath = Path.Combine(localSlicingResults, "ordered_slice.log");
                                var allLines = File.ReadAllLines(slicePath)
                                    .Where(x => !string.IsNullOrWhiteSpace(x))
                                    .Select(x => new { FileName = x.Split(':')[0], Line = int.Parse(x.Split(':')[1]) })
                                    .OrderBy(x => x.FileName).ThenBy(y => y.Line)
                                    .Select(x => x.FileName + ":" + x.Line.ToString())
                                    .ToArray();
                                File.WriteAllLines(newSlicePath, allLines);

                                var compilationSeconds = afterCompilationTime.Subtract(initTime).TotalSeconds;
                                var totalSeconds = lastTime.Subtract(initTime).TotalSeconds;

                                var timeLine = $"{initTime:yyyyMMdd HH:mm:ss}-{afterCompilationTime:yyyyMMdd HH:mm:ss}-{lastTime:yyyyMMdd HH:mm:ss}";
                                var secondsDiffLine = $"{compilationSeconds}-{totalSeconds}";
                                File.WriteAllLines(timePath, new string[] { secondsDiffLine, timeLine });
                            }
                            if (timeOut)
                                File.WriteAllLines(Path.Combine(localSlicingResults, "time.log"), new string[] { });
                        }
                        
                    }
                    catch(Exception)
                    {

                    }
                    i++;
                }
            }
            if (collectResults)
                File.AppendAllText(summaryPath, summaryText.ToString());
        }

        public static void RunCase(string caseName, int line, string args)
        {
            var srcPath = Path.Combine(sourceCodePath, caseName);
            var localOutput = Path.Combine(outputFolderPath, caseName);
            var localsrc = Path.Combine(localOutput, "src");
            var localCompilation = Path.Combine(localOutput, "compilation");
            var localSlicingResults = Path.Combine(localOutput, "output");
            var slicePath = Path.Combine(localSlicingResults, "slice.log");

            Directory.CreateDirectory(localOutput);
            Directory.CreateDirectory(localsrc);
            Directory.CreateDirectory(localCompilation);
            Directory.CreateDirectory(localSlicingResults);

            // Copiar código del otro lado
            foreach (var file in Directory.GetFiles(srcPath))
                File.Copy(file, Path.Combine(localsrc, Path.GetFileName(file)), true);
            //// Reemplazar el done por el número de variable que estamos sliceando
            //var mainFile = Path.Combine(localsrc, oldenProgram.Key + ".java");
            //var allText = File.ReadAllText(mainFile).Replace("\"Done!\"", $"slicingVariable{i}");
            //File.WriteAllText(mainFile, allText);
            // Compilar código
            bool timeOut;
            var javaClassCompilation = $"javac {localsrc}\\*.java -g -d {localCompilation}";
            RunCommand(javaClassCompilation, out timeOut);
            var javaJarCompilation = $"jar cfe {localCompilation}\\{caseName}.jar {caseName} -C {localCompilation} .";
            RunCommand(javaJarCompilation, out timeOut);
            // Ejecutar Slicer4j
            var outputLine = line;
            var slicerCommand = $"python3 {slicer4jPath} -j {localCompilation}\\{caseName}.jar -o {localSlicingResults} -b {caseName}:{outputLine} -m \"{caseName} {args}\"";
            RunCommand(slicerCommand, out timeOut);
        }

        public static void RunCompiled(string caseName)
        {
            var localOutput = Path.Combine(outputFolderPath, caseName);
            var localSlicingResults = Path.Combine(localOutput, "output");
            var slicePath = Path.Combine(localSlicingResults, "slice.log");
            var timePath = Path.Combine(localSlicingResults, "time.log");
            var localJar = Path.Combine(binFolderPath, caseName + ".jar");

            Directory.CreateDirectory(localOutput);
            Directory.CreateDirectory(localSlicingResults);

            // Compilar código
            bool timeOut;
            // Ejecutar Slicer4j
            var targetApp = @"com.lafhis.example.App";
            var targetMethod = targetApp + @".main";
            //var targetVariable = "total";
            var outputLine = 34;
            DateTime initTime = DateTime.Now;

            var slicerCommand = $"python3 {slicer4jPath} -j {localJar} -o {localSlicingResults} -b {targetMethod}:{outputLine}  -m \"{caseName}\"";
            RunCommand(slicerCommand, out timeOut);

            DateTime lastTime = DateTime.Now;
            // Order the result:
            if (File.Exists(slicePath))
            {
                var newSlicePath = Path.Combine(localSlicingResults, "ordered_slice.log");
                var allLines = File.ReadAllLines(slicePath)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Select(x => new { FileName = x.Split(':')[0], Line = int.Parse(x.Split(':')[1]) })
                    .OrderBy(x => x.FileName).ThenBy(y => y.Line)
                    .Select(x => x.FileName + ":" + x.Line.ToString())
                    .ToArray();
                File.WriteAllLines(newSlicePath, allLines);

                var totalSeconds = lastTime.Subtract(initTime).TotalSeconds;

                var timeLine = $"{initTime:yyyyMMdd HH:mm:ss}-{lastTime:yyyyMMdd HH:mm:ss}";
                var secondsDiffLine = $"{totalSeconds}";
                File.WriteAllLines(timePath, new string[] { secondsDiffLine, timeLine });
            }
        }

        public static int TimeoutMax = 7200000;
        public static void RunCommand(string command, out bool timeout)
        {
            timeout = false;
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            Process p = Process.Start(processInfo);
            StringBuilder sb = new StringBuilder();
            while (!p.StandardOutput.EndOfStream)
            {
                sb.AppendLine(p.StandardOutput.ReadLine());
            }

            //if (timeoutMilliseconds.HasValue)
            if (!p.WaitForExit(TimeoutMax))
            {
                p.Kill();
                timeout = true;
            }

            //return (timeout, p.TotalProcessorTime);
        }

        #region Results
        public static Dictionary<string, Dictionary<int, Slicer4JResult>> Results = 
            new Dictionary<string, Dictionary<int, Slicer4JResult>>();

        public class Slicer4JResult
        {
            public double Seconds { get; set; }
            public double TraceSize { get; set; }
            public double Lines { get; set; }
        }
        #endregion
    }
}
