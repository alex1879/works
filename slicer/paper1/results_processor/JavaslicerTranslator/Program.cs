﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace JavaslicerTranslator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Inputs que esperamos:
            // 1. Archivo del slice: output/JavaSlicerResults/BH/BH.slice
            // 2. Archivo donde queremos el resultado (el slice en mejor formato): output/JavaSlicerResults/BH/BH.result
            // 3. Archivo de traza generada: output/JavaSlicerResults/BH/BH.trace
            // 4. Summary file (donde ponemos los datos): output/JavaSlicerResults/BH/SummaryResultFile
            // 5. Tiempo de ejecución común JAVA
            // 6. Tiempo de traza de JavaSlicer
            // 7. Tiempo de analisis de JavaSlicer
            // 8. Nombre del programa
            // 9. Inputs del programa

            // -----------------
            // MODO BÁSICO: 
            // 1er input: path del slice proveniente del javaslicer
            // 2do input: path de donde queremos el resultado

            // Para probar:
            //var slice = @"C:\Users\Camilo\Desktop\Slicer\integracion\docs\Baseline Mayo 2017\Second experiment\test.slice";
            //var result = @"C:\Users\Camilo\Desktop\Slicer\integracion\docs\Baseline Mayo 2017\Second experiment\test.result";

            if (args == null || args.Count() < 2)
                throw new Exception("Ingrese los path de entrada y salida");
            
            var slice = args[0];
            var result = args[1];

            if (!File.Exists(slice))
                throw new Exception("No se encuentra el input");
            
            var lines = File.ReadAllLines(slice).ToList();
            var posicionFinished = lines.IndexOf("finished");
            // El +2 es para saltear el finished y para saltear la que le sigue que no es línea
            var sliceLines = lines.Skip(posicionFinished + 2).ToList();
            // Removemos las últimas 4 líneas que no van
            sliceLines = sliceLines.Take(sliceLines.Count - 4).ToList();
            // A este punto tenemos todas. OK. Hacemos "split" y nos quedamos con la primer parte que tiene todo
            // Nos quedamos los que no empiecen con "java." (paquetes del sistema) y que tienen un par de caracteres al menos (hay casos que tienen pocos)
            sliceLines = sliceLines.Select(x => x.Split(' ').First().Trim()).Where(x => !x.StartsWith("java.") && x.Count() > 5).ToList();
            // A este punto tenemos paquetes.Clase.Método:línea. Sacamos las repetidas
            sliceLines = sliceLines.Distinct().ToList();
            // Ahora extraemos el nombre de la clase, el cual es... de la parte izquierda el anteúltimo término
            // Me hago un ayudín: dado una lista devuelve el anteúltimo: (sino queda feo el código)
            // Ya que estoy limpio esos signos $$ que denotan multinivel de clases
            Func<List<string>, string> seleccionarAnteultimo = x => x[x.Count - 2].Split('$').First();
            var lineasFinales = sliceLines.Select(x => seleccionarAnteultimo(x.Split(':').First().Split('.').ToList()) + ".java:" + x.Split(':').Last());
            // TODO IMPORTANTE: Dejar solo las que queremos slicear.... (tiene todas!)
            File.WriteAllLines(result, lineasFinales);

            // MODO COMPLEJO: Si recibió más inputs (traza, archivo salida, los 2 anteriores, tiempo programa original, tiempo traza y tiempo slice) entonces guarda la línea que quiero para la planilla (ya que estoy...)
            if (args.Count() >= 8)
            {
                var traceFile = args[2];
                var resultFile = args[3];
                var tiempoOriginal = args[4];
                var tiempoTraza = args[5];
                var tiempoSlice = args[6];
                var programName = args[7];
                var total_time = int.Parse(tiempoTraza) + int.Parse(tiempoSlice);
                // Recolecto los parámetros
                var sb = new StringBuilder();
                for (var i = 8; i < args.Count(); i++)
                    sb.Append(args[i] + " ");

                if (!File.Exists(traceFile))
                    throw new Exception("No se encuentra el archivo traza");

                var tamañoTraza = Math.Round((decimal)new FileInfo(traceFile).Length / (decimal)1024, 0); // Lo paso a KB
                var tamañoSlice = Math.Round((decimal)new FileInfo(slice).Length / (decimal)1024, 0); // Lo paso a KB
                //File.AppendAllLines(resultFile, new string[] { string.Format("{5} {0} {1} {2} {3} {4} {6}", tiempoOriginal, tiempoTraza, tamañoTraza, tiempoSlice, tamañoSlice, programName, sb.ToString()) });
                File.AppendAllLines(resultFile, new string[] { $"{programName}|{sb.ToString().Trim()}|{tiempoOriginal}|{tiempoTraza}|{tiempoSlice}|{total_time}|{Path.GetFullPath(result)}|{tamañoTraza}" });

                
            }
        }
    }
}
