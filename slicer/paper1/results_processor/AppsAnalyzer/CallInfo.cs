﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;

namespace AppsAnalyzer
{
    internal class Call
    {
        public string ClassName { get; set; }
        public string MethodName { get; set; }

        public Call(string ClassName, string MethodName)
        {
            this.ClassName = ClassName;
            this.MethodName = MethodName;
        }

        public override int GetHashCode()
        {
            return (ClassName + "." + MethodName).GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return ((Call)obj).ClassName == this.ClassName && ((Call)obj).MethodName == this.MethodName;
        }
    }

    internal class CallInfo
    {
        public Call CallInstance { get; set; }
        public IMethodSymbol MethodSymbol { get; set; }

        public HashSet<Call> OtherCalls { get; set; }

        public bool Analyzed { get; set; } = false;

        public CallInfo(Call Instance, IMethodSymbol MethodSymbol)
        {
            this.CallInstance = Instance;
            this.MethodSymbol = MethodSymbol;
        }
    }

    internal class InternalStructures
    {
        public static InternalStructures Current = new InternalStructures();

        public static void Reset() => Current = new InternalStructures();

        private InternalStructures() { }

        public Dictionary<Call, CallInfo> CallsInformation { get; } = new Dictionary<Call, CallInfo>();

        HashSet<Call> PendingCalls { get; } = new HashSet<Call>();

        public void AddCall(Call call)
        {
            if ((!CallsInformation.ContainsKey(call)) || !CallsInformation[call].Analyzed)
                PendingCalls.Add(call);
        }

        public Call GetNextCall()
        {
            if (!HasNext())
                throw new Exception("Bad operation");

            var next = PendingCalls.First();
            PendingCalls.Remove(next);
            return next;
        }

        public bool HasNext()
        {
            return PendingCalls.Count > 0;
        }
    }
}
