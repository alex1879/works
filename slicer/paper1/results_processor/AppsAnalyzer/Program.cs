﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace AppsAnalyzer
{
    class Program
    {
        public static Solution UserSolution;
        public static Dictionary<string, CSharpCompilation> Compilations = new Dictionary<string, CSharpCompilation>();
        static IDictionary<Call, IMethodSymbol> AssociatedSymbol = new Dictionary<Call, IMethodSymbol>();

        static void Main(string[] args)
        {
            //PickTests();
            FillCallGraph();

            // ACORDARSE DE: WarnAsErrorDoesNotEmit_SpecificDiagnosticOption --> 5277, 5279, 5280, 5283
        }

        static void PickTests()
        {
            var SolutionPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\Compilers.sln";
            var ResultPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";

            var TestsClasses = new string[] { "StatementParsingTests", "TypeTests", "BindingTests", "FlowTests", "CompilationEmitTests", "PDBTests" };

            // Load the solution
            if (MSBuildLocator.CanRegister)
                MSBuildLocator.RegisterDefaults();
            var msBuildWorkspace = MSBuildWorkspace.Create();
            UserSolution = msBuildWorkspace.OpenSolutionAsync(SolutionPath).Result;

            // Compile the projects
            foreach (var project in UserSolution.Projects.Where(x => InstrumentProject(x)))
                Compilations[project.Name.Replace("(netcoreapp3.1)", "")] = (CSharpCompilation)project.GetCompilationAsync().Result;


            var result = new Dictionary<string, ISet<string>>();
            foreach (var testClass in TestsClasses)
            {
                var selectedMethods = new HashSet<string>();
                var symbol = GetSymbolByClass(testClass);
                var tests = symbol.GetMembers().Where(x => x.Kind == SymbolKind.Method).ToList();
                var rnd = new Random();
                for (var i = 0; i < 30; i++)
                {
                    var next = "";
                    do
                    {
                        var n = rnd.Next(1, tests.Count);
                        next = tests[n].Name;
                    } while (selectedMethods.Contains(next));
                    selectedMethods.Add(next);
                }
                result.Add(testClass, selectedMethods);
            }

            // Save results
            var sb = new StringBuilder();
            foreach (var kv in result)
                sb.AppendLine($"{kv.Key};" + string.Join(",", kv.Value));
            File.WriteAllText(ResultPath, sb.ToString());
        }

        static void FillCallGraph()
        {
            var Tests = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";
            var SolutionPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\Compilers.sln";
            
            // Load the solution
            if (MSBuildLocator.CanRegister)
                MSBuildLocator.RegisterDefaults();
            var msBuildWorkspace = MSBuildWorkspace.Create();
            UserSolution = msBuildWorkspace.OpenSolutionAsync(SolutionPath).Result;

            // Compile the projects
            foreach (var project in UserSolution.Projects.Where(x => InstrumentProject(x)))
                Compilations[project.Name.Replace("(netcoreapp3.1)", "")] = (CSharpCompilation)project.GetCompilationAsync().Result;

            var lines = File.ReadAllLines(Tests).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach(var line in lines)
            {
                var className = line.Split(';')[0];
                var methods = line.Split(';')[1].Split(',').ToList();
                foreach(var method in methods)
                {
                    var folder = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis2\{0}", className);
                    System.IO.Directory.CreateDirectory(folder);

                    var CallGraphResult = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis2\{0}\RoslynAnalysisResults_{1}.txt", 
                        className, method);
                    var AssertsResult = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis2\{0}\AssertsResults_{1}.txt", 
                        className, method);
                    FillCallGraphFile(CallGraphResult, AssertsResult, className, method);
                }
            }
        }

        static void FillCallGraphFile(string CallGraphResultPath, string AssertsResultPath, string InitialClassName, string InitialMethodName)
        {
            InternalStructures.Reset();

            var InitCall = new Call(InitialClassName, InitialMethodName);
            var SymbolInfo = GetSymbol(InitCall);
            var AssertLines = GetAssertLines(SymbolInfo);
            PrintAssertLines(AssertLines, AssertsResultPath);

            // Look for dependencies
            InternalStructures.Current.AddCall(InitCall);
            while (InternalStructures.Current.HasNext())
            {
                var next = InternalStructures.Current.GetNextCall();
                var callInfo = GetCallInfo(next);
                callInfo.Analyzed = true;
                callInfo.OtherCalls = GetCalls(next);
                foreach (var call in callInfo.OtherCalls.Where(x => GetCallInfo(x) != null))
                    InternalStructures.Current.AddCall(call);
            }

            PrintResults(CallGraphResultPath);
        }

        static HashSet<int> GetAssertLines(IMethodSymbol symbol)
        {
            var firstReference = symbol.DeclaringSyntaxReferences.FirstOrDefault();

            if (firstReference != null)
            { 
                var expressions = firstReference.GetSyntax().DescendantNodes()
                    .Where(x => x.Kind() == SyntaxKind.InvocationExpression && x.ToString().Contains("Assert.")).ToList();
                var set = new HashSet<int>(expressions.SelectMany(x => 
                            CheckLine(x.GetLocation().SourceTree.FilePath, x.GetLocation().GetLineSpan().StartLinePosition.Line)
                        ));
                return set;
            }
            return new HashSet<int>();
        }

        static HashSet<int> CheckLine(string filePath, int line)
        {
            var returnSet = new HashSet<int>();
            var lines = File.ReadAllLines(filePath);
            if (lines[line - 1].Contains("Assert."))
                returnSet.Add(line);
            if (lines[line].Contains("Assert."))
                returnSet.Add(line + 1);
            if (lines[line - 2].Contains("Assert."))
                returnSet.Add(line - 1);

            // To 6...
            if (lines[line + 1].Contains("Assert."))
                returnSet.Add(line + 2);
            if (lines[line + 2].Contains("Assert."))
                returnSet.Add(line + 3);
            if (lines[line + 3].Contains("Assert."))
                returnSet.Add(line + 4);
            if (lines[line + 4].Contains("Assert."))
                returnSet.Add(line + 5);
            if (lines[line + 5].Contains("Assert."))
                returnSet.Add(line + 6);

            return returnSet;
        }

        public static bool InstrumentProject(Project project)
        {
            return InstrumentProject(project.Name, true);
        }

        static bool InstrumentProject(string projectName, bool checkfornetcore = false)
        {
            if ((!projectName.Contains("netcoreapp3.1")) && checkfornetcore)
                return false;

            if (projectName.Contains("VisualBasic"))
                return false;

            if (projectName.Contains("Test"))
                return true;

            if (projectName.Replace("(netcoreapp3.1)", "").Trim() == "Microsoft.CodeAnalysis.CSharp")
                return true;

            return false;
        }

        static INamedTypeSymbol GetSymbolByClass(string className)
        {
            var foundSymbol = false;
            foreach (var project in UserSolution.Projects.Where(x => x.Name.Contains("Test") && InstrumentProject(x)))
            {
                var results = Microsoft.CodeAnalysis.FindSymbols.SymbolFinder.FindDeclarationsAsync
                    (project, className, true).Result;

                if (results.Count() > 0)
                    return results.OfType<INamedTypeSymbol>().FirstOrDefault();
            }
            return null;
        }

        static CallInfo GetCallInfo(Call call)
        {
            if (InternalStructures.Current.CallsInformation.ContainsKey(call))
                return InternalStructures.Current.CallsInformation[call];

            var symbol = GetSymbol(call);
            CallInfo callInfo = null;
            if (symbol != null)
            {
                callInfo = new CallInfo(call, symbol);
            }
            InternalStructures.Current.CallsInformation.Add(call, callInfo);
            return callInfo;
        }

        static CallInfo SetCallInfo(Call call, ISymbol symbol)
        {
            if (symbol is IMethodSymbol && InstrumentProject(GetProjectNameBySymbol((IMethodSymbol)symbol)))
            {
                if (InternalStructures.Current.CallsInformation.ContainsKey(call) && InternalStructures.Current.CallsInformation[call] != null)
                    return InternalStructures.Current.CallsInformation[call];

                InternalStructures.Current.CallsInformation[call] = new CallInfo(call, (IMethodSymbol)symbol);
                return InternalStructures.Current.CallsInformation[call];
            }
            return null;
        }

        public static IMethodSymbol GetSymbol(Call call)
        {
            if (AssociatedSymbol.ContainsKey(call))
                return AssociatedSymbol[call];

            var foundSymbol = false;
            IMethodSymbol methodSymbol = null;
            foreach (var project in UserSolution.Projects.Where(x => InstrumentProject(x)))
            {
                var results = Microsoft.CodeAnalysis.FindSymbols.SymbolFinder.FindDeclarationsAsync
                    (project, call.ClassName, true).Result; //  + "." + call.MethodName

                if (results.Count() > 0)
                {
                    foreach(var result in results.Where(x => x is INamedTypeSymbol))
                    {
                        var members = ((INamedTypeSymbol)(results.Single())).GetMembers(call.MethodName);
                        if (members.Count() > 0)
                        {
                            methodSymbol = (IMethodSymbol)members.First();
                            foundSymbol = true;
                        }
                    }
                    if (foundSymbol)
                        break;
                }
            }

            AssociatedSymbol[call] = methodSymbol;
            return AssociatedSymbol[call];
        }

        static HashSet<Call> GetCalls(Call call)
        {
            var returnSet = new HashSet<Call>();
            var callInfo = GetCallInfo(call);
            if (callInfo != null)
            {
                var project = GetProjectNameBySymbol(callInfo.MethodSymbol);
                foreach(var location in callInfo.MethodSymbol.Locations)
                { 
                    var ast = location.SourceTree;
                    var span = Microsoft.CodeAnalysis.Text.TextSpan.FromBounds(location.SourceSpan.Start, location.SourceSpan.End);
                    var syntaxNode = (CSharpSyntaxNode)ast.GetCompilationUnitRoot().FindNode(span);
                    if (syntaxNode is MethodDeclarationSyntax)
                    {
                        foreach (var descendantNode in syntaxNode.DescendantNodes().Where(x => x is InvocationExpressionSyntax))
                        {
                            var symbol = Compilations[project].GetSemanticModel(descendantNode.SyntaxTree).GetSymbolInfo((CSharpSyntaxNode)descendantNode);
                            if (symbol.Symbol != null && symbol.Symbol is IMethodSymbol)
                            {
                                var nextCall = new Call(((IMethodSymbol)symbol.Symbol).ContainingType.Name, ((IMethodSymbol)symbol.Symbol).Name);
                                var symbolInfo = SetCallInfo(nextCall, (IMethodSymbol)symbol.Symbol);
                                if (symbolInfo != null)
                                {
                                    returnSet.Add(nextCall);
                                }
                            }
                        }
                    }
                }
            }
            return returnSet;
        }

        static void PrintResults(string path)
        {
            var sb = new StringBuilder();
            foreach (var call in InternalStructures.Current.CallsInformation.Where(x => x.Value != null))
            {
                var line = $"{GetProjectNameBySymbol(call.Value.MethodSymbol)};{GetFileNameBySymbol(call.Value.MethodSymbol)};{call.Key.ClassName};{call.Key.MethodName}";
                sb.AppendLine(line);
            }
            File.WriteAllText(path, sb.ToString());
        }

        static void PrintAssertLines(HashSet<int> lines, string assertResultPath)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Join(";", lines.Select(x => x.ToString())));
            File.WriteAllText(assertResultPath, sb.ToString());
        }

        static string GetProjectNameBySymbol(IMethodSymbol symbol)
        {
            return symbol.ContainingModule.ToString().Replace(".dll", "");
        }

        public static string GetFileNameBySymbol(IMethodSymbol symbol)
        {
            return symbol.Locations.First().SourceTree.FilePath;
        }
    }
}
