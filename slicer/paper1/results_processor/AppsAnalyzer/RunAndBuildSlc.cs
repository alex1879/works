﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Diagnostics;
using System.Xml.Serialization;

namespace AppsAnalyzer
{
    class RunAndBuildSlc
    {
        static void Main()
        {
            var Tests = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\SelectedMethods.txt";
            var SolutionPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\src\Compilers.sln";
            var TraceMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\traces\aut";
            var ConfigMainFolder = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\aut";
            var OtherFileLines = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\generalLineNumbers.txt";
            var RoslynInstrumentedSrc = @"C:\Users\alexd\Desktop\Slicer\Roslyn\instrumented\src";
            var TempPath = @"C:\temp";
            var OriginalSlcFile = @"C:\Users\alexd\Desktop\Slicer\Roslyn\config\Roslyn_Slice.slc";
            var ResultsPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\results\aut";
            var MainResultPath = @"C:\Users\alexd\Desktop\Slicer\Roslyn\others\summaryMainProcess.txt";

            var generalLines = LoadGeneralLines(OtherFileLines);
            var mainSb = new StringBuilder();

            var projDict = new Dictionary<string, string>();
            projDict["StatementParsingTests"] = "Microsoft.CodeAnalysis.CSharp.Syntax.UnitTests.csproj";
            projDict["TypeTests"] = "Microsoft.CodeAnalysis.CSharp.Symbol.UnitTests.csproj";
            projDict["BindingTests"] = "Microsoft.CodeAnalysis.CSharp.Semantic.UnitTests.csproj";
            projDict["FlowTests"] = "Microsoft.CodeAnalysis.CSharp.Semantic.UnitTests.csproj";
            projDict["CompilationEmitTests"] = "Microsoft.CodeAnalysis.CSharp.Emit.UnitTests.csproj";
            projDict["PDBTests"] = "Microsoft.CodeAnalysis.CSharp.Emit.UnitTests.csproj";

            // Load the solution
            if (MSBuildLocator.CanRegister)
                MSBuildLocator.RegisterDefaults();
            var msBuildWorkspace = MSBuildWorkspace.Create();
            Program.UserSolution = msBuildWorkspace.OpenSolutionAsync(SolutionPath).Result;

            // Compile the projects
            //foreach (var project in Program.UserSolution.Projects.Where(x => Program.InstrumentProject(x)))
            //    Program.Compilations[project.Name.Replace("(netcoreapp3.1)", "")] = (CSharpCompilation)project.GetCompilationAsync().Result;

            // Empty temp folder
            foreach (var file in Directory.GetFiles(TempPath))
                File.Delete(file);

            var classesToAnalyze = new string[] { /*"StatementParsingTests", "BindingTests",*/ /*"TypeTests", "FlowTests",*/ "CompilationEmitTests", /*"PDBTests"*/ };

            var lines = File.ReadAllLines(Tests).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
            foreach (var line in lines)
            {
                var className = line.Split(';')[0];
                if (!classesToAnalyze.Contains(className))
                    continue;

                var methods = line.Split(';')[1].Split(',').ToList();
                foreach (var method in methods)
                {
                    var ok = new string[] {
                        //"ErrorTypeSymbolWithArity",
                        //"ExplicitlyImplementGenericInterface",
                        //"ImplementNestedInterface_02",
                        //"ImportedVersusSource",
                        //"InaccessibleTypesSkipped",
                        //"IsExplicitDefinitionOfNoPiaLocalType_01",
                        //"IsExplicitDefinitionOfNoPiaLocalType_02",
                        //"IsExplicitDefinitionOfNoPiaLocalType_08",
                        //"IsExplicitDefinitionOfNoPiaLocalType_09",
                        //"IsExplicitDefinitionOfNoPiaLocalType_15",
                        //"IsExplicitDefinitionOfNoPiaLocalType_17",

                        // Binding
                        //"UnimplementedInterfaceSquiggleLocation13"
                        
                        // Flowtest
                        //"General",
                        //"ThrowStatement",
                        //"IrrefutablePattern_1",
                        //"UseDef_CondAccess",
                        //"UseDef_ExceptionFilters1",
                        //"UseDef_ExceptionFilters3",
                        //"UseDef_ExceptionFilters6",
                        //"ForEachStatement", //(SE TILDA 357K)
                        //"WhidbeyBug479106" // OK

                        // CompilationEmit
                        "RefAssemblyClient_EmitAllTypes"
                        //"EmitLambdaInConstructorBody",
                        //"EmitLambdaInConstructorInitializerAndBody",
                        //"EmitNestedLambdaWithAddPlusOperator",
                        //"PlatformMismatch_05",
                        //"RefAssemblyClient_EmitVariance_InSuccess",
                        //"RefAssembly_VerifyTypesAndMembersOnStruct",
                        //"RefAssembly_CryptoHashFailedIsOnlyReportedOnce",
                        //"RefAssembly_ReferenceAssemblyAttributeAlsoInSource",
                        //"RefAssembly_StrongNameProvider_Arm64"
                    };
                    if (!ok.Any(x => method.Contains(x)))
                        continue;

                    var folder = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis\{0}", className);
                    var CallGraphResult = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis\{0}\RoslynAnalysisResults_{1}.txt",
                        className, method);
                    var AssertsResult = string.Format(@"C:\Users\alexd\Desktop\Slicer\Roslyn\others\analysis\{0}\AssertsResults_{1}.txt",
                        className, method);

                    Console.WriteLine($"Executing class: {className} - method: {method}");

                    var currentTraceFolder = TraceMainFolder + string.Format(@"\{0}\{1}", className, method);
                    var currentConfigFolder = ConfigMainFolder + string.Format(@"\{0}", className, method);
                    var resPath = Path.Combine(TempPath, string.Format("{0}.{1}.res", className, method));
                    System.IO.Directory.CreateDirectory(currentTraceFolder);
                    System.IO.Directory.CreateDirectory(currentConfigFolder);

                    var slcFile = Path.Combine(currentConfigFolder, method + ".slc");

                    var specificLinesTxt = File.ReadAllLines(AssertsResult).First();
                    var specificLines = specificLinesTxt.Split(";").Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => int.Parse(x.Trim())).ToList();

                    var projectFile = System.IO.Directory.GetFiles(RoslynInstrumentedSrc, "*", SearchOption.AllDirectories)
                        .Single(x => x.Contains(projDict[className]) && x.EndsWith(".csproj"));

                    InternalStructures.Reset();
                    var InitCall = new Call(className, method);
                    var SymbolInfo = Program.GetSymbol(InitCall);
                    var fileNameOfSymbol = Program.GetFileNameBySymbol(SymbolInfo);
                    var namespaceName = SymbolInfo.ContainingNamespace;

                    // Execute test
                    //projectFile = projectFile.Replace(@"C:\Users\alexd\Desktop\Slicer\Roslyn\instrumented\src", "");

                    var command = string.Format("test \"{0}\" --framework net5.0 --filter \"FullyQualifiedName={1}.{2}.{3}\"", projectFile, namespaceName, className, method);
                    var result = RunTest(command, resPath, 600000);

                    if (!result.Timeout)
                    {
                        var allLinesRes = File.ReadAllLines(resPath).ToList();
                        if (allLinesRes.Any(x => x.Contains("Failed:     1")))
                            mainSb.AppendLine(string.Format("Error running test {0}:{1}", className, method));

                        // Move file
                        var filesInFolder = System.IO.Directory.GetFiles(TempPath);
                        var filesToMove = filesInFolder.OrderBy(x => new FileInfo(x).LastWriteTimeUtc).ToList();
                        var filesInfo = filesInFolder.Select(x => new FileInfo(x)).ToList();
                        var maxCantCandidates = (filesInfo.Count - 1) / 2;
                        // For timing: .Where(x => DateTime.Now.Subtract(File.GetCreationTime(x)).TotalMinutes < 4)

                        if (filesToMove.Count == 0)
                        {
                            mainSb.AppendLine(string.Format("Error to get files {0}:{1}", className, method));
                            continue;
                        }
                        else
                        {
                            // Delete previous traces
                            foreach (var file in Directory.GetFiles(currentTraceFolder))
                                File.Delete(file);

                            #region Previous algorithm
                            //var fileWithMaxSize = filesToMove.First();
                            //var length = new FileInfo(fileWithMaxSize).Length;
                            //foreach (var fileToMove in filesToMove)
                            //{
                            //    if (!fileToMove.Contains(className) &&
                            //        (new FileInfo(fileToMove).Length > length ||
                            //        fileWithMaxSize.Contains(className)))
                            //    { 
                            //        fileWithMaxSize = fileToMove;
                            //        length = new FileInfo(fileToMove).Length;
                            //    }
                            //    System.IO.File.Move(fileToMove, fileToMove.Replace(TempPath, currentTraceFolder));
                            //}
                            //var finalTracePath = fileWithMaxSize.Replace(TempPath, currentTraceFolder);

                            //File.WriteAllText(finalTracePath, File.ReadAllText(finalTracePath).Replace("#", ""));
                            #endregion

                            var desiredStartWith = 25046;
                            var desiredStartWith2 = 25014;

                            var candidates = new List<string>();
                            //var counter = 0;
                            foreach (var fileToMove in filesToMove)
                            {
                                var newTraceFilePath = fileToMove.Replace(TempPath, currentTraceFolder);
                                System.IO.File.Move(fileToMove, newTraceFilePath);

                                if (StartWith(newTraceFilePath) == desiredStartWith /*&& counter < maxCantCandidates*/)
                                    candidates.Add(newTraceFilePath);

                                //counter++;
                            }

                            if (candidates.Count == 0)
                                candidates.AddRange(Directory.GetFiles(currentTraceFolder));
                                //foreach (var newTraceFilePath in Directory.GetFiles(currentTraceFolder))
                                //{
                                //    if (StartWith(newTraceFilePath) == desiredStartWith2)
                                //        candidates.Add(newTraceFilePath);
                                //}

                            if (candidates.Count == 0)
                                ;

                            var finalTracePath =
                                candidates.Select(x => new { file = x, length = new FileInfo(x).Length, date = new FileInfo(x).LastWriteTime })
                                //.OrderByDescending(y => y.date).First().file;
                                .OrderByDescending(y => y.length).First().file;
                            File.WriteAllText(finalTracePath, File.ReadAllText(finalTracePath).Replace("#", ""));

                            // Build slc
                            var stream = System.IO.File.OpenRead(OriginalSlcFile.Trim());
                            var serializer = new XmlSerializer(typeof(UserConfiguration));
                            var userConfiguration = (UserConfiguration)serializer.Deserialize(stream);

                            var currentResultPath = ResultsPath + string.Format(@"\{0}\{1}", className, method);
                            var resultName = string.Format(@"{0}_{1}", className, method);
                            userConfiguration.results.name = resultName;
                            userConfiguration.results.outputFolder = currentResultPath;
                            userConfiguration.criteria.fileTraceInputPath = finalTracePath;
                            userConfiguration.criteria.lines = (GetFileLine(generalLines, fileNameOfSymbol, specificLines)).Select(x => new UserConfiguration.Criteria.FileLine() { file = x.Item1, line = x.Item2 }).ToArray();

                            // Save to file
                            var streamToSave = System.IO.File.OpenWrite(slcFile);
                            serializer.Serialize(streamToSave, userConfiguration);

                            mainSb.AppendLine(string.Format("OK {0}:{1}", className, method));
                        }
                    }
                    else
                        mainSb.AppendLine(string.Format("Error to run {0}:{1}", className, method));
                }
            }
            File.AppendAllText(MainResultPath, mainSb.ToString());
        }

        public static IEnumerable<Tuple<string, int>> GetFileLine(Dictionary<string, ISet<int>> generalLines, string fileName, List<int> lines)
        {
            foreach (var kv in generalLines)
                foreach (var l in kv.Value)
                    yield return new Tuple<string, int>(kv.Key, l);

            foreach(var l in lines)
                yield return new Tuple<string, int>(fileName, l);
        }

        public static Dictionary<string, ISet<int>> LoadGeneralLines(string path)
        {
            var allLines = File.ReadAllLines(path).ToList();
            var retDict = new Dictionary<string, ISet<int>>();
            for (var i = 0; i < allLines.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(allLines[i]))
                    break;

                var file = allLines[i + 1].Replace("<file>", "").Replace("</file>", "").Trim();
                var line = int.Parse(allLines[i + 2].Replace("<line>", "").Replace("</line>", "").Trim());

                if (!retDict.ContainsKey(file))
                    retDict[file] = new HashSet<int>();
                retDict[file].Add(line);

                i = i + 3;
            }
            return retDict;
        }

        public static void RunCommand(string command, string output, out bool timeout)
        {
            timeout = false;
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardOutput = true
            };

            Process p = Process.Start(processInfo);
            StringBuilder sb = new StringBuilder();
            while (!p.StandardOutput.EndOfStream)
            {
                sb.AppendLine(p.StandardOutput.ReadLine());
            }
            File.WriteAllText(output, sb.ToString());

            //if (timeoutMilliseconds.HasValue)
            if (!p.WaitForExit(600000))
            {
                p.Kill();
                timeout = true;
            }

            //return (timeout, p.TotalProcessorTime);
        }

        public static (bool Timeout, TimeSpan ElapsedTime) RunTest(string command, string outputPath, int? timeoutMilliseconds = null)
        {
            ProcessStartInfo procStartInfo = new ProcessStartInfo();
            procStartInfo.FileName = "dotnet";
            procStartInfo.Arguments = command;
            procStartInfo.WorkingDirectory = "C:\\Users\\alexd\\Desktop\\Slicer"; //Path.GetDirectoryName(projectFilePath);
            procStartInfo.UseShellExecute = false;
            procStartInfo.CreateNoWindow = false;
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.RedirectStandardError = true;

            if (procStartInfo.EnvironmentVariables.ContainsKey("MSBuildSDKsPath"))
            {
                procStartInfo.EnvironmentVariables.Remove("MSBuildSDKsPath");
                procStartInfo.EnvironmentVariables.Remove("MSBuildExtensionsPath");
                procStartInfo.EnvironmentVariables.Remove("MSBUILD_EXE_PATH");
            }

            StringBuilder sb = new StringBuilder();
            Process pr = new Process();
            pr.StartInfo = procStartInfo;

            pr.OutputDataReceived += (s, ev) =>
            {
                if (string.IsNullOrWhiteSpace(ev.Data)) { return; }
                sb.AppendLine(ev.Data);
            };

            pr.ErrorDataReceived += (s, err) => { };

            pr.EnableRaisingEvents = true;
            pr.Start();
            pr.BeginOutputReadLine();
            pr.BeginErrorReadLine();

            pr.WaitForExit();
            var timeout = false;
            if (timeoutMilliseconds.HasValue)
                if (!pr.WaitForExit(timeoutMilliseconds.Value))
                {
                    pr.Kill();
                    timeout = true;
                }

            File.WriteAllText(outputPath, sb.ToString());

            return (timeout, pr.TotalProcessorTime);
        }

        static int StartWith(string path)
        {
            var valToRet = 0;

            var lines = File.ReadAllLines(path);
            if (lines.Length > 0)
            {
                var firstLine = lines.First().Replace("#", "");
                var firstVal = firstLine.Split(',');
                var firstInt = 0;
                if (int.TryParse(firstVal[0], out firstInt))
                {
                    return firstInt;
                }
            }

            return valToRet;
        }
    }
}
