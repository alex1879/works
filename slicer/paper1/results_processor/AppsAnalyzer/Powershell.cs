﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace AppsAnalyzer
{
    class Powershell
    {
        static void Main()
        {
            RunTest("", "", null);
        }

        public static (bool Timeout, TimeSpan ElapsedTime) RunTest(string command, string outputPath, int? timeoutMilliseconds = null)
        {
            ProcessStartInfo procStartInfo = new ProcessStartInfo();
            procStartInfo.FileName = "C:\\Windows\\system32\\WindowsPowerShell\\v1.0\\powershell.exe";
            //procStartInfo.Arguments = command;
            procStartInfo.WorkingDirectory = "C:\\Users\\alexd\\Desktop\\Slicer\\Powershell\\instrumented"; //Path.GetDirectoryName(projectFilePath);
            procStartInfo.UseShellExecute = false;
            procStartInfo.CreateNoWindow = true;
            procStartInfo.RedirectStandardInput = true;
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.RedirectStandardError = true;
            
            StringBuilder sb = new StringBuilder();
            Process pr = new Process();
            pr.StartInfo = procStartInfo;

            pr.OutputDataReceived += (s, ev) =>
            {
                if (string.IsNullOrWhiteSpace(ev.Data)) { return; }
                sb.AppendLine(ev.Data);
            };

            pr.ErrorDataReceived += (s, err) => { };

            pr.EnableRaisingEvents = true;
            pr.Start();

            StreamWriter sw = pr.StandardInput;
            sw.WriteLine("Import-Module ./build.psm1");
            sw.WriteLine("Start-PSPester -Path test/powershell/Host/ConsoleHost.Tests.ps1");
            sw.WriteLine("exit");
            
            pr.BeginOutputReadLine();
            pr.BeginErrorReadLine();

            pr.WaitForExit();
            var timeout = false;
            if (timeoutMilliseconds.HasValue)
                if (!pr.WaitForExit(timeoutMilliseconds.Value))
                {
                    pr.Kill();
                    timeout = true;
                }

            //File.WriteAllText(outputPath, sb.ToString());
            return (timeout, pr.TotalProcessorTime);
        }
    }
}
